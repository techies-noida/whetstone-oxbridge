'use strict';

var Video = Twilio.Video;
var activeRoom;
var previewTracks;
var identity;
var roomName;
var role;

// Attach the Tracks to the DOM.
function attachTracks(tracks, container) {
  tracks.forEach(function(track) {
    container.appendChild(track.attach());
  });
}

// Attach the Participant's Tracks to the DOM.
function attachParticipantTracks(participant, container) {
  var tracks = Array.from(participant.tracks.values());
  attachTracks(tracks, container);
}

// Detach the Tracks from the DOM.
function detachTracks(tracks) {
  tracks.forEach(function(track) {
    track.detach().forEach(function(detachedElement) {
      detachedElement.remove();
    });
  });
}

// Detach the Participant's Tracks from the DOM.
function detachParticipantTracks(participant) {
  var tracks = Array.from(participant.tracks.values());
  detachTracks(tracks);
}

// When we are about to transition away from this page, disconnect
// from the room, if joined.
window.addEventListener('beforeunload', leaveRoomIfJoined);

// Obtain a token from the server in order to connect to the Room.
roomName = document.getElementById('room-name').value;

//$.getJSON('https://167.99.231.76/create/token', function(data) {
  $.getJSON('https://whetstone-oxbridge.com/create/token/?room_name='+roomName, function(data) {
  identity = data.identity;
  role=data.role;
  document.getElementById('room-controls').style.display = 'block';

  // Bind button to join Room.
  document.getElementById('button-join').onclick = function() {
    if(role == 2){
      var appointment_id = $('#appointment_id').val();
      $.ajax({
        type: "POST",
        url: "https://whetstone-oxbridge.com/Api/check_call_status",
        data: {appointment_id:appointment_id, call_status: 'Started'},
        cache: false,
        success: function(response){
          //alert("response:"+data)
          if(response == 1){
            roomName = document.getElementById('room-name').value;
            if (!roomName) {
              alert('Please enter a room name.');
              return;
            }
            log("Joining room '" + roomName + "'...");
            var connectOptions = {
              name: roomName,
              logLevel: 'debug'
            };
            if (previewTracks) {
              connectOptions.tracks = previewTracks;
            }
            // Join the Room with the token from the server and the
            // LocalParticipant's Tracks.
            Video.connect(data.token, connectOptions).then(roomJoined, function(error) {
              log('Could not connect to Twilio: ' + error.message);
            });
          }else{
            alert("You can't join the call until the applicant starts the call. please try again!");
            return true;
          }  
        }
      });
    }else{
      roomName = document.getElementById('room-name').value;
      if (!roomName) {
        alert('Please enter a room name.');
        return;
      }
      log("Joining room '" + roomName + "'...");
      var connectOptions = {
        name: roomName,
        logLevel: 'debug'
      };
      if (previewTracks) {
        connectOptions.tracks = previewTracks;
      }
      // Join the Room with the token from the server and the
      // LocalParticipant's Tracks.
      Video.connect(data.token, connectOptions).then(roomJoined, function(error) {
        log('Could not connect to Twilio: ' + error.message);
      });
    }
  };

  // Bind button to leave Room.
  document.getElementById('button-leave').onclick = function() {
    log('Leaving room...');
    activeRoom.disconnect();
  };
});

// Successfully connected!
function roomJoined(room) {
  window.room = activeRoom = room;

  log("Joined as '" + identity + "'");
  document.getElementById('button-join').style.display = 'none';
  document.getElementById('button-leave').style.display = 'inline';

  // Attach LocalParticipant's Tracks, if not already attached.
  var previewContainer = document.getElementById('local-media');
  if (!previewContainer.querySelector('video')) {
    attachParticipantTracks(room.localParticipant, previewContainer);
  }

  // Attach the Tracks of the Room's Participants.
  room.participants.forEach(function(participant) {
    log("Already in Room: '" + participant.identity + "'");
    //alert(participant.sid);
    //$("#p_id").val(participant.sid);
    $("#session_time").val("connected");
    var previewContainer = document.getElementById('remote-media');
    attachParticipantTracks(participant, previewContainer);
  });

  // When a Participant joins the Room, log the event.
  room.on('participantConnected', function(participant) {
  	 //alert(participant.sid);
  	//$("#p_id").val(participant.sid);
  	$("#session_time").val("connected");
    log("Joining: '" + participant.identity + "'");
  });

  // When a Participant adds a Track, attach it to the DOM.
  room.on('trackAdded', function(track, participant) {
    log(participant.identity + " added track: " + track.kind);
    //alert("participant sid:"+participant.sid +" participant identity"+participant.identity);
    if(participant.identity.includes('student')){
       $("#p_id").val(participant.sid);
    }
    /*if(role==2){
      alert("participant sid:"+participant.sid +" participant identity"+participant.identity);
      $("#p_id").val(participant.sid);
    }*/
    
    $("#session_time").val("connected");
    var previewContainer = document.getElementById('remote-media');
    attachTracks([track], previewContainer);
  });

  // When a Participant removes a Track, detach it from the DOM.
  room.on('trackRemoved', function(track, participant) {
    log(participant.identity + " removed track: " + track.kind);
    detachTracks([track]);
  });

  // When a Participant leaves the Room, detach its Tracks.
  room.on('participantDisconnected', function(participant) {
    log("Participant '" + participant.identity + "' left the room");
    detachParticipantTracks(participant);
  });

  // Once the LocalParticipant leaves the room, detach the Tracks
  // of all Participants, including that of the LocalParticipant.
  room.on('disconnected', function() {
    log('Left');
    if (previewTracks) {
      previewTracks.forEach(function(track) {
        track.stop();
      });
      previewTracks = null;
    }
    detachParticipantTracks(room.localParticipant);
    room.participants.forEach(detachParticipantTracks);
    activeRoom = null;
    document.getElementById('button-join').style.display = 'inline';
    document.getElementById('button-leave').style.display = 'none';
       //compose recorded video
    setTimeout(function(){ 
      var room_sid = room.sid;
      var interviewer_id = $('#interviewer_id').val();
       var appointment_id = $('#appointment_id').val();
       var p_id = $("#p_id").val();
     // alert(appointment_id);
      //interviewer_id = document.getElementById('interviewer_id').value;
      if(room_sid == '' || room_sid == undefined){
        alert('room sid is not found');
        return false;
      }
      $.ajax({
        type: "POST",
        url: "https://whetstone-oxbridge.com/TwilioController/compose_video",
        data: {room_sid:room_sid,interviewer_id:interviewer_id,appointment_id:appointment_id,p_id:p_id},
        cache: false,
        success: function(data){
          //$('#video_div').html(data);
          //alert(data);
          if(data == 1)
          {
           // alert("Video is composed Successfully");
          }  
        }
      });
    }, 2000);
    //end composed video


  });
}

// Preview LocalParticipant's Tracks.
/*document.getElementById('button-preview').onclick = function() {
  var localTracksPromise = previewTracks
    ? Promise.resolve(previewTracks)
    : Video.createLocalTracks();

  localTracksPromise.then(function(tracks) {
    window.previewTracks = previewTracks = tracks;
    var previewContainer = document.getElementById('local-media');
    if (!previewContainer.querySelector('video')) {
      attachTracks(tracks, previewContainer);
    }
  }, function(error) {
    console.error('Unable to access local media', error);
    log('Unable to access Camera and Microphone');
  });
};*/

// Activity log.
function log(message) {
  var logDiv = document.getElementById('log');
  logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
  logDiv.scrollTop = logDiv.scrollHeight;
}

// Leave Room.
function leaveRoomIfJoined() {
  if (activeRoom) {
    activeRoom.disconnect();
  }
}
