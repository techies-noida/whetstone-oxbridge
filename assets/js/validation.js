
/* ******************************************  Send request as Tutor Join  ***************************************************/
$.validator.addMethod(
  "regexschool",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
"School name should be character"
);
$.validator.addMethod(
  "regexuniversity",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
"University name should be character"
);
$.validator.addMethod(
  "regexcollege",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
"College name should be character"
);
$.validator.addMethod(
  "regexcourse",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
"Course name should be character"
);
$.validator.addMethod(
  "regexstudentname",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
"Name should be character"
);

$.validator.addMethod(
  "regexalpspecial",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
'Only alphabet and special character (,/" &.) are allowed'
);
$.validator.addMethod(
  "regexsetpass",
  function(value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
},
'Password should be one captial letter , one symbol or special character(#@%$), or  one number '
);

$('#request_join_tutor').validate({
    rules:{
        name:{
            required:true,
            minlength:3, 
            maxlength:25, 
            regexstudentname: "^[A-Za-z ]+$", 
        },
        email_address:{
            required:true,
            maxlength:80,

        },
        university:{
            required:true,

        },
        college:{
            required:true,

        },
        mobile_number:{
            required:true,
            digits: true,
            minlength: 10,
            maxlength:15,  
        },
        subject:{
            required:true,
            minlength:4,
            maxlength:70,
            //regexcourse: "^[A-Za-z. ]+$", 


        },
        year_of_study:{
            required:true,

        },
        working_as:{
            required:true,
            minlength:10, 
                // maxlength:70,
                //regexalpspecial: '^[A-Za-z,/"&. ]+$',

            },

            wish_to:{
                required:true,
                minlength:10, 
               // maxlength:500,
               //regexalpspecial: '^[A-Za-z,/"&. ]+$',

           },
           extra_curricular:{
            required:true,
            minlength:10, 
                // maxlength:70,
                //regexalpspecial: '^[A-Za-z,/"&. ]+$',
                

            },
            img: {
               required:true,
           },
           terms_condition:{
            required:true,

        },
    },
    messages:{
        name:{
            required:"  Please enter your name ",
            minlength: "Name must contain at least 3 character",
            maxlength: "Name should not contain more than 25 character ",     
        },
        email_address:{
            required:"Please enter email address ", 
            maxlength: "Email address should not contain more than 80 character",  
        },
        university:{
            required:"  Please select university name ", 
        },
        college:{
            required:"  Please select college name ",    
        },
        mobile_number:{
            required:"  Please enter mobile number",
            minlength: "Please enter 10 digits of mobile number",
            maxlength: "Please enter 15 digits of mobile number",     
        },
        subject:{
            required:"  Please enter course name ", 
            minlength: "Course name contain at least 4 character",
            maxlength: "Course name should not contain more than 70 character ",    
        },
        year_of_study:{
            required:"  Please select year of study ", 
        },
        working_as:{
            required:"  Please enter academic interests ", 
            minlength: "Academic interests contain at least 10 character",
                // maxlength: "Academic interests should not contain more than 70 character ",   
            },
            wish_to:{
                required:"  Please enter wish to told ", 
                minlength: "Wish to told should contain at least 10 character",
                // maxlength: "Wish to should not contain more than 500 character ", 

            },
            extra_curricular:{
                required:"  Please enter extra curricular ", 
                minlength: "Extra curricular contain at least 10 character",
                // maxlength: "Extra curricular should not contain more than 70 character ", 

            },
            img: {
                required:"Please select profile image", 
            },
            terms_condition:{
                required:"Check policy",   
            },

        },
        

    });

$('#check_request').validate({
    rules:{
        request_id:{
            required:true,
            digits: true,
            minlength: 12,
            maxlength:12,  
        },

    },
    messages:{
        request_id:{
            required:"  Please enter request Id ",
            minlength: "Please enter 12 digits of request Id",
            maxlength: "Please enter 12 digits of request Id",     
        },

    },


});

$('#create_tutor_account').validate({
    rules:{
        first_name:{
            required:true,
            minlength: 3,
            maxlength: 25, 
            regexstudentname: "^[A-Za-z ]+$", 
        },
        last_name:{

            minlength: 3,
            maxlength:60,  
        },
        email_address:{
            required:true,
            email:true,
        },
        passwords:{
            required:true,
            minlength: 6,
            maxlength: 15,
            regexsetpass: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#@%$]).{6,}$",
        },
        confirm_passwords:{
            required:true,
            equalTo: "#passwords",
        },
        mobile_number:{
            required:true,
            digits: true,
            minlength: 10,
            maxlength:15,  
        },

        phone_number:{
            required:true,
            digits: true,
            minlength: 11,
            maxlength: 25,  
        },
        university:{
            required:true,
            minlength:4, 
            maxlength:35,
            //regexschool: "^[A-Za-z ]+$",
            //regexschool: "^[a-zA-Z0-9!@#$&()-`.+,/\"]*$",

        },
        desire_university:{
            required:true,
            minlength:4, 
            maxlength:35,
            //regexuniversity: "^[A-Za-z ]+$",

        },
        desire_college:{
            required:true,
            minlength:4, 
            maxlength:35,
            //regexcollege: "^[A-Za-z ]+$",
            //regexcollege: "^[a-zA-Z0-9!@#$&()-`.+,/\"]*$",

        },
        desire_course:{
            required:true,
            minlength:4, 
            //regexcourse: "^[A-Za-z. ]+$",

        },
        dob:{
            required:true,

        },
        gender:{
            required:true,

        },
        country:{
            required:true,

        },
        college:{
            required:true,

        },
        subject:{
            required:true,
            minlength:4, 
            maxlength:70,
            //regexcourse: "^[A-Za-z. ]+$",

        },
        study_year:{
            required:true,
            minlength:3, 
            maxlength:60,

        },
        interview_exprience:{
            required:true,

        },
        acadmic_intrests:{
            required:true,
            minlength:10, 
            //regexalpspecial: '^[A-Za-z,/"&. ]+$',

        },
        wish_to:{
         required:true,
         minlength:10, 
         //regexalpspecial: '^[A-Za-z,/"&. ]+$',

     },
     about_us:{
        required:true,
        minlength:10, 
        maxlength:600,

    },
    extra_curicular:{
        required:true,
        minlength:10, 
        //regexalpspecial: '^[A-Za-z,/"&. ]+$',

    },
    terms:{
        required:true,
    },

},
messages:{
    first_name:{
        required:"  Please enter name ",
        minlength: "Name at least 3 character",
        maxlength: "Name should not more than 25 character ",    
    },
    last_name:{

        minlength: "Please enter 3 digits of request Id",
        maxlength: "Please enter 12 digits of request Id",     
    },
    email_address:{
        required:"  Please enter email_address ",
        email:"Please enter valid email id ",
    },
    passwords:{
        required: "Please enter password",
        minlength: "Please eneter minimum 6 character",
        maxlength:"Password should not more than 15 character",


    },
    confirm_passwords:{
        required: "Please enter confirm password",
        equalTo:"Confirm password not match ",
    },
    phone_number:{
        required:"  Please enter phone number",
        minlength: "Please enter 11 digits of phone number",
        maxlength: "Phone number should not more than 25 digits",     
    },

    mobile_number:{
        required:"  Please enter mobile number",
        minlength: "Please enter 10 digits of mobile number",
        maxlength: "Please enter 25 digits of mobile number",     
    },
    dob:{
        required:"  Please enter valid Date of birth", 
    },
    gender:{
        required:"  Please select Gender", 
    },
    country:{
        required:"  Please select country", 
    },
    university:{
        required:"  Please enter school name ",  
        minlength: "Scool name at least 4 character",
        maxlength: "School name should not more than 35 character ",  
    },
    desire_university:{
        required:"  Please enter University name ",  
        minlength: "University name at least 4 character",
        maxlength: "University name should not more than 35 character ",  
    },
    desire_college:{
        required:"  Please enter college name ",  
        minlength: "College name at least 4 character",
        maxlength: "College name should not more than 35 character ",  
    },
    desire_course:{
        required:"  Please enter course name ",  
        minlength: "Course name at least 4 character",
        maxlength: "Course name should not more than 70 character ",  
    },
    college:{
        required:"  Please enter college name ",    
    },
    subject:{
        required:"  Please enter course name ",  
        minlength: "Course name at least 4 character",
        maxlength: "Course name should not more than 70 character ",   
    },

    study_year:{
        required:"  Please enter course name ", 
        minlength: "Course name at least 3 character",
        maxlength: "Course  name should not more than 60 character ",    
    },

    interview_exprience:{
        required:"  Please enter interview experience",    
    },

    acadmic_intrests:{
        required:"  Please enter acadmic_interests ", 
        minlength: "Acadmic-interests at least 10 character",  
    },

    wish_to:{
        required:"  Please enter about Oxbridge interview ", 
        minlength: "Please enter at least 10 character", 
    },
    about_us:{
        required:"  Please enter about you ", 
        minlength: "Please enter at least 10 character",
        maxlength: "About should not more than 500 character ",    
    },

    extra_curicular:{
        required:"  Please enter extra-curicular ", 
        minlength: "Extra-curicular at least 10 character",  
    },
    terms:{
        required: "Accept terms",

    },

},


});


$('#change_password').validate({
    rules:{

        old_password:{
            required:true,
            minlength: 6,
            maxlength: 15,
        },
        new_password:{
            required:true,
            minlength: 6,
            maxlength: 15,
            regexsetpass: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$",

        },
        confirm_passwords:{
            required:true,
            equalTo: "#new_password",
        },
        email_address:{
            required:"  Please enter email_address ",
            email:"Please enter valid email id ",
        },

    },
    messages:{

        old_password:{
            required: "Please enter password",
            minlength: "Please eneter minimum 6 character",
            maxlength:"Password should not more than 15 character",


        },
        new_password:{
            required: "Please enter password",
            minlength: "Please eneter minimum 6 character",
            maxlength:"Password should not more than 15 character",



        },
        confirm_passwords:{
            required: "Please re-enter new password",
            equalTo:"Password does not match ",
        },



    },


});

//   $('#interviewer_contact_form').validate({
//         rules:{

//             university_name:{
//                 required:true,
//                  minlength: 4,
//                 maxlength: 25,
//             },
//             college_name:{
//                 required:true,
//                 minlength: 4,
//                 maxlength: 25,

//             },
//             cource_name:{
//                 required:true,
//                 minlength: 4,
//                 maxlength: 25,
//             },


//         },
//         messages:{

//             university_name:{
//                 required: "Please enter university name",
//                 minlength: "Please enter minimum 4 character",
//                 maxlength:"University name should not more than 25 character",


//             },
//             college_name:{
//                 required: "Please enter College name",
//                 minlength: "Please enter minimum 4 character",
//                 maxlength:"College name should not more than 25 character",



//             },
//             cource_name:{
//                 required: "Please enter Course name",
//                 minlength: "Please enter minimum 4 character",
//                 maxlength:"Course name should not more than 25 character",
//             },
//             email_address:{
//                 required:"  Please enter email_address ",
//                 email:"Please enter valid email id ",
//             },


//         },


// });  



