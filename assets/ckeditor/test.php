<!DOCTYPE html>
<html>
<head>
	<title>ck editor image test</title>
	<script src="ckeditor.js"></script>
	<script src="samples/js/sample.js"></script>
	<link rel="stylesheet" href="samples/css/samples.css">
	<link rel="stylesheet" href="samples/toolbarconfigurator/lib/codemirror/neo.css">
</head>
<body>
	<h2>Image from local</h2>
	<textarea id="editor" name="editor"></textarea>
	<h2>Image from save </h2>
	<textarea id="editor1" name="editor1"></textarea>
</body>
<script>
	CKEDITOR.replace( 'editor', {
		extraPlugins: 'easyimage',
		cloudServices_tokenUrl: 'https://41245.cke-cs.com/token/dev/ggMSZPgRqvPslGyrY1x3OIOy575wAVurfd1ctVRh402fGP0KvmDStr7nQO2n',
		cloudServices_uploadUrl: 'https://41245.cke-cs.com/easyimage/upload/'
	});
	//var data = CKEDITOR.instances.editor.getData();

	CKEDITOR.replace( 'editor1', {
		extraPlugins: 'easyimage',
		cloudServices_tokenUrl: 'https://41245.cke-cs.com/token/dev/ggMSZPgRqvPslGyrY1x3OIOy575wAVurfd1ctVRh402fGP0KvmDStr7nQO2n',
		cloudServices_uploadUrl: 'https://41245.cke-cs.com/easyimage/upload/'
	});
	CKEDITOR.instances.editor1.setData('<figure class="easyimage easyimage-full"><img alt="" sizes="100vw" src="https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png" srcset="https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_140 140w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_280 280w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_420 420w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_560 560w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_700 700w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_840 840w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_980 980w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_1120 1120w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_1260 1260w, https://41245.cdn.cke-cs.com/k02k9fwkgBrboLSEUWpu/images/d32c6e28c79051c5f6728626775f485bfb14052dcb649a0b.png/w_1366 1366w" width="1366" /><figcaption></figcaption></figure>')
</script>
</html>