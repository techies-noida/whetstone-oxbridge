<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'oxbridge';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// $route['about-us'] = 'oxbridge/about';
$route['about-us'] = 'oxbridge/dynamic_about';

// $route['terms-conditions'] = 'oxbridge/terms';
// $route['privacy-policy'] = 'oxbridge/privacy';
$route['terms-conditions'] = 'oxbridge/dynamic_terms';
$route['privacy-policy'] = 'oxbridge/dynamic_privacy';


$route['interviewers'] = 'oxbridge/interviewers';
$route['work-for-us'] = 'oxbridge/work_for_us';
$route['resources/personal-statement'] = 'oxbridge/resource';
$route['schools'] = 'oxbridge/school';
$route['prices'] = 'oxbridge/prices';
$route['become-tutor'] = 'interviewer/join_request';
$route['check-request-status'] = 'interviewer/request_status';
$route['request-status'] = 'interviewer/check_request_status';
$route['interviewers-list'] = 'interviewer/interviewer_list';
$route['change_password'] = 'loginSignup/change_password';
$route['logout'] = 'loginSignup/logout';
$route['profile-details'] = 'oxbridge/profile';
$route['student-notifications'] = 'Oxbridge/student_notifications';
$route['student/download-video'] = 'Oxbridge/download_student_video';
$route['interviewer-notifications'] = 'Interviewer/interview_notifications';

$route['book-now/(:any)'] = 'Appointment/pay/$1';
$route['checkout'] = 'Appointment/checkout';
$route['checkout-success/(:any)'] = 'Appointment/checkout_success/$1';


$route['session/interviewer/(:any)'] = 'TwilioController/user1/$1';
$route['session/student/(:any)'] = 'TwilioController/user2/$1';

$route['create/token'] = 'TwilioController/create_token';






/******************************** Admin Panel ***************************************/

$route['admin'] = 'Admin/login';
$route['admin-dashboard'] = 'Admin/Admin_panel/dashboard';
$route['interviewer-request'] = 'Admin/Admin_panel/request';
$route['view-interviewer-request/(:any)'] = 'Admin/Admin_panel/view_interviewer_request/$1';
$route['admin/contact-us'] = 'Admin/Admin_panel/contact_request';
$route['admin/news-letter-subscriber'] = 'Admin/Admin_panel/newsletter_subscriber';

$route['interviewers'] = 'Admin/Admin_panel/interviewers';
$route['students'] = 'Admin/Student';
$route['admin/student/dashboard/(:any)'] = 'Admin/Student/dashboard/$1';
$route['admin/student/edit-profile/(:any)'] = 'Admin/Student/edit_profile/$1';
$route['admin/interviewer'] = 'Admin/Interviewer_admin/';
$route['interviewer-contact-form'] = 'Admin/Admin_panel/interviewer_contact_form';
$route['admin/all-notifications'] = 'Admin/Admin_panel/all_notificaions';
$route['admin/send-newsletter-mail'] = 'Admin/Admin_panel/send_newsletter_mail';
$route['admin/reviews'] = 'Admin/Admin_panel/reviews';

$route['admin/interviewer-dashboard/(:any)'] = 'Admin/Interviewer_admin/dashboard/$1';
$route['admin/interviewer/edit-profile/(:any)'] = 'Admin/Interviewer_admin/edit_profile/$1';





$route['admin/school-intrested-user'] = 'Admin/Student/intrested_user';
$route['admin/home-page'] = 'Web_home_page';

//Added by ravindra 06-11-2018
$route['admin/coupn'] = 'Admin/Admin_panel/coupn';
$route['admin/add-coupn'] = 'Admin/Admin_panel/add_coupn';
$route['admin/add-coupn-action'] = 'Admin/Admin_panel/add_coupn_action';
$route['delete/coupn'] = 'Admin/Admin_panel/delete_coupn';
$route['admin/edit-coupn'] = 'Admin/Admin_panel/edit_coupn';
$route['admin/edit-coupn-action'] = 'Admin/Admin_panel/coupn_edit_action';
$route['admin/appointments'] = 'Admin/Admin_panel/appointment_list';
$route['admin/appointment-request'] = 'Admin/Admin_panel/appointment_request_list';
$route['admin/sesssion/review/(:any)'] = 'Admin/Admin_panel/reviews_details/$1';

//Added By Pranav 27-11-2018
$route['admin/about-us'] = 'Admin/Add_Content_Controller/about_us';
$route['admin/privacy'] = 'Admin/Add_Content_Controller/privacy';
$route['admin/terms'] = 'Admin/Add_Content_Controller/terms';
$route['admin/school'] = 'Admin/Add_Content_Controller/school';
$route['admin/add-work-for-us'] = 'Admin/Add_Content_Controller/add_work_for_us';
$route['admin/edit-work-for-us/(:any)'] = 'Admin/Add_Content_Controller/edit_work_us/$1';
$route['admin/work-for-us'] = 'Admin/Add_Content_Controller/work_for_us';
$route['admin/ajax/delete-work'] = 'Admin/Add_Content_Controller/delete_work_ajax';



/****************** New Dynamic pages 29/11/2018 *********************/
// $route['new-home'] = 'oxbridge/dynamic_home';
// $route['new-about-us'] = 'oxbridge/dynamic_about';

// $route['new-terms-conditions'] = 'oxbridge/dynamic_terms';
// $route['new-privacy-policy'] = 'oxbridge/dynamic_privacy';

// $route['new-work-for-us'] = 'oxbridge/dynamic_work_for_us';
// $route['new-schools'] = 'oxbridge/dynamic_school';

/***************Test code for edit video by ravindra ************************************/
$route['edit-video'] = 'TwilioController/edit_video';








