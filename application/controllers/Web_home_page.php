<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_home_page extends CI_Controller {

	function __construct() 
      {
       parent::__construct();
        $this->load->model('admin/Student_model');
        $this->load->model('Common_model'); 
        if (!isset($this->session->userdata['admin_logged_in']))
          {
           redirect(base_url().'admin');
          } 

      }
  
  public function index() 
  {
   
    /*$result['title']='Students'; 
    $result['students']=$this->Student_model->get_studentList(); 
    $this->load->view('admin/students_list',$result);*/
    $result['banner']=$this->Common_model->getData($table='banner');
    $result['choose_us']=$this->Common_model->getData($table='home_why_choose_us');
     $result['home']=$this->Common_model->getDataByID($table='home_page',$fname='serial_number',$id=1);
   // echo '<pre>';print_r($result);  echo '</pre>'; exit();
    $result['title']='home-page';  
    // print_r($result);
    // die();
     $this->load->view('admin/cms-home',$result);       
  } 

   public function banner() 
  {
   
    /*$result['title']='Students'; 
    $result['students']=$this->Student_model->get_studentList(); 
    $this->load->view('admin/students_list',$result);*/
    $result['title']='banner';  
     $this->load->view('admin/banner',$result);       
  } 


   public function add_banner()

    {
         //echo 'welcome to add'; exit();
         $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/banner/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/banner/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
          $title=$this->input->post('title');
          $sub_title=$this->input->post('subtitle'); 
          $link=$this->input->post('link'); 
          $image=$path_one;  
          $date = date('Y-m-d H:i:s', time());
          
          $data = array('title' =>$title ,
                         'sub_title'=>$sub_title,
                         'link'=>$link,
                          'banner_image'=>$image,
                         'created_date'=>$date,
                         
                       );   
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
          $result=$this->db->insert('banner', $data);
           
           $this->session->set_flashdata('success',  array('message' => 'Home page banner add successfully'));
             redirect(base_url().'admin/home-page');
             
    }

    public function update_banner()

    {
         $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/banner/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/banner/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
          $title=$this->input->post('title');
          $sub_title=$this->input->post('subtitle'); 
          $link=$this->input->post('link'); 
          $id=$this->input->post('id'); 
          $image=$path_one;  
          $date = date('Y-m-d H:i:s', time());
          
          $data = array('title' =>$title ,
                         'sub_title'=>$sub_title,
                         'link'=>$link,
                          'banner_image'=>$image,
                          'serial_number'=>$id,
                         'created_date'=>$date,
                         
                       );   
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
          $result=$this->Common_model->updateData($table='banner',$fname='serial_number',$id,$data);
           
           $this->session->set_flashdata('success',  array('message' => 'Banner Updated successfully'));
             redirect(base_url().'admin/home-page');
          
    }

     public function deleteBanner($id)  // Delete Banner From admin Panel 
      {
       // echo $id; exit();
         
        $result=$this->Common_model->deleteData($table='banner',$fname='serial_number',$id);
        if($result){
              $this->session->set_flashdata('success', array('message' => 'Banner deteded'));
              redirect(base_url().'admin/home-page');
        } else{
              $this->session->set_flashdata('error', array('message' => ' Try again banner not deteded'));
              redirect(base_url().'admin/home-page');
        }      
        
      }


 public function what_we_do()

    {
        $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/home_page/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
           
          $date = date('Y-m-d H:i:s', time());
          $image="";
          $id=1;
          $data = array('what_we_do' =>$this->input->post('what_we_do') ,
                         'what_we_do_link'=>$this->input->post('what_we_do_link'),
                         'what_we_do_link'=>$this->input->post('what_we_do_link'),
                         'what_we_do_image'=>$path_one,
                         'serial_number'=>1,
                       );
          //echo 1; exit();
           $result=$this->Common_model->updateData($table='home_page',$fname='serial_number',$id,$data);
           $this->session->set_flashdata('success',  array('message' => 'What we do content updated'));
             redirect(base_url().'admin/home-page');
          
    }


    public function how_it_work()

    {
        /*$this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/home_page/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }

            $this->load->library('upload');
             if (!empty($_FILES['img1']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img1'))
                {
                $data = $this->upload->data();
                $two=$data['file_name'];
                $path='/uploads/home_page/';
                $path_two= $path.''.$two;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $two='';
              $path_two=$this->input->post('img2');
            }

            $this->load->library('upload');
             if (!empty($_FILES['img2']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img2'))
                {
                $data = $this->upload->data();
                $three=$data['file_name'];
                $path='/uploads/home_page/';
                $path_three= $path.''.$three;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $three='';
              $path_three=$this->input->post('img3');
            }
*/

           
          $date = date('Y-m-d H:i:s', time());
          $image="";
          $id=1;
          $data = array('how_it_work_content1' =>$this->input->post('how_it_work_content1') ,
                         'how_it_work_content2'=>$this->input->post('how_it_work_content2'),
                         'how_it_work_content3'=>$this->input->post('how_it_work_content3'),
                         'how_it_work_url'=>$this->input->post('how_it_work_url'),
                         /*'how_it_work_image1'=>$path_one,
                         'how_it_work_image2'=>$path_two,
                         'how_it_work_image3'=>$path_three,*/
                         'serial_number'=>1,
                       );
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
           $result=$this->Common_model->updateData($table='home_page',$fname='serial_number',$id,$data);
           $this->session->set_flashdata('success',  array('message' => 'How it works Area content updated'));
             redirect(base_url().'admin/home-page');
          
    }

    public function real_time_video()

    {
        $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/home_page/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }

            $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img2'))
                {
                $data = $this->upload->data();
                $two=$data['file_name'];
                $path='/uploads/home_page/';
                $path_two= $path.''.$two;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $two='';
              $path_two=$this->input->post('img3');
            }
           
          $date = date('Y-m-d H:i:s', time());
          $image="";
          $id=1;
          $data = array('real_time_video_content' =>$this->input->post('editor1') ,
                        'real_time_video_heading' =>$this->input->post('heading'), 
                         /*'what_we_do_link'=>$this->input->post('what_we_do_link'),
                         'what_we_do_link'=>$this->input->post('what_we_do_link'),*/
                        'real_time_video_image1'=>$path_one,
                        'real_time_video_image2'=>$path_two,
                         'serial_number'=>1,
                       );
          //echo 1; exit();
          // echo '<pre>';print_r($data);  echo '</pre>'; exit();
           $result=$this->Common_model->updateData($table='home_page',$fname='serial_number',$id,$data);
           $this->session->set_flashdata('success',  array('message' => 'Real-Time Video Feedback Updated'));
             redirect(base_url().'admin/home-page');
          
    }


    public function school_section()

    {
        $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/home_page/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
           
          $date = date('Y-m-d H:i:s', time());
          $image="";
          $id=1;
          $data = array('school_heading' =>$this->input->post('school_heading') ,
                         'school_content'=>$this->input->post('school_content'),
                         'school_link'=>$this->input->post('school_link'),
                         'school_image'=>$path_one,
                         'serial_number'=>1,
                       );
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
           $result=$this->Common_model->updateData($table='home_page',$fname='serial_number',$id,$data);
           $this->session->set_flashdata('success',  array('message' => 'School  content updated'));
             redirect(base_url().'admin/home-page');
          
    }

    public function contact_section()

    {
        $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/home_page/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/home_page/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
           
          $date = date('Y-m-d H:i:s', time());
          $image="";
          $id=1;
          $data = array('contact_title' =>$this->input->post('contact_title') ,
                         'contact_content'=>$this->input->post('contact_content'),
                         'contact_email'=>$this->input->post('contact_email'),
                         'contact_image'=>$path_one,
                         'serial_number'=>1,
                       );
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
           $result=$this->Common_model->updateData($table='home_page',$fname='serial_number',$id,$data);
           $this->session->set_flashdata('success',  array('message' => 'Contact us content updated'));
             redirect(base_url().'admin/home-page');
          
    }

    public function add_choose_us()

    {
         $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/banner/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/banner/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
          $heading=$this->input->post('heading');
          $content=$this->input->post('content');  
          
          $image=$path_one;  
          $date = date('Y-m-d H:i:s', time());

          $data = array('heading' =>$heading ,
                         'content'=>$content,
                          'image'=>$image,
                         
                         
                       );   
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
          $result=$this->db->insert('home_why_choose_us', $data);
          //$result=$this->Common_model->updateData($table='home_why_choose_us',$fname='serial_number',$id,$data);
           
           $this->session->set_flashdata('success',  array('message' => 'Why choose_us content added successfully'));
             redirect(base_url().'admin/home-page');
          
    }  


  public function update_choose_us()

    {
         $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './uploads/banner/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/uploads/banner/';
                $path_one= $path.''.$one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
          $heading=$this->input->post('heading');
          $content=$this->input->post('content');  
          $id=$this->input->post('id'); 
          $image=$path_one;  
          $date = date('Y-m-d H:i:s', time());

          $data = array('heading' =>$heading ,
                         'content'=>$content,
                          'image'=>$image,
                          'serial_number'=>$id,
                         
                       );   
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
          $result=$this->Common_model->updateData($table='home_why_choose_us',$fname='serial_number',$id,$data);
           
           $this->session->set_flashdata('success',  array('message' => 'Why choose_us content updated successfully'));
             redirect(base_url().'admin/home-page');
          
    }

    public function delete_choose_us($id)  // Delete Banner From admin Panel 

      {
       // echo $id; exit();
         
        $result=$this->Common_model->deleteData($table='home_why_choose_us',$fname='serial_number',$id);
        if($result){
              $this->session->set_flashdata('success', array('message' => 'Why-choose-us content deleted'));
              redirect(base_url().'admin/home-page');
        } else{
              $this->session->set_flashdata('error', array('message' => ' Try again for delete why-choose-us content'));
              redirect(base_url().'admin/home-page');
        }      
        
      }  


 
    

}
