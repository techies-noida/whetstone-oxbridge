<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

	function __construct() 

      {
       parent::__construct();
        $this->load->model('Interview_schedule_model');
        $this->load->model('Common_model');  
            
            
            
        //die;    
      }


  public function paynow()
    {   

        //$data['id']=$id;

       /* if($this->input->post('submit')){*/
        
        $invoice_id=time();  
       $interviewer_id= $this->input->post('interviewer_id');      
        $customer_data = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'start'=>$this->input->post('start'),
            'end'=>$this->input->post('end'),
            'booking_date'=>$this->input->post('appointment_date'),
            'payment_status' => 'N',
            'invoice_id' => 'INV-' . $invoice_id,
            'payment_date' => date('Y-m-d'),

            

        );
        $date = date('Y-m-d H:i:s', time());
        $appoint_data= array(
              'interviewer_id'=>$this->input->post('interviewer_id'),
             'student_id' => $this->session->userdata['logged_in']['user_id'],
             'schedule_id'=>$this->input->post('schedule_id'),
            'booking_date'=>$this->input->post('appointment_date'),
            'start_time'=>$this->input->post('start_time'),
            'end_time'=>$this->input->post('end_time'),
            'created_date'=>$date, );
        //echo '<pre>'; print_r($customer_data); echo '</pre>';exit();
        $payment_id = $this->db->insert('appointment', $appoint_data);
        $payment_id = $this->db->insert('payment', $customer_data);
         $message ='Your appointment booked successfully';
           $this->session->set_flashdata('success',  array('message' => $message));
        redirect(base_url().'interviewer/profile/'.$interviewer_id);
       /* $plan_session_array = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'currency'=>'USD',
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'payment_status' => 'N',
            'invoice_id' => 'INV-' . $invoice_id,
            'payment_date' => date('Y-m-d'),
        );
        $this->session->set_userdata('checkout_session', $plan_session_array);

        redirect(base_url().'checkout');
     // }*/

      $this->load->view('book_now',$data);


    }

     
    public function pay($id=null)
    {   

        $data['id']=$id;

        if($this->input->post('submit')){
        $invoice_id=time();        
        $customer_data = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'payment_status' => 'N',
            'invoice_id' => $invoice_id,

            'payment_date' => date('Y-m-d'),

        );
        $payment_id = $this->db->insert('payment', $customer_data);
        $plan_session_array = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'currency'=>'USD',
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'payment_status' => 'N',
            'start'=>$this->input->post('start'),
            'end'=>$this->input->post('end'),
            'booking_date'=>$this->input->post('booking_date'),
            'invoice_id' => $invoice_id,
            'payment_date' => date('Y-m-d'),
        );
        $this->session->set_userdata('checkout_session', $plan_session_array);

        redirect(base_url().'checkout');
      }

      $this->load->view('book_now',$data);


    }



    public function checkout()
    {
        
       require_once(APPPATH.'libraries/Stripe.php');//or you
        
        $data=array();
        $params = array(
            "testmode"   => "on",
            "private_live_key" => "sk_live_xxxxxxxxxxxxxxxxxxxxx",
            "public_live_key"  => "pk_live_xxxxxxxxxxxxxxxxxxxxx",
            "private_test_key" => "sk_test_A8b1OqtauljKLjhZ1imBIJZA",
            "public_test_key"  => "pk_test_rkqUBNs86mfmdWZ7kMv6oui6"
        );

        if ($params['testmode'] == "on") {
            Stripe::setApiKey($params['private_test_key']);
            $pubkey = $params['public_test_key'];
        } else {
            Stripe::setApiKey($params['private_live_key']);
            $pubkey = $params['public_live_key'];
        }

        if(isset($_POST['stripeToken']))
        {

            if($this->session->userdata('checkout_session')=='')
            {
                redirect(base_url().'checkout-success/2');
            }

            $invoice_id = $this->session->userdata('checkout_session')['invoice_id'];

            $amount=round($this->session->userdata('checkout_session')['amount']);
            $amount_cents = $amount*100;// Chargeble amount
            $invoiceid = $invoice_id;// Invoice ID
            $description = "Invoice #" . $invoiceid;

            try {
                $charge = Stripe_Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => strtolower($this->session->userdata('checkout_session')['currency']),
                        "source" => $_POST['stripeToken'],
                        "description" => $description,
                        "metadata" => array("invoice_id" => $this->session->userdata('checkout_session')['invoice_id'])
                    )
                );

                $charge = $charge->__toArray(true);
                //file_put_contents('strip_responce.txt', serialize($charge));
                // var_dump($charge['source']['address_zip_check']);die;

                if ($charge['source']['address_zip_check'] == "fail") {
                    throw new Exception("zip_check_invalid");
                } else if ($charge['source']['address_line1_check'] == "fail") {
                    throw new Exception("address_check_invalid");
                } else if ($charge['source']['cvc_check'] == "fail") {
                    throw new Exception("cvc_check_invalid");
                }
                // Payment has succeeded, no exceptions were thrown or otherwise caught

                $result = "success";

            } catch(Stripe_CardError $e) {

                $error = $e->getMessage();
                $result = $e->getMessage();

            } catch (Stripe_InvalidRequestError $e) {
                $result = $e->getMessage();
            } catch (Stripe_AuthenticationError $e) {
                $result = $e->getMessage();
            } catch (Stripe_ApiConnectionError $e) {
                $result = $e->getMessage();
            } catch (Stripe_Error $e) {
                $result = $e->getMessage();
            } catch (Exception $e) {

                if ($e->getMessage() == "zip_check_invalid") {
                    $result = $e->getMessage();
                } else if ($e->getMessage() == "address_check_invalid") {
                    $result = $e->getMessage();
                } else if ($e->getMessage() == "cvc_check_invalid") {
                    $result = $e->getMessage();
                } else {
                    $result = $e->getMessage();
                }
            }

            //var_dump($result);die;

            if($result=='success') {
                $invoice_no = $this->session->userdata('checkout_session')['invoice_id'];
                $where = array('invoice_id' => $invoice_no);

                $date = date('Y-m-d H:i:s', time());
        $appoint_data= array(
              'interviewer_id'=>$this->session->userdata('checkout_session')['interviewer_id'],
              'payment_id'=>$this->session->userdata('checkout_session')['invoice_id'],
             'student_id' => $this->session->userdata['logged_in']['user_id'],
             
            'booking_date'=>$this->session->userdata('checkout_session')['booking_date'],
            'start_time'=>$this->session->userdata('checkout_session')['start'],
            'end_time'=>$this->session->userdata('checkout_session')['end'],
            'created_date'=>$date, );
        //echo '<pre>'; print_r($customer_data); echo '</pre>';exit();
        $payment_id = $this->db->insert('appointment', $appoint_data);
                
                //////////////////////////////update payment status//////////////////////////
                $this->db->where($where);
                $this->db->update('payment', $where=array('payment_status'=>'Y'));
                $this->session->unset_userdata('checkout_session');
                redirect(base_url().'checkout-success/1');
            }
            else
            {
                
                $this->session->unset_userdata('checkout_session');
                redirect(base_url().'checkout-success/2');
            }


        }

        
        $this->load->view('checkout', $data);
        

    }
    public function checkout_success($id)
    {
      if($id==1)
      $data['msg']='Payment Successfully Paid.';
      else
      $data['msg']='Sorry, Error While Payment, Please Try Again';

     /* $this->load->view('checkout_success',$data);*/
      $this->load->view('order_done',$data);

    }
    public function book()
    {
      //echo $uniqueId= time().'-'.mt_rand(); exit();
      $date = date('Y-m-d H:i:s', time());
      if (isset($this->session->userdata['logged_in'])){
          $user_id=($this->session->userdata['logged_in']['user_id']);


          $result=$this->Interview_schedule_model->check_schedule_availbility($user_id,$schedule_date);
          if($result)
          {
            echo 0; exit();
          }
          else{
              
              $data = array('start_time' => $this->input->post('start_time'),
                  'interviewer_id' => $interviewer_id,
                  'student_id' => $user_id,
                  'end_time'=>$this->input->post('end_time'),
                  'booking_date'=>$this->input->post('schedule_date'), 
                  'created_date'=>$date,
                  'booking_status'=>'booked',
                  ); 
              $result=$this->db->insert('appointment', $data); 
             if($result)
              echo 1; exit();

              }
        }else{
           redirect(base_url());
        }



       
    }

    public function bookings() // send request for become tutor

    {
      //$result['university']=$this->Interviewer_model->get_university_list();
      $this->load->view('event_bookings');
    }
    public function availibility_list()
    {
     // $user_id='8819761956973354';
      if (isset($this->session->userdata['logged_in']))
          $user_id=($this->session->userdata['logged_in']['user_id']);
      $result=$this->Interview_schedule_model->get_schedule($user_id);
      $data_events = array();

     foreach($result as $r) {
         $title=$r->start_time.' - '.$r->end_time;
         $data_events[] = array(
             "title" => $title,
             "description" => 'Available',
             "start" => $r->schedule_date,
             "backgroundColor"=>'green'
         );
           
         
     }

     echo json_encode(array("events" => $data_events));
     exit();
     // echo '<pre>'; print_r($data_events); echo '</pre>';exit();
    } 
        
 
    

}
