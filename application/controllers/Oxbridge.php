<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oxbridge extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('LoginSignup_model');
		$this->load->model('Common_model');
		$this->load->model('Interviewer_model');
		$this->load->model('Interview_schedule_model');


	}
	public function index()
	{
		//$this->load->view('welcome_message');
		$data['interviewer']=$this->Interviewer_model->top_interviewer_list();
    $data['banner']=$this->Common_model->getData($table='banner');
    $data['choose_us']=$this->Common_model->getData($table='home_why_choose_us');
     $data['home']=$this->Common_model->getDataByID($table='home_page',$fname='serial_number',$id=1);
     // print_r($result);
     // die();
		 //echo '<pre>';print_r($data); echo'/pre>'; exit();
		$this->load->view('index',$data);
	}

	public function dynamic_home()
	{
		
		$data['interviewer']=$this->Interviewer_model->top_interviewer_list();
		$data['banner']=$this->Common_model->getData($table='banner');
		$data['home']=$this->Common_model->getData($table='home_page'); 
		$data['choose']=$this->Common_model->getData($table='home_why_choose_us'); 
		 //echo '<pre>';print_r($data); echo'/pre>'; exit();
		$this->load->view('new_index',$data);
	}

	public function about()
	{

		//$this->load->view('welcome_message');

		$this->load->view('about');
	}
	public function download_student_video(){
		$file = $this->input->get('name');
		$student_id = $this->input->get('student_id');
		if(empty($file) || empty($student_id)){
			$message = "Sorry, download link is not valid!";
		}
		$check_user = $this->LoginSignup_model->is_exist_student_id($student_id);
		if(count($check_user)){
			$message ="";
		}else{
			$message = "Sorry, Invalid user id!";
		}
		$data['student_id'] = $student_id;
		$data['download_link'] = base_url()."compose_video/download/".$file;
		$data['message'] = $message;
		$this->load->view('download_video',$data);
	}

	public function dynamic_about()
	{

		$data['about']=$this->Common_model->getData($table='about_us'); 
		$data['home']=$this->Common_model->getData($table='home_page'); 
		//echo '<pre>';print_r($data); echo'/pre>'; exit();
		$this->load->view('new_about',$data);
	}

	public function interviewers()
	{

		
		$this->load->view('interviewers');
	}

	public function request_tutor()
	{
		
		$this->load->view('tutor_request');
	}

	public function work_for_us()
	{
	$data['work_for']=$this->Common_model->getData($table='work_for_us');		
		$this->load->view('workforus', $data);
	}

	public function dynamic_work_for_us()
	{
		$data['work_for']=$this->Common_model->getData($table='work_for_us'); 
		$this->load->view('new_workforus',$data);
	}


	public function resource()
	{
		
		$this->load->view('resources');
	}
	public function school()
	{
		$result['school'] = $this->Common_model->getDataByID($table = 'school', $fname = 'id', $id = 1);
		
		$this->load->view('schools', $result);
	}

	public function prices()
	{
		
		$this->load->view('prices');
	}

	public function dynamic_school()
	{
		$data['school']=$this->Common_model->getData($table='school'); 
		$this->load->view('new_schools',$data);
	}

	public function profile()
	{ 
		if (isset($this->session->userdata['logged_in'])){

			$role_id=($this->session->userdata['logged_in']['user_role_id']);
			if($role_id==3){
				$user_id=($this->session->userdata['logged_in']['user_id']); 
				$status=$this->Interviewer_model->get_student_profile_status($user_id);
				if($status=='incomplete'){

					redirect(base_url().'student/edit_profile/'.$user_id);

				}else{

					$result['title']='student-dashboard';	
					$result['user']=$this->Interviewer_model->get_student_details($user_id); 
					$result['appointment']=$this->Interview_schedule_model->student_appointments($user_id); 
				// echo '<pre>';print_r($result); echo '</pre>';exit();
					$this->load->view('manage-profile-student',$result);
				}

			}else{
				$result['title']='dashboard';
				$user_id=($this->session->userdata['logged_in']['user_id']);


				//echo $user_id; exit();
				//$result['user']=$this->Interviewer_model->get_interviewer_details($user_id); 
				// $data['details']=$this->Interviewer_model->get_interviewer_details($user_id); 
				$result['details']=$this->Interviewer_model->get_interviewer_dashbaord($user_id); 

				$result['appointment']=$this->Interview_schedule_model->interviewer_appointment_booking($user_id); 
				//echo '<pre>';print_r($result); echo '</pre>';exit();
				$this->load->view('manage-profile-interviewer',$result);

			}    

		} else{

			redirect(base_url());
		}

		
	}

	public function ratings()
	{
		$date = date('Y-m-d H:i:s', time());
		$room_name=$this->input->post('room_name');
		$rating= $this->input->post('rating');
		$comments= $this->input->post('comment'); 
		$comments2= $this->input->post('comment2'); 
		$comments3= $this->input->post('comment3'); 
		$comments4= $this->input->post('comment4'); 
		$user_type= $this->input->post('user_type');
		$student_id= $this->input->post('student_id');
		$appointment_id= $this->input->post('appointment_id');

		$question1= $this->input->post('q1');
		$question2= $this->input->post('q2');
		$question3= $this->input->post('q3');

		$interviewer_id= $this->input->post('interviewer_id');
		$data= array('student_id' =>$student_id, 
			'interviewer_id' =>$interviewer_id,
			'appointment_id' =>$appointment_id,
			'rating' =>$rating,
			'comments' =>$comments,
			'comments2' =>$comments2,
			'comments3' =>$comments3,
			'comments4' =>$comments4,
			'question1' =>$question1,
			'question2' =>$question2,
			'question3' =>$question3,
			'user_type' =>$user_type,
			'review_date' =>$date,
		);
		$this->db->insert($table='reviews',$data);  
		//echo 1; exit();
		if($user_type=='student'){

			$result=$this->Common_model->getDataByID($table='users',$fname='user_id',$student_id);
			$user_email=$result[0]->email_address;
			$user_name=$result[0]->first_name;

			$msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Dear  '.ucwords($user_name).'</p>
			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Your video feedback is being processed. This can take up to 20 minutes, but we will send it over to you as soon as it is finished.</p>';
			$subject="Interview completed";
			$data['msg']=$msg;
			$message = $this->load->view('mail/oxbridge_email_signature',$data,true);
			$mail_send=send_user_mail($user_email,$message,$subject);


			$message = "Thank you for your review. We hope you found your interview useful. You will soon be receiving your video and written feedback via email";
			$this->session->set_flashdata('success',  array('message' => $message));
			redirect(base_url().'profile-details');
	        //redirect(base_url().'session/interviewer/'.$room_name);      
		       // echo '<pre>';print_r($data); echo '</pre>'; exit();
		}else{
			$message = "Thank you for your feedback. Once we have received a review from your interview and the video feedback has been processed you will then be paid for your work. This will happen in the next 24 hours.";
			$this->session->set_flashdata('success',  array('message' => $message));
       // redirect(base_url().'session/student/'.$room_name); 
			redirect(base_url().'profile-details'); 

		}

	}

	public function contact_us()

	{
		$date = date('Y-m-d H:i:s', time());
		$email=$this->input->post('contact_email');
		$user_name=$this->input->post('contact_name'); 
		$comments=$this->input->post('contact_comment');   
		$data= array('user_name' =>$user_name,'email_address'=>$email,'comments'=>$comments,'contact_date'=>$date);  
		$result=$this->Common_model->insertdata($table='contact_us',$data);
		echo 1;
		$msg='<p class="text-gray">Dear  '.ucwords($user_name).'</p>
		<p class="text-gray">Thank you for contacting us </p> 
		<p>One of our representatives will be in contact with you shortly regarding your inquiry. If you ever have any questions that require immediate assistance, please call us at (+440) 20-8133 8277. Have a great day!
		</p>';

		$admin_msg= '
		<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Please find the new inquiry details- </p>
		<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Name :</b> '.ucwords($user_name).'</p>
		<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b> Email :</b> '.$email.'</p>
		<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Message : </b>'.$comments.'</p>';

		$subject='Thank You For Contacting Us';
	          			//$mail_data= array('user_name' =>$user_name,'user_id'=>$user_id );
		$data1['msg']=$msg;
		$admin_data['msg']=$admin_msg;
	       // $message = $this->load->view('mail/email_template',$data,true);
		$message = $this->load->view('mail/oxbridge_email_signature',$data1,true);
		$result=send_user_mail($email,$message,$subject);
		$admin_email='info@whetstone-oxbridge.com';
		$admin_subject='Whetstone Oxbridge new inquiry';
		$admin_message = $this->load->view('mail/oxbridge_email_signature',$admin_data,true);
		$result=send_user_mail($admin_email,$admin_message,$admin_subject);
		exit();		
	}

	public function newsletter()

	{
		$date = date('Y-m-d H:i:s', time());
		$email=$this->input->post('newsletter_email');  
		$result=$this->Common_model->check_duplicate($table='newsletter',$fname='email_address',$email);
		if($result){
			echo 0; exit();


		}else{
			$data= array('email_address' =>$email,'subscribe_date'=>$date,'verify_status'=>0);  
			$result=$this->Common_model->insertdata($table='newsletter',$data);
			echo 1;
			$msg='<p class="text-gray"> '.ucwords($email).'</p>
			<p class="text-gray">Please verify your Email Id and keep up to date with the latest Oxbridge news and application advice..</p>
			<p class="text-gray"> Verify Email <a href="https://whetstone-oxbridge.com/oxbridge/verify_newsletterEmail/?email='.$email.'"> <b>Click here </b></a></p>';
			$subject=' Confirm to subscribe Oxbridge newsletter';
			$data['msg']=$msg;
		        //$message = $this->load->view('mail/email_template',$data,true);
			$message = $this->load->view('mail/oxbridge_email_signature',$data,true);
			$result=send_user_mail($email,$message,$subject);
			exit();	
		}	
	}
	public function student_notifications()
	{
		$result['title']='student-dashboard';	
		$id=($this->session->userdata['logged_in']['user_id']);
		$result['user']=$this->Interviewer_model->get_student_details($id);
		$result['notifications'] = $this->Interviewer_model->get_notifications_lists($id);
		$this->load->view('student_notifications', $result);
		// print_r($result['notifications']);
		// echo $id;
	}
	public function verify_newsletterEmail()	

	{
		$email=$this->input->get('email');
		$date = date('Y-m-d H:i:s', time());
		$data=$arrayName = array('email_address' => $email,'verify_status'=>1,'verify_date'=>$date);
		$data_result=$this->Common_model->updateData($table='newsletter',$fname='email_address',$email,$data);
		if($data_result)
		{
			$msg='<p class="text-gray">New user subscribe Oxbridge newsletter by email id   '.$email.'   and verified it. </p>';
			$subject='New user subscribe Oxbridge newsletter';
			$admin_email='info@whetstone-oxbridge.com';
				//$admin_email='devendra@coretechies.com';
			$data1['msg']=$msg;
			$message = $this->load->view('mail/oxbridge_email_signature',$data1,true);
			$result=send_user_mail($admin_email,$message,$subject);
			redirect(base_url());
		}

	}

	public function interested_user()

	{
		$date = date('Y-m-d H:i:s', time());
		$email=$this->input->post('interested_user_email'); 
		$name=$this->input->post('interested_user_name');
		$school_name=$this->input->post('school_name');
		$phone=$this->input->post('interested_user_phone');
		$message=$this->input->post('interested_user_message'); 
		$interview=$this->input->post('interview');
		$road_show=$this->input->post('roadshow');
		$result=$this->Common_model->check_duplicate($table='interested_user',$fname='email_address',$email);
		if($result){
			echo 0; exit();


		}else{
			$data= array('user_name'=>$name,'school_name'=>$school_name,'email_address' =>$email,'phone_number' =>$phone,'message'=>$message,'interview'=>$interview,'road_show'=>$road_show,'created_date'=>$date);
				//print_r($data); exit();  
			$result=$this->Common_model->insertdata($table='interested_user',$data);


			$msg='<p class="text-gray">Dear  '.ucwords($name).'</p>
			<p class="text-gray">Thank you for joining us </p> 
			<p>One of our representatives will be in contact with you shortly regarding your inquiry. If you ever have any questions that require immediate assistance, please call us at (+440) 20-8133 8277. Have a great day!
			</p>';

			$admin_msg= '
			<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Please find the new interested user details- </p>
			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Name :</b> '.ucwords($name).'</p>
			<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b> Email :</b> '.$email.'</p>
			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>School Name : </b>'.$school_name.'</p>
			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Contact Number : </b>'.$phone.'</p>
			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Intrerseted In : </b>'.$interview.'and  '.$road_show.'</p>';

			$subject='Thank You For Joining Us';
	          			//$mail_data= array('user_name' =>$user_name,'user_id'=>$user_id );
			$data1['msg']=$msg;
			$admin_data['msg']=$admin_msg;
	       // $message = $this->load->view('mail/email_template',$data,true);
			$message = $this->load->view('mail/oxbridge_email_signature',$data1,true);
			$result=send_user_mail($email,$message,$subject);
			$admin_email='info@whetstone-oxbridge.com';
			$admin_subject='New Interested User';
			$admin_message = $this->load->view('mail/oxbridge_email_signature',$admin_data,true);
			$result=send_user_mail($admin_email,$admin_message,$admin_subject);
			//exit();	
			echo 1;
		        /*$msg='<p class="text-gray">'.ucwords($name).'</p>
		            <p class="text-gray">Have Intrested with details</p>
		            <p class="text-gray"><b>School Name</b>.'$school_name'.</p>';
				$subject=' Confirm to subscribe Oxbridge newsletter';
		        $data['msg']=$msg;
		        $message = $this->load->view('mail/email_template',$data,true);
		        $result=send_user_mail($email,$message,$subject);*/
		        exit();	
		    }	
		} 


		public function bulk_request()

		{
			//echo 'welcome to submit form'; exit();
			$date = date('Y-m-d H:i:s', time());
			$contact_person=$this->input->post('contact_person'); 
			$school_name=$this->input->post('school_name');
			$school_email=$this->input->post('school_email');
			$school_contact=$this->input->post('school_contact');
			$student_number=$this->input->post('student_number'); 
			$comments=$this->input->post('comments');

			$result=$this->Common_model->check_duplicate($table='school_bulk_request',$fname='school_email',$school_email);
			if($result){
				echo 0; exit();


			}else{
				$data= array('contact_person'=>$contact_person,'school_name'=>$school_name,'school_email' =>$school_email,'school_contact_number' =>$school_contact,'total_applicant'=>$student_number,'comments'=>$comments,'request_date'=>$date);
				//print_r($data); exit();  
				$result=$this->Common_model->insertdata($table='school_bulk_request',$data);
				echo 1;
		        /*$msg='<p class="text-gray">'.ucwords($name).'</p>
		            <p class="text-gray">Have Intrested with details</p>
		            <p class="text-gray"><b>School Name</b>.'$school_name'.</p>';
				$subject=' Confirm to subscribe Oxbridge newsletter';
		        $data['msg']=$msg;
		        $message = $this->load->view('mail/email_template',$data,true);
		        $result=send_user_mail($email,$message,$subject);*/
		        exit();	
		    }	
		}    		

		

		public function terms()
		{

			$this->load->view('terms.php');
		}

		public function privacy()
		{

			$this->load->view('policy');
		}

		public function dynamic_terms()
		{

			$data['terms']=$this->Common_model->getData($table='terms'); 
			$this->load->view('new_terms',$data);
		}

		public function dynamic_privacy()
		{
			$data['privacy']=$this->Common_model->getData($table='privacy_policy'); 
		//print_r($data); exit();
			$this->load->view('new_policy',$data);
		}



		public function student_screen()
		{

			$this->load->view('interview_e');
		}
		public function tutor_screen()
		{

			$this->load->view('interview_r');
		}

		public function thanks()
		{

			$this->load->view('thanks2');
		}

		public function discount_coupon()
		{
			$coupn_code = $_POST["discount_coupon"];
			$result = $this->Common_model->discount_coupon($coupn_code);
			echo $result;
		}
		public function discount_amount()
		{
			$new_price = $_POST["new_price"];
			$old_plan_session_array =  $this->session->userdata('purchase_plan');
			$new_plan_session_array = array(
				'student_id' => $old_plan_session_array['student_id'],
				'package' => $old_plan_session_array['package'],
				'interviewer_id'=> $old_plan_session_array['interviewer_id'],
				'amount'=> $old_plan_session_array['amount'],
				'new_amount'=> $new_price,
				'discount_coupon'=> $_POST["discount_coupon"],
				'discount_amount'=> $old_plan_session_array['amount'] - $new_price,
				'attachment'=> $old_plan_session_array['attachment'],
				'session_start_time'=> $old_plan_session_array['session_start_time'],
				'session_end_time'=> $old_plan_session_array['session_end_time'],
				'appointment_date'=> $old_plan_session_array['appointment_date'],
				'message'=> $old_plan_session_array['message']
			);
			$this->session->set_userdata('purchase_plan', $new_plan_session_array);
			print_r($new_plan_session_array);
		}
		// public function update_coupn_usage()
		// {
		// 	$coupn_code = $_POST["discount_coupon"];
		// 	$coupn_usage = $this->Common_model->coupn_usage($coupn_code);
		// 	$updated_data = array(
		// 		'coupn_usage'  => $coupn_usage + 1       
		// 	); 
		// 	$this->Common_model->update_coupn_usage($coupn_code, $updated_data);  
		// }

		function test_mail()
		{

			$email='arvind@coretechies.com';
			$message='Testing Mail';
			$subject='You are the best Mail';
			$ci =& get_instance();
			$config = Array(
				'protocol' => 'mail',
				'smtp_host' => 'smtp.gmail.com',
				'smtp_port' => 465,
      	'smtp_user' => 'info@whetstone-oxbridge.com', // change it to yours
      	'smtp_pass' => 'qonQu2-joqdik-fifwug', // change it to yours
      	'smtp_timeout' => '8',
      	'mailtype' => 'html',
      	'charset' => 'utf-8',
      	'wordwrap' => TRUE
      );


			$ci->load->library('email', $config);
			$ci->email->set_newline("\r\n");
          $ci->email->from('info@whetstone-oxbridge.com'); // change it to yours
          $ci->email->to($email);// change it to yours
          $ci->email->subject($subject);
          $ci->email->message($message);
          if($ci->email->send())
          {
          //return true;
          	echo 'send mail';
          }
          else
          {
          //return false;
          	show_error($ci->email->print_debugger());
        // show_error($ci->email->print_debugger());
          }

      }  

  }
