<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interviewer extends CI_Controller {

	function __construct() 
  {
   parent::__construct();
   $this->load->model('Interviewer_model');
   $this->load->model('Interview_schedule_model');
   $this->load->model('Common_model');   

 }
   public function join_request() // send request for become tutor

   {
    $result['university']=$this->Interviewer_model->get_university_list();
    $this->load->view('tutor_request',$result);
  } 

  public function get_college($university_id)

  {
    $university = $this->db->get_where(' college' , array('university_id' => $university_id))->result_array();
    foreach ($university as $row) {
      echo '<option value="' . $row['college_id'] . '">' . $row['college_name'] . '</option>';   
    }
  }  

  public function add_join_request()

  {


   $pic=$this->input->post('picture');
   define('UPLOAD_DIR', './uploads/profile/tutor/');
   $pic = str_replace('data:image/png;base64,', '', $pic);
   $pic = str_replace(' ', '+', $pic);
   $data = base64_decode($pic);
   $path='/uploads/profile/tutor/';
   $file = UPLOAD_DIR. uniqid() . '.png';

   $success = file_put_contents($file, $data);
   $new_profile = substr($file, 1);

   $email_address=$this->input->post('email_address');  
   $result=$this->Interviewer_model->check_duplicate_user($email_address); 
   if($result){
     $this->session->set_flashdata('error',  array('message' => 'You have already requested for interviewer acccount'));
  	       // redirect(base_url().'become-tutor');
     redirect(base_url().'work-for-us/#subscribe');
   }else{
     $request_id=custom_random($digit=11);
     $name=$this->input->post('name');
     $date = date('Y-m-d H:i:s', time());
     $data = array('name' => $this->input->post('name'),
      'email_address'=>$this->input->post('email_address'),
      'working_interest'=>$this->input->post('working_as'),
      'university_id'=>$this->input->post('university'),
      'wish_to_know'=>$this->input->post('wish_to'),
      'extra_curricular'=>$this->input->post('extra_curricular'),
      'subject'=>$this->input->post('subject'),
      'year_of_study'=>$this->input->post('year_of_study'),
      'college_id'=>$this->input->post('college'),
      'phone_number'=>$this->input->post('mobile_number'),
      'profile_image'=>$new_profile,
      'request_id'=>$request_id,
      'request_date'=>$date,
      'request_status'=>'waiting'
    );	
  			//echo '<pre>';print_r($data);  echo '</pre>';exit();
     $this->db->insert('tutor_join_request', $data);
     /*$message = 'Thanks for requesting Join as tutor yor request Id is '.$request_id.'<br> You can check your request status <a href="'.base_url().'interviewer/check_request_status?request_id='.$request_id.'"> <b>Click here </b></a><br> Team will responce soon for your request';*/


     $msg='<h4 style="color: #2aaebf !important; font-size: 18px">Application Received</h4>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Thank you for applying to interview for Whetstone Oxbridge.</p>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">We are processing your information as quickly as possible and will let you know whether or not your application has been successful ASAP. </p>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> In the meantime, it is important to start thinking about your availability. The greater the number of available slots in your calendar, the more work you are likely to get.</p>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Please check your request status.<a href="https://whetstone-oxbridge.com/check-request-status"> <b>Click here </b></a></p>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Request Id : </b>'.$request_id.'</p>';
     $admin_msg= '
     <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Please find the new Interviewer request details- </p>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Name :</b> '.ucwords($name).'</p>
     <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b> Email :</b> '.$email_address.'</p>
     <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Request Id : </b>'.$request_id.'</p>';
     $subject='Application Received';
     $admin_subject='New Interviewer Registred';

     $data['msg']=$msg;
                //$message = $this->load->view('mail/email_template',$data,true);
     $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
     $result=send_user_mail($email_address,$message,$subject);
     $admin_email='info@whetstone-oxbridge.com';
     $admin_data['msg']=$admin_msg;
     $admin_message = $this->load->view('mail/oxbridge_email_signature',$admin_data,true);
     $result=send_user_mail($admin_email,$admin_message,$admin_subject);
           // exit();
     $data1=array('name'=>$name,'request_id'=>$request_id);
     $details['message']=$data1;
     $this->load->view('tutor-thankyou',$details);
   }
 }


  // Setup tutor profile display
 public function setup_account() 

 {
  $request_id=$this->input->get('request_id');
  $data['status']=$this->Interviewer_model->get_request_details($request_id);
      //echo '<pre>';print_r($data); echo '</pre>'; exit();
  $status=$data['status'][0]->request_status;
  $country_list = $this->Common_model->getDataByOrder($table="country",$fname="country_name");
  $data['country'] = array_reverse($country_list);
  if($status=='verified')
      //
    $this->load->view('setup-profile_new',$data);
  else{

    $this->load->view('tutor-request-status',$data);
  }


}  

public function create_account()

{

 $pic=$this->input->post('picture');
 if (strlen($pic)>300){
  define('UPLOAD_DIR', './uploads/profile/tutor/');
  $pic = str_replace('data:image/png;base64,', '', $pic);
  $pic = str_replace(' ', '+', $pic);
  $data = base64_decode($pic);
  $path='/uploads/profile/tutor/';
  $file = UPLOAD_DIR. uniqid() . '.png';

  $success = file_put_contents($file, $data);
  $new_profile = substr($file, 1);


}
else{
  $new_profile=$this->input->post('img1');

}
$first_name=$this->input->post('first_name');
$last_name=$this->input->post('last_name'); 
$email_address=$this->input->post('email_address');
$password=md5($this->input->post('passwords')); 
$phone_number=$this->input->post('mobile_number');  
          // $image=$path_one;  
$date = date('Y-m-d H:i:s', time());
$user_id=custom_random($digit=15); 
$user_name=$first_name.' '.$last_name;
$result=$this->Common_model->check_duplicate($table='users',$fname='email_address',$email_address);
if($result){
  $message = ucwords($user_name).', Your account has now been successfully created. <br> Please now go to your profile and register your availability in your interview calendar. ';
  $this->session->set_flashdata('success',  array('message' => $message));
  redirect(base_url().'interviewer/thankyou'); 

}else{
  $data= array('first_name' =>$first_name,'last_name'=>$last_name,'phone_number'=>$phone_number,'email_address'=>$email_address,'password'=>$password,'user_id'=>$user_id,'user_role_id'=>2,'profile_image'=>$new_profile,'created_date'=>$date,'status'=>1);

  $data2 = array('gender' =>$this->input->post('gender') ,
   'date_of_birth'=>$this->input->post('dob'),
   'country_id'=>$this->input->post('country'),
   'university_id'=>$this->input->post('university_id'),
   'college_id'=>$this->input->post('college_id'),

   'subject'=>$this->input->post('subject'),
   'course_year'=>$this->input->post('study_year'),
   'experience'=>$this->input->post('interview_exprience'),
   'academic_interest'=>$this->input->post('acadmic_intrests'),
   'wish_to_told_oxbridge'=>$this->input->post('wish_to'),
   'extra_curricular_interest'=>$this->input->post('extra_curicular'),
   'about_us'=>'',
   'created_date'=>$date,
   'user_id'=>$user_id,
   'account_status'=>'verified',
 );   
          //echo '<pre>';print_r($data);  echo '</pre>';
         // echo '<pre>';print_r($data2);  echo '</pre>';
          //exit();
          // $data2= array('account_status' =>'verified','user_id'=>$user_id,'created_date'=>$date); 
  $user_name=$first_name.' '.$last_name;
  $result=$this->db->insert('users', $data);
  $result=$this->db->insert('tutor_information',$data2);

  $message = ucwords($user_name).', Your account created successfully!';
  $this->session->set_flashdata('success',  array('message' => $message));
  redirect(base_url().'interviewer/thankyou'); 
}   
}

public function interviewer_list()

{
  $data = $this->Interviewer_model->get_interviewer_list();
  $newdata = $this->shuffle_assoc($data);
  $result['interviewer']= $newdata;
  $this->load->view('interviewers',$result);
  
  // print_r($newdata);
}   

function shuffle_assoc($list) { 
  if (!is_array($list)) return $list; 

  $keys = array_keys($list); 
  shuffle($keys); 
  $random = array(); 
  foreach ($keys as $key) { 
    $random[$key] = $list[$key]; 
  }
  return $random; 
}


	public function profile($id) // tutor profile display

	{ 

    $result=$this->Common_model->check_duplicate($table='users',$fname='user_id',$id);
    if($result){
      $data['interviewer_id']=$id;
      //$data['details']=$this->Interviewer_model->get_interviewer_details($id);
      $data['details']=$this->Interviewer_model->get_interviewer_dashbaord($id);
      $data['reviews']=$this->Interviewer_model->interviewerTopReview($id);
      
      $this->load->view('interviewer_profile',$data);
    }
    else{
      //echo 'invalid  user id';
      $this->interviewer_list();
      redirect(base_url().'interviewers-list');

    }
  }
  public function edit_profile() // tutor profile display

  {

    if (isset($this->session->userdata['logged_in'])){
      $user_id=($this->session->userdata['logged_in']['user_id']);
      $result['title']='interviewer-edit-profile';
       //$result['details']=$this->Interviewer_model->get_interviewer_details($user_id); 
      $result['details']=$this->Interviewer_model->get_interviewer_dashbaord($user_id);
      $result['user']=$this->Interviewer_model->get_profile_details($user_id);
      $result['country']=$this->Common_model->getDataByOrder($table="country",$fname="country_name");
      $result['college']=$this->Common_model->getDataByOrder($table="college",$fname="college_name");
      $result['university']=$this->Common_model->getDataByOrder($table="university",$fname="university_name");
      // echo '<pre>';print_r($result); echo '</pre>';exit();
      $this->load->view('edit-profile',$result);

    } else{

     redirect(base_url());
   }
   

 }  

 public function fetch_collage_list()
 {
   $result = $this->Common_model->fetch_collage_list($_POST['university_id']);
   print_r(json_encode($result));
 }

 public function update_profile()
 {

   if($this->check_duplicate_edit_email($this->input->post('email_address'), $this->input->post('user_id'))){
      $this->session->set_flashdata('error',  array('message' => 'University email address  is already registered.'));
      redirect(base_url().'interviewer/edit_profile');
   }
   $pic=$this->input->post('picture');
    //echo '<br> profile is '.$new_profile=$this->input->post('img1'); exit();
   if (strlen($pic)>300){
    define('UPLOAD_DIR', './uploads/profile/tutor/');
    $pic = str_replace('data:image/png;base64,', '', $pic);
    $pic = str_replace(' ', '+', $pic);
    $data = base64_decode($pic);
    $path='/uploads/profile/tutor/';
    $file = UPLOAD_DIR. uniqid() . '.png';

    $success = file_put_contents($file, $data);
    $new_profile = substr($file, 1);


  }
  else{
    $new_profile=$this->input->post('img1');

  }

  $date = date('Y-m-d H:i:s', time());
  $user_id=$this->input->post('user_id'); 
  $image="";
  $data = array('first_name' =>$this->input->post('first_name') ,
   'phone_number'=>$this->input->post('phone_number'),
   'profile_image'=>$new_profile,
   'user_id'=>$user_id,
 );
  $data2 = array('gender' =>$this->input->post('gender') ,
   'date_of_birth'=>$this->input->post('dob'),
   'country_id'=>$this->input->post('country'),
   'subject'=>$this->input->post('subject'),
   'college_id'=>$this->input->post('college'),
   'university_id'=>$this->input->post('university'),
   // 'about_us'=>$this->input->post('about_us'),
   'course_year'=>$this->input->post('study_year'),
   'experience'=>$this->input->post('interview_exprience'),
   'academic_interest'=>$this->input->post('acadmic_intrests'),
   'wish_to_told_oxbridge'=>$this->input->post('wish_to'),
   'extra_curricular_interest'=>$this->input->post('extra_curicular'),
   'last_updated_date'=>$date,
   'user_id'=>$user_id,
   // 'about_us'=>$this->input->post('about_us'),
 );
           // echo '<pre>';print_r($data); echo '</pre>';
           // echo '<pre>';print_r($data2); echo '</pre>';exit();
  $result=$this->Common_model->updateData($table='users',$fname='user_id',$user_id,$data);
  $result=$this->Common_model->updateData($table='tutor_information',$fname='user_id',$user_id,$data2);
  $this->session->set_flashdata('success',  array('message' => 'Your profile has been updated'));
  redirect(base_url().'interviewer/edit_profile');

}

	public function request_status() // Check Request Status

	{
		
   $this->load->view('check_tutor_request_status');		
 }

 public function check_request_status()
 {
  $request_id=$this->input->get('request_id');
  $result=$this->Interviewer_model->check_request_id($request_id); 
  if($result){
   $data['status']=$this->Interviewer_model->get_request_details($request_id); 

       // $this->load->view('display_tutor_request_status',$data);
   $this->load->view('tutor-request-status',$data);
 }else{
   $this->session->set_flashdata('error',  array('message' => 'Please check request id is invalid '));
   redirect(base_url().'interviewer/request_status');

 }
}



public function  ratings()  
{
  if (isset($this->session->userdata['logged_in'])){
   $id=($this->session->userdata['logged_in']['user_id']);
   $data['title']='interviewer-rating';
        //$data['details']=$this->Interviewer_model->get_interviewer_details($id);
   $data['details']=$this->Interviewer_model->get_interviewer_dashbaord($id);
   $data['reviews']=$this->Interviewer_model->interviewerAllReview($id);
   $this->load->view('interviewer_ratings',$data);
 } else{

   redirect(base_url());
 }
} 



  public function interview_notifications()
  {
    if (isset($this->session->userdata['logged_in'])){
   $id=($this->session->userdata['logged_in']['user_id']);
   $data['title']='interviewer-notifications';
   $data['notifications'] = $this->Interviewer_model->get_notifications_lists_interview($id);
   $data['details']=$this->Interviewer_model->get_interviewer_dashbaord($id);
   $data['reviews']=$this->Interviewer_model->interviewerAllReview($id);
   $this->load->view('interview_notifications',$data);
 } else{

   redirect(base_url());
 }
  }

public function  bookings()  
{

  if (isset($this->session->userdata['logged_in'])){
   $id=($this->session->userdata['logged_in']['user_id']);
   $data['title']='interviewer-booking';
   $data['details']=$this->Interviewer_model->get_interviewer_details($id);
   $data['appointment']=$this->Interview_schedule_model->interviewer_appointment_booking($id);
        //echo '<pre>';print_r($data); echo '</pre>'; exit();
   $this->load->view('interviewer_bookings',$data);
 } else{

   redirect(base_url());
 }
}  



  public function send_mail_student() // send mail when user have travelling to connent

  {
    $new_message= $_POST['message'];

      //echo 'interviewer_id is '.$student_id.'and message is '.$new_message; exit();
    $result = $this->db->get_where('users' , array('user_id' => $_POST['student_id']))->result_array();
    $email_address=$result[0]['email_address'];
    $user_name=$result[0]['first_name'];
     // $email_address='devendra@coretechies.com';
    $msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Dear  '.ucwords($user_name).'</p>

    <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">We have been notified that you are currently meant to be in a scheduled interview.</p>
    <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>Your interviewer has sent the following message:<br>  "</b><span style="font-style: italic; font-size: 25px;">"'.$new_message.'"</span></p>
    <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>Make sure:</b></p>
    <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>1.</b>You are not trying to access the interview platform via mobile or tablet, use a laptop or desktop.</p>

    <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>2:</b>You have a strong enough internet connection. If you are unsure about this, then it'.'’'.'s worth running a speed test. (www.speedtest.net)</p>
    ';
    $subject='Having trouble connecting';
    $data['msg']=$msg;
            //$message = $this->load->view('mail/email_template',$data,true);
    $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
    $result=send_user_mail($email_address,$message,$subject);
              //echo 'An email alert has been sent.';
    echo 'Message sent to applicant.';

  }   
  function check_mail()
  {
    $email='devendra@coretechies.com';
    $request_id='38745387567876435';
    //$message='Testing mail';
    $subject="Test mail ";
    $message = 'Thanks for requesting Join as tutor yor request Id is'.$request_id.'<br> You can check your request status <a href="'.base_url().'interviewer/check_request_status?request_id='.$request_id.'"> <b>Click here </b></a><br> Team will responce soon for your request';
     //send_mail($email,$message,$subject);
     //verifyMail($email,$message);
    send_user_mail($email,$message,$subject);
  }

  function send_user_mail1()
  {
      //$ci =& get_instance();

    $email='devendra@coretechies.com';
    $request_id='38745387567876435';
    //$message='Testing mail';
    $subject="Test mail ";
    $message = 'Thanks for requesting Join as tutor yor request Id is'.$request_id.'<br> You can check your request status <a href="'.base_url().'interviewer/check_request_status?request_id='.$request_id.'"> <b>Click here </b></a><br> Team will responce soon for your request';
    $config = Array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://smtp.googlemail.com',
      'smtp_port' => 465,
      'smtp_user' => 'devendra.fibso@gmail.com', // change it to yours
      'smtp_pass' => 'devendra@9648yadav', // change it to yours
      'mailtype' => 'html',
      'charset' => 'iso-8859-1',
      'wordwrap' => TRUE
    );

    /*$message = 'Thanks for  requestiing Join as tutor yor request Id is 123456734345 can check your request status <br> Team will responce soon for your request';*/
    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");
          $this->email->from('devendra.fibso@gmail.com'); // change it to yours
          $this->email->to($email);// change it to yours
          $this->email->subject($subject);
          $this->email->message($message);
          if($this->email->send())
          {
          //return true;
            echo 'send mail'; exit();
          }
          else
          {
          //return false;
           show_error($this->email->print_debugger());
         }

       }
       public function set_profile()

       {
	    	//$this->load->view('setup-profile');
         $this->session->set_flashdata('success',  array('message' => 'Your tutor profile Complete Please login '));
         redirect(base_url().'interviewer/thankyou');

       }

       public function thanks()
       {
    	//$this->load->view('tutor-thankyou');
         $this->load->view('tutor-thankyou');

       }
       public function thankyou()
       {
      //$this->load->view('tutor-thankyou');
        $this->load->view('thanks');

      }
      public function interviewer_contact_form()
      {
        $university_name = $this->input->post('university_name');
        $college_name =   $this->input->post('college_name');
        $course_name = $this->input->post('cource_name');
        $interviewer_email_address =  $this->input->post('email_address');
        $data = array(
          'university_name' => $this->input->post('university_name'), 
          'college_name' => $this->input->post('college_name'), 
          'course_name  ' => $this->input->post('cource_name'), 
          'interviewer_email_address' => $this->input->post('email_address'), );
        $this->Interviewer_model->interviewer_contact_form($data);
        echo 1;
        $msg='<p class="text-gray">Dear Interviewer</p>
        <p class="text-gray">Thank you for contacting us </p> 
        <p>One of our representatives will be in contact with you shortly regarding your inquiry. If you ever have any questions that require immediate assistance. Have a great day!
        </p>';

        $admin_msg= '
        <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Please find the new inquiry details- </p>
        <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>University Name :</b> '.$university_name.'</p>
        <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>College Name :</b> '.$college_name.'</p>
        <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Course Name :</b> '.$course_name.'</p>
        <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Email : </b>'.$interviewer_email_address.'</p>';

        $subject='Thank You For Contacting Us';
                  //$mail_data= array('user_name' =>$user_name,'user_id'=>$user_id );
        $data1['msg']=$msg;
        $admin_data['msg']=$admin_msg;
         // $message = $this->load->view('mail/email_template',$data,true);
        $message = $this->load->view('mail/oxbridge_email_signature',$data1,true);
        $result=send_user_mail($interviewer_email_address,$message,$subject);
        $admin_email='info@whetstone-oxbridge.com';
        $admin_subject='Interviewer : Whetstone Oxbridge new inquiry';
        $admin_message = $this->load->view('mail/oxbridge_email_signature',$admin_data,true);
        $result=send_user_mail($admin_email,$admin_message,$admin_subject);
        exit();   
      }
      public function check_duplicate_edit_email($email, $id) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email_address', $email);
        $this->db->where('user_id !=', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
        return true;
        } 
        else 
        {
        return false;
        } 
      }
    }
