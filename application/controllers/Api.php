<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Chat_model');
		$this->load->model('Common_model');
		$this->load->model('Appointment_model');
	}
	
	
	public function send_message()
	{
		$message = $this->input->get('message', null);
		$nickname = $this->input->get('nickname', '');
		$guid = $this->input->get('guid', '');
		$sender_id = $this->input->get('sender_id', '');
		$receiver_id = $this->input->get('receiver_id', '');
		$appointment_id = $this->input->get('appointment_id', '');
		$room_id = $this->input->get('room_name', ''); 
		
		$this->Chat_model->add_message($message, $nickname, $guid,$sender_id,$receiver_id,$appointment_id,$room_id);
		$this->_setOutput($message);

		$call_status=$this->Appointment_model->check_call_status($appointment_id,$status='Active');
		if($call_status){
			$data= array('call_status' =>'Started','call_updated_by'=>$receiver_id,'appointment_id'=>$appointment_id);
		    $result=$this->Common_model->updateData($table='appointment',$fname='appointment_id',$appointment_id,$data);

		}


	}

	public function send_message_connect()
	{
		$message = $this->input->get('message', null);
		$nickname = $this->input->get('nickname', '');
		$sender_id = $this->input->get('sender_id', '');
		$receiver_id = $this->input->get('receiver_id', '');
		$appointment_id = $this->input->get('appointment_id', '');
		$room_id = $this->input->get('room_name', '');

		$call_status=$this->Appointment_model->check_call_status($appointment_id,$status='Active');
		if($call_status){
			$data= array('call_status' =>'Started','call_updated_by'=>$receiver_id,'appointment_id'=>$appointment_id);
		    $result=$this->Common_model->updateData($table='appointment',$fname='appointment_id',$appointment_id,$data);

		}
		

		if($message=='Welcome') 
		 {
		 	$result=$this->Chat_model->check_duplicate($message,$appointment_id);
		 	if(!$result){
		 		$this->Chat_model->add_message($message, $nickname, $guid,$sender_id,$receiver_id,$appointment_id,$room_id);
		        $this->_setOutput($message);

		 	}

		 }
		 else{
		 	$this->Chat_model->add_message($message, $nickname, $guid,$sender_id,$receiver_id,$appointment_id,$room_id);
		    $this->_setOutput($message);

		 }
		
		
	}
	
	
	public function get_messages()
	{
		$timestamp = $this->input->get('timestamp', null);
		$sender_id = $this->input->get('sender_id', '');
		$receiver_id = $this->input->get('receiver_id', '');
		$appointment_id = $this->input->get('appointment_id', '');
		
		$messages = $this->Chat_model->get_messages($timestamp,$sender_id,$receiver_id,$appointment_id);

		
		$this->_setOutput($messages);
	}
	
	
	private function _setOutput($data)
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data);
	}
	public function check_call_status(){
		$status = $this->input->post('call_status');
		$appointment_id = $this->input->post('appointment_id');
		$call_status=$this->Appointment_model->check_call_status($appointment_id,$status);
		if($call_status){
			echo 1;
		}else{
			echo 0;
		}
	}
	public function get_call_status(){
		$appointment_id = $this->input->post('appointment_id');
		$call_status=$this->Appointment_model->get_call_status($appointment_id);
		if($call_status){
			echo $call_status;
		}else{
			echo 0;
		}
	}
}