﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	function __construct() 
      {
        parent::__construct();
        $this->load->model('Interviewer_model');
        $this->load->model('Interview_schedule_model');
        $this->load->model('LoginSignup_model');
        $this->load->model('Common_model');
        $this->load->model('Twilio_model');   
        }
  
  public function edit_profile() // tutor profile display

  {

    if (isset($this->session->userdata['logged_in'])){
      $user_id=($this->session->userdata['logged_in']['user_id']);
     // echo 'user_id is'.$user_id; exit();
     // $result['user']=$this->Interviewer_model->get_profile_details($user_id);
      //$result['details']=$this->LoginSignup_model->get_user_details($user_id);
      $result['user']=$this->Interviewer_model->get_student_details($user_id);
      $country_lists = $this->Common_model->getDataByOrder($table="country",$fname="country_name");
      $result['country']= array_reverse($country_lists);
      $id=($this->session->userdata['logged_in']['serial_number']);
      $result['details']=$this->LoginSignup_model->get_user_details($id);
      $result['title']='edit-student-profile';
        
        //$user_id=($this->session->userdata['logged_in']['user_id']);
      
        //  echo '<pre>';print_r($result); echo '</pre>';exit();
       $this->load->view('edit_student_profile',$result);
         
         } else{
          
           redirect(base_url());
         }
   

  }  

  public function update_profile()

    {
           $pic=$this->input->post('picture');
             //echo '<br> profile is '.$new_profile=$this->input->post('img1'); exit();
             if (strlen($pic)>300){
              define('UPLOAD_DIR', './uploads/profile/tutor/');
              $pic = str_replace('data:image/png;base64,', '', $pic);
              $pic = str_replace(' ', '+', $pic);
              $data = base64_decode($pic);
              $path='/uploads/profile/tutor/';
              $file = UPLOAD_DIR. uniqid() . '.png';
              
              $success = file_put_contents($file, $data);
               $new_profile = substr($file, 1);

              
            }
            else{
              $new_profile=$this->input->post('img1');
             
            }
              
           
          $date = date('Y-m-d H:i:s', time());
          $user_id=$this->input->post('user_id'); 
          $image="";

          $data = array('first_name' =>$this->input->post('first_name') ,
                         'phone_number'=>$this->input->post('phone_number'),
                         'profile_image'=>$new_profile,
                         'user_id'=>$user_id,
                        
                       );
            $data2 = array(
                         /*'gender' =>$this->input->post('gender') ,
                         'date_of_birth'=>$this->input->post('dob'),
                          'about_us'=>$this->input->post('about_us'),*/

                         'country_id'=>$this->input->post('country'),
                        
                         'university_id'=>$this->input->post('university'),
                         'college_id'=>$this->input->post('college'),
                         'course'=>$this->input->post('course'),
                         'course_year'=>$this->input->post('course_year'),
                         'last_updated_date'=>$date,
                         'user_id'=>$user_id,
                         'profile_status'=>'complete',
                         'desire_university'=>$this->input->post('desire_university'),
                          'desire_college'=>$this->input->post('desire_college'),
                          'desire_course'=>$this->input->post('desire_course'),
                       );
            //echo '<pre>';print_r($data); echo '</pre>';
          //echo  '<pre>';print_r($data2); echo '</pre>';exit();
           $result=$this->Common_model->updateData($table='users',$fname='user_id',$user_id,$data);
           $result=$this->Common_model->updateData($table='student_information',$fname='user_id',$user_id,$data2);
           $this->session->set_flashdata('success',  array('message' => 'Your profile has been updated'));
             redirect(base_url().'student/edit_profile');
          
    }

 public function bookings() // student Bookings

  {

    if (isset($this->session->userdata['logged_in'])){
        $user_id=($this->session->userdata['logged_in']['user_id']);
         $status=$this->Interviewer_model->get_student_profile_status($user_id);
         if($status=='incomplete'){

          redirect(base_url().'student/edit_profile/'.$user_id);

         }else{
          $result['user']=$this->Interviewer_model->get_student_details($user_id);
          $id=($this->session->userdata['logged_in']['serial_number']);
          $result['details']=$this->LoginSignup_model->get_user_details($id);
          $result['appointment']=$this->Interview_schedule_model->student_appointments($user_id);
          $result['title']='student-booking';
            //   echo '<pre>';print_r($result); echo '</pre>';exit();
           $this->load->view('student_bookings',$result);
        }   
         
    } else{
          
           redirect(base_url());
         }
   

  }  

   public function ratings() // Student ratings 

  {

    if (isset($this->session->userdata['logged_in'])){
      $user_id=($this->session->userdata['logged_in']['user_id']);
        $status=$this->Interviewer_model->get_student_profile_status($user_id);
        if($status=='incomplete'){

          redirect(base_url().'student/edit_profile/'.$user_id);

        }else{

      $result['user']=$this->Interviewer_model->get_student_details($user_id);
      // $result['reviews']=$this->Interviewer_model->studentAllReview($user_id);
      $result['video']=$this->Twilio_model->get_edit_video_list($user_id);             
      $result['feedback']=$this->Twilio_model->student_previous_feedback($user_id);
      $result['title']='student-rating';
        //   echo '<pre>';print_r($result); echo '</pre>';exit();
       $this->load->view('student_rating',$result);
    }   
         
    } else{
          
           redirect(base_url());
         }
   

  } 


   public function send_mail_interviewer($interviewer_id)

    {
      $result = $this->db->get_where('users' , array('user_id' => $interviewer_id))->result_array();
     $email_address=$result[0]['email_address'];
      $user_name=$result[0]['first_name'];
     //$email_address='devendra@coretechies.com';
     
         $msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Dear  '.ucwords($user_name).'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">We have been notified that you are currently meant to be in a scheduled interview.</p>
             <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Please ensure that you are there within the next 5 minutes to avoid having to pay the full £50 cost of the interview..</p>
            ';
            $subject=' Urgent: Interview Now';
                $data['msg']=$msg;
                //$message = $this->load->view('mail/email_template',$data,true);
            $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
             $result=send_user_mail($email_address,$message,$subject);
              echo 'Mail sent to interviewer';
      
    }

   public function send_mail_connection_issues()

    {
      $new_message= $_POST['message'];

      //echo 'interviewer_id is '.$interviewer_id.'and message is '.$new_message; exit();
      $result = $this->db->get_where('users' , array('user_id' => $_POST['interviewer_connect']))->result_array();
     $email_address=$result[0]['email_address'];
      
      $user_name=$result[0]['first_name'];
     //$email_address='devendra@coretechies.com';
     
         $msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Dear  '.ucwords($user_name).'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">We have been notified that you are currently meant to be in a scheduled interview.</p>
             <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Your applicant has sent the following message:</p>
             <p style="font-size: 25px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif; font-style: italic;">"'."$new_message".'"</p>
            ';
             
            $subject='Trouble to connect session';
                $data['msg']=$msg;
                //$message = $this->load->view('mail/email_template',$data,true);
            $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
             $result=send_user_mail($email_address,$message,$subject);
              echo 'Message sent to interviewer';
      
    }


   public function previous_feedback($review_id)
    {
      //$data = [];
      //$data['feedback']= $this->db->get_where('reviews' , array('review_id' => $review_id))->result_array();
       $data['feedback']=$this->Interviewer_model->student_feedback_detail($review_id);
     //echo '<pre>';print_r($data); echo '</pre>'; exit();
      //load the view and saved it into $html variable
      //$html=$this->load->view('feedback_pdf', $data, true);
       $html=$this->load->view('feedback_pdf', $data,true);

      //this the the PDF filename that user will get to download
      $pdfFilePath = "interviewer-feedback.pdf";

      //load mPDF library
      $this->load->library('m_pdf');

      //generate the PDF from the given html
      $this->m_pdf->pdf->WriteHTML($html);

      //download it.
      $this->m_pdf->pdf->Output($pdfFilePath, "D");   
    }      

  


	

    
    

}
