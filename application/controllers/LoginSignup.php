<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginSignup extends CI_Controller 
{
	function __construct() 
      {
       parent::__construct();
        $this->load->model('LoginSignup_model');
         $this->load->model('Interviewer_model');
        $this->load->model('Common_model');
        
        $this->load->library('session');      
      }
    /*-----  Login page ------- */
	public function index()
	
		{
			if (isset($this->session->userdata['logged_in'])){
          	redirect(base_url().'index.php/metheone');
         	} 
         	else{
         	$this->load->view('login');
         	}
		}

     /*----   User Login  ------*/
	public function userlogin()
    	{ 
    		//echo 'welcome to login '; exit();
	        $password=$this->input->post('password'); 
	        $email=$this->input->post('email');  
	        //exit();
		    $data= (object)array(
		      'email'=>$this->input->post('email'),
		      'password'=> md5($password),
		              );
		    $result=$this->LoginSignup_model->check_user_login($data);
		    if($result==true){ 
			      $email=$this->input->post('email');
			      $user_info=$this->LoginSignup_model->user_info($email);
			      //echo '<pre>';print_r($user_info); echo '</pre>';exit();
			     $id=$user_info[0]->serial_number;
			      $status=$user_info[0]->status;
			     
			     
			        if($status==1){
			      	    //$results=$this->LoginSignup_model->UserLastlogin($id);
			      	    
			      	    /*if(count($results)<1){
			      	    	$time=$date = date('Y-m-d H:i:s', time());
			      	    }else{ $time= $results[0]->login_time;}*/
			      	    $user_name=$user_info[0]->first_name.' '.$user_info[0]->last_name;
			        	$session_data = array(
				          'serial_number'=>$user_info[0]->serial_number,
				          'user_id'=>$user_info[0]->user_id,
				          'user_name' => $user_name,
				          'phone_number' => $user_info[0]->phone_number,
				          'email_address' => $user_info[0]->email_address,
				          'user_role_id' => $user_info[0]->user_role_id, 
				          'profile_image'=>$user_info[0]->profile_image,
				          'last_login_date'  =>$user_info[0]->last_login_date,    
				          'login' => true
				          );
			             //print_r($session_data); exit();
				        $this->session->set_userdata('logged_in',$session_data);
				        echo 1;exit(); 
				      	}else{
				      		echo 2; exit();
				      	}
				}else{
				      echo 0; exit();
				    }
    	}

    public function student_signup() // Student signup
    	{ 
    		//echo 'welcome to login '; exit();
    		$date = date('Y-m-d H:i:s', time());
    		 $user_id=custom_random($digit=15);
	         $password=md5($this->input->post('password')); 
	          $email=$this->input->post('email_address');
	          $user_name=$this->input->post('user_name');   
	       
		    $result=$this->Common_model->check_duplicate($table='users',$fname='email_address',$email);
		    if($result){
		    	    echo 0; exit();

			      
			}else{
				    $data= array('first_name' =>$user_name,'email_address'=>$email,'password'=>$password,'user_id'=>$user_id,'user_role_id'=>3,'created_date'=>$date);  
				    //print_r($data); exit();
				    $result=$this->Common_model->insertdata($table='users',$data);

				     //$subject="Verify account";
				    //$message = 'Hi  ' .$user_name.'<br> Please verify your account    <a href="'.base_url().'loginSignup/verify_account/'.$user_id.'"> <b>Click here </b></a>';
                     //$url='https://167.99.231.76/loginSignup/verify_account/';
                      //$url='https://localhost/oxbridge/loginSignup/verify_account/';
                      $msg='<h4 style="color: #2aaebf !important; font-size: 18px">
                                                                Congratulations!
                                                            </h4>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Welcome to Whetstone Oxbridge.</p>

            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Please<a href="https://whetstone-oxbridge.com/loginSignup/verify_account/'.$user_id.'"> <b>Click here </b></a>to fill your profile information before you can browse and book Whetstone Oxbridge practice interviews.</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">If you have any questions please do not hesitate to <a href="https://whetstone-oxbridge.com/#contact"><b>Contact us</b></a>. </p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">We hope you find the platform useful and rewarding.</p>';

				    echo 1; 

				    $subject='Welcome to Whetstone Oxbridge.';
          			//$mail_data= array('user_name' =>$user_name,'user_id'=>$user_id );
          			$data['msg']=$msg;
          			//$message = $this->load->view('mail/email_template',$data,true);
				    $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
				     $result=send_user_mail($email,$message,$subject);
				    exit();		
		    }
				/*}else{
				      echo 0; exit();
				    }*/
    	}

     public function verify_account($id)	

	    {
	    	$date = date('Y-m-d H:i:s', time());
	    	$data=$arrayName = array('user_id' => $id,'status'=>1 );
	    	$data2=$arrayName = array('user_id' => $id,'account_status'=>'verified','profile_status'=>'incomplete','account_verification_date'=>$date);
	    	//$this->Common_model->insertData($table='student_information',$data2)
	    	$result=$this->Common_model->check_duplicate($table='student_information',$fname='user_id',$id);
	    	if(!$result){
	    		$this->db->insert($table='student_information',$data2);
	    	 }
	    	$data_result=$this->Common_model->updateData($table='users',$fname='user_id',$id,$data);
	    	//$this->db->insert($table='student_information',$data2);
	    	// if($data_result)
	    	// {
	    	// 	 redirect(base_url());
	    	// }
	    	$this->auto_login($id);

	    }


    public function forgot_password()

    	{ 
	         $email=$this->input->post('email_address');
		    $result=$this->Common_model->check_duplicate($table='users',$fname='email_address',$email);
		    if($result){
		    	$result=$this->Common_model->getDataByID($table='users',$fname='email_address',$email);
		    	 $user_name=$result[0]->first_name.' '.$result[0]->last_name;
		    	 $password=$result[0]->password; //exit();
		    	$new_password=randomPassword();
		    	$secure_password=md5($new_password);
		    	$data = array('email_address' =>$email,'password'=>$secure_password );
		    	$this->db->where('email_address', $email);
                $this->db->update('users', $data);
				
				$msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Dear  '.ucwords($user_name).'</p>
            			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Your account login details here.</p>
            			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Email Id: '.$email.'</p>
            			<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Password: '.$new_password.'</p>';
				$subject="Oxbridge account login password";
				     //echo $message; exit();
				    //verifyMail($email,$message);
			    echo 1; 
			   // send_mail($email,$message,$subject);
			    $data['msg']=$msg;
          		$message = $this->load->view('mail/oxbridge_email_signature',$data,true);
				$result=send_user_mail($email,$message,$subject);
				exit();  
			}else{
				echo 0; exit();
				    	
		    }
				
    	}

  	/*-----  user Logout ---------- */
	public function logout()
	    {
	    	
	      if(isset($this->session->userdata['logged_in']) )
	      {
	      	 	       $session_data = array(
	                      'serial_number' => '',
	                      'user_name' => '',
	                      'user_id' => '',
	                      'phone_number'=>'',
	                      'email' => '',
	                      'user_role_id' => '',
	                      'login' => false
	                        );	       
	        session_destroy();
	        $this->session->unset_userdata('logged_in',$session_data);
	        redirect(base_url());
	        //echo 'thanks for visting You have logut successfully'; exit();
	      }
	    }
	public function  change_password()  
	{
		if (isset($this->session->userdata['logged_in'])){
			 $id=($this->session->userdata['logged_in']['user_id']);
			 $role_id=($this->session->userdata['logged_in']['user_role_id']);
			if($role_id==2){
	        	//$data['details']=$this->Interviewer_model->get_interviewer_details($id);
	        	 $data['details']=$this->Interviewer_model->get_interviewer_dashbaord($id);
	        	$data['title']='interviewer-change-password';
				$this->load->view('change_password',$data);
			}else{

				$status=$this->Interviewer_model->get_student_profile_status($id);
				 if($status=='incomplete'){

				 	redirect(base_url().'student/edit_profile/'.$user_id);

				}else{

				 $data['user']=$this->Interviewer_model->get_student_details($id);
				 //echo '<pre>';print_r($data); echo '</pre>';exit();
	        	 $data['title']='student-change-password';
				 $this->load->view('student_change_password',$data);
				} 
			}
				
        }else{
         	
         	 redirect(base_url());
         }
	} 
	public function update_password()

    { 
      if (isset($this->session->userdata['logged_in'])){
          $email=($this->session->userdata['logged_in']['email_address']);
        }
          $password=$this->input->post('old_password'); 
          $newpassword=$this->input->post('new_password');
          $p=md5($password);
          $q=md5($newpassword);
          // echo 'user email is '.$email.' old password is     '.$p. 'and new password is '.$q;exit();
          $data= (object)array(
          'email'=>$email,
          'password'=> md5($password),
              );
          $result=$this->LoginSignup_model->check_password($data);
          //echo 'hiii result id '.$result; exit();

      if($result==1){ 
      	//echo 'welcome to update password'; exit();
          $data = array('email_address' =>$email ,
          'password'=> md5($newpassword) );
          $result=$this->Common_model->updateData($table='users',$fname='email_address',$email,$data);
          if($result){
             $message = ' Your account password updated successfully';
           $this->session->set_flashdata('success',  array('message' => $message));
             redirect(base_url().'change_password'); 
          }
        }else{
        		//echo 'old password not match'; exit();
           $message = ucwords($user_name).' Old password does not match';
           $this->session->set_flashdata('error',  array('message' => $message));
             redirect(base_url().'change_password');
        }
    } 
	public function change_passwords()

    	{ 
	        $email=$this->input->post('email_address');
		    $result=$this->Common_model->check_duplicate($table='users',$fname='email_address',$email);
		    if($result){
		    	$result=$this->Common_model->getDataByID($table='users',$fname='email_address',$email);
		    	$user_name=$result[0]->first_name.' '.$result[0]->last_name;
		    	$password=$result[0]->password;

				$msg = 'Hi  ' .$user_name.'<br> Your account login Id: '.$email.'<b>Password: '.$password;
				$subject="Oxbridge account login password";
				     //echo $message; exit();
				    //verifyMail($email,$message);
			    echo 1; 

			    $data['msg']=$msg;
          		$message = $this->load->view('mail/oxbridge_email_signature',$data,true);
				$result=send_user_mail($email,$message,$subject);
				exit(); 
			   /* send_mail($email,$message,$subject);
			    exit();	*/  
			}else{
				echo 0; exit();
				    	
		    }
				/*}else{
				      echo 0; exit();
				    }*/
    	}    

	public function userlist()
	{
		$result=$this->LoginSignup_model->registerUserList();
		echo '<pre>'; print_r($result); echo '</pre>'; exit();
	}

	/* code by ravindra start here */

	public function auto_login($id){
		$user_info=$this->LoginSignup_model->user_info_by_id($id);
		$status=$user_info[0]->status;
		if($status==1){
			$user_name=$user_info[0]->first_name.' '.$user_info[0]->last_name;
			$session_data = array(
			'serial_number'=>$user_info[0]->serial_number,
			'user_id'=>$user_info[0]->user_id,
			'user_name' => $user_name,
			'phone_number' => $user_info[0]->phone_number,
			'email_address' => $user_info[0]->email_address,
			'user_role_id' => $user_info[0]->user_role_id, 
			'profile_image'=>$user_info[0]->profile_image,
			'last_login_date'  =>$user_info[0]->last_login_date,    
			'login' => true
		);
		$this->session->set_userdata('logged_in',$session_data);
		 	redirect(base_url().'student/edit_profile/'.$id);
		}else{
			redirect(base_url());
		}
	}   

}
