<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interviewer_schedule extends CI_Controller {

	function __construct() 

      {
       parent::__construct();
        $this->load->model('Interview_schedule_model');
        $this->load->model('Interviewer_model');
        $this->load->model('Common_model');      
      }

   public function index() // send request for become tutor

    {  
      if (isset($this->session->userdata['logged_in'])){
         $id=($this->session->userdata['logged_in']['user_id']);
         $data['title']='calendar';
         //$data['user']=$this->Interviewer_model->get_interviewer_details($id);  
         $data['details']=$this->Interviewer_model->get_interviewer_dashbaord($id);
        $this->load->view('events_see',$data);
    }else
     redirect(base_url()); 
    }


function get_available_data_ajax()
    {         
            
date_default_timezone_set("Asia/Calcutta");
            $where_booked = array('interviewer_id' => $this->input->get('interviewer_id'), 'booking_date >=' =>date('Y-m-d', strtotime("+1 day")));
            //$order_by = 'session_start_time asc';
            $booked_session = $this->Common_model->get_listing_details('appointment', '', $where_booked);
            //var_dump($booked_session);die;
            if ($booked_session) {
                foreach ($booked_session as $row_booked) {
                    
                    $chunks = explode(' ', $row_booked->start_time);
                    $chunks1 = explode(' ', $row_booked->end_time);
                     
                    $start_time = $row_booked->booking_date.' '.date("H:i:s", strtotime($chunks[0]));
                    $end_full_time = $row_booked->booking_date.' '.date("H:i:s", strtotime($chunks1[0]));
                  

                    //var_dump($start_time,$end_full_time);die;


                    $end_time = $row_booked->end_time;
                    $end_time = date("H:i:s", strtotime($end_time));
                    
                    
                    $datetime1 = new DateTime($start_time);
                    $start_full=$datetime1->format(DateTime::ATOM);

                    $datetime2 = new DateTime($end_full_time);
                    $end_full=$datetime2->format(DateTime::ATOM);


                    $result_data[] = array(
                        "title" => 'booked'.$end_time,
                        //"url"=>base_url().'event/event-info/'.$raw->event_id,
                        "start" => $start_full,
                        "end" => $end_full,
                        "appointment_date" => $row_booked->booking_date,
                        "available_start" => '',
                        "available_end" => '',
                        "cls" => 'open', // optional
                        "color" => '#e0003c', // optional
                        "background" => '#e0003c' // optional
                        //"location"=>$raw->event_location,
                        //"description"=>$raw->event_description,
                    );
                }
            }

        $where_condition = array('user_id' => $this->input->get('interviewer_id'));
        $appointment_listing = $this->db->select('*')->where($where_condition)->get('interviewer_availibility')->result();
        //var_dump($appointment_listing);
        
       if ($appointment_listing)
        {
            
            foreach ($appointment_listing as $raw) {
               
                $start_time=$raw->start_time;
                $end_time=$raw->end_time;
                $available_start = date("H:i", strtotime($start_time));
                $available_end = date("H:i", strtotime($end_time));
                    $result_data[] = array(
                        "title" => 'Available',
                        "url"=>'',
                        "id"=>$raw->schedule_id,
                        "start" => $raw->start_full,
                        "end" => $raw->end_full,
                        "appointment_date" => $raw->schedule_date,
                        "available_start" => $available_start,
                        "available_end" => $available_end,
                        "cls" => '', // optional
                        "color" => '', // optional
                        "background" => '' // optional
                        );
                
            }
            echo json_encode($result_data);
        } else {

        }
    }

function add_session_availablety_ajax()
    {
        //var_dump($_POST);die;
      if ($this->input->post('end') == "00:00:00") {
        $end_time_insert = "23:59:59";
      } else {
       $end_time_insert = $this->input->post('end');
      }
        $insert_data=array(
            'user_id'=>$this->session->userdata['logged_in']['user_id'],
            'schedule_date'=>$this->input->post('date'),
            'start_time'=>$this->input->post('start'),
            'end_time'=>$end_time_insert,
            'start_full'=>$this->input->post('start_full'),
            'end_full'=>$this->input->post('end_full'),
            
        );

        $insert_data = $this->db->insert('interviewer_availibility',$insert_data);
        if($insert_data)
        {
            $json_data = array($insert_data);
        }

        echo json_encode($json_data);
    }

    function update_session_availablety_ajax()
    {
       //var_dump($_POST);die;
        $where=array('schedule_id'=>$this->input->post('id'));
        $insert_data=array(
            'schedule_date'=>$this->input->post('date'),
            'start_time'=>$this->input->post('start'),
            'end_time'=>$this->input->post('end'),
            'start_full'=>$this->input->post('start_full'),
            'end_full'=>$this->input->post('end_full'),
            
        );

        $insert_data = $this->db->update('interviewer_availibility',$insert_data,$where);
    }

    function delete_session_availablety_ajax()
    {
        $where=array('schedule_id'=>$this->input->post('id'));

         $this->db->delete('interviewer_availibility', $where);
$response = array(
                    'status' => true,
                    'msg'=>'',
                );
echo json_encode($response);die;
        
    }

function get_time_input()
    {
        //var_dump($_POST);die;
        $return_message = array(
            'status' => false,
            'response' => ''
        );
        
        $package = $this->input->post('package');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $vander_id = $this->input->post('vander_id');
        $appointment_date = $this->input->post('appointment_date');
        $appointment_end_date = $this->input->post('appointment_end_date');
        $session_time = $this->input->post('session_time');

        //get appointment_date-> start & end time
        $order_by = '';


                
        //var_dump($_POST,$avilable_date,$appointment_date);
        $times[] = create_time_range($start, $end, $package, '', $vander_id, $appointment_date,'','',$appointment_end_date);
        $return_message = array(
            'status' => true,
            'response' => $times
        );
        echo json_encode($return_message);
       die;
    }

    function check_time_already_booked()
    {   //var_dump($_POST);die;
        $return_message = array(
            'status' => false,
            'response' => ''
        );
        $package = $this->input->post('package');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $vander_id = $this->input->post('vander_id');
        $appointment_date = $this->input->post('appointment_date');
        $astrologer_data = $this->Common_model->check_time_already_booked($start, $end, $vander_id, $appointment_date);
        if ($astrologer_data) {
            if ($astrologer_data == 1) {
                $return_message = array(
                    'status' => true,
                    'response' => ''
                );
            } else {
                $return_message = array(
                    'status' => false,
                    'response' => ''
                );
            }
        } else {
            $return_message = array(
                'status' => false,
                'response' => ''
            );
        }

        echo json_encode($return_message);
    }
    function add_appointment()
    {   $this->session->unset_userdata('purchase_plan');
        //var_dump($_POST);die;
        $return_message = array(
            'status' => false,
            'response' => ''
        );
       
        $astrologer_data = $this->Common_model->check_time_already_booked($this->input->post('session_start_time'), $this->input->post('session_end_time'), $this->input->post('vendor_id'), $this->input->post('appointment_date'));
        if ($astrologer_data) {
            if ($astrologer_data == 1) {
                $return_message = array(
                    'status' => false,
                    'response' => 'Sorry, Session Already Booked'
                );
                header('Content-Type: application/json');
                echo json_encode($return_message);
                die;
            }
        }
        $new_name = time();
       
        $attachment='';
        // load Upload library
        $config = array(
            'upload_path' => './uploads/attachment/',
            'allowed_types' => 'jpg|jpeg|png|pdf|doc|docx',
            'max_size' => '9898982048',
            'multi' => 'all',
            'file_name' => $new_name
        );
        $this->load->library('upload');
      $this->upload->initialize($config);
        if($this->upload->do_upload("upload_file")){
        $data = array('upload_data' => $this->upload->data());
 
        $attachment = $data['upload_data']['file_name'];
        }
        if (!$this->upload->do_upload('upload_file')) {
        // var_dump($this->upload->display_errors());die;
           

        }

                 $plan_session_array = array(
                'student_id' => $this->input->post('from_profile_id'),
                'package' => $this->input->post('astro_pack'),
                'interviewer_id'=>$this->input->post('vendor_id'),
                'amount'=> $this->Interviewer_model->package_price(),
                'attachment'=>$attachment,
                'session_start_time'=>$this->input->post('time_start'),
                'session_end_time'=>$this->input->post('time_end'),
                'appointment_date'=>date('Y-m-d',strtotime($this->input->post('appointment_date'))),
                'message'=>$this->input->post('appointment_message')
            );
            //var_dump($this->input->post('appointment_session_date1'),date('Y-m-d',strtotime($this->input->post('appointment_session_date1'))));die;
            $this->session->set_userdata('purchase_plan', $plan_session_array);
            // print_r($plan_session_array);
            // die();
            $response_message = array(
                'status' => true,
                "response" => 'Request Accepted Successfully.',
                "redirect_url" => base_url() . 'book-now/'.$this->input->post('vendor_id')
            );
            header('Content-Type: application/json');
            echo json_encode($response_message);
            die;
      
    }

    public function add_schedule() // Set time for Interviewer avalibality 
    {
      if (isset($this->session->userdata['logged_in'])){
          $user_id=($this->session->userdata['logged_in']['user_id']);
          $schedule_date=$this->input->post('schedule_date');
          $result=$this->Interview_schedule_model->check_schedule_availbility($user_id,$schedule_date);
          if($result)
          {
            echo 0; exit();
          }
          else{
              $date = date('Y-m-d H:i:s', time());
              $start=$this->input->post('start_time');
              $end=$this->input->post('end_time');
             /* $start_time = date("Y-m-d H:i", strtotime($start));
              $end_time = date("Y-m-d H:i", strtotime($end));*/
               $start_time = strtotime($start);
              $end_time = strtotime($end);
              
              $data = array('start_time' => $start_time,
                  'user_id' => $user_id,
                  'end_time'=>$end_time,
                  'schedule_date'=>$this->input->post('schedule_date'), 
                  'created_date'=>$date,
                  'status'=>'availible',
                  );  
              //echo '<pre>';print_r($data);  echo '</pre>';exit();
              $result=$this->db->insert('interviewer_availibility', $data);
             if($result)
              echo 1; exit();

              }
        }else{
           redirect(base_url());
        }   
    }

    public function bookings() // send request for become tutor

    {
      if (isset($this->session->userdata['logged_in'])){
         $id=($this->session->userdata['logged_in']['user_id']);
         $role_id=($this->session->userdata['logged_in']['user_role_id']);
          if($role_id==2){
           $user="interviewer_id";
           $data['booking']=$this->Interview_schedule_model->student_appointment_booking($id,$user); 
          }else{
             $user="student_id";
          $data['booking']=$this->Interview_schedule_model->student_appointment_booking($id,$user); 
         }
       //print_r($data); exit();
        $this->load->view('event_bookings',$data);
    }else{
     redirect(base_url()); 
    }
    }
      //$this->load->view('event_bookings');
    //}
    public function availibility_list()
      {
       // $user_id='8819761956973354';
        if (isset($this->session->userdata['logged_in']))
            $user_id=($this->session->userdata['logged_in']['user_id']);
        $result=$this->Interview_schedule_model->get_schedule($user_id);
        //print_r($result); exit();
        $data_events = array();

       foreach($result as $r) {
          // $title=$r->start_time.' - '.$r->end_time;
          $status=$r->status;
          $start=$r->start_time;
          $end=$r->end_time;
          $start_time = date("h:iA", $start);
          $end_time = date("h:iA", $end);
              $title=$start_time.' - '.$end_time;
           if($status=='availible')$color='green'; else $color='red';
           $data_events[] = array(
                "schedule_id"=>$r->schedule_id,
               "title" => $title,
               "description" => 'Available',
               "start" => $r->schedule_date,
               "backgroundColor"=>$color,
           );   
       }

       echo json_encode(array("events" => $data_events));
       exit();
       //'date'=>$this->input->post('appointmnet_date'),
      } 

    public function slot_listById($id)

      {
       // $id="8819761956973354";
        $result=$this->Interview_schedule_model->get_schedule($id);
        $data_events = array();
        foreach($result as $r) {
           $status=$r->status;
          $start=$r->start_time;
          $end=$r->end_time;
          $start_time = date("h:iA", $start);
          $end_time = date("h:iA", $end);
              $title=$start_time.' - '.$end_time;
           if($status=='availible')$color='green'; else $color='red';
           $data_events[] = array(
               "schedule_id"=>$r->schedule_id,
               "title" => $title,
               "description" => 'Available',
               "start" => $r->schedule_date,
               "backgroundColor"=>$color,
           );  
        }
        echo json_encode(array("events" => $data_events));
        exit();
       // echo '<pre>'; print_r($data_events); echo '</pre>';exit();
      } 

    public function schedule_time() //get time slot when user select package

      {
      
        $slot=$this->input->post('slot_time');
        $id =$_POST['schedule_id'];
        //$id="29";
        //$slot=30;
        $result=$this->Interview_schedule_model->get_schedule_timer($id);
       // print_r($result); 
        $start_time=$result[0]->start_time;
        $end_time=$result[0]->end_time;

         $difference = abs($end_time - $start_time)/$slot;
         $total=$difference/60;
          $list=round($total, 0);
  
        //echo '<br>';
           $value=0;
         for($i=0;$i<$list;$i++)
         {

          if($i==0){
            $time_int=0;
           $time = date("h:i A", strtotime('+'.$time_int.' minute', $start_time));
           echo '<option value="' . $time . '">' . $time . '</option>';
            $start_time = strtotime($time);
          }
          else{
            $time = date("h:i A", strtotime('+'.$slot.' minute', $start_time));
           echo '<option value="' . $time . '">' . $time . '</option>';
            $start_time = strtotime($time);
            
          }
          //$starttimestamp=$time;
         }
        
       // exit();
       // echo '<pre>'; print_r($data_events); echo '</pre>';exit();
      } 

    public function end_schedule_time() 
      {

         $start_time =$_POST['slot_id'];
       $package =$_POST['package']; 
       // $start_time="7:30";
        //$package=30;
        $starttimestamp = strtotime($start_time);
         $time = date("h:i A", strtotime('+'.$package.' minute', $starttimestamp));
        echo $time; exit();
        //echo '<option value="' . $time . '">' . $time . '</option>';

      } 
        
        
 
    

}
