<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

	function __construct() 

      {
       parent::__construct();
        $this->load->model('Interview_schedule_model');
        $this->load->model('Common_model');
         $this->load->model('Appointment_model');  
         
        //die;    
      }


  public function paynow()
    {   

        //$data['id']=$id;

       /* if($this->input->post('submit')){*/
        
        $invoice_id=time();  
       $interviewer_id= $this->input->post('interviewer_id');      
        $customer_data = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'start'=>$this->input->post('start'),
            'end'=>$this->input->post('end'),
            'booking_date'=>$this->input->post('appointment_date'),
            'payment_status' => 'N',
            'invoice_id' => 'INV-' . $invoice_id,
            'payment_date' => date('Y-m-d'),

            

        );
        $date = date('Y-m-d H:i:s', time());
        $appoint_data= array(
              'interviewer_id'=>$this->input->post('interviewer_id'),
             'student_id' => $this->session->userdata['logged_in']['user_id'],
             'schedule_id'=>$this->input->post('schedule_id'),
            'booking_date'=>$this->input->post('appointment_date'),
            'start_time'=>$this->input->post('start_time'),
            'end_time'=>$this->input->post('end_time'),
            'created_date'=>$date, );
        //echo '<pre>'; print_r($customer_data); echo '</pre>';exit();
        $payment_id = $this->db->insert('appointment', $appoint_data);
        $payment_id = $this->db->insert('payment', $customer_data);
         $message ='Your appointment booked successfully';
           $this->session->set_flashdata('success',  array('message' => $message));
        redirect(base_url().'interviewer/profile/'.$interviewer_id);
       /* $plan_session_array = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'currency'=>'USD',
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'payment_status' => 'N',
            'invoice_id' => 'INV-' . $invoice_id,
            'payment_date' => date('Y-m-d'),
        );
        $this->session->set_userdata('checkout_session', $plan_session_array);

        redirect(base_url().'checkout');
     // }*/

      $this->load->view('book_now',$data);


    }

     
    public function pay($id=null)
    {   

        $data['id']=$id;

        if($this->input->post('submit')){
        $invoice_id=time();        
        $customer_data = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'payment_status' => 'N',
            'invoice_id' => $invoice_id,

            'payment_date' => date('Y-m-d'),
        );
        $payment_id = $this->db->insert('payment', $customer_data);
        $plan_session_array = array(
            'user_id' => $this->session->userdata['logged_in']['user_id'],
            'amount' => $this->input->post('amount'),
            'currency'=>'GBP',
            'interviewer_id'=>$this->input->post('interviewer_id'),
            'payment_status' => 'N',
            'start'=>$this->input->post('start'),
            'end'=>$this->input->post('end'),
            'booking_date'=>$this->input->post('booking_date'),
            'message'=>$this->input->post('message'),
             'room_name'=>$this->input->post('room_name'),
            'invoice_id' => $invoice_id,
            'payment_date' => date('Y-m-d'),
        );
        $this->session->set_userdata('checkout_session', $plan_session_array);
        /*********** dont go to payment gateway if amount is zero start******************/
        if(!$this->input->post('amount')){
          $invoice_id = $this->session->userdata('checkout_session')['invoice_id'];
          $amount=round($this->session->userdata('checkout_session')['amount']);
          $amount_cents = $amount*100;// Chargeble amount
          $invoiceid = $invoice_id;// Invoice ID
          $description = "Invoice #" . $invoiceid;
          $invoice_no = $this->session->userdata('checkout_session')['invoice_id'];

          $where = array('invoice_id' => $invoice_no);
          $date = date('Y-m-d H:i:s', time());
          $appoint_data= array(
                'interviewer_id'=>$this->session->userdata('checkout_session')['interviewer_id'],
                'payment_id'=>$this->session->userdata('checkout_session')['invoice_id'],
               'student_id' => $this->session->userdata['logged_in']['user_id'],
               'attachment'=>$this->session->userdata('purchase_plan')['attachment'],
              'booking_date'=>$this->session->userdata('checkout_session')['booking_date'],
              'start_time'=>$this->session->userdata('checkout_session')['start'],
              'end_time'=>$this->session->userdata('checkout_session')['end'],
              'message'=>$this->session->userdata('checkout_session')['message'],
              'room_name'=>$this->session->userdata('checkout_session')['room_name'],
              'created_date'=>$date, 
          );
          $payment_id = $this->db->insert('appointment', $appoint_data);
          $this->db->where($where);
          $this->db->update('payment', $where=array('payment_status'=>'Y'));
          $invoice_session_array = array(
              'booking_date'=>$this->session->userdata('checkout_session')['booking_date'],
              'amount'=>$this->session->userdata('checkout_session')['amount'],
              'main_amount'=>$this->session->userdata('purchase_plan')['amount'],
              'discount_coupon'=>$this->session->userdata('purchase_plan')['discount_coupon'],
              'discount_amount'=>$this->session->userdata('purchase_plan')['discount_amount'],
              'start'=>$this->session->userdata('checkout_session')['start'],
              'end'=>$this->session->userdata('checkout_session')['end'],
              'invoice_id' => $invoice_id,
              'payment_date' => date('Y-m-d'),
          );
          $applicant_name= $this->session->userdata['logged_in']['user_name'];
          $applicant_email= $this->session->userdata['logged_in']['email_address'];
          $schedule_date=$this->session->userdata('checkout_session')['booking_date'];
          $start_time=$this->session->userdata('checkout_session')['start'];
          $end_time=$this->session->userdata('checkout_session')['end'];
          $interviewer_id=$this->session->userdata('checkout_session')['interviewer_id'];
          $applicant_id= $this->session->userdata['logged_in']['user_id'];
          /* send notification to interviewer */
          $notification= array(
              'interviewer_id' =>$interviewer_id,
              'applicant_id' =>$applicant_id,
              'created_date' =>$date,
              'message'=>'You have received an appointment from',
               'notify_interviewer'=>'Yes',
               'notify_applicant'=>'No',
          );
          $result=$this->Common_model->insertdata($table='notification',$notification);
          $query=$this->db->select('first_name,email_address') ->from('users')->where('user_id',$interviewer_id)->get();
          $mail_details=$query->result();
          $interviewer_emails=$mail_details[0]->email_address;
          $interviewer_name=$mail_details[0]->first_name;
          $admin_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Thank you for booking a practice interview with details</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>';

              $Applicant_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Thank you for booking a practice interview with details</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">You will be reminded via email 1 hour and then 15 minutes before your scheduled time.</p>
              <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>Before your interview make sure you: </b><br>
              1.Are in a quiet place where you won’t be disturbed.<br>
              2.Have a decent internet connection.<br>
              3.Have re-read through your Personal Statement.<br>
              4.Have read through any feedback from previous Whetstone interviews
              </p>
              <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Please <a href="https://whetstone-oxbridge.com/uploads/Do-and-do-not .pdf">click the link</a> below to download our do&rsquo;s and don&rsquo;ts of Oxbridge interviews.
              </p>
             ';
            //$email2='info@whetstone.com';
            $admin_email='info@whetstone-oxbridge.com';
            $admin_subject=' New Appointment Booking ';
            $subject=' New Appointment Booking ';
            // $applicant_email='devendra@coretechies.com';
            $data_applicant['msg']=$Applicant_msg;
            $message = $this->load->view('mail/oxbridge_email_signature',$data_applicant,true);
            $result=send_user_mail($applicant_email,$message,$subject); // applicant mail
            $result=send_user_mail($interviewer_emails,$message,$subject); // Interviewer mail

            $data['msg']=$admin_msg;
            $message_admin = $this->load->view('mail/oxbridge_email_signature',$data,true);
            $result=send_user_mail($admin_email,$message_admin,$admin_subject); // admin mail
            $this->session->set_userdata('invoice_session',$invoice_session_array);
            $this->session->unset_userdata('checkout_session');
            $this->session->unset_userdata('purchase_plan');
            redirect(base_url().'checkout-success/1');
        }
        /*********** dont go to payment gateway if amount is zero  end******************/
        redirect(base_url().'checkout');
      }

      $this->load->view('book_now',$data);


    }



    public function checkout()
    {
        
       require_once(APPPATH.'libraries/Stripe.php');//or you
        
        $data=array();
        $params = array(
            "testmode"   => "on",
            "private_live_key" => "sk_live_xxxxxxxxxxxxxxxxxxxxx",
            "public_live_key"  => "pk_live_xxxxxxxxxxxxxxxxxxxxx",
            "private_test_key" => "sk_test_A8b1OqtauljKLjhZ1imBIJZA",
            "public_test_key"  => "pk_test_rkqUBNs86mfmdWZ7kMv6oui6"
        );

        if ($params['testmode'] == "on") {
            Stripe::setApiKey($params['private_test_key']);
            $pubkey = $params['public_test_key'];
        } else {
            Stripe::setApiKey($params['private_live_key']);
            $pubkey = $params['public_live_key'];
        }

        if(isset($_POST['stripeToken']))
        {

            if($this->session->userdata('checkout_session')=='')
            {
                redirect(base_url().'checkout-success/2');
            }

            $invoice_id = $this->session->userdata('checkout_session')['invoice_id'];

            $amount=round($this->session->userdata('checkout_session')['amount']);
            $amount_cents = $amount*100;// Chargeble amount
            $invoiceid = $invoice_id;// Invoice ID
            $description = "Invoice #" . $invoiceid;

            try {
                $charge = Stripe_Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => strtolower($this->session->userdata('checkout_session')['currency']),
                        "source" => $_POST['stripeToken'],
                        "description" => $description,
                        "metadata" => array("invoice_id" => $this->session->userdata('checkout_session')['invoice_id'])
                    )
                );

                $charge = $charge->__toArray(true);
                //file_put_contents('strip_responce.txt', serialize($charge));
                // print_r($charge);
                // die();

                if ($charge['source']['address_zip_check'] == "fail") {
                    throw new Exception("zip_check_invalid");
                } else if ($charge['source']['address_line1_check'] == "fail") {
                    throw new Exception("address_check_invalid");
                } else if ($charge['source']['cvc_check'] == "fail") {
                    throw new Exception("cvc_check_invalid");
                }
                // Payment has succeeded, no exceptions were thrown or otherwise caught

                $result = "success";

            } catch(Stripe_CardError $e) {

                $error = $e->getMessage();
                $result = $e->getMessage();

            } catch (Stripe_InvalidRequestError $e) {
                $result = $e->getMessage();
            } catch (Stripe_AuthenticationError $e) {
                $result = $e->getMessage();
            } catch (Stripe_ApiConnectionError $e) {
                $result = $e->getMessage();
            } catch (Stripe_Error $e) {
                $result = $e->getMessage();
            } catch (Exception $e) {

                if ($e->getMessage() == "zip_check_invalid") {
                    $result = $e->getMessage();
                } else if ($e->getMessage() == "address_check_invalid") {
                    $result = $e->getMessage();
                } else if ($e->getMessage() == "cvc_check_invalid") {
                    $result = $e->getMessage();
                } else {
                    $result = $e->getMessage();
                }
            }

            //var_dump($result);die;

            if($result=='success') {
                $invoice_no = $this->session->userdata('checkout_session')['invoice_id'];
                $where = array('invoice_id' => $invoice_no);

                $date = date('Y-m-d H:i:s', time());
        $appoint_data= array(
              'interviewer_id'=>$this->session->userdata('checkout_session')['interviewer_id'],
              'payment_id'=>$this->session->userdata('checkout_session')['invoice_id'],
             'student_id' => $this->session->userdata['logged_in']['user_id'],
             'attachment'=>$this->session->userdata('purchase_plan')['attachment'],
            'booking_date'=>$this->session->userdata('checkout_session')['booking_date'],
            'start_time'=>$this->session->userdata('checkout_session')['start'],
            'end_time'=>$this->session->userdata('checkout_session')['end'],
            'message'=>$this->session->userdata('checkout_session')['message'],
            'room_name'=>$this->session->userdata('checkout_session')['room_name'],
            'created_date'=>$date, );
        //echo '<pre>'; print_r($customer_data); echo '</pre>';exit();
        $payment_id = $this->db->insert('appointment', $appoint_data);
                
                //////////////////////////////update payment status//////////////////////////
                $this->db->where($where);
                $this->db->update('payment', $where=array('payment_status'=>'Y'));
              
                $invoice_session_array = array(
                    'booking_date'=>$this->session->userdata('checkout_session')['booking_date'],
                    'amount'=>$this->session->userdata('checkout_session')['amount'],
                    'main_amount'=>$this->session->userdata('purchase_plan')['amount'],
                    'discount_coupon'=>$this->session->userdata('purchase_plan')['discount_coupon'],
                    'discount_amount'=>$this->session->userdata('purchase_plan')['discount_amount'],
                    'start'=>$this->session->userdata('checkout_session')['start'],
                    'end'=>$this->session->userdata('checkout_session')['end'],
                    'invoice_id' => $invoice_id,
                    'payment_date' => date('Y-m-d'),
                );



                $applicant_name= $this->session->userdata['logged_in']['user_name'];
                $applicant_email= $this->session->userdata['logged_in']['email_address'];
                $schedule_date=$this->session->userdata('checkout_session')['booking_date'];
                $start_time=$this->session->userdata('checkout_session')['start'];
                $end_time=$this->session->userdata('checkout_session')['end'];
                $interviewer_id=$this->session->userdata('checkout_session')['interviewer_id'];
                 $applicant_id= $this->session->userdata['logged_in']['user_id'];
                 $room_name = $this->session->userdata('checkout_session')['room_name'];

                /* send notification to interviewer */
                  $notification= array(
                      'interviewer_id' =>$interviewer_id,
                      'applicant_id' =>$applicant_id,
                      'created_date' =>$date,
                      'message'=>'You have received an appointment from',
                       'notify_interviewer'=>'Yes',
                       'notify_applicant'=>'No',
                       );
                  $result=$this->Common_model->insertdata($table='notification',$notification);


                 $query=$this->db->select('first_name,email_address') ->from('users')->where('user_id',$interviewer_id)->get();
                 $mail_details=$query->result();
                 $interviewer_emails=$mail_details[0]->email_address;
                 $interviewer_name=$mail_details[0]->first_name;
                 $query1=$this->db->select('message,attachment') ->from('appointment')->where('room_name',$room_name)->get();
                 $mail_user_details=$query1->result();
                 $applicant_message = $mail_user_details[0]->message;
                 $applicant_attachment = $mail_user_details[0]->attachment;
                 if (empty($applicant_message)) {
                   $applicant_message = "Not mention";
                 }
                 if (empty($applicant_attachment)) {
                   $applicant_attachment1 = "Not provided";
                 } else {
                  $applicant_attachment1 = base_url() . '/uploads/attachment/' . $applicant_attachment;
                 }

                 $admin_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> An Oxbridge Applicant has just booked an interview with you </p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>';

              $Applicant_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Thank you for booking a practice interview with details</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">You will be reminded via email 1 hour and then 15 minutes before your scheduled time.</p>
              <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>Before your interview make sure you: </b><br>
              1.Are in a quiet place where you won’t be disturbed.<br>
              2.Have a decent internet connection.<br>
              3.Have re-read through your Personal Statement.<br>
              4.Have read through any feedback from previous Whetstone interviews
              </p>
              <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Please <a href="https://whetstone-oxbridge.com/uploads/Do-and-do-not .pdf">click the link</a> below to download our do&rsquo;s and don&rsquo;ts of Oxbridge interviews.
              </p>
             ';
             if (empty($applicant_attachment)) {
              $inter_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> An Oxbridge Applicant has just booked an interview with you </p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Message :</b> '.$applicant_message.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Attachment :</b> '.$applicant_attachment1.'</p>';
           } else {
             $inter_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> An Oxbridge Applicant has just booked an interview with you </p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Message :</b> '.$applicant_message.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Attachment :</b> <a href="'.$applicant_attachment1.'">Click to see attachment</a></p>';
           }

           //$email2='info@whetstone.com';
             $admin_email='info@whetstone-oxbridge.com';
             $admin_subject=' New Interview Booking ';
             $subject=' New Interview Booking ';
             // $applicant_email='devendra@coretechies.com';
                $data_applicant['msg']=$Applicant_msg;
                $message = $this->load->view('mail/oxbridge_email_signature',$data_applicant,true);
                $result=send_user_mail($applicant_email,$message,$subject); // applicant mail
                $data_interviewer['msg']=$inter_msg;
                $message = $this->load->view('mail/oxbridge_email_signature',$data_interviewer,true);
                $result=send_user_mail($interviewer_emails,$message,$subject); // Interviewer mail

                  $data['msg']=$admin_msg;
                 $message_admin = $this->load->view('mail/oxbridge_email_signature',$data,true);
                $result=send_user_mail($admin_email,$message_admin,$admin_subject); // admin mail



                $this->session->set_userdata('invoice_session',$invoice_session_array);
                $this->session->unset_userdata('checkout_session');
                $this->session->unset_userdata('purchase_plan');
                redirect(base_url().'checkout-success/1');
            }
            else
            {
                //$this->session->unset_userdata('checkout_session');
                //$this->session->unset_userdata('purchase_plan');
                redirect(base_url().'checkout-success/2');
            }


        }

        
        $this->load->view('checkout', $data);
        

    }
    public function checkout_success($id)
    {
      if($id==1){
         $data['msg']='Payment Successfully Paid.';
         $this->load->view('order_done',$data);
      }else{
         $data['msg']='Sorry, Error While Payment, Please Try Again';
         $this->load->view('order_fail',$data);
      }
      
    }
    public function book()
    {
      //echo $uniqueId= time().'-'.mt_rand(); exit();
      $date = date('Y-m-d H:i:s', time());
      if (isset($this->session->userdata['logged_in'])){
          $user_id=($this->session->userdata['logged_in']['user_id']);


          $result=$this->Interview_schedule_model->check_schedule_availbility($user_id,$schedule_date);
          if($result)
          {
            echo 0; exit();
          }
          else{
              
              $data = array('start_time' => $this->input->post('start_time'),
                  'interviewer_id' => $interviewer_id,
                  'student_id' => $user_id,
                  'end_time'=>$this->input->post('end_time'),
                  'booking_date'=>$this->input->post('schedule_date'), 
                  'created_date'=>$date,
                  'booking_status'=>'booked',
                  ); 
              $result=$this->db->insert('appointment', $data); 
             if($result)
              echo 1; exit();

              }
        }else{
           redirect(base_url());
        }



       
    }

    public function bookings() // send request for become tutor

    {
      //$result['university']=$this->Interviewer_model->get_university_list();
      $this->load->view('event_bookings');
    }
    public function availibility_list()
    {
     // $user_id='8819761956973354';
      if (isset($this->session->userdata['logged_in']))
          $user_id=($this->session->userdata['logged_in']['user_id']);
      $result=$this->Interview_schedule_model->get_schedule($user_id);
      $data_events = array();

     foreach($result as $r) {
         $title=$r->start_time.' - '.$r->end_time;
         $data_events[] = array(
             "title" => $title,
             "description" => 'Available',
             "start" => $r->schedule_date,
             "backgroundColor"=>'green'
         );
           
         
     }

     echo json_encode(array("events" => $data_events));
     exit();
     // echo '<pre>'; print_r($data_events); echo '</pre>';exit();
    }


  public function appointment_request()
  {
       //echo 'welcome to reschedule ';
       
      $date = date('Y-m-d H:i:s', time());
      $appointment_requiest_id=custom_random($digit=15);
          $schedule_date=$this->input->post('schedule_date'); 
          $start_time=$this->input->post('schedule_start_time'); 
            $end_time= date('h:i A',strtotime('+0 hour +45 minutes',strtotime($start_time)));
         // $end_time=$this->input->post('schedule_end_time');
          $applicant_id=$this->input->post('applicant_id');
          $applicant_email=$this->input->post('applicant_email');
         // $application_name=$this->input->post('applicant_name');
          $interviewer_id=$this->input->post('interviewer_id');
          $interviewer_email=$this->input->post('interviewer_email');
         // $interviewer_name=$this->input->post('interviewer_name');
          $data= array('schedule_date' =>$schedule_date,
                      'start_time' =>$start_time,
                      'end_time' =>$end_time,
                      'interviewer_id' =>$interviewer_id,
                      'applicant_id' =>$applicant_id,
                      'request_date' =>$date,
                      'appointment_request_id'=>$appointment_requiest_id
                       );
         // print_r($data); exit();
          $notification= array(
                      'interviewer_id' =>$interviewer_id,
                      'applicant_id' =>$applicant_id,
                      'created_date' =>$date,
                      'message'=>'new request for appointment',
                      'notify_interviewer'=>'Yes',
                       'notify_applicant'=>'No',
                       );


           $result=$this->Common_model->insertdata($table='appointment_request',$data);
           $result=$this->Common_model->insertdata($table='notification',$notification);
           $user_data=$this->Common_model-> getDataByID($table='users',$fname='user_id',$interviewer_id);
           $interviewer_email=$user_data[0]->email_address;
           $interviewer_name=$user_data[0]->first_name;
           $applicant_name=($this->session->userdata['logged_in']['user_name']);
           $applicant_email=($this->session->userdata['logged_in']['email_address']);
           

            $applicant_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Thank you for submitting your preferred time. We will check if our interviewer is available at this time and get back to you in the next 24 hours with an answer. <span></span></p>';
            $admin_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> New request of appointment availability</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
             <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Eamil :</b> '.$applicant_email.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Email :</b> '.$interviewer_email.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>';
             $interviewer_msg= '
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> New request of appointment availability</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Applicant Name :</b> '.$applicant_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Name :</b> '.$interviewer_name.'</p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Interviewer Email :</b> '.$interviewer_email.'</p>
            <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>Schedule appointment on : </b>'.$schedule_date.'</p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b> Start Time : </b>'.$start_time.'</p><p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <b>End Time :  </b>'.$end_time.'</p>';


           //$email2='info@whetstone.com';
             $admin_email='info@whetstone-oxbridge.com';
             $admin_subject='New Request for availability ';
             $subject=' Request for availability ';
                //$mail_data= array('user_name' =>$user_name,'user_id'=>$user_id );
                $data['msg']=$admin_msg;
                $data_applicant['msg']=$applicant_msg;
                $data_interviewer['msg']= $interviewer_msg;
                //$message = $this->load->view('mail/email_template',$data,true);
                $message = $this->load->view('mail/oxbridge_email_signature',$data_applicant,true);
                $result=send_user_mail($applicant_email,$message,$subject); // applicant mail
                $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
                $result=send_user_mail($admin_email,$message,$admin_subject);// admin mail
                 $message = $this->load->view('mail/oxbridge_email_signature',$data_interviewer,true);
                $result=send_user_mail($interviewer_email,$message,$admin_subject); // interviewer mail
              echo 1; 
            exit();   
           //exit();
          
  } 

  public function end_schedule_time_request() 
      {

         $start_time =$_POST['new_schedule'];
       $package =45; 
       // $start_time="7:30";
        //$package=30;
        $starttimestamp = strtotime($start_time);
         $time = date("h:i A", strtotime('+'.$package.' minute', $starttimestamp));
        echo $time; exit();
        //echo '<option value="' . $time . '">' . $time . '</option>';

      }

  public function update_Call_status()
  {
    //echo 'welcoe to leave session'; exit();
    $message = $this->input->get('message', null);
    $nickname = $this->input->get('nickname', '');
    $sender_id = $this->input->get('sender_id', '');
    $receiver_id = $this->input->get('receiver_id', '');
    $appointment_id = $this->input->get('appointment_id', '');
    $room_id = $this->input->get('room_name', '');

    $call_status=$this->Appointment_model->check_call_status($appointment_id,$status='Started');
    if($call_status){
      $data= array('call_status' =>'Completed','call_updated_by'=>$receiver_id,'appointment_id'=>$appointment_id);
        $result=$this->Common_model->updateData($table='appointment',$fname='appointment_id',$appointment_id,$data);

    }           
 }

 public function update_notification()
  {
    //echo 'welcoe to leave session'; exit();
    $message = $this->input->get('message', null);
    $user_id=($this->session->userdata['logged_in']['user_id']);
    if($message=='applicant')
    {

      $query= $this->db->select('notification_id,interviewer_id,applicant_id,message')->from('notification as n')->where('notify_applicant','Yes','applicant_view_status',0,'interviewer_id',$user_id)->get();
      $messages=$query->result();
      $total=count($messages);
     if($total>0){
        for($i=0;$i<$total;$i++){
        $data= array('applicant_view_status' =>1,'notification_id'=>$messages[$i]->notification_id);
        //echo json_encode($data);
          $result=$this->Common_model->updateData($table='notification',$fname='notification_id',$messages[$i]->notification_id,$data);
        }
      }
    }else{
       $query= $this->db->select('notification_id,interviewer_id,applicant_id,message')->from('notification as n')->where('interviewer_view_status',0,'interviewer_id',$user_id)->get();
        $messages=$query->result();
        $total=count($messages);
      if($total>0){
          for($i=0;$i<$total;$i++){
          $data= array('interviewer_view_status' =>1,'notification_id'=>$messages[$i]->notification_id);
          //echo json_encode($data);
            $result=$this->Common_model->updateData($table='notification',$fname='notification_id',$messages[$i]->notification_id,$data);
          }
      }
    }           
 }

  public function read_notificationByadmin()
  {
    //echo 'welcoe to leave session'; exit();
    $message = $this->input->get('message', null);
    $user_id=($this->session->userdata['logged_in']['user_id']);
     $query= $this->db->select('notification_id,interviewer_id,applicant_id,message')->from('notification as n')->where('notification_status',0)->get();
      $messages=$query->result();
      $total=count($messages);
      //echo json_encode($messages);
      //print_r($messages); exit();
    if($total>0){
      for($i=0;$i<$total;$i++){
      $data= array('notification_status' =>1,'notification_id'=>$messages[$i]->notification_id);
      //echo json_encode($data);
        $result=$this->Common_model->updateData($table='notification',$fname='notification_id',$messages[$i]->notification_id,$data);
      }

    }          
 }

 public function reminder_mail() // Reminder Mail Before 1 Hour By Cron job
      {
          $time = date('Y-m-d H:i:s', time()); 
          $date = date('Y-m-d'); 
          $real_time = date('h:i A', strtotime($time));
          //$start_time=$row->start_time;
          //$booking_date=$row->booking_date;
          $schedule_time= date('h:i A',strtotime('+1 hour +0 minutes',strtotime($real_time)));
           $result=$this->Appointment_model->get_remainder($date,$schedule_time);
            //echo '<pre>';print_r($result); echo '</pre>'; exit();
           if($result){
             for($i=0;$i<count($result);$i++)
               {
                $room_name=$result[$i]->room_name;
                $interviewer_id=$result[$i]->interviewer_id;
                $applicant_id=$result[$i]->student_id;
                $created_date= date('Y-m-d H:i:s', time()); 
                $notification= array(
                            'interviewer_id' =>$interviewer_id,
                            'applicant_id' =>$applicant_id,
                            'created_date' =>$created_date,
                            'message'=>'You have an appointment within one hour with',
                            'notify_interviewer'=>'Yes',
                             'notify_applicant'=>'Yes',
                             );
                $notifiy=$this->Common_model->insertdata($table='notification',$notification);
                //echo $result[0]->email_address;
          
            $applicant_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>A gentle reminder that you have an interview coming up soon.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>We remind you 1 hour and then 15 mins before any scheduled interview.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>You <a href="#">can access the interview screen </a> 15 mins before the scheduled time in order to set yourself up in preparation.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span><b>Before your interview make sure you: </b></span></p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>
             1. Are in a quiet place where you won'.'’t be disturbed.<br>
             2. Have a decent internet connection.<br>
             3. Have re-read through your Personal Statement.<br>
             4. Have read through any feedback from previous Whetstone interviews.
             </span>
             
             </p>
            ';
             $Interviewer_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>A gentle reminder that you have an interview coming up soon.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>We remind you 1 hour and then 15 mins before any scheduled interview.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>You <a href="#"> can access the interview screen </a> 15 mins before the scheduled time in order to set yourself up in preparation.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><span> <b>Before your interview make sure you: </b></span></p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>
             1. Have a decent internet connection..<br>
             2. Have read through the Personal Statement submitted by the applicant.<br>
             3. Have read through any previous feedback and have a range of question difficulties and styles prepared. <br>
             4. Have uploaded any questions or content onto the live googledoc on the interview page (if you are using this).<br> 
             5: Are in a quiet place where you won'.'’t be disturbed.
             </span>
             
             </p>
            ';
            $data['msg']=$applicant_msg;
            $data_interviewer['msg']=$Interviewer_msg;
            $subject="Interview reminder";
                
          /*  if($result){*/

             
                 //$applicant_email=$result[$i]->email_address;
                 $appointment_id=$result[$i]->appointment_id;
                 $applicant_email='arvind@coretechies.com';
                
                $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
                $mail_result=send_user_mail($applicant_email,$message,$subject); //  Send Reminder mail to applicant mail
               
                 
                 /* send mail to interviewer */
                  $interviewer_id=$result[$i]->interviewer_id;
                 $query=$this->db->select('email_address') ->from('users')->where('user_id',$interviewer_id)->get();
                 $mail_details=$query->result();
                 $interviewer_email=$mail_details[0]->email_address;
                 //$interviewer_email='devendra@coretechies.com';

               
                 $Interviewer_message = $this->load->view('mail/oxbridge_email_signature',$data_interviewer,true);
                 $mail_result2=send_user_mail($interviewer_email,$Interviewer_message,$subject); //  Send Reminder mail to Interviewer
                 
                 $update_data= array('appointment_id' =>$appointment_id ,'reminder_email_status'=>1 );
                 $this->db->where('appointment_id', $appointment_id)->update('appointment', $update_data);
                
               }
              

               //}
               echo 'mail send';

             }
            return;   
      }

      public function reminder_mail_oneday() // Reminder Mail Before 24 Hour 
      {
          $time = date('Y-m-d H:i:s', time()); 
          $date = date('Y-m-d'); 

$tomorrow_date = date('Y-m-d',strtotime($date . "+1 days"));
          $real_time = date('h:i A', strtotime($time));
          //$start_time=$row->start_time;
          //$booking_date=$row->booking_date;
          $schedule_time= date('h:i A',strtotime('+0 hour +0 minutes',strtotime($real_time)));
          // echo $schedule_time.'and date is '.$tomorrow_date; //exit();
           $result=$this->Appointment_model->get_remainder($tomorrow_date,$schedule_time);
           // echo '<pre>';print_r($result); echo '</pre>'; exit();
           if($result){
             for($i=0;$i<count($result);$i++)
               {
                $room_name=$result[$i]->room_name;
                $interviewer_id=$result[$i]->interviewer_id;
                $applicant_id=$result[$i]->student_id;
                $created_date= date('Y-m-d H:i:s', time()); 
                $notification= array(
                            'interviewer_id' =>$interviewer_id,
                            'applicant_id' =>$applicant_id,
                            'created_date' =>$created_date,
                            'message'=>'You have appointment after 24 hour',
                            'notify_interviewer'=>'Yes',
                             'notify_applicant'=>'Yes',
                             );
                $notifiy=$this->Common_model->insertdata($table='notification',$notification);
                //echo $result[0]->email_address;
          
            $applicant_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>A gentle reminder that you have an interview coming up in 24 hours.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>We remind you 1 hour and then 15 mins before any scheduled interview.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>You <a href="#">can access the interview screen </a> 15 mins before the scheduled time in order to set yourself up in preparation.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span><b>Before your interview make sure you: </b></span></p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>
             1. Are in a quiet place where you won'.'’t be disturbed.<br>
             2. Have a decent internet connection.<br>
             3. Have re-read through your Personal Statement.<br>
             4. Have read through any feedback from previous Whetstone interviews.
             </span>
             
             </p>
            ';
             $Interviewer_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>A gentle reminder that you have an interview coming up in 24 hours.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>We remind you 1 hour and then 15 mins before any scheduled interview.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>You <a href="#"> can access the interview screen </a> 15 mins before the scheduled time in order to set yourself up in preparation.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><span> <b>Before your interview make sure you: </b></span></p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>
             1. Have a decent internet connection..<br>
             2. Have read through the Personal Statement submitted by the applicant.<br>
             3. Have read through any previous feedback and have a range of question difficulties and styles prepared. <br>
             4. Have uploaded any questions or content onto the live googledoc on the interview page (if you are using this).<br> 
             5: Are in a quiet place where you won'.'’t be disturbed.
             </span>
             
             </p>
            ';
            $data['msg']=$applicant_msg;
            $data_interviewer['msg']=$Interviewer_msg;
            $subject="Interview reminder after 24 hours";
                
          /*  if($result){*/

             
                // $applicant_email=$result[$i]->email_address;
                 $appointment_id=$result[$i]->appointment_id;
                 $applicant_email='arvind@coretechies.com';
                
                $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
                $mail_result=send_user_mail($applicant_email,$message,$subject); //  Send Reminder mail to applicant mail
               
                 
                 /* send mail to interviewer */
                  $interviewer_id=$result[$i]->interviewer_id;
                 $query=$this->db->select('email_address') ->from('users')->where('user_id',$interviewer_id)->get();
                 $mail_details=$query->result();
                 $interviewer_email=$mail_details[0]->email_address;
                 //$interviewer_email='devendra@coretechies.com';

               
                 $Interviewer_message = $this->load->view('mail/oxbridge_email_signature',$data_interviewer,true);
                 $mail_result2=send_user_mail($interviewer_email,$Interviewer_message,$subject); //  Send Reminder mail to Interviewer
                 
                 $update_data= array('appointment_id' =>$appointment_id ,'reminder_email_status'=>1 );
                 $this->db->where('appointment_id', $appointment_id)->update('appointment', $update_data);
                
               }
              

               //}
               echo 'mail send';

             }
            return;   
      }

      public function second_reminder_mail() //  Second Reminder Mail Before 15 Minute by Core Job
      {
          $time = date('Y-m-d H:i:s', time()); 
          $date = date('Y-m-d'); 
          $real_time = date('h:i A', strtotime($time));
          $schedule_time= date('h:i A',strtotime('+0 hour +15 minutes',strtotime($real_time)));
          // $result=$this->Appointment_model->get_remainderSecond();
          //echo $schedule_time; exit();
          $result=$this->Appointment_model->get_remainderSecond($date,$schedule_time);

           //echo '<pre>';print_r($result); echo '</pre>'; 
           //exit();
           if($result){
             
          //echo $result[0]->email_address;
          // echo '<pre>';print_r($result); echo '</pre>'; exit();
            
              for($i=0;$i<count($result);$i++)
               {
                /* send notification to applicant and admin*/
                $interviewer_id=$result[$i]->interviewer_id;
                $applicant_id=$result[$i]->student_id;
                $created_date= date('Y-m-d H:i:s', time()); 
                $notification= array(
                      'interviewer_id' =>$interviewer_id,
                      'applicant_id' =>$applicant_id,
                      'created_date' =>$created_date,
                      'message'=>'You have an appointment within 15 minutes with',
                      'notify_interviewer'=>'Yes',
                      'notify_applicant'=>'Yes',
                       );
              $notifiy=$this->Common_model->insertdata($table='notification',$notification);

              /* Send remainder mail */
                $room_name=$result[$i]->room_name;
                $applicant_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>A gentle reminder that you have an interview coming up soon.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>We remind you 1 hour and then 15 mins before any scheduled interview.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>You <a href="https://whetstone-oxbridge.com/session/interviewer/'.$room_name.'">can access the interview screen </a> 15 mins before the scheduled time in order to set yourself up in preparation.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span><b>Before your interview make sure you: </b></span></p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">
             <span>1. Are in a quiet place where you won'.'’t be disturbed.<br>
             2. Have a decent internet connection.<br>
             3. Have re-read through your Personal Statement.<br>
             4. Have read through any feedback from previous Whetstone interviews.</span>
             </p>
            ';
             $Interviewer_msg= '<p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>A gentle reminder that you have an interview coming up soon.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>We remind you 1 hour and then 15 mins before any scheduled interview.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> <span>You <a href="https://whetstone-oxbridge.com/session/interviewer/'.$room_name.'">can access the interview screen </a> 15 mins before the scheduled time in order to set yourself up in preparation.</span></p>
            <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><span> <b>Before your interview make sure you: </b></span></p>
             <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> 
             <span>1. Have a decent internet connection.<br>
             2. Have read through the Personal Statement submitted by the applicant.<br>
             3. Have read through any previous feedback and have a range of question difficulties and styles prepared. <br>
             4. Have uploaded any questions or content onto the live googledoc on the interview page (if you are using this).<br> 
             5: Are in a quiet place where you won'.'’t be disturbed. </span>
             </p>
            ';
            $data['msg']=$applicant_msg;
            $data_interviewer['msg']=$Interviewer_msg;
            $subject="Interview reminder II";

                 //$applicant_email=$result[$i]->email_address;
                 $appointment_id=$result[$i]->appointment_id;
                 $applicant_email='arvind@coretechies.com';
                
                 $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
                 $mail_result=send_user_mail($applicant_email,$message,$subject); //  Send Reminder mail to applicant mail
               
                 
                 /* send mail to interviewer */
                  $interviewer_id=$result[$i]->interviewer_id;
                 $query=$this->db->select('email_address') ->from('users')->where('user_id',$interviewer_id)->get();
                 $mail_details=$query->result();
                 $interviewer_email=$mail_details[0]->email_address;
                // $interviewer_email='devendra@coretechies.com';

               
                 $Interviewer_message = $this->load->view('mail/oxbridge_email_signature',$data_interviewer,true);
                 $result=send_user_mail($interviewer_email,$Interviewer_message,$subject); //  Send Reminder mail to Interviewer
                 //
                 $update_data= array('appointment_id' =>$appointment_id ,'reminder_email_status'=>2 );
                 $this->db->where('appointment_id', $appointment_id)->update('appointment', $update_data);
                
               }
              

              
               echo 'mail send';
            }

            $this->reminder_mail();  // Send Reminder Mail 1 hour before 
            $this->reminder_mail_oneday(); // Send Reminder Mail 24 hour before 
      }

        
        

      //}
}

