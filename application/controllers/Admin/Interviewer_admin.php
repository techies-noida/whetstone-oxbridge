<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interviewer_admin extends CI_Controller {

	function __construct() 
      {
       parent::__construct();
        $this->load->model('admin/Student_model');
        $this->load->model('Common_model'); 
        $this->load->model('Interviewer_model');
        $this->load->model('Interview_schedule_model');
        if (!isset($this->session->userdata['admin_logged_in']))
          {
           redirect(base_url().'admin');
          } 

      }
  
  public function index() 
  {
   echo 'welcome '; exit();
    $result['title']='Students'; 
    $result['students']=$this->Student_model->get_studentList();  
    $this->load->view('admin/students_list',$result);         
  } 

  public function dashboard($user_id) 

  {
    //echo $user_id ; exit();
    $result['title']='inteviewer'; 
    $result['profile']=$this->Interviewer_model->get_interviewer_dashbaord($user_id); 
    $result['appointment']=$this->Interview_schedule_model->interviewer_appointment_booking($user_id);
     //echo '<pre>'; print_r($result); echo '</pre>'; exit();
    $this->load->view('admin/interviewer_dashboard',$result);         
  } 
  public function edit_profile($user_id) 

  {
   
     $result['title']='inteviewer';
   
      $result['user']=$this->Interviewer_model->get_profile_details($user_id);
     $result['country']=$this->Common_model->getDataByOrder($table="country",$fname="country_name");
      //echo '<pre>'; print_r($result); echo '</pre>'; exit(); 
     $this->load->view('admin/edit_interviewer_profile',$result);         
  } 


 public function update_profile()

    {
         $pic=$this->input->post('picture');
             //echo '<br> profile is '.$new_profile=$this->input->post('img1'); exit();
             if (strlen($pic)>300){
              define('UPLOAD_DIR', './uploads/profile/tutor/');
              $pic = str_replace('data:image/png;base64,', '', $pic);
              $pic = str_replace(' ', '+', $pic);
              $data = base64_decode($pic);
              $path='/uploads/profile/tutor/';
              $file = UPLOAD_DIR. uniqid() . '.png';
              
              $success = file_put_contents($file, $data);
               $new_profile = substr($file, 1);

              
            }
            else{
              $new_profile=$this->input->post('img1');
             
            }
           
          $date = date('Y-m-d H:i:s', time());
          $user_id=$this->input->post('user_id'); 
          $image="";
          $data = array('first_name' =>$this->input->post('first_name') ,
                         'phone_number'=>$this->input->post('phone_number'),
                         'profile_image'=>$new_profile,
                         'user_id'=>$user_id,
                       );
            $data2 = array('gender' =>$this->input->post('gender') ,
                         'date_of_birth'=>$this->input->post('dob'),
                         'country_id'=>$this->input->post('country'),
                         'subject'=>$this->input->post('subject'),
                         'about_us'=>$this->input->post('about_us'),
                         'course_year'=>$this->input->post('study_year'),
                         'experience'=>$this->input->post('interview_exprience'),
                         'academic_interest'=>$this->input->post('acadmic_intrests'),
                         'wish_to_told_oxbridge'=>$this->input->post('wish_to'),
                         'extra_curricular_interest'=>$this->input->post('extra_curicular'),
                         'last_updated_date'=>$date,
                         'user_id'=>$user_id,
                       );
            //echo '<pre>';print_r($data); echo '</pre>';
           //echo '<pre>';print_r($data2); echo '</pre>';exit();
           $result=$this->Common_model->updateData($table='users',$fname='user_id',$user_id,$data);
           $result=$this->Common_model->updateData($table='tutor_information',$fname='user_id',$user_id,$data2);
           $this->session->set_flashdata('success',  array('message' => 'Your profile has been updated'));
             redirect(base_url().'admin/interviewer/edit-profile/'.$user_id);
          
    }

  public function interviewer_appointment_feedback($appointment_id) 

  {
    //echo 'appointment_id is'.$appointment_id; exit();
   
     // $result['title']='inteviewer';
      $result['review']=$this->Student_model->appointment_reviewById($appointment_id);
       $result['feedback']=$this->Student_model->appointment_feedbackById($appointment_id);
       echo json_encode($result);
       //return($result);    
  } 

  public function student_appointment_feedback($appointment_id) 

  {
    //echo 'appointment_id is'.$appointment_id; exit();
   
     // $result['title']='inteviewer';
      $result['review']=$this->Student_model->appointment_reviewStudentById($appointment_id);
       $result['feedback']=$this->Student_model->appointment_feedbackById($appointment_id);
       echo json_encode($result);
       //return($result);    
  } 

 
 
    

}
