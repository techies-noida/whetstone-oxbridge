<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_panel extends CI_Controller {

  function __construct() 
  {
   parent::__construct();
   $this->load->model('admin/Interviewer_model');
   $this->load->model('Common_model'); 
   $this->load->helper('form');
   $this->load->library('form_validation');
   if (!isset($this->session->userdata['admin_logged_in']))
   {
     redirect(base_url().'admin');
   } 

 }

 public function dashboard() 
 {
  $result['title']='Dashboard';
  $result['request']=$this->Interviewer_model->recent_request_list();    
  $this->load->view('admin/index',$result);      
} 
  public function request()  // Become Interviewer  request list

  {
    $result['title']='Interviewer-request'; 
    $result['request']=$this->Interviewer_model->request_list(); 
    // $result=$this->User_model->check_admin_login($data); 
    $this->load->view('admin/interviewer_request',$result);      
  }

  public function verify_request($id,$name) // Interviewer request verify

  {  

    $date = date('Y-m-d H:i:s', time());
    $data= array( 
      'request_id'=>$id,
      'request_status'=>$name,
              //'verify_date'=>$date
    );  
    $result=$this->Common_model->updateData($table='tutor_join_request',$fname='request_id',$id,$data);
    if($result){
      if($name=='verified'){
       $query=$this->db->select('email_address,request_id')->from('tutor_join_request')->where('request_id',$id)->get();
       $mail_details=$query->result();
       echo $interviewer_email=$mail_details[0]->email_address;

       $msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Congratulations! We loved your application and are pleased to welcome you onboard as a Whetstone interviewer.</p>
       <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>You can now: </b><br>
       1. <a href="'.base_url().'interviewer/setup_account?request_id='.$id.'"> Update your profile with additional information.</a> <br>
       2. <a href="https://whetstone-oxbridge.com/">Login</a><br>
       3.Set your interview schedule.
       </p><p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> If you are having any difficulties figuring out how the site works, please do not hesitate to contact <a href="info@whetstone-oxbridge.com">info@whetstone-oxbridge.com</a> and a member of the team will guide you though the on-boarding process. </p>';
       $subject='Welcome to Whetstone';

       $data['msg']=$msg;
                //$message = $this->load->view('mail/email_template',$data,true);
       $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
       $result=send_user_mail($interviewer_email,$message,$subject);
       $this->session->set_flashdata('success', array('message' => 'Interviewer Request verified'));
       redirect(base_url().'interviewer-request');
     }
     else{
      $this->session->set_flashdata('error', array('message' => 'Interviewer request rejected'));
      redirect(base_url().'interviewer-request'); 
    }  

  }else{
    $this->session->set_flashdata('error', array('message' => 'Please proceed again for verified/rejected account'));
    redirect(base_url().'Interviewer-request'); 
  }  

}

  public function contact_request()  // Contact Us Request List

  {
    $result['title']='Contact-Us'; 
    $query=$this->db->order_by('contact_date','DESC')->get('contact_us'); 
    $result['request']=$query->result();
     // print_r($result); exit();
    $this->load->view('admin/contact_us_user_list',$result);      
  } 

  public function newsletter_subscriber()  // News Letter Subscriber List

  {
    $result['title']='Subscriber'; 
    $query=$this->db->order_by('serial_number','DESC')->get('newsletter'); 
    $result['request']=$query->result();
      //print_r($result); exit();
    $this->load->view('admin/newsletter_subscriber_list',$result);      
  }  

    public function change_account_view_status($id,$name) // Interviewer request verify

    {  

      $date = date('Y-m-d H:i:s', time());
      $data= array( 
        'user_id'=>$id,
        'account_view_status'=>$name,
              //'verify_date'=>$date
      );  
        // print_r($data); exit();
      $result=$this->Common_model->updateData($table='users',$fname='user_id',$id,$data);
      if($result){
        if($name=='Activate'){
                /* $query=$this->db->select('email_address,request_id')->from('users')->where('request_id',$id)->get();
                $mail_details=$query->result();
                echo $interviewer_email=$mail_details[0]->email_address;

                   $msg='<p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Congratulations! We loved your application and are pleased to welcome you onboard as a Whetstone interviewer.</p>
                        <p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"><b>You can now: </b><br>
                        1. <a href="https://whetstone-oxbridge.com/">Login</a> <br>
                        2.Update your profile with additional information.<br>
                        3.Set your interview schedule.
                        </p><p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> If you are having any difficulties figuring out how the site works, then there are on boarding help videos on your dashboard.</p><p style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;"> Also, please do not hesitate to <a href="#">email us</a> if you are struggling with anything. </p>';
         $subject='Welcome to Whetstone';
          
         $data['msg']=$msg;
                //$message = $this->load->view('mail/email_template',$data,true);
            $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
            $result=send_user_mail($interviewer_email,$message,$subject);*/
            $this->session->set_flashdata('success', array('message' => 'Interviewer account activated'));
            redirect(base_url().'interviewers');
          }
          else{
            $this->session->set_flashdata('success', array('message' => 'Interviewer account deactivated'));
            redirect(base_url().'interviewers'); 
          }  


        }else{
          $this->session->set_flashdata('error', array('message' => 'Please proceed again for activate/deactivate account'));
          redirect(base_url().'interviewers'); 
        }  

      }

  public function interviewers()  //  Interviewers  list
  
  {
    $result['title']='Interviewers'; 
    $result['interviewers']=$this->Interviewer_model->get_interviewer_list(); 
      //echo '<pre>'; print_r($result);echo '</pre>'; exit();
      // $result=$this->User_model->check_admin_login($data); 
    $this->load->view('admin/interviewers_list',$result);      
  }

  public function appointment_list() 
  {
    $result['title']='session_list';
    $result['session_list'] = $this->Interviewer_model->appointment_booking();
      //echo '<pre>'; print_r($result); echo'</pre>'; exit();
    $this->load->view('admin/session_list',$result);
  }

  public function appointment_request_list() 
  {
    $result['title']='request_session_list';
    $result['session_list'] = $this->Interviewer_model->requested_appointment();
    $this->load->view('admin/session_request_list',$result);
  }

    public function session_list()  //  Interviewers  list

    {
      $result['title']='Interviewers'; 
      $result['interviewers']=$this->Interviewer_model->get_interviewer_list(); 
      // $result=$this->User_model->check_admin_login($data); 
      $this->load->view('admin/interviewers_list',$result);      
    }    
    public function table() 
    {
    //echo 'Welcome to Admin panel'; exit();
      $this->load->view('admin/table');
    }    
   //code added by ravindra 06-11-2018
    public function add_coupn() 
    {
      $result['title']='Add Coupn';
      $this->load->view('admin/add_coupn',$result);
    } 
  //this function is used for add coupn   
    public function add_coupn_action() 
    {
      $this->form_validation->set_rules('name', 'name', 'required|is_unique[coupn.name]');
      $this->form_validation->set_rules('description', 'description', 'required');
      $this->form_validation->set_rules('start_date', 'Start Date', 'required');
      $this->form_validation->set_rules('end_date', 'end Date', 'required');
      $this->form_validation->set_rules('limit', 'limit', 'required');
      $this->form_validation->set_rules('code', 'code', 'required');
      $this->form_validation->set_rules('percentage', 'percentage', 'required');
      if ($this->form_validation->run() == FALSE){
        $result['title']='Add Coupn';
        $this->load->view('admin/add_coupn',$result);
      }else{
        $data = array(
          'name' => $this->input->post('name'),
          'description' => $this->input->post('description'),
          'start_date' => $this->input->post('start_date'),
          'end_date' => $this->input->post('end_date'),
          'coupn_limit' => $this->input->post('limit'),
          'code' => $this->input->post('code'),
          'percentage' => $this->input->post('percentage'),
          'created_at' => date('Y-m-d'),
          'updated_at' => date('Y-m-d')
        );
        if($this->db->insert('coupn', $data)){
          $this->session->set_flashdata('success', array('message' => 'Coupn Has been added successfully.'));
          redirect(base_url().'admin/add-coupn');
        }else{
          $this->session->set_flashdata('error', array('message' => 'Internal server error.'));
          redirect(base_url().'admin/add-coupn');
        }
      }
    }
  //this function is used for listing all coupn
    public function coupn() 
    {
      $result['title']='Coupn List';
      $result['coupn_list'] = $this->Common_model->getData('coupn');
      $result['package_price'] = $this->Interviewer_model->get_package_price();
      // print_r($result);
      // die();
      $this->load->view('admin/coupn',$result);
    }
    public function update_price()
    {
      if (isset($_POST)) {
        $data = array(
          'value' => $_POST['package_price'],
          'updated_at ' => date('d-m-Y h:m:s') );

        $result = $this->Interviewer_model->update_price($data);
        echo $result;
      }
    }
  //this function is used for delete coupn
    public function delete_coupn() 
    { 
      $this->db->where('serial_number', $this->input->get('id'));
      if($this->db->delete('coupn')){
        $this->session->set_flashdata('success', array('message' => 'Coupn has been deleted successfully.'));
        redirect(base_url().'admin/coupn');
      }else{
        $this->session->set_flashdata('error', array('message' => 'Internal server error.'));
        redirect(base_url().'admin/coupn');
      }
    }
  //this function is used for render edit coupn page
    public function edit_coupn() 
    { 
      $id = $this->input->get('id');
      if(isset($id) && !empty($id)){
        $result['coupn'] = $this->Common_model->getDataByID('coupn','serial_number',$id);
        $result['title']='Edit Coupn';
        $this->load->view('admin/edit_coupn',$result);
      }else{
        $this->session->set_flashdata('error', array('message' => 'Invalid coupn id'));
        redirect(base_url().'admin/coupn');
      }
    }
  //this function is used for render edit coupn page
    public function coupn_edit_action() 
    {
      $id = $this->input->post('id');
      $this->form_validation->set_rules('name', 'name', 'required');
      $this->form_validation->set_rules('description', 'description', 'required');
      $this->form_validation->set_rules('start_date', 'Start Date', 'required');
      $this->form_validation->set_rules('end_date', 'end Date', 'required');
      $this->form_validation->set_rules('limit', 'limit', 'required');
      $this->form_validation->set_rules('code', 'code', 'required');
      $this->form_validation->set_rules('percentage', 'percentage', 'required');
      if ($this->form_validation->run() == FALSE){
        $result['coupn'] = $this->Common_model->getDataByID('coupn','serial_number',$id);
        $result['title']='Edit Coupn';
        $this->load->view('admin/edit_coupn',$result);
      }else{
        $data = array(
          'name' => $this->input->post('name'),
          'description' => $this->input->post('description'),
          'start_date' => $this->input->post('start_date'),
          'end_date' => $this->input->post('end_date'),
          'coupn_limit' => $this->input->post('limit'),
          'code' => $this->input->post('code'),
          'percentage' => $this->input->post('percentage'),
          'updated_at' => date('Y-m-d')
        );
        if($this->is_coupn_exist($this->input->post('name'),$id)){
          $this->session->set_flashdata('error', array('message' => 'Coupn name is already exist.'));
          redirect('admin/edit-coupn?id='.$id);
          exit;
        }
        $this->db->where('serial_number', $id);
        if($this->db->update('coupn', $data)){
          $this->session->set_flashdata('success', array('message' => 'Coupn has been updated successfully.'));
          redirect('admin/edit-coupn?id='.$id);
          exit;
        }else{
          $this->session->set_flashdata('error', array('message' => 'Internal server error.'));
          redirect('admin/edit-coupn?id='.$id);
          exit;
        }
      }
    }
    public function interviewer_contact_form()
    {
      $result['title']='interviewer-contact-form'; 
      $result['interviewers']=$this->Interviewer_model->get_interviewer_contact_list(); 
      $this->load->view('admin/interviewer_contact_form',$result); 
    }
    public function view_interviewer_request()
    {
      $request_id = $this->uri->segment(2);
      $result['title']='Interviewer-request'; 
      $result['interviewers']=$this->Interviewer_model->single_request_list($request_id); 
      $this->load->view('admin/view_interviewer_request',$result); 
    // echo $request_id;
    // print_r($result['interviewers']);
    }
    public function send_mail()
    {
      $result['email_lists'] = $this->Common_model->email_address_lists();
      $result['title']='Interviewer-request';
      $this->load->view('admin/send_mail',$result);
    // print_r($result['email_lists']);
    }
    public function send_newsletter_mail()
    {
      $result['email_lists'] = $this->Common_model->newsletter_email_address_lists();
      $result['title']='Subscriber';
      $this->load->view('admin/send_newsletter_mail',$result);
    // print_r($result['email_lists']);
    }
    public function send_multiple_mail()
    {
      $email_address = $this->input->get('email_address[]');
      $subject = $this->input->get('subject[]');
      $msg = $this->input->get('message[]');
    // print_r(sizeof($email_address));
      for ($i=0; $i < sizeof($email_address); $i++) { 
        // $email = $email_address[$i];
        $data['msg']=$msg;
        $message = $this->load->view('mail/oxbridge_email_signature',$data,true);
        $this->send_multiple_email($email_address[$i], $subject, $message);
      }
      echo 1;
    }
    public function all_notificaions()
    {
      $result['title']='all_notificaions';
      $this->load->view('admin/all_notifications',$result);
    }
    public function reviews_details()
    {   
      $appointment_id =  $this->uri->segment('4'); 
      $result['reviews_interviewer_details']  =$this->Interviewer_model->reviews_interviewer_details($appointment_id);
      $result['reviews_student_details']  =$this->Interviewer_model->reviews_student_details($appointment_id);
      $result['download_video_url'] = $this->Interviewer_model->download_video_url($appointment_id);
      $result['title']='session_list';
      $this->load->view('admin/review_details',$result);
      // print_r($result);
    }
    public function verify_req()
    {
      $data = array('request_status' => 'Verified' );
      $this->Interviewer_model->updated_requested_appointment($_POST['id'], $data);
    }
    public function reject_req()
    {
      $data = array('request_status' => 'reject' );
      $this->Interviewer_model->updated_requested_appointment($_POST['id'], $data);
    }
    public function reviews()
    {
      $result['title']='reviews';
      $result['review'] = $this->Interviewer_model->review_listing();
      // print_r($result['review']);
      // die();
      $this->load->view('admin/reviews',$result);
    }
    public function review_block()
    {
        $updated_data = array(
          'status'  => 1       
        ); 
        $result = $this->Interviewer_model->update_review_status($_POST["id"], $updated_data);  
        echo $result;
    }
    public function review_active()
    {
        $updated_data = array(
          'status'  => 0       
        ); 
        $result = $this->Interviewer_model->update_review_status($_POST["id"], $updated_data);  
        echo $result;
    }

    // public function newsletter_email_address_export_lists()
    // {
    //   $result['newsletter_data'] = $this->Common_model->newsletter_email_address_export_lists();
    //   $this->load->view('admin/newsletter_export_view',$result);
    //   print_r($result['newsletter_data']);
    // }
    function export_action()
    {
      $this->load->library("excel");
      $object = new PHPExcel();

      $object->setActiveSheetIndex(0);

      $table_columns = array("Email Address");

      $column = 0;

      foreach($table_columns as $field)
      {
       $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
       $column++;
     }

     $employee_data = $this->Common_model->newsletter_email_address_export_lists();

     $excel_row = 2;

     foreach($employee_data as $row)
     {
       $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->email_address);
       $excel_row++;
     }

     $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="Newsletter Data.xls"');
     $object_writer->save('php://output');
   }
   public function send_multiple_email($email, $subject, $message)
   {
    $ci =& get_instance();
    $config = Array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://smtp.googlemail.com',
        // 'protocol' => 'mail',
        // 'smtp_host' => 'smtp.gmail.com',
      'smtp_port' => 465,
        'smtp_user' => 'info@whetstone-oxbridge.com', // change it to yours
        'smtp_pass' => 'qonQu2-joqdik-fifwug', // change it to yours
        // 'smtp_timeout' => '8',
        'mailtype' => 'html',
        // 'charset' => 'utf-8',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
      );


    $ci->load->library('email', $config);
    $ci->email->set_newline("\r\n");
            $ci->email->from('info@whetstone-oxbridge.com'); // change it to yours
            $ci->email->to($email);// change it to yours
            $ci->email->subject($subject);
            $ci->email->message($message);
            if($ci->email->send())
            {
              return true;
            //echo 'send mail';
            }
            else
            {
              return false;
           //show_error($ci->email->print_debugger());
            }
          }
  //this function is used for check coupn name is exist or not
          public function is_coupn_exist($name, $id){
            $this->db->select('*');
            $this->db->from('coupn');
            $this->db->where('name',$name);
            $this->db->where('serial_number !=',$id);
            $query = $this->db->get();
            if($query->num_rows()){
              return true;
            }else{
              return false;
            }
          }
        }
