<?php
/**
 * Created by PhpStorm.
 * User: Pranav Mehra
 * Date: 26/11/2018
 * Time: 17:57
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Add_Content_Controller extends CI_Controller {

function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        if (!isset($this->session->userdata['admin_logged_in']))
        {
            redirect(base_url().'admin');
        }
    }

    public function about_us() {
        $this->db->select('');
        $this->db->from('about_us');
        $query = $this->db->get();
        $get_about_data = $query->row();
        if(isset($_POST['content_1'])) {
            $content_1 = $this->input->post('editor1');

            if($_FILES['image_1']['name']) {
                $path = FILE_URL;
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('image_1')) {
                    $error = array('error' => $this->upload->display_errors());
                    var_dump($error);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
                if (!empty($data['upload_data']['file_name'])) {
                    $import_image_1 = $data['upload_data']['file_name'];
                } else {
                    $import_image_1 = 0;
                }
            } else {
                $this->db->select('');
                $this->db->from('about_us');
                $query = $this->db->get();
                $get_image = $query->row();
                $import_image_1 = $get_image->image_1;
            }

            if($content_1 || $import_image_1) {
                if(!$get_about_data) {
                    $insert_data = [
                        'body_1' => $content_1,
                        'image_1' => $import_image_1,
                    ];
                    $insert_status = $this->db->insert('about_us',$insert_data);
                    if($insert_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
                        redirect(base_url() . 'admin/about-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/about-us');
                    }
                } else {
                    if($content_1 && !$import_image_1) {
                        $update_data = [
                            'body_1' => $content_1,
                            'image_1' => $import_image_1,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    } elseif(!$content_1 && $import_image_1) {
                        $update_data = [
                            'image_1' => $import_image_1,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    } else {
                        $update_data = [
                            'body_1' => $content_1,
                            'image_1' => $import_image_1,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    $this->db->where('id',$get_about_data->id);
                    $update_status = $this->db->update('about_us',$update_data);
                    if($update_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
                        redirect(base_url() . 'admin/about-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/about-us');
                    }
                }
            } else {
                $this->session->set_flashdata('error', array('error' => 'Please try again.'));
                redirect(base_url() . 'admin/about-us');
            }
        } elseif (isset($_POST['content_2'])) {
            $content_2 = $this->input->post('editor2');
            if($content_2) {
                $this->db->select('');
                $this->db->from('about_us');
                $query = $this->db->get();
                $get_about_data = $query->row();
                if($get_about_data) {
                    $update_data = [
                        'body_2' => $content_2,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $this->db->where('id',$get_about_data->id);
                    $update_status = $this->db->update('about_us',$update_data);
                    if($update_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
                        redirect(base_url() . 'admin/about-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/about-us');
                    }
                } else {
                    $insert_data = [
                        'body_2' => $content_2,
                    ];
                    $insert_status = $this->db->insert('about_us',$insert_data);
                    if($insert_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
                        redirect(base_url() . 'admin/about-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/about-us');
                    }
                }
            } else {
                $this->session->set_flashdata('error', array('error' => 'Please fill the field'));
                redirect(base_url() . 'admin/about-us');
            }
        } elseif (isset($_POST['content_3'])) {
            $content_3 = $this->input->post('editor3');

            if($_FILES['image_2']['name']) {
                $path = FILE_URL;
                $config['upload_path'] = $path;
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('image_2')) {
                    $error = array('error' => $this->upload->display_errors());
                    var_dump($error);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
                if (!empty($data['upload_data']['file_name'])) {
                    $import_image_2 = $data['upload_data']['file_name'];
                } else {
                    $import_image_2 = 0;
                }
            } else {
                $this->db->select('');
                $this->db->from('about_us');
                $query = $this->db->get();
                $get_image = $query->row();
                $import_image_2 = $get_image->image_2;
            }

            if($content_3 || $import_image_2) {
                if(!$get_about_data) {
                    $insert_data = [
                        'body_3' => $content_3,
                        'image_2' => $import_image_2,
                    ];
                    $insert_status = $this->db->insert('about_us',$insert_data);
                    if($insert_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
                        redirect(base_url() . 'admin/about-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/about-us');
                    }
                } else {
                    if($content_3 && !$import_image_2) {
                        $update_data = [
                            'body_3' => $content_3,
                            'image_2' => $import_image_2,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    } elseif(!$content_3 && $import_image_2) {
                        $update_data = [
                            'image_2' => $import_image_2,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    } else {
                        $update_data = [
                            'body_3' => $content_3,
                            'image_2' => $import_image_2,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    $this->db->where('id',$get_about_data->id);
                    $update_status = $this->db->update('about_us',$update_data);
                    if($update_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
                        redirect(base_url() . 'admin/about-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/about-us');
                    }
                }
            } else {
                $this->session->set_flashdata('error', array('error' => 'Please try again.'));
                redirect(base_url() . 'admin/about-us');
            }

        } else {
            $this->db->select('');
            $this->db->from('about_us');
            $query = $this->db->get();
            $about_us = $query->row();
            if($about_us) {
                $result['about_us'] = $about_us;
            } else {
                $result['about_us'] = NULL;
            }
            $result['banner'] = $this->Common_model->getData($table = 'banner');
            $result['choose_us'] = $this->Common_model->getData($table = 'home_why_choose_us');
            $result['home'] = $this->Common_model->getDataByID($table = 'home_page', $fname = 'serial_number', $id = 1);
            $result['title'] = 'about-us';
            $this->load->view('admin/about_us', $result);
        }
    }

    public function privacy() {
        if(isset($_POST['content_1'])) {
            $content = $this->input->post('editor1');

            $this->db->select('');
            $this->db->from('privacy_policy');
            $query = $this->db->get();
            $get_privacy = $query->row();

            if($get_privacy) {
                $update_data = [
                    'body' => $content,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->db->where('id',$get_privacy->id);
                $update_status = $this->db->update('privacy_policy',$update_data);

                if($update_status == true) {
                    $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
                    redirect(base_url() . 'admin/privacy');
                } else {
                    $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                    redirect(base_url() . 'admin/privacy');
                }
            } else {
                $insert_data = [
                    'body' => $content,
                ];
                $insert_status = $this->db->insert('privacy_policy',$insert_data);
                if($insert_status == true) {
                    $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
                    redirect(base_url() . 'admin/privacy');
                } else {
                    $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                    redirect(base_url() . 'admin/privacy');
                }
            }
        } else {
            $this->db->select('');
            $this->db->from('privacy_policy');
            $query = $this->db->get();
            $get_privacy = $query->row();

            if ($get_privacy) {
                $result['privacy'] = $get_privacy;
            } else {
                $result['privacy'] = NULL;
            }
            $result['banner'] = $this->Common_model->getData($table = 'banner');
            $result['choose_us'] = $this->Common_model->getData($table = 'home_why_choose_us');
            $result['home'] = $this->Common_model->getDataByID($table = 'home_page', $fname = 'serial_number', $id = 1);
            $result['title'] = 'privacy';
            $this->load->view('admin/privacy', $result);
        }
    }

    public function terms() {
        if(isset($_POST['content_1'])) {
            $content = $this->input->post('editor1');

            $this->db->select('');
            $this->db->from('terms');
            $query = $this->db->get();
            $get_terms = $query->row();

            if($get_terms) {
                $update_data = [
                    'body' => $content,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->db->where('id',$get_terms->id);
                $update_status = $this->db->update('terms',$update_data);

                if($update_status == true) {
                    $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
                    redirect(base_url() . 'admin/terms');
                } else {
                    $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                    redirect(base_url() . 'admin/terms');
                }
            } else {
                $insert_data = [
                    'body' => $content,
                ];
                $insert_status = $this->db->insert('terms',$insert_data);
                if($insert_status == true) {
                    $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
                    redirect(base_url() . 'admin/terms');
                } else {
                    $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                    redirect(base_url() . 'admin/terms');
                }
            }
        } else {
            $this->db->select('');
            $this->db->from('terms');
            $query = $this->db->get();
            $get_terms = $query->row();

            if ($get_terms) {
                $result['terms'] = $get_terms;
            } else {
                $result['terms'] = NULL;
            }
            $result['banner'] = $this->Common_model->getData($table = 'banner');
            $result['choose_us'] = $this->Common_model->getData($table = 'home_why_choose_us');
            $result['home'] = $this->Common_model->getDataByID($table = 'home_page', $fname = 'serial_number', $id = 1);
            $result['title'] = 'terms';
            $this->load->view('admin/terms', $result);
        }
    }

    // public function school() {
    //     $this->db->select('');
    //     $this->db->from('school');
    //     $query = $this->db->get();
    //     $get_school = $query->row();

    //     if(isset($_POST['content_1'])) {
    //         $content_1 = $this->input->post('editor1');
    //         $content_2 = $this->input->post('editor2');

    //         if($_FILES['image_1']['name']) {
    //             $path = FILE_URL_SCHOOL;
    //             $config['upload_path'] = $path;
    //             $config['allowed_types'] = 'gif|jpg|png';
    //             $this->upload->initialize($config);
    //             if (!$this->upload->do_upload('image_1')) {
    //                 $error = array('error' => $this->upload->display_errors());
    //                 var_dump($error);
    //             } else {
    //                 $data = array('upload_data' => $this->upload->data());
    //             }
    //             if (!empty($data['upload_data']['file_name'])) {
    //                 $import_image_1 = $data['upload_data']['file_name'];
    //             } else {
    //                 $import_image_1 = 0;
    //             }
    //         } else {
    //             $import_image_1 = $get_school->image_1;
    //         }

    //         if($content_1 || $content_2 || $import_image_1) {
    //             if(!$get_school) {
    //                 $insert_data = [
    //                     'body_1' => $content_1,
    //                     'body_2' => $content_2,
    //                     'image_1' => $import_image_1,
    //                 ];
    //                 $insert_status = $this->db->insert('school',$insert_data);
    //                 if($insert_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             } else {
    //                 if($content_1 && !$content_2 && !$import_image_1) {
    //                     $update_data = [
    //                         'body_1' => $content_1,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 } elseif(!$content_1 && $content_2 && !$import_image_1) {
    //                     $update_data = [
    //                         'body_2' => $content_2,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 } elseif(!$content_1 && !$content_2 && $import_image_1) {
    //                     $update_data = [
    //                         'image_1' => $import_image_1,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 } else {
    //                     $update_data = [
    //                         'body_1' => $content_1,
    //                         'body_2' => $content_2,
    //                         'image_1' => $import_image_1,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 }
    //                 $this->db->where('id',$get_school->id);
    //                 $update_status = $this->db->update('school',$update_data);
    //                 if($update_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             }
    //         } else {
    //             $this->session->set_flashdata('error', array('error' => 'Please try again.'));
    //             redirect(base_url() . 'admin/school');
    //         }
    //     } elseif(isset($_POST['content_2'])) {
    //         $content_3 = $this->input->post('editor3');
    //         $content_4 = $this->input->post('editor4');

    //         if($_FILES['image_2']['name']) {
    //             $path = FILE_URL_SCHOOL;
    //             $config['upload_path'] = $path;
    //             $config['allowed_types'] = 'gif|jpg|png';
    //             $this->upload->initialize($config);
    //             if (!$this->upload->do_upload('image_2')) {
    //                 $error = array('error' => $this->upload->display_errors());
    //                 var_dump($error);
    //             } else {
    //                 $data = array('upload_data' => $this->upload->data());
    //             }
    //             if (!empty($data['upload_data']['file_name'])) {
    //                 $import_image_2 = $data['upload_data']['file_name'];
    //             } else {
    //                 $import_image_2 = 0;
    //             }
    //         } else {
    //             $import_image_2 = $get_school->image_1;
    //         }

    //         if($content_3 || $content_4 || $import_image_2) {
    //             if(!$get_school) {
    //                 $insert_data = [
    //                     'body_3' => $content_3,
    //                     'body_4' => $content_4,
    //                     'image_2' => $import_image_2,
    //                 ];
    //                 $insert_status = $this->db->insert('school',$insert_data);
    //                 if($insert_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             } else {
    //                 if($content_3 && !$content_4 && !$import_image_2) {
    //                     $update_data = [
    //                         'body_3' => $content_3,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 } elseif(!$content_3 && $content_4 && !$import_image_2) {
    //                     $update_data = [
    //                         'body_4' => $content_4,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 } elseif(!$content_3 && !$content_4 && $import_image_2) {
    //                     $update_data = [
    //                         'image_2' => $import_image_2,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 } else {
    //                     $update_data = [
    //                         'body_3' => $content_3,
    //                         'body_4' => $content_4,
    //                         'image_2' => $import_image_2,
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     ];
    //                 }
    //                 $this->db->where('id',$get_school->id);
    //                 $update_status = $this->db->update('school',$update_data);
    //                 if($update_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             }
    //         } else {
    //             $this->session->set_flashdata('error', array('error' => 'Please try again.'));
    //             redirect(base_url() . 'admin/school');
    //         }
    //     } elseif(isset($_POST['content_3'])) {
    //         $content_5 = $this->input->post('editor5');

    //         if($content_5) {
    //             if($get_school) {
    //                 $update_data = [
    //                     'center_body' => $content_5,
    //                     'updated_at' => date('Y-m-d H:i:s'),
    //                 ];
    //                 $this->db->where('id',$get_school->id);
    //                 $update_status = $this->db->update('school',$update_data);
    //                 if($update_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             } else {
    //                 $insert_data = [
    //                     'center_body' => $content_5,
    //                 ];
    //                 $insert_status = $this->db->insert('school',$insert_data);
    //                 if($insert_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             }
    //         } else {
    //             $this->session->set_flashdata('error', array('error' => 'Please fill the field'));
    //             redirect(base_url() . 'admin/about-us');
    //         }
    //     }  elseif(isset($_POST['content_4'])) {
    //         $main_title = $this->input->post('editor6');
    //         $content_6 = $this->input->post('editor7');

    //         if ($main_title || $content_6) {
    //             if ($get_school) {
    //                 $update_data = [
    //                     'main_title' => $main_title,
    //                     'body_5' => $content_6,
    //                     'updated_at' => date('Y-m-d H:i:s'),
    //                 ];
    //                 $this->db->where('id', $get_school->id);
    //                 $update_status = $this->db->update('school', $update_data);
    //                 if ($update_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             } else {
    //                 $insert_data = [
    //                     'main_title' => $main_title,
    //                     'body_5' => $content_6,
    //                 ];
    //                 $insert_status = $this->db->insert('school', $insert_data);
    //                 if ($insert_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             }
    //         } else {
    //             $this->session->set_flashdata('error', array('error' => 'Please fill the field'));
    //             redirect(base_url() . 'admin/about-us');
    //         }
    //     } elseif(isset($_POST['content_5'])) {
    //         if($_FILES['upcoming']['name']) {
    //             $path = FILE_URL_SCHOOL;
    //             $config['upload_path'] = $path;
    //             $config['allowed_types'] = 'gif|jpg|png';
    //             $this->upload->initialize($config);
    //             if (!$this->upload->do_upload('upcoming')) {
    //                 $error = array('error' => $this->upload->display_errors());
    //                 var_dump($error);
    //             } else {
    //                 $data = array('upload_data' => $this->upload->data());
    //             }
    //             if (!empty($data['upload_data']['file_name'])) {
    //                 $upcoming = $data['upload_data']['file_name'];
    //             } else {
    //                 $upcoming = 0;
    //             }
    //         } else {
    //             $upcoming = $get_school->upcoming;
    //         }

    //         if($upcoming) {
    //             if($get_school) {
    //                 $update_data = [
    //                     'upcoming' => $upcoming,
    //                     'updated_at' => date('Y-m-d H:i:s'),
    //                 ];
    //                 $this->db->where('id',$get_school->id);
    //                 $update_status = $this->db->update('school',$update_data);
    //                 if($update_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Updated Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             } else {
    //                 $insert_data = [
    //                     'upcoming' => $upcoming,
    //                 ];
    //                 $insert_status = $this->db->insert('school',$insert_data);
    //                 if($insert_status == true) {
    //                     $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
    //                     redirect(base_url() . 'admin/school');
    //                 } else {
    //                     $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
    //                     redirect(base_url() . 'admin/school');
    //                 }
    //             }
    //         } else {
    //             $this->session->set_flashdata('error', array('error' => 'Please fill the field'));
    //             redirect(base_url() . 'admin/about-us');
    //         }
    //     }  else {
    //         if($get_school) {
    //             $result['school'] = $get_school;
    //         } else {
    //             $result['school'] = NULL;
    //         }
    //         $result['banner'] = $this->Common_model->getData($table = 'banner');
    //         $result['choose_us'] = $this->Common_model->getData($table = 'home_why_choose_us');
    //         $result['home'] = $this->Common_model->getDataByID($table = 'home_page', $fname = 'serial_number', $id = 1);
    //         $result['title'] = 'school';
    //         $this->load->view('admin/school', $result);
    //     }
    // }

    public function school()
    {
        
            $result['school'] = $this->Common_model->getDataByID($table = 'school', $fname = 'id', $id = 1);
            $result['title'] = 'school';
            // print_r($result);
            // die();
            $this->load->view('admin/school', $result);
    }
    public function save_school_data()
    {
        if (empty($_FILES["image_1"]["name"])) {
            $image1 = $_POST['hidden_image_1'];
        }else {
            $image1 = $this->upload_image1();
        }
        if (empty($_FILES["image_2"]["name"])) {
            $image2 = $_POST['hidden_image_2'];
        }else {
            $image2 = $this->upload_image2();
        }

        $data = array(
            'body_1' => $_POST['content1'],
            'body_2' => $_POST['content2'],
            'body_3' => $_POST['content3'],
            'body_4' => $_POST['content4'],
            'footer_text' => $_POST['footer_text'],
            'image_1' => $image1,
            'image_2' => $image2);

        $this->db->where("id", 1); 
        if ($this->db->update('school', $data)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function save_school_data2()
    {
        $data = array(
            'center_body' => $_POST['center_body'],
            'main_title' => $_POST['main_title'],
            'body_5' => $_POST['body_5']
        );

        $this->db->where("id", 1); 
        if ($this->db->update('school', $data)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function upload_image1()  
    {  
        if(isset($_FILES["image_1"]))  
        {  
            $extension = explode('.', $_FILES['image_1']['name']);  
            $new_name = rand() . '.' . $extension[1];  
            $destination = './uploads/home_page/' . $new_name;  
            move_uploaded_file($_FILES['image_1']['tmp_name'], $destination);  
            return $destination;  
        } 
    }

    function upload_image2()  
    {  
        if(isset($_FILES["image_2"]))  
        {  
            $extension = explode('.', $_FILES['image_2']['name']);  
            $new_name = rand() . '.' . $extension[1];  
            $destination = './uploads/home_page/' . $new_name;  
            move_uploaded_file($_FILES['image_2']['tmp_name'], $destination);  
            return $destination;  
        } 
    }

    public function work_for_us() {
        $this->db->select('');
        $this->db->from('work_for_us');
        $query = $this->db->get();
        $get_work_for_us = $query->result();

        if($get_work_for_us) {
            $result['work_for_us'] = $get_work_for_us;
        } else {
            $result['work_for_us'] = NULL;
        }

        $result['banner'] = $this->Common_model->getData($table = 'banner');
        $result['choose_us'] = $this->Common_model->getData($table = 'home_why_choose_us');
        $result['home'] = $this->Common_model->getDataByID($table = 'home_page', $fname = 'serial_number', $id = 1);
        $result['title'] = 'work-for-us';
        $this->load->view('admin/work_for_us', $result);
    }

    public function add_work_for_us() {
        if(isset($_POST['content_1'])) {
            $this->form_validation->set_rules('editor1','Title','trim|required');
            //$this->form_validation->set_rules('editor2','Body','trim|required');

            if($this->form_validation->run() == true) {
                if($_FILES['image_1']['name']) {
                    $path = FILE_URL_WORK;
                    $config['upload_path'] = $path;
                    $config['allowed_types'] = 'gif|jpg|png';
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('image_1')) {
                        $error = array('error' => $this->upload->display_errors());
                        var_dump($error);
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                    }
                    if (!empty($data['upload_data']['file_name'])) {
                        $image_1 = $data['upload_data']['file_name'];
                    } else {
                        $image_1 = 0;
                    }
                }

                if($image_1) {
                    $insert_data = [
                        'title' => $this->input->post('editor1'),
                        'body' => $this->input->post('editor2'),
                        'image' => $image_1,
                    ];
                    $insert_status = $this->db->insert('work_for_us',$insert_data);

                    if($insert_status == true) {
                        $this->session->set_flashdata('success', array('message' => 'Content Added Successfully'));
                        redirect(base_url() . 'admin/work-for-us');
                    } else {
                        $this->session->set_flashdata('error', array('error' => 'Something Went Wrong, Please try again.'));
                        redirect(base_url() . 'admin/add-work-for-us');
                    }
                } else {
                    $this->session->set_flashdata('error', array('error' => 'Please select image'));
                    redirect(base_url() . 'admin/add-work-for-us');
                }
            } else {
                $this->session->set_flashdata('error', array('error' => 'Please fill the field'));
                redirect(base_url() . 'admin/add-work-for-us');
            }
        } else {
            $result['banner'] = $this->Common_model->getData($table = 'banner');
            $result['choose_us'] = $this->Common_model->getData($table = 'home_why_choose_us');
            $result['home'] = $this->Common_model->getDataByID($table = 'home_page', $fname = 'serial_number', $id = 1);
            $result['title'] = 'work-for-us';
            $this->load->view('admin/add_work_for_us', $result);
        }
    }

    public function edit_work_us($id) 

    {

        $result['work_for_us'] = $this->Common_model->getDataByID($table = 'work_for_us', $fname = 'id', $id);
        $result['title'] = 'work-for-us';
       // echo '<pre>'; echo  print_r($result); echo '</pre>'; exit();
        $this->load->view('admin/edit_work_for_us', $result);
    }

    public function update_work_for_us()

    {
       //echo 'welcome to update banner'; exit();
         $this->load->library('upload');
             if (!empty($_FILES['img']['name'])){
              $config['upload_path'] = './assets/admin/assets/work_for_us/';
              $config['allowed_types'] = '*';
              //$config['max_size'] = '1000';       
              $this->upload->initialize($config);
                // Upload file 1
                if ($this->upload->do_upload('img'))
                {
                $data = $this->upload->data();
                $one=$data['file_name'];
                $path='/assets/admin/assets/work_for_us/';
                $path_one= $one;
                }else{
                  echo $this->upload->display_errors();
                }
            }
            else{
              $one='';
              $path_one=$this->input->post('img1');
            }
          $title=$this->input->post('editor1');
          $body=$this->input->post('editor2'); 
          $id=$this->input->post('id'); 
         
          $image=$path_one;  
          $date = date('Y-m-d H:i:s', time());
          
          $data = array('title' =>$title ,
                         'body'=>$body,
                          'image'=>$image,
                          'id'=>$id,
                         'updated_at'=>$date,
                         
                       );   
          //echo '<pre>';print_r($data);  echo '</pre>'; exit();
          $result=$this->Common_model->updateData($table='work_for_us',$fname='id',$id,$data);
           
           $this->session->set_flashdata('success',  array('message' => 'Work-for-us changes update successfully'));
             redirect(base_url().'admin/work-for-us');
          
    }

    public function delete_work_ajax() {
        header('Content-type: application/json');
        $id = $this->input->post('id');
        $this->db->where('id',$id);
        $status = $this->db->delete('work_for_us');
        echo json_encode($status);
    }
}
