<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	function __construct() 
      {
       parent::__construct();
        $this->load->model('admin/Student_model');
        $this->load->model('Common_model'); 
        $this->load->model('Interviewer_model');
        $this->load->model('Interview_schedule_model');
        if (!isset($this->session->userdata['admin_logged_in']))
          {
           redirect(base_url().'admin');
          } 

      }
  
  public function index() 
  {
   
    $result['title']='Students'; 
    $result['students']=$this->Student_model->get_studentList();  
    $this->load->view('admin/students_list',$result);         
  } 

  public function dashboard($user_id) 

  {
    //echo $id ; exit();
    $result['title']='Students'; 
    $result['profile']=$this->Student_model->get_student_details($user_id); 
    $result['appointment']=$this->Student_model->student_appointments($user_id); 
     //echo '<pre>'; print_r($result); echo '</pre>'; exit();
    $this->load->view('admin/student_dashboard',$result);         
  } 
  public function edit_profile($user_id) 

  {
   
     $result['title']='Students';
     $result['profile']=$this->Student_model->get_student_details($user_id); 
     $result['country']=$this->Common_model->getDataByOrder($table="country",$fname="country_name");
      //echo '<pre>'; print_r($result); echo '</pre>'; exit(); 
     $this->load->view('admin/edit_student_profile',$result);         
  } 


  public function update_profile()

    {
        //$this->load->library('upload');
                 $pic=$this->input->post('picture');
             //echo '<br> profile is '.$new_profile=$this->input->post('img1'); exit();
             if (strlen($pic)>300){
              define('UPLOAD_DIR', './uploads/profile/tutor/');
              $pic = str_replace('data:image/png;base64,', '', $pic);
              $pic = str_replace(' ', '+', $pic);
              $data = base64_decode($pic);
              $path='/uploads/profile/tutor/';
              $file = UPLOAD_DIR. uniqid() . '.png';
              
              $success = file_put_contents($file, $data);
               $new_profile = substr($file, 1);

              
            }
            else{
              $new_profile=$this->input->post('img1');
             
            }

            
           
          $date = date('Y-m-d H:i:s', time());
          $user_id=$this->input->post('user_id');
          
          $image="";
          $data = array('first_name' =>$this->input->post('first_name') ,
                         'phone_number'=>$this->input->post('phone_number'),
                         //'profile_image'=>$path_one,
                         'profile_image'=>$new_profile,
                         'user_id'=>$user_id,
                         
                       );
            $data2 = array(
                        
                         'country_id'=>$this->input->post('country'),
                         'university_id'=>$this->input->post('school_name'),
                         'college_id'=>$this->input->post('college'),
                         'course'=>$this->input->post('course'),
                         'course_year'=>$this->input->post('course_year'),
                         'last_updated_date'=>$date,
                         'user_id'=>$user_id,
                         'desire_university'=>$this->input->post('desire_university'),
                          'desire_college'=>$this->input->post('desire_college'),
                          'desire_course'=>$this->input->post('desire_course'),
                       );
         // echo '<pre>';print_r($data); echo '</pre>';
        // echo '<pre>';print_r($data2); echo '</pre>';exit();
           $result=$this->Common_model->updateData($table='users',$fname='user_id',$user_id,$data);
           $result=$this->Common_model->updateData($table='student_information',$fname='user_id',$user_id,$data2);
           $this->session->set_flashdata('success',  array('message' => 'Applicant profile  has been updated '));
             redirect(base_url().'admin/student/edit-profile/'.$user_id);
          
    }

  public function intrested_user() 
  {
    
     $result['title']='intrested_user'; 
    $result['intrested_user']=$this->Student_model->get_school_intrested_user();
    //echo '<pre>'; print_r($result); echo '</pre>'; exit();
    $this->load->view('admin/school_intrested_user_list',$result);         
  } 

  public function video_url_using_appointment_id()
  {
   // print_r($_POST['appointment_id']);
   $result = $this->Student_model->video_url_using_appointment_id($_POST['appointment_id']);
   if (empty($result)) {
     $video_url = '';
   } else {
    $video_url = $result;
   }
   echo base_url() . 'compose_video/composed/' . $video_url;
  }
 
    

}
