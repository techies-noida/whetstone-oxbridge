<?php
class Student_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
   

  public function get_studentList()  // Get student list
  
    {
      $this->db->select('u.serial_number,u.user_id,first_name,last_name,email_address,phone_number,desire_university,desire_college,desire_course,university_id,user_role_id,profile_image,u.created_date,status');
      $this->db->from('users u');
      $this->db->join('student_information si','u.user_id=si.user_id');
      $this->db->where('u.user_role_id',3);
      $this->db->order_by('serial_number','DESC');
      $query = $this->db->get();
      $result = $query->result();
      //echo '<pre>';print_r($result); echo '</pre>';exit();
    return ($result);
    } 

  public function get_school_intrested_user()  // Get intrested user list form school page
  
    {
      $this->db->select('*');
      $this->db->from('interested_user');
      $this->db->order_by('serial_number','DESC');
      $query = $this->db->get();
      $result = $query->result();
      //echo '<pre>';print_r($result); echo '</pre>';exit();
      return ($result);
    }

   public function student_appointments($user_id)  // Get studnet appointment list with inteviewer

    {
        $this->db->select(' appointment_id,a.schedule_id,a.start_time,a.end_time,booking_date,interviewer_id,first_name,profile_image,room_name,a.created_date,call_status');
        $this->db->from('appointment as a');
        $this->db->join('users as u','a.student_id=u.user_id');
        $this->db->where('a.student_id', $user_id);
        $this->db->order_by('booking_date','ASC');
        $query = $this->db->get();
        $result=$query->result();
        //echo '<pre>';print_r($result); echo '</pre>'; exit();
        
        return $result;
    }
    
    public function get_student_details($id)  //Get students details for dislay on profile

      {
        $this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,phone_number,date_of_birth,university_id,college_id,about_us,course,course_year,account_status,country_id,desire_university,desire_college,desire_course,profile_status');
        //$this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,phone_number');
        $this->db->from('users as u');
        //$this->db->join('country as cont','cont.country_id=ti.country_id');
        $this->db->join('student_information as stu','u.user_id=stu.user_id');
        $this->db->where('user_role_id',3);
        $this->db->where('u.user_id',$id);
        $this->db->where('status',1);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
         //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      } 

    public function appointment_feedbackById($appointment_id)  //Get appointment feedback by Id

    {
        $this->db->select(' appointment_id,student_id,interviewer_id,rating,comments,comments2,comments3,comments4,question1,question2,question3,review_date');
        $this->db->from('reviews as r');
        $this->db->where('user_type','interviewer');
        $this->db->where('appointment_id',$appointment_id);
        //$this->db->join('users as u','a.applicant_id=u.user_id');
        //$this->db->order_by('request_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
       //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
    }

    public function appointment_reviewById($appointment_id)  //Get appointment feedback by Id

    {
        $this->db->select('appointment_id,student_id,first_name,interviewer_id,rating,comments,question1,question2,question3,profile_image,review_date');
        $this->db->from('reviews as r');
        $this->db->where('user_type','student');
        $this->db->where('appointment_id',$appointment_id);
        $this->db->join('users as u','r.student_id=u.user_id');
        //$this->db->order_by('request_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
       //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
    }

     public function appointment_reviewStudentById($appointment_id)  //Get appointment feedback by Id

    {
        $this->db->select('appointment_id,student_id,first_name,interviewer_id,rating,comments,question1,question2,question3,profile_image,review_date');
        $this->db->from('reviews as r');
        $this->db->where('user_type','student');
        $this->db->where('appointment_id',$appointment_id);
        $this->db->join('users as u','r.interviewer_id=u.user_id');
        //$this->db->order_by('request_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
       //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
    }              
      
public function video_url_using_appointment_id($appointment_id)
{
  $this->db->select('video_url');
    $this->db->from('appointment_video');
    $this->db->where('appointment_id', $appointment_id);
    $this->db->where('download_status', 1);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $result = $query->result_array();
      return $result[0]['video_url'];
    } else {
      return '';
    }
}
     


     

}
