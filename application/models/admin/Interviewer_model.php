<?php
class Interviewer_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
   

 
  public function request_list()   // All Backend User Registred List
    {
        
        $this->db->select('request_id,name, email_address,subject,university_name,college_name,request_status,request_date,working_interest');
        $this->db->from('tutor_join_request tjr'); 
        $this->db->join('university u', 'tjr.university_id=u.university_id', 'left');
        $this->db->join('college c', 'tjr.college_id=c.college_id', 'left');
       $this->db->order_by('serial_id','DESC');
        $query = $this->db->get();
        $result = $query->result();
       // echo '<pre>';print_r($result); echo '</pre>';exit();
        return ($result);  
    }

  public function recent_request_list()   // All Backend User Registred List
    {
        
        $this->db->select('request_id,name, email_address,subject,university_name,college_name,request_status,request_date,working_interest');
        $this->db->from('tutor_join_request tjr'); 
        $this->db->join('university u', 'tjr.university_id=u.university_id', 'left');
        $this->db->join('college c', 'tjr.college_id=c.college_id', 'left');
       $this->db->order_by('serial_id','DESC');
       $this->db->limit('5');
        $query = $this->db->get();
        $result = $query->result();
       // echo '<pre>';print_r($result); echo '</pre>';exit();
        return ($result);  
    }  

     public function get_interviewer_list()  //Interviewer list 

      {
        $this->db->select('first_name,last_name,u.user_id,u.phone_number,u.email_address,u.profile_image,status,university_name,college_name,about_us,ti.subject,u.created_date,account_view_status');
        $this->db->from('users as u');
        $this->db->join('tutor_join_request as tjr','u.email_address=tjr.email_address');
        $this->db->join('tutor_information as ti','u.user_id=ti.user_id');
        $this->db->join('university as uni','uni.university_id=tjr.university_id');
         $this->db->join('college as clg','clg.college_id=tjr.college_id');
         $this->db->where_not_in('u.user_id','7293629326720177');
        $this->db->where('user_role_id',2);
        $this->db->where('status',1);
        $this->db->order_by('u.serial_number','DESC');
        $query = $this->db->get();
        $result = $query->result();
        //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      }

   public function appointment_booking()  //Get Session Details

    {
        $this->db->select(' appointment_id,a.schedule_id,a.start_time,a.end_time,booking_date,booking_date,a.created_date,first_name,student_id,interviewer_id,profile_image,call_status');
        $this->db->from('appointment as a');
        $this->db->join('users as u','a.student_id=u.user_id');
        $this->db->order_by('booking_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
       //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
    } 

  public function requested_appointment()  //Get requested  Session list 

    {
        $this->db->select(' appointment_request_id,applicant_id,interviewer_id,a.start_time,a.end_time,schedule_date,request_status,request_date,first_name,profile_image');
        $this->db->from('appointment_request as a');
        $this->db->join('users as u','a.applicant_id=u.user_id');
        $this->db->order_by('request_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
       //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
    } 

  public function appointment_feedbackById($appointment_id)  //Get appointment feedback by Id

    {
        $this->db->select(' appointment_id,student_id,interviewer_id,rating,comments,comments2,comments3,comments4,review_date');
        $this->db->from('reviews as r');
        $this->db->where('user_type','interviewer');
        $this->db->where('appointment_id',$appointment_id);
        //$this->db->join('users as u','a.applicant_id=u.user_id');
        //$this->db->order_by('request_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
       echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
    }   

    public function get_interviewer_contact_list()
      {
       $this->db->select('*');
        $this->db->from('request_an_interviewer');
        $query = $this->db->get();
      $result = $query->result();
      return $result;
      }

    public function single_request_list($request_id)   // All Backend User Registred List
    {
        
        $this->db->select('request_id,name, email_address,subject,phone_number,year_of_study,wish_to_know,extra_curricular,university_name,college_name,request_status,request_date,profile_image,working_interest');
        $this->db->from('tutor_join_request tjr'); 
        $this->db->join('university u', 'tjr.university_id=u.university_id', 'left');
        $this->db->join('college c', 'tjr.college_id=c.college_id', 'left');
        $this->db->where('tjr.request_id',$request_id);
       $this->db->order_by('serial_id','DESC');
        $query = $this->db->get();
        $result = $query->result();
       // echo '<pre>';print_r($result); echo '</pre>';exit();
        return ($result);  
    }    
public function review_listing()
{
        $this->db->select('review_id,student_id,interviewer_id,rating,comments,status');
        $this->db->from('reviews');
        $this->db->where('user_type', 'student');
       $this->db->order_by('review_id','DESC');
        $query = $this->db->get();
      $result = $query->result_array();
      return $result;
}
public function update_review_status($id,$data)
{
  $this->db->where("review_id", $id); 
  if ($this->db->update('reviews', $data)) {
     return 1;
   } else {
    return 0;
   }
}
public function reviews_interviewer_details($id)
{
  $this->db->select('*');
  $this->db->from('reviews');
  $this->db->where('user_type', 'interviewer');
  $this->db->where('appointment_id', $id);
  $this->db->order_by('review_id','DESC');
  $query = $this->db->get();
  $result = $query->result_array();
  return $result;
}
public function reviews_student_details($id)
{
  $this->db->select('*');
  $this->db->from('reviews');
  $this->db->where('user_type', 'student');
  $this->db->where('appointment_id', $id);
  $this->db->order_by('review_id','DESC');
  $query = $this->db->get();
  $result = $query->result_array();
  return $result;
}
public function download_video_url($id)
{
  $this->db->select('video_url');
  $this->db->from('appointment_video');
  $this->db->where('download_status', 1);
  $this->db->where('appointment_id', $id);
  $query = $this->db->get();
  $result = $query->result_array();
  return $result;
}

public function updated_requested_appointment($id, $data)
{
  $this->db->where('appointment_request_id', $id);
  $this->db->update('appointment_request', $data);
}
  

      public function get_package_price()
  {
    $this->db->select('value');
    $this->db->from('price');
    $this->db->where('price_id', 1);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $result = $query->result_array();
      return $result[0]['value'];
    } else {
      return 0;
    }
  }

  public function update_price($data)
      {
        $this->db->where('price_id', 1);
        if ($this->db->update('price', $data)) {
          return 1;
        } else {
          return 0;
        }
      }    

     


     

}
