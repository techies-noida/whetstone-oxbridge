<?php
class User_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
    public function check_admin_login($data) // Check Login User Email Id And Password
    {
        $email=$data->email;
        $password=$data->password;
         
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email_address', $email);
        $this->db->where('password', $password);
        /*$query = $this->db->get();
        $result = $query->result();
      print_r($result); exit();*/
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
          //echo 'password match'; exit();
          return true;
        } else {
          //echo 'password not  match'; exit();
          return false;
        }
    }

   
  public function admin_info($email)  // Get Login User Details by email id
  
    {
      $this->db->select('*');
      $this->db->from('admin');
      $this->db->where('email_address', $email);
      $query = $this->db->get();
      $result = $query->result();
      //print_r($result); exit();
    return ($result);
    }



  public function check_duplicate_user($email)  //Check duplicate user E-mail Id*

    {
      $this->db->select('user_email');
        $this->db->from('backend_user');
        $this->db->where('user_email', $email);
        //$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
          return true;
        } 
        else 
        {
            return false;
        }
    }

  public function  user_profile_details($id)  // Get user profile details by user ID
    {
      $this->db->select('first_name,last_name,user_contact,user_email,profile_image');
      $this->db->from('backend_user');
      $this->db->where('user_id',$id);
      $query=$this->db->get();
      $result=$query->result();
      return($result);
    }  
  public function createUserLog($table,$data)   // Insert User details in backend_user_log Table
    {
      $this->db->insert($table,$data);
      return $this->db->insert_id();
      
    }
  public function UserLastLog($id)   //  backend_user_log Table user Last log details
    {
      $this->db->select('login_time,logid,logout_time');
      $this->db->from('backend_user_log'); 
      $this->db->where('user_id', $id);
      $this->db->order_by('logid','DESC');
      $this->db->limit(1);
      $query = $this->db->get();
      $result = $query->result();
      //print_r($result);
      return ($result);; 
    } 
  public function UserLastlogin($id)   //  get Last login Time
    {
      $this->db->select('login_time,logid,logout_time');
      $this->db->from('backend_user_log'); 
      $this->db->where('user_id', $id);
      $this->db->order_by('logid','DESC');
      $this->db->limit(1,1);
      $query = $this->db->get();
      $result = $query->result();
      //print_r($result);
      return ($result);;  
    }  
  
 
 

}
