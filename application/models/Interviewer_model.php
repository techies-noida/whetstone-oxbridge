<?php
class Interviewer_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
    public function get_university_list()  // Get all university name
  
    {
      $this->db->select('university_id,university_name');
      $this->db->from('university u');
      $this->db->order_by('university_name','ASC');
      $query = $this->db->get();
      $result = $query->result();
      //echo '<pre>';print_r($result); echo '</pre>';exit();
    return ($result);
    } 

    public function get_college_list($id)  // Get user details for display on profile page
    {
      $this->db->select('college_id,college_name');
      $this->db->from('college');
      $this->db->order_by('college_name','ASC');
      $this->db->where('university_id','$id');
      $query = $this->db->get();
      $result = $query->result();
      //echo '<pre>';print_r($result); echo '</pre>';exit();
    return ($result);
    }
  public function check_duplicate_user($email)  //Check duplicate user E-mail Id*

    {
      $this->db->select('email_address');
        $this->db->from('tutor_join_request');
        $this->db->where('email_address', $email);
        //$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
         // echo 'user_find';exit();
          return true;
        } 
        else 
        {
           //echo 'user not find';exit();
            return false;
        }
    }
    public function check_request_id($request_id)  //Check duplicate user E-mail Id*

    {
      $this->db->select('request_id');
        $this->db->from('tutor_join_request');
        $this->db->where('request_id', $request_id);
        //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        if ($query->num_rows() == 1) 
        {
          
          //echo 'user_find';exit();
          return true;
        } 
        else 
        {
          //echo 'user not find';exit();
            return false;
        }
    }

    public function check_setup_account($email_address)  //Check Tutor account created

    {
      $this->db->select('email_address');
        $this->db->from('users');
        $this->db->where('email_address', $email_address);
        $this->db->where('status',1);
        //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        if ($query->num_rows() == 1) 
        {
          //echo 'user_find';exit();
          return true;
        } 
        else 
        {
          //echo 'user not find';exit();
            return false;
        }
    }
    public function get_request_details($request_id)  //Check duplicate user E-mail Id*

      {
        $this->db->select('name,email_address,request_id,phone_number,request_status,profile_image,tjr.university_id,university_name,tjr.college_id,college_name,subject,year_of_study,working_interest,wish_to_know,extra_curricular');
        $this->db->from('tutor_join_request as tjr');
        $this->db->join('university as u','u.university_id=tjr.university_id');
        $this->db->join('college as clg','clg.college_id=tjr.college_id');
        $this->db->where('request_id', $request_id);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
      }
   //check_request_status
  public function check_user_login($data) // Check Login User Email Id And Password
    {
        $email=$data->email;
        $password=$data->password; //exit();
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email_address', $email);
        $this->db->where('password', $password);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
          //echo 'password match'; exit();
          return true;
        } else {
          //echo 'password not  match'; exit();
          return false;
        }
    }

   
  public function user_info($email)  // Get Login User Details by email id
  
    {
      $this->db->select('*');
      $this->db->from('users');
      $this->db->where('email_address', $email);
      $query = $this->db->get();
      $result = $query->result();
      //print_r($result); exit();
    return ($result);
    }

   public function get_user_details($id)  // Get user details for display on profile page
  
    {
      $this->db->select('u.serial_number,first_name,last_name,email_address,phone_number,user_role_id,name,profile_image');
      $this->db->from('users u');
       $this->db->join('user_roles ur', 'u.user_role_id=ur.serial_number', 'left');
      $this->db->where('u.serial_number',$id);
      $query = $this->db->get();
      $result = $query->result();
      //echo '<pre>';print_r($result); echo '</pre>';exit();
    return ($result);
    } 

  public function get_interviewer_list()  //Interviewer list 

      {
        $this->db->select('first_name,last_name,u.user_id,u.email_address,u.profile_image,status,university_name,college_name,about_us,ti.subject');
        $this->db->from('users as u');
        $this->db->join('tutor_join_request as tjr','u.email_address=tjr.email_address');
        $this->db->join('tutor_information as ti','u.user_id=ti.user_id');
        $this->db->join('university as uni','uni.university_id=tjr.university_id');
         $this->db->join('college as clg','clg.college_id=tjr.college_id');
         $this->db->where_not_in('u.user_id','7293629326720177');
        $this->db->where('user_role_id',2);
        $this->db->where('status',1);
        $this->db->where('account_view_status','Activate');
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        // echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      } 

  public function top_interviewer_list()  //Get Interviewer list last Top 4

      {
        $this->db->select('first_name,last_name,u.user_id,u.email_address,u.profile_image,status,university_name,college_name,about_us,ti.subject');
        $this->db->from('users as u');
        $this->db->join('tutor_join_request as tjr','u.email_address=tjr.email_address');
        $this->db->join('tutor_information as ti','u.user_id=ti.user_id');
        $this->db->join('university as uni','uni.university_id=tjr.university_id');
         $this->db->join('college as clg','clg.college_id=tjr.college_id');
        $this->db->where('user_role_id',2);
        $this->db->where('status',1);
        $this->db->order_by('u.serial_number','DESC');
        $this->db->limit(4);
        $query = $this->db->get();
        $result = $query->result();
        // echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      }       

   public function get_interviewer_details($id)  //Get Interviewer details for dislay on profile

      {
        $this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,university_name,college_name,ti.subject,u.phone_number,country_name,course_year');
        //$this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,phone_number');
        $this->db->from('users as u');
        $this->db->join('tutor_join_request as tjr','u.email_address=tjr.email_address');
        $this->db->join('tutor_information as ti','ti.user_id=u.user_id');
       $this->db->join('university as uni','uni.university_id=tjr.university_id');
      $this->db->join('college as clg','clg.college_id=tjr.college_id');
      $this->db->join('country as cont','cont.country_id=ti.country_id');
        $this->db->where('user_role_id',2);
        $this->db->where('u.user_id',$id);
        $this->db->where('u.status',1);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
         //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      }

     /*public function get_interviewer_dashbaord($id)  //Get Interviewer details for dislay on profile

      {
        $this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,university_name,college_name,academic_interest,wish_to_told_oxbridge,extra_curricular_interest,subject,course_year,experience');
        $this->db->from('users as u');
        $this->db->join('tutor_information as ti','ti.user_id=u.user_id');
        $this->db->join('university as uni','uni.university_id=ti.university_id');
        $this->db->join('college as clg','clg.college_id=ti.college_id');
        $this->db->where('user_role_id',2);
        $this->db->where('u.user_id',$id);
        $this->db->where('status',1);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
         //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      }*/
  
      public function get_interviewer_dashbaord($id)  //Get Interviewer details for dislay on profile

      {
        $this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,university_name,college_name,academic_interest,wish_to_told_oxbridge,extra_curricular_interest,subject,course_year,experience');
        $this->db->from('users as u');
        $this->db->join('tutor_information as ti','ti.user_id=u.user_id');
        $this->db->join('university as uni','uni.university_id=ti.university_id');
        $this->db->join('college as clg','clg.college_id=ti.college_id');
        $this->db->where('user_role_id',2);
        $this->db->where('u.user_id',$id);
        $this->db->where('status',1);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
         //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      }
     public function get_profile_details($id)  //Get Interviewer details for dislay on profile

      {
        $this->db->select('u.user_id,first_name,last_name,u.email_address,u.phone_number,u.profile_image,status,university_name,college_name,gender,date_of_birth,about_us,country_id,tui.subject,course_year,experience,academic_interest,extra_curricular_interest,wish_to_told_oxbridge');
        $this->db->from('users as u');
        $this->db->join('tutor_join_request as tjr','u.email_address=tjr.email_address');
        $this->db->join('tutor_information as tui','tui.user_id=u.user_id');
        $this->db->join('university as uni','uni.university_id=tui.university_id');
        $this->db->join('college as clg','clg.college_id=tui.college_id');
        $this->db->where('user_role_id',2);
        $this->db->where('u.user_id',$id);
        $this->db->where('status',1);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
         //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      }   

     public function get_student_details($id)  //Get students details for dislay on profile

      {
        $this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,phone_number,date_of_birth,university_id,college_id,about_us,course,course_year,account_status,country_id,desire_university,desire_college,desire_course,profile_status');
        //$this->db->select('u.user_id,first_name,last_name,u.email_address,u.profile_image,status,phone_number');
        $this->db->from('users as u');
        //$this->db->join('country as cont','cont.country_id=ti.country_id');
        $this->db->join('student_information as stu','u.user_id=stu.user_id');
        $this->db->where('user_role_id',3);
        $this->db->where('u.user_id',$id);
        $this->db->where('status',1);
          //$this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
         //echo '<pre>';print_r($result); echo '</pre>';exit();
        return $result;
      } 

    public function get_student_profile_status($user_id)  //get student ptofile status

    {
      $this->db->select('profile_status');
      $this->db->from('student_information');
      $this->db->where('user_id',$user_id);
      $query = $this->db->get();
      $result = $query->result();
      $status=$result[0]->profile_status;
      return $status;
       //echo '<pre>';print_r($result); echo '</pre>';exit();
        
    } 

    public function interviewerTopReview($interviewer_id)  //Get Interviewer Top reviews given by student 

      {
        $this->db->select('interviewer_id,student_id,rating,comments,review_date,first_name');
        $this->db->from('reviews r');
        $this->db->join('users u','r.student_id=u.user_id');
        $this->db->where('interviewer_id',$interviewer_id);
         $this->db->where('user_type','student');
        $this->db->where('r.status', 1);
        $this->db->order_by('review_date','DESC');
        $this->db->limit('3');
        $query = $this->db->get();
        $result = $query->result();
        //$status=$result[0]->profile_status;
        return $result;
        // echo '<pre>';print_r($result); echo '</pre>';exit();
          
      }

    public function interviewerAllReview($interviewer_id)  //Get Interviewer All reviews given by student 

      {
        $this->db->select('interviewer_id,student_id,rating,comments,review_date,first_name');
        $this->db->from('reviews r');
        $this->db->join('users u','r.student_id=u.user_id');
        $this->db->where('interviewer_id',$interviewer_id);
        $this->db->where('user_type','student');
        $this->db->order_by('review_date','DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
        // echo '<pre>';print_r($result); echo '</pre>';exit();
          
      } 

     public function studentAllReview($student_id)  //Get Studnet All reviews given by Interviewer

      {
        $this->db->select('interviewer_id,student_id,rating,comments,review_date,first_name');
        $this->db->from('reviews r');
        $this->db->join('users u','r.interviewer_id=u.user_id');
        $this->db->where('student_id',$student_id);
        $this->db->where('user_type','interviewer');
        $this->db->order_by('review_date','DESC');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
        // echo '<pre>';print_r($result); echo '</pre>';exit();
          
      } 

      public function get_notifications_lists($id)
      {
        $this->db->select('notification_id,interviewer_id,applicant_id,message,n.created_date,first_name');
        $this->db->from('notification as n');
        $this->db->join('users as u','n.interviewer_id=u.user_id');
        $this->db->where('notify_applicant','Yes');
        $this->db->where('applicant_id',$id);
        $this->db->order_by('notification_id','DESC');
        $query = $this->db->get();
        return $query->result_array();
      }
      public function get_notifications_lists_interview($id)
      {
        $this->db->select('notification_id,interviewer_id,applicant_id,message,n.created_date,first_name');
        $this->db->from('notification as n');
        $this->db->join('users as u','n.applicant_id=u.user_id');
        $this->db->where('interviewer_id',$id);
        $this->db->order_by('notification_id','DESC');
        $query = $this->db->get();
        return $query->result_array();
      }


    public function student_feedback_detail($review_id)  // Student feedback details in PDF

      {
          $this->db->select('review_id, student_id,interviewer_id,first_name,profile_image,college_name,university_name,comments,comments2,comments3,comments4,review_date');
          $this->db->from('reviews as r');
          $this->db->join('users as u','r.interviewer_id=u.user_id');
          $this->db->join('tutor_information as ti','r.interviewer_id=ti.user_id');
          $this->db->join('university as ui','ti.university_id=ui.university_id');
            $this->db->join('college as clg','ti.college_id=clg.college_id');
          $this->db->where('review_id', $review_id);
          $query = $this->db->get();
          $result=$query->result();
          //echo '<pre>';print_r($result); echo '</pre>'; exit();
            return $result;
          
      }                     
        
      public function interviewer_contact_form($data)
      
        {
         $this->db->insert('request_an_interviewer',$data);
        }  


      
    public function package_price()
  {
    $this->db->select('value');
    $this->db->from('price');
    $this->db->where('price_id', 1);
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $result = $query->result_array();
      return $result[0]['value'];
    } else {
      return 0;
    }
  }
     


     

}
