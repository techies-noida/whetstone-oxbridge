<?php
class Common_model extends CI_Model{
  function __construct() {
   parent::__construct();

 }

  public function getData($table)   // Select All Data from  any table

  {
    $query=$this->db->get($table); 
    $result=$query->result();
      //echo '<pre>';print_r($result); echo '</pre>';exit();
    return ($result);     
  }


  public function insertData($table,$data)   // Insert Data in any Table

  {
    $this->db->insert($table,$data);
    return true;
  } 

  public function insertDataGetLastId($table,$data)   // Insert Data in any Table

  {
    $this->db->insert($table,$data);
    $insert_id = $this->db->insert_id();
    return  $insert_id;
      //return true;
  }   


  public function updateData($table,$fname,$id,$data)  // update any Table by using Id

  {
    $this->db->where($fname, $id);
    $this->db->update($table, $data);
    return true; 
  }

  public function deleteData($table,$fname,$id)   // Delete Single Record From table by Id

  {
    $this->db->where($fname, $id);
    $this->db->delete($table);
    return true;
  }

  public function getDataByID($table,$fname,$id)   // Select Data form table by any particular Id
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($fname,$id);
    $query=$this->db->get();
    $result=$query->result();
        //echo '<pre>'; print_r($result);  echo '</pre>';exit();
    return ($result);   
  } 


   // select All Data from tabel by any particulat field name Order By Decending Order
  public function getDataByOrder($table,$fname) 

  {
    $this->db->order_by($fname,'DESC');
    $query=$this->db->get($table); 
    $result=$query->result();
       //echo '<pre>';print_r($result); echo '</pre>';exit();
    return ($result);     
  }


  // Check table for dupliacte value with any field name
  public function check_duplicate($table,$fname,$name)
  
  {

    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($fname, $name);
    $this->db->limit(1);
    $query = $this->db->get();
    if ($query->num_rows() == 1) 
    {
      return true;
    } 
    else 
    {
      return false;
    }

  }

  public function get_total_company_coin() // total  coin of company account

  {
    $this->db->select_sum('coin_amount');
    $result = $this->db->get('company_coin')->row();  
    return $result->coin_amount;
  }  

  function get_full_description($table_name, $select_filds, $where_condition, $order = null)
  {
    if ($select_filds != '') {
      $this->db->select($select_filds);
    } else {
      $this->db->select('*');
    }
    if ($order) {
      $this->db->order_by($order);
    }


    $this->db->from($table_name);
    $this->db->where($where_condition);
    $query = $this->db->get();
    $result = $query->row();
        //echo $this->db->last_query();
    return $result;
  }
  function insert_details($table_name, $insert_details)
  {
    $return_value = $this->db->insert($table_name, $insert_details);
    return $return_value;
  }

    /**
     * this function insert data and return last insert id
     * @param $table_name (database insert table name)
     * @param $insert_details (insert data array)
     * @return mixed (return last insert id)
     */
    function get_lastinsertid_after_insertdata($table_name, $insert_details)
    {
      $return_value = $this->db->insert($table_name, $insert_details);
      $last_insert_id = $this->db->insert_id();
        //echo $this->db->last_query();die;
      return $last_insert_id;
    }

    /**
     * this function use for insert batch details
     * @param $table_name
     * @param $insert_details
     * @return mixed
     */
    function insert_batch_details($table_name, $insert_details)
    {
      $return_value = $this->db->insert_batch($table_name, $insert_details);
      return $return_value;
    }

    /**
     * this function use for update data in database
     * @param $table_name (database update table name)
     * @param $update_data (update data array)
     * @param $where_condition (update condition array)
     * @param $orwhere_condition (update secound condition array like or where)
     * @return mixed (return update successfully YES or NOT)
     */
    function update_details($table_name, $update_data, $where_condition = null, $orwhere_condition = null)
    {
      if ($where_condition != '') {
        $this->db->where($where_condition);
      }
      if ($orwhere_condition != '') {
        $this->db->or_where($orwhere_condition);
      }
      $return_data = $this->db->update($table_name, $update_data);
         //echo $this->db->last_query();die;
      return $return_data;
    }

    /**
     * this function use for get listing data from database
     * @param $table_name (get listing database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (get data where condition if send black then no condition apply all details fetch else condition base data fetch)
     * @return mixed (return get data array)
     */
    function get_listing_details($table_name, $select_filds, $where_condition, $order_by = null,$limit=null)
    {
      if ($select_filds != '') {
        $this->db->select($select_filds);
      } else {
        $this->db->select('*');
      }
      if ($where_condition != '') {
        $this->db->where($where_condition);
      }
      if ($order_by) {
        $this->db->order_by($order_by);
      }
      if($limit)
      {
        $this->db->limit($limit);
      }
      $query = $this->db->get($table_name);
      $result = $query->result();
         //echo $this->db->last_query();
      return $result;
    }

    function check_time_already_booked($start, $end, $vander_id, $appointment_date)
    {
      $start = date("G:i", strtotime($start));
      $end = date("G:i", strtotime($end));
      $sql = "SELECT `appointment_id` FROM `appointment` WHERE
      `booking_date` = '$appointment_date' and `interviewer_id` ='$vander_id' and `start_time` BETWEEN '$start' and '$end' and `end_time` BETWEEN '$start' and '$end'";
      $query = $this->db->query($sql);
        // echo $this->db->last_query();exit;
      $result = $query->result();
      if ($result) {
        return 1;
      } else {
        return 0;
      }

    }
    public function discount_coupon($coupn_code)
    {
      $this->db->select('percentage');
      $this->db->from('coupn');
      $this->db->where("code", $coupn_code);
      $query = $this->db->get();

      if($query->num_rows() > 0)
      {
        $data = $query->result_array();
        return $data[0]['percentage'];
      } else {
        return 0;
      }
    }
    public function coupn_usage($coupn_code)
    {
      $this->db->select('coupn_usage');
      $this->db->from('coupn');
      $this->db->where("code", $coupn_code);
      $query = $this->db->get();

      if($query->num_rows() > 0)
      {
        $data = $query->result_array();
        return $data[0]['coupn_usage'];
      } else {
        return 0;
      }
    }
    public function update_coupn_usage($coupn_code, $data)
    {
      $this->db->where("code", $coupn_code); 
      $this->db->update('coupn', $data); 
    }
    public function email_address_lists()
    {
      $this->db->select('email_address');
      $this->db->from('users');
      $query = $this->db->get();
      return $query->result();
    }
    public function newsletter_email_address_lists()
    {
      $this->db->select('email_address');
      $this->db->from('newsletter');
      $this->db->where("verify_status", 1);
      $query = $this->db->get();
      return $query->result();
    }

    public function newsletter_email_address_export_lists()
    {
      $this->db->select('*');
      $this->db->from('newsletter');
      $this->db->where("verify_status", 1);
      $query = $this->db->get();
      return $query->result();
    }

    public function updated_shared_whiteboard($room_id, $data)
    {
      $this->db->where("room_name", $room_id);
      if ($this->db->update('appointment', $data)) {
        echo 1;
      } else {
        echo 0;
      }
    }
    public function fetch_shared_whiteboard($room_id)
    {
      $this->db->select('white_dashboard');
      $this->db->from('appointment');
      $this->db->where("room_name", $room_id);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
        $result = $query->result_array();
        return $result[0]['white_dashboard'];
      } else {
        return '';
      }
    }

    public function fetch_collage_list($id)
    {
      $this->db->select('*');
      $this->db->from('college');
      $this->db->where("university_id", $id);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
        return $query->result_array();
      } else {
        return array();
      }
    }



  }
