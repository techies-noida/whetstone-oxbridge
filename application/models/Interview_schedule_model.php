<?php
class Interview_schedule_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
   
  public function check_schedule_availbility($user_id,$schedule_date)  //check availibility

    {
      $this->db->select('schedule_date');
        $this->db->from('interviewer_availibility');
        $this->db->where('schedule_date',$schedule_date);
        $this->db->where('user_id', $user_id);
        //$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
          //echo 'find';exit();
          return true;
        } 
        else 
        {
           //echo ' not find';exit();
            return false;
        }
    }

  public function get_schedule($user_id)  //check availibility

    {
        $this->db->select('schedule_id,schedule_date,start_time,end_time,status');
        $this->db->from('interviewer_availibility');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $result=$query->result();
        
        return $result;
    }  


    public function get_schedule_timer($schedule)  //get schedule timer by schedule id

    {
        $this->db->select('schedule_id,schedule_date,start_time,end_time,status');
        $this->db->from('interviewer_availibility');
        $this->db->where('schedule_id', $schedule);
        $query = $this->db->get();
        $result=$query->result();
        
        return $result;
    }  

     public function student_appointment_booking($user_id,$user)  //get schedule timer by schedule id

    {
        $this->db->select(' appointment_id,a.schedule_id,a.start_time,a.end_time,booking_date,schedule_date,first_name,profile_image,a.created_date');
        $this->db->from('appointment as a');
        $this->db->join('interviewer_availibility as ia','a.schedule_id=ia.schedule_id');
         $this->db->join('users as u','a.student_id=u.user_id');
        $this->db->where($user, $user_id);
        $query = $this->db->get();
        $result=$query->result();
        //print_r($result); exit();
        
        return $result;
    }  

     public function interviewer_appointment_booking($user_id)  // Get Interviewer Appointments

    {
        $this->db->select(' appointment_id,a.schedule_id,a.start_time,a.end_time,booking_date,call_status,student_id,first_name,profile_image,room_name,attachment,a.created_date,a.message');
        $this->db->from('appointment as a');
        $this->db->join('users as u','a.interviewer_id=u.user_id');
        $this->db->where('a.interviewer_id', $user_id);
        //$this->db->order_by('booking_date','DESC');
         $this->db->order_by('appointment_id','DESC');
        $query = $this->db->get();
        $result=$query->result();
       // echo '<pre>';print_r($result); echo '</pre>'; exit();
        
        return $result;
    }  
    
     public function student_appointments($user_id)  // Get Interviewer Appointments

    {
        $this->db->select(' appointment_id,a.schedule_id,a.start_time,a.end_time,booking_date,call_status,interviewer_id,first_name,profile_image,room_name,a.created_date');
        $this->db->from('appointment as a');
        $this->db->join('users as u','a.student_id=u.user_id');
        $this->db->where('a.student_id', $user_id);
        //$this->db->order_by('booking_date','DESC');
        $this->db->order_by('appointment_id','DESC');
        $query = $this->db->get();
        $result=$query->result();
        //echo '<pre>';print_r($result); echo '</pre>'; exit();
        
        return $result;
    }  
    
    
    

      

     


     

}
