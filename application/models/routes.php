<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'oxbridge';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['about-us'] = 'oxbridge/about';
$route['terms-conditions'] = 'oxbridge/terms';
$route['privacy-policy'] = 'oxbridge/privacy';
$route['interviewers'] = 'oxbridge/interviewers';
$route['work-for-us'] = 'oxbridge/work_for_us';
$route['resources/personal-statement'] = 'oxbridge/resource';
$route['schools'] = 'oxbridge/school';
$route['become-tutor'] = 'interviewer/join_request';
$route['check-request-status'] = 'interviewer/request_status';
$route['request-status'] = 'interviewer/check_request_status';
$route['interviewers-list'] = 'interviewer/interviewer_list';
$route['change_password'] = 'loginSignup/change_password';
$route['logout'] = 'loginSignup/logout';
$route['profile-details'] = 'oxbridge/profile';

$route['book-now/(:any)'] = 'Appointment/pay/$1';
$route['checkout'] = 'Appointment/checkout';
$route['checkout-success/(:any)'] = 'Appointment/checkout_success/$1';

/*$route['twilio/user1'] = 'TwilioController/user1';*/
$route['twilio/user1/(:any)'] = 'TwilioController/user1/$1';
$route['create/token'] = 'TwilioController/create_token';






/******************************** Admin Panel ***************************************/

$route['admin'] = 'Admin/login';
$route['admin-dashboard'] = 'Admin/Admin_panel/dashboard';
$route['interviewer-request'] = 'Admin/Admin_panel/request';

$route['interviewers'] = 'Admin/Admin_panel/interviewers';
$route['students'] = 'Admin/Student';








