<?php
/**
 * 
 */
class Dashboard_model extends CI_model
{
	
	// function __construct(argument)
	// {
	// 	# code...
	// }
	public function no_of_driver()
	{
		$this->db->select('*');
		$this->db->from('driver');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return sizeof($query->result_array());
		} else {
			return 0;
		}
	}
	public function no_of_user()
	{
		$this->db->select('*');
		$this->db->from('user');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return sizeof($query->result_array());
		} else {
			return 0;
		}
	}
	public function driver_assign($week)
	{
		$this->db->select('driverId');
		$this->db->from('dutyschedules');
		$this->db->where('weekcommencing', $week);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		} else {
			return array();
		}
	}
	public function driver_name($id)
	{
		$this->db->select('firstName,lastName');
		$this->db->from('driver');
		$this->db->where('driverId', $id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$data = $query->result_array();
			return $data[0]['firstName'] . ' ' . $data[0]['lastName'];
		} else {
			return array();
		}
	}
	public function duty_assign($id,$date)
	{
		$this->db->select('schedule_Id');
		$this->db->from('dutyschedules');
		$this->db->where('driverId', $id);
		$this->db->where('scheduleDate', $date);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		} else {
			return array();
		}
	}
	public function schedule_name($id)
	{
		$this->db->select('duty');
		$this->db->from('schedules');
		$this->db->where('schedule_Id', $id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$data = $query->result_array();
			return $data[0]['duty'];
		} else {
			$output = "";
			return $output;
		}
	}
	public function driver_holiday_list()
	{
		$this->db->select('driverId');
		$this->db->from('holiday');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		} else {
			return array();
		}
	}
	public function holiday_using_id($id)
	{
		$this->db->select('startDate,endDate');
		$this->db->from('holiday');
		$this->db->where('driverId', $id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		} else {
			return array();
		}
	}
	public function User_log_listing()
	{
		$this->db->select('firstName,lastName,activity,userlogs.created_at');
		$this->db->from('userlogs');
		$this->db->join('user', 'userlogs.userlogs_iser_id=user.userId', '');
		$this->db->order_by('userlogs_id', 'desc');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		} else {
			return array();
		}
	}
	public function excel_driver_name($id)
	{
		$this->db->select('firstName,lastName');
		$this->db->from('driver');
		$this->db->where('driverId', $id);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$data = $query->result_array();
			return $data[0]['firstName'] . ' ' . $data[0]['lastName'];
		} else {
			return '';
		}
	}
}
?>