<?php
class  Twilio_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
   
  public function get_appointment_details($room_name)  //get appointment details

    {
      $this->db->select('appointment_id,student_id,interviewer_id,booking_date, start_time,end_time,room_name');
        $this->db->from('appointment');
        $this->db->where('room_name',$room_name);
        $query = $this->db->get();
         $result=$query->result();
         //echo '<pre>';print_r($result); echo '</pre>'; exit();
          return $result;
    }

     public function get_profile_pic($user_id)  //get appointment details

    {
      $this->db->select('user_id, profile_image');
        $this->db->from('users');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();
         $result=$query->result();
         //echo '<pre>';print_r($result); echo '</pre>'; exit();
          return $result;
    }

  public function check_room_name($room_name)  //check room name exist in table 

    {
        $this->db->select('room_name');
        $this->db->from('appointment');
        $this->db->where('room_name', $room_name);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
          //echo 'room find'; exit();
          return true;
        } else {
          //echo 'room not find'; exit();
          return false;
        }
    }  

   public function get_compose_List($user_id)  // composed  video list  

    {
        $this->db->select('room_sid,serial_number');
        $this->db->from('appointment_video');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $result=$query->result();
         //echo '<pre>';print_r($result); echo '</pre>'; exit();
          return $result;
        
    } 

    public function get_video_list($user_id)  // composed  video list  

    {
        $this->db->select('room_sid,interviewer_id,serial_number,url,created_date');
        $this->db->from('appointment_video');
        $this->db->where('user_id', $user_id);
        $this->db->order_by('created_date','DESC');
        $query = $this->db->get();

        $result=$query->result();
         //echo '<pre>';print_r($result); echo '</pre>'; exit();
          return $result;
        
    } 

     public function get_edit_video_list($user_id)  // composed  video list  

    {
        $this->db->select('room_sid,interviewer_id,serial_number,url,,video_url,created_date');
        $this->db->from('appointment_video');
        $this->db->where('user_id', $user_id);
        $this->db->where('edit_video_status',1);
        $this->db->order_by('created_date','DESC');
        $query = $this->db->get();

        $result=$query->result();
         //echo '<pre>';print_r($result); echo '</pre>'; exit();
          return $result;
        
    } 

  public function student_previous_feedback($user_id)  // composed  video list  

    {
        $this->db->select('review_id, student_id,interviewer_id,first_name,profile_image,review_date');
        $this->db->from('reviews as r');
        $this->db->join('users as u','r.interviewer_id=u.user_id');
        $this->db->where('student_id', $user_id);
        $this->db->where('user_type','interviewer');
        $this->db->order_by('review_date','DESC');
        $query = $this->db->get();
        $result=$query->result();
        //echo '<pre>';print_r($result); echo '</pre>'; exit();
          return $result;
        
    }   
    

  
    
    

      

     


     

}
