<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_model extends CI_Model {  
  
	function add_message($message, $nickname, $guid,$sender_id,$receiver_id,$appointment_id,$room_id)
	{
		$data = array(
			'message'	=> (string) $message,
			'nickname'	=> (string) $nickname,
			//'guid'		=> (string)	$guid,
			'timestamp'	=> time(),
			'sender_id'	=> $sender_id,
			'receiver_id'	=> $receiver_id,
			'room_name'	=> $room_id,
			'appointment_id'	=> $appointment_id,
		);
		  
		$this->db->insert('real_time_chat', $data);
	}

	function get_messages($timestamp,$sender_id,$receiver_id,$appointment_id)
	{
		$this->db->where('timestamp >', $timestamp);
		$this->db->where('sender_id',$sender_id);
		$this->db->where('receiver_id',$receiver_id);
		$this->db->where('appointment_id',$appointment_id);
		$this->db->order_by('timestamp', 'DESC');
		$this->db->limit(1); 
		$query = $this->db->get('real_time_chat');
		
		return array_reverse($query->result_array());
	}

	public function check_duplicate($message,$appointment_id)
  
    {
        
        $this->db->select('*');
        $this->db->from('real_time_chat');
        $this->db->where('message', $message);
         $this->db->where('appointment_id', $appointment_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
          return true;
        } 
        else 
        {
            return false;
        }
  
    }

}