<?php
class Appointment_model extends CI_Model{
    function __construct() {
         parent::__construct();

    }
   
  public function check_schedule_availbility($interviewer_id,$booking_date)  //check availibility

    {
      $this->db->select('appointment_id');
        $this->db->from('appointment_id');
        $this->db->where('booking_date',$schedule_date);
        $this->db->where('interviewer_id', $user_id);
        //$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
          //echo 'find';exit();
          return true;
        } 
        else 
        {
           //echo ' not find';exit();
            return false;
        }
    }

  public function get_schedule($user_id)  //check availibility

    {
        $this->db->select('schedule_id,schedule_date,start_time,end_time,status');
        $this->db->from('interviewer_availibility');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $result=$query->result();
        
        return $result;
    } 


  public function check_call_status($appointment_id,$status)  //check call status

    {
      $this->db->select('appointment_id');
        $this->db->from('appointment');
        $this->db->where('call_status',$status);
        $this->db->where('appointment_id', $appointment_id);
        //$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) 
        {
          //echo 'find';exit();
          return true;
        } 
        else 
        {
           //echo ' not find';exit();
            return false;
        }
    }
    public function get_call_status($appointment_id)  //check call status
    {
      $this->db->select('call_status');
      $this->db->from('appointment');
      $this->db->where('appointment_id', $appointment_id);
      $query = $this->db->get();
      if ($query->num_rows() == 1) 
      {
        $result=$query->result();
        return $result[0]->call_status;
      } 
      else 
      {
         //echo ' not find';exit();
          return false;
      }
    }


  public function get_remainder($date,$schedule_time)  //Appointment Reminder Mail before 1 hr

    {
           
             $time = date('Y-m-d H:i:s', time()); 
              $real_time = date('h:i A', strtotime($time));
            
          //$date = date('Y-m-d'); 
        $this->db->select('appointment_id,interviewer_id,student_id,booking_date,room_name,start_time,end_time,call_status,first_name,email_address');
        $this->db->from('appointment as a');
        $this->db->join('users as u','a.student_id=u.user_id');
        $this->db->where('booking_date', $date);
        $this->db->where('start_time <=',$schedule_time, 'start_time >',$real_time);
        $this->db->where('reminder_email_status ',0);
        $query = $this->db->get();
        $result=$query->result();
        //print_r($result); exit();
        
        return $result;
    } 

  //public function get_remainderSecond()  //Appointment Reminder Mail before 15 Minute
     public function get_remainderSecond($date,$schedule_time) 

    {
        $time = date('Y-m-d H:i:s', time()); 
        $real_time = date('h:i A', strtotime($time));
        $this->db->select('appointment_id,interviewer_id,student_id,booking_date,room_name,start_time,end_time,call_status,first_name,email_address');
        $this->db->from('appointment as a');
        $this->db->join('users as u','a.student_id=u.user_id');
        $this->db->where('booking_date', $date);
        $this->db->where('start_time <=',$schedule_time, 'start_time >',$real_time);
        $this->db->where('reminder_email_status ',1);
        $query = $this->db->get();
        $result=$query->result();
        //print_r($result); exit();
        return $result;
    }       
    
    
    

      

     


     

}
