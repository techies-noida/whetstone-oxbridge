<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student Information | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
    <link  href="<?php echo base_url();?>assets/js/imageuploader/css/fineCrop.css" rel="stylesheet">
    <link  href="<?php echo base_url();?>assets/js/imageuploader/css/layout.css" rel="stylesheet">
  
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> 
   
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup'); 
  $this->load->view('common/student_header');?>
  <!-- END MENU --> 
  <!-- end about -->
<section id="testimonial" style='background: #f6fef7'>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
             <!-- Start single team member -->
            <!-- <div class="col-md-9">

              <div class="title-area">
                <h2 class="title" >Edit Profile</h2>
                <span class="line"></span>           
              </div>
            </div> -->

            
              <div class="clearfix"></div>

              <!--  Left side bar -->
               <?php $this->load->view('common/student_sidebar');?>
              <!-- left side bar -->

                  <div class="col-md-9" >
                      <div style="margin-top: -50px">
  <!-- Tab panes -->
  <div class="tab-content">
    <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
                      <?php
                      if($this->session->flashdata('error')) {
                         $message = $this->session->flashdata('error');
                         echo'
                          <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
      <div role="tabpanel" class="tab-pane active" id="student">
           <div class="contact-area-right" style="margin-top: 40px">
                  <!--  <form method="post" action="" class="comments-form contact-form" > -->
                    <form  id="create_tutor_account" method="post"  onsubmit="return getpicture()" class="comments-form contact-form" role="form"  method='post'<?php echo form_open_multipart(base_url().'student/update_profile')?>
                       <div class="col-md-5">
                          <!--  <h4 style="color:#2aaebf !important; margin-bottom: 30px">Personal Information</h4>
 -->
                      <div class="form-group">  
                         <label style="color:#444; font-weight: normal">School name</label>
                      <input type="text" name="university" class="form-control" placeholder="" value="<?php echo $user[0]->university_id;?>" id="university">
                      </div>
                       <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Name</label>
                      <input type="text" name="first_name" class="form-control" placeholder="" value="<?php echo ucwords($user[0]->first_name);?>">
                      <input type="hidden" name="user_id" class="form-control" placeholder="" value="<?php echo $user[0]->user_id;?>">
                    </div>

                    <div class="form-group">                        
                      <label style="color:#444; font-weight: normal"> Email address </label>
                      <input type="text" name="email_address" class="form-control" placeholder="" value="<?php  echo $user[0]->email_address;?>" style="color: gray;" readonly>
                    </div>

                      <!-- <div class="form-group">  
                         <label style="color:#444; font-weight: normal"> Course name</label>
                      <input type="text" name="course" class="form-control" placeholder="" value="<?php  echo $user[0]->course;?>">
                    </div>

                     <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Year of study</label>
                      <input type="text" name="course_year" class="form-control" placeholder="" value="<?php   echo $user[0]->course_year;?>">
                    </div>
 -->
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Phone number</label>
                      <input type="text" name="phone_number" value="<?php echo $user[0]->phone_number;?>" class="form-control" placeholder="">
                    </div>

                    
                     
                     
                    

                    <!-- <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Gender</label>
                      <select class="form-control" name="gender" style="border-radius: 0">

                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Transgender">Transgender</option>
                      </select>
                    </div>
                            <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">DOB</label>
                       <input type="date" name="dob"  class="form-control" value="<?php  echo $user[0]->date_of_birth;?>">
                    </div> -->
                   
                      <!-- <div class="form-group">  
                         <label style="color:#444; font-weight: normal">About us</label>
                         <textarea class="form-control" name="about_us" style="height: 60px"><?php   echo $user[0]->about_us;?></textarea>
                    </div>  -->


                     <div class="col-lg-12" style="background: #f8f8f8; border:1px solid #f5f5f5; padding: 25px">
                               <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Your Display Picture</label>
                               </div>
                                <input type="file" id="upphoto" name="img" style="display:none;">
                                 <input type="hidden" id="upphoto1" name="picture" > 
                                 <input type="hidden"  name="img1" value="<?php echo  $user[0]->profile_image;?>" style="display: ;">
                                <label for="upphoto">
                                    <div class="inputLabel">
                                        click here to upload an image
                                    </div>
                                </label>

                            </div>

                             <div class="col-lg-12">
                               
                                 <?php  $image=$user[0]->profile_image;
                                  if($image=='')
                             echo '<img id="croppedImg" src="'.base_url().'assets/images/default.png" alt="team member img">';
                              else
                          echo '<img id="croppedImg" src="'.base_url().$image.'" alt="team member img">';
                              ?>
                            </div>


                    
                    
                   
                       </div> 
                       
                       <div class="col-md-offset-1 col-md-6">
                           
                          <!--  <h4 style="color:#2aaebf !important; margin-bottom: 30px">Academic Information</h4> -->
                    
                    
                    <!-- <div class="form-group">  
                         <label style="color:#444; font-weight: normal">School name</label>
                      <input type="text" name="university" class="form-control" placeholder="" value="<?php echo $user[0]->university_id;?>" >
                    </div> -->
                     <!-- <div class="form-group">  
                         <label style="color:#444; font-weight: normal"> Current course</label>
                      <input type="text" name="course" class="form-control" placeholder="" value="<?php  echo $user[0]->course;?>">
                    </div>

                     <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Current year of study</label>
                      <input type="text" name="course_year" class="form-control" placeholder="" value="<?php   echo $user[0]->course_year;?>">
                    </div> -->
                    

                   

                     <?php if($user[0]->country_id==''){?>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Country</label>
                       <select class="form-control" name="country" style="border-radius: 0">
                        
                        <option value="">Select</option>
                        <?php foreach($country as $row){ 
                         echo '<option value="'.$row->country_id.'">'.$row->country_name.'</option>';
                         }  ?>
                      </select>
                    </div> <?php }else{?>

                     <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Country</label>
                       <select class="form-control" name="country" style="border-radius: 0">
                        <option value="">Select</option>
                       <?php foreach ($country as $row) {?>
                        <option <?php if($row->country_id == $user[0]->country_id){ echo 'selected="selected"'; } ?> value="<?php echo $row->country_id;?>"><?php echo $row->country_name;?></option><?php }?>
                     
                      </select>
                    </div>
                  <?php } ?>

                   
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal"> University interested in</label>
                      <input type="text" name="desire_university" class="form-control" placeholder="" value="<?php  echo $user[0]->desire_university;?>">
                    </div>

                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal"> College interested in</label>
                      <input type="text" name="desire_college" id="desire_college" class="form-control" placeholder="" value="<?php  echo $user[0]->desire_college;?>">
                      <span style="color:black;font-size: 16px;font-weight: normal;">(If you are unsure at this stage please write unsure)</span>
                    </div>

                     <div class="form-group">  
                         <label style="color:#444; font-weight: normal"> Course interested in</label>
                      <input type="text" name="desire_course" class="form-control" placeholder="" value="<?php echo $user[0]->desire_course;?>">
                      <input type="hidden" name="alert" id="alert_status" class="form-control" placeholder="" value="<?php echo $user[0]->profile_status;?>">
                    </div> 

                           <button class="comment-btn" type="submit">Update </button>
                       
                       </div>

                        
                  </form>
                 </div>
      </div> <!--student form ends-->
</div>
                 </div>
              </div>
          </div>
        </div>
             
      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
 
  <?php  $this->load->view('common/footer');?>
  <!-- End footer -->
  
   <div class="cropHolder">
        <div id="cropWrapper">
            <img id="inputImage" src="images/face.jpg">
        </div>
        <div class="cropInputs">
            <div class="inputtools">
                <p>
                    <span>
                        <i class="fa fa-arrows-h"></i>
                    </span>
                    <span>horizontal movement</span>
                </p>
                <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
            </div>
            <div class="inputtools">
                <p>
                    <span>
                        <i class="fa fa-arrows-v"></i>
                    </span>
                    <span>vertical movement</span>
                </p>
                <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
            </div>
            <br>
            <button class="cropButtons" id="zplus">
                <i class="fa fa-search-plus"></i>
            </button>
            <button class="cropButtons" id="zminus">
                <i class="fa fa-search-minus"></i>
            </button>
            <br>
            <button id="cropSubmit">submit</button>
            <button id="closeCrop">Close</button>
        </div>
    </div>

     <!-- alert  Modal content-->
      <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="alert_model" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header " style="width: 100%;background-color:#155760;height: 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Profile details incomplete</h4>
              </div>
              <div class="modal-body" >
                <p>You must complete your profile details before you can book your first interview.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>

      <!-- model  -->

   
    
    
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/imageuploader/js/fineCrop.js"></script>
    
     <script>
        $("#upphoto").finecrop({
            viewHeight: 500,
            cropWidth: 200,
            cropHeight: 200,
            cropInput: 'inputImage',
            cropOutput: 'croppedImg',
            zoomValue: 50
        });

         function getpicture(){
          $("#upphoto1").val($("#croppedImg").attr("src"));
          return true;
        }
    </script>

    <!-- alert popup -->
    <script type="text/javascript">
    $(window).on('load',function(){
         var msg=$("#alert_status").val();
         //alert(msg);
         if(msg=='incomplete'){
          $('#alert_model').modal('show');
         }
       // 
    });
    /* code by ravindra 26-02-2019 */
    function testInput(event) {
      var value = String.fromCharCode(event.which);
      var pattern = new RegExp(/[a-zåäö ]/i);
      return pattern.test(value);
    }
    //$('#university').bind('keypress', testInput);
    //$('#desire_college').bind('keypress', testInput);
</script>
    
  </body>
</html>