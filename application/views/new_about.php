<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> New About us</title>
     <?php $this->load->view('common/header_assets');?>
    <style>
        #fenellasabout p{color:#eee !important}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>About Us</h2>
              <p>As a professional teacher…</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">About</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
   <!-- Start about  -->
  <section id="about" style="border-bottom:1px solid #eee; padding-bottom: 0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">About us</h2>
            <span class="line"></span>

            <!-- <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p> -->
          </div>
        </div>
        <div class="col-md-12">
          <div class="about-content">
            <div class="row" style="background: #555">
                  
              <div class="col-md-9">
                <div class="our-skill">
                                    
                  <div class="our-skill-content" id="fenellasabout" style="padding-top: 40px; padding-left: 35px; color:#eee">
                     <?= $about[0]->body_1;?>
                    <?php  // echo $about[0]->image_1;?>
                 <!--  <p style="color:#eee">As a professional teacher and recent Cambridge graduate (First Class), I decided to set up Whetstone as a valuable way for both Oxbridge applicants, and for the teachers and parents supporting them, to prepare for interviews at Oxford and Cambridge.</p>
                  <p style="color:#eee">We provide an online interview practice service for students from students. Whetstone connects applicants with current Oxford and Cambridge students enrolled in the course and even college that they are looking to apply to. Applicants then hone and sharpen their interview skills whilst encountering the most up-to-date Oxbridge interview questions and advice from students who successfully went through the process only a year or so before.</p> -->
                   
                  </div>                  
                </div>
              </div>
                <div class="col-md-3" style="max-height: 340px; overflow: hidden; padding-right: 0">
                    <!-- <img src='<?php //  echo base_url();?>assets/images/blog-img-1.jpg'/> -->
                      <img src='<?= FILE_PATH.$about[0]->image_1;?>'/> 
                </div> 
                           
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end about -->
   
  <!-- Start about  -->
  <section id="about" style="padding-top:0">
    <div class="container">
      <div class="row">
      
        <div class="col-md-12">
          <div class="about-content">
            <div class="row">
              <div class="col-md-12">
                <div class="our-skill">
                  <h3>Why is interview practice important? </h3>                  
                  <div class="our-skill-content">
                     <?= $about[0]->body_2;?>
                  <!-- <p>When you study at Oxford and Cambridge you are taught in weekly one-on-one supervisions (Cambridge) or tutorials (Oxford). The Oxbridge interview is designed to be as close to this experience as possible. The interviewers are going to want to see that you can thrive and not crumble under the pressure of on-the-spot analytical thinking.This is very different from the way applicants are often taught to approach problems in school. </p>
               <p>The interviewer will want to see that you can engage with questions in a thoughtful and thought-provoking manner. It’s not how much you know, it’s how you think. And this, like any new skill, needs to be practiced. </p>
               <p>Whetstone offers you the ability to practice these skills in an accessible way. With our online interview preparation and practice sessions you can take your own time and process feedback at your own rate. We know that preparation for these interviews can be daunting, however we’ve all done it and we’re here to help you now succeed. </p>
               <p>It doesn’t matter how knowledgeable you are, if you are too nervous to respond to questions in the way they are hoping to see then you will not be successful.</p> -->
                   
                  </div>                  
                </div>
              </div>
                       
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- Start Service -->
  <section id="service">
    <div class="container-fluid">
      
        <div class="row">
            <div class="side">
                <div class="side-content">
                    <div>
                        <h4 style="color:#fff; opacity: 0.5"><?= $home[0]->school_heading;?></h4>
                        <h2><span style="color: #ffffff;"><?= $home[0]->school_content;?></span></h2>
                    <a itemprop="url" href="<?= $home[0]->school_link;?>" target="_self" style="color: #ffffff" class="eltdf-btn eltdf-btn-medium eltdf-btn-simple eltdf-btn-arrow">
                        <span class="eltdf-btn-text">Read More</span>
                    </a>		
                    </div>
                </div>
                <div class="side-img" style="background-image: url(<?php echo base_url().$home[0]->school_image;?>)">
                </div>
            </div>
      </div>
    </div>
  </section>
  <!-- End Service -->
   <?php  $this->load->view('common/newsletter');?>
  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
    
  </body>
</html>