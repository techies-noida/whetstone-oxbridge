<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Become Interviewer | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>   
     <style type="text/css">
       .required:after {color: red;content: '*';}
 .l-error{color:red;}.error {color: red;font-size: 15px;font-weight: normal;}.form-control{color: black;}
     </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Start header -->
   <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  
  <!-- END MENU --> 
  <!-- Start subscribe us -->
  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
             <!--  <h2 class="wow fadeInUp">Join for become tutor  </h2> -->
              <p>If you are interested in becoming on of our interviewers & sharing your knowledge with the next generation then please fill up the form below and submit an application</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End subscribe us -->
  <!-- end about -->
<section id="testimonial" style='background: #fff'>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >Request For Become Tutor</h2>
                <span class="line"></span>           
              </div>
            </div>

                  
            
                  <div class="col-md-10" style="float: none; margin: auto">
                 <div class="contact-area-right">

                  <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
                      <?php
                      if($this->session->flashdata('error')) {
                         $message = $this->session->flashdata('error');
                         echo'
                          <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
                   <form  id="request_join_tutor" method="post" class="comments-form contact-form" role="form"  method='post'<?php echo form_open_multipart(base_url().'interviewer/add_join_request')?>
                       <div class="col-md-6">
                           <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Name</label>
                      <input type="text" name="name" class="form-control" placeholder="" >
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">  University email address </label>
                      <input type="email"  name="email_address"class="form-control" placeholder="" >
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">University</label>
                      <!-- <input type="text" name="university" class="form-control" placeholder=""> -->
                      <select class="form-control" name="university" id="university" onchange="return get_college(this.value)" required>
                        <option value="" >Select University</option>
                        <?php foreach ($university as $row) {?>
                        <option value="<?php echo $row->university_id;?>"><?php echo $row->university_name;?></option><?php }?>
                      </select>
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">College</label>
                     <!--  <input type="text" name="college" class="form-control" placeholder=""> -->
                     <select class="form-control" name="college" id="college_id">
                          <option value="">Select University first </option>
                      </select>
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Course</label>
                      <input type="text" name="subject" class="form-control" placeholder="" required>
                    </div>
                    
                       </div>
                       
                        <div class="col-md-6">
                           <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Main academic interests</label>
                      <input type="text" name="working_as" class="form-control" placeholder="" required>
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">What do you wish to told before your Oxbridge interview</label>
                      <input type="text"  name="wish_to"class="form-control" placeholder="" required>
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Extra curricular interest</label>
                      <input type="text" name="extra_curricular" class="form-control" placeholder="" required>
                    </div>
                     <div class="form-group" style="color:#888">                 
                          <label style="color:#444; font-weight: normal"> Upload picture</label>
                      <input type="file" name="image" style="height: auto;">
                    </div>
                     <div class="form-group" style="color:#888">                        
                      <input type="checkbox" name='terms_condition' style="height: auto;"> I agree with GDPR Policy
                    </div>
                        <button class="comment-btn" type="submit">Submit</button>
                       <!--   <button class="btn signin-btn" type="submit">Login</button> -->
                       </div>
                  </form>
                 </div>
                 </div>
         
              </div>
          </div>
        </div>
        <div class="col-md-6"></div>        
      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
  <!-- start script for validation  -->
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
   <!-- End  script for validation  -->

   <!--  Get college list on select university  -->
    <script type="text/javascript">
        function get_college(university_id) {
          //alert(university_id); 
            $.ajax({
                url: '<?php echo base_url();?>interviewer/get_college/' + university_id ,
                success: function(response)
                {
                  //alert(response);
                   jQuery('#college_id').html(response);
                }
            });
        }
    </script>
    
    
  </body>
</html>