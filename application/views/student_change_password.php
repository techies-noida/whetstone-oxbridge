<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Change Password | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
    <style>
        .sidebar-widget{float:none; display: block;}
        .team-member-name{float:none; display: block;}
        .single-team-member{float:none; display: block;}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
   $this->load->view('common/student_header');
  ?>
  <!-- END MENU -->

  <!-- end about -->


<section id="testimonial" style='background: #f6fef7; padding-top: 0'>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding: 0">
          <div class="container">
          <div class="row">

              <br>

                  <div class="col-md-12" style="padding: 0" >
                      <div style="margin-top: 0px">
  <!-- Tab panes -->
  <div class="tab-content">


    <div role="tabpanel" class="tab-pane active" id="student">


    <div class="contact-area-right" style="margin-top: 0px">
    <form id="change_password" method="post" style="margin:0; padding:0" action="<?php echo base_url();?>loginSignup/update_password" class="comments-form contact-form">
         <!--  left sidebar -->
         <?php $this->load->view('common/student_sidebar');?>
         <div class=" col-md-9 col-sm-6 col-xs-12">
             <div class="content-wrapper">
                <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Change Password</h4>



           <?php
        if($this->session->flashdata('success')) {
           $message = $this->session->flashdata('success');
           echo'
            <div class=" alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fa fa-check-circle"></i>'.$message['message'].
            '</div>';
        }?>
        <?php
        if($this->session->flashdata('error')) {
           $message = $this->session->flashdata('error');
           echo'
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <i class="fa fa-check-circle"></i>'.$message['message'].
            '</div>';
        }?>
             <div class="form-group">
        <label style="color:#444; font-weight: normal">Old Password</label>
        <input type="password" name='old_password' id="old_password" class="form-control" placeholder="">
      </div>
      <div class="form-group">
        <label style="color:#444; font-weight: normal">New Password</label>
        <input type="password" name="new_password" id="new_password"class="form-control" placeholder="">
      </div>
      <div class="form-group">
        <label style="color:#444; font-weight: normal">Re-type Password</label>
        <input type="password" name="confirm_passwords" id="confirm_passwords" class="form-control" placeholder="">
      </div>
          <button class="comment-btn" type="submit">Change </button>
         </div>
    </form>
  </div>
  </div>
  </div> <!--student form ends-->
</div>
                 </div>

              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
  </body>
</html>
