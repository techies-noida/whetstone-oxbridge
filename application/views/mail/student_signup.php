<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        body
        {
            margin: 0px;
        }
        .wrapper
        {
            width: 100%;
            height: auto;
            background-color: #edf1f7;
            padding: 55px 300px;
            box-sizing: border-box;
        }
        .wrapper-content
        {
            background-color: white;
            border-top: 5px solid #0084ff;
        }
        .content-header
        {
            height: auto;
            width: 100%;
            background-color: white;
            border-bottom: 1px solid #eeeeee;
            padding: 20px;
            box-sizing: border-box;
        }
        .content-body
        {
            width: 100%;
            height: auto;
            padding: 20px 20px 10px 20px;
            box-sizing: border-box;
        }
        .text-gray
        {
            font-size: 16px;
            font-weight: 600;
            color: #818e94;
            line-height: 1.2;
        }
        .text-dark
        {
            font-size: 16px;
            font-weight: 600;
            color: #2d3039;
            line-height: 1.2;
        }
        a
        {
            color: #0084ff;
        }
        .text-regard
        {
            font-size: 16px;
            font-weight: 600;
            color: #818e94;
            line-height: 0;
        }
        .content-footer
        {
            width: 100%;
            height: auto;
            padding: 0px 20px 20px 20px;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="wrapper-content">
        <div class="content-header">
            <!--LOGO-->
            <img src="<?php echo 'https://167.99.231.76/assets/images/core.png';?>" alt="logo">
        </div>
        <?php // print_r($mail_details);
         $user_id=$mail_details['user_id'];?>
        <div class="content-body">
            <p class="text-gray">Dear <?php echo ucwords($mail_details['user_name']);?></p>
            <p class="text-gray">Thank you for signing up with Whetstone Oxbridge, we look forward to supporting you with your application.</p>
            <p class="text-gray">Please click on the link <a href="<?php echo 'https://167.99.231.76/loginSignup/verify_account/'.$user_id;?>"> <b>Click here </b></a>';</p>
           
            <p class="text-gray">Once you have successfully complete your profile you can book your first interview..</p>
           
        </div>
        <div class="content-footer">
            <p class="text-regard" >Many thanks,</p>
            <p class="text-regard" >Whetstone Oxbridge Team </p>
        </div>

    </div>
</div>
</body>
</html>