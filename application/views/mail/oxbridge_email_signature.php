<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Squads</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
        <style>
            .main-wrapper{width: 50%}
            @media (min-width: 768px) and (max-width: 1024px) {
                .main-wrapper{width: 70%}
            }
            @media (min-width: 481px) and (max-width: 767px) {
                .main-wrapper{width: 90%}
            }
            @media (min-width: 320px) and (max-width: 480px) { 
                .main-wrapper{width: 100%}
            }
        </style>
</head>

<body style="height: 100%; background: #f1f1f1; font-family: sans-serif; font-size: 15px; color: #676a6d; margin: 0;">
	<div>
		<div  style="position: absolute; width: 100%; height: 100%; display: table;">
			<div  style="display: table-cell; vertical-align: middle; ">
				<div class="main-wrapper" style="border:1px solid #eee; border-top: 3px solid #2aaebf !important; min-height: 400px;  margin: 0 auto; background-color: #fff; ">
<!--                                    <table style="width: 100%; padding: 15px; border-bottom: 1px solid #eee">
                                        <tr>
                                            <td style='text-align: left; vertical-align: middle'><img src="assets/images/fvms_icon.png" height="40"/><img src="assets/images/trakie fvms whitr.png" height=28"/></td>
                                            <td style="text-align:right">
                                                <a href="#"><span style="margin: 0px;"><img src="assets/images/fb.png" alt="Facebook" style="width: 30px;"></span></a>
                                                <a href="#"><span style="margin: 0px;"><img src="assets/images/tw.png" alt="Twitter" style="width: 30px;"></span></a>
                                                <a href="#"><span style="margin: 0px;"><img src="assets/images/in.png" alt="Mail" style="width: 30px;"></span></a>
                                                <a href="#"><span style="margin: 0px;"><img src="assets/images/g.png" alt="Mail" style="width: 30px;"></span></a>
                                                <a href="#"><span style="margin: 0px;"><img src="assets/images/i.png" alt="Mail" style="width: 30px;"></span></a>
                                            </td>
                                        </tr>
                                    </table>-->
                                    <div>
						<div  style="padding: 28px 0px; padding-bottom:0 ">
							<div  style="padding: 0px 60px;">
                                 <?php echo $msg;?>
                                                           <!--  <h4 style="color: #2aaebf !important; font-size: 18px">
                                                                Congratulations !
                                                            </h4>
                                                                <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Thanks for signing up for <b>Whetstone Oxbridge</b>. Please confirm your registration by clicking on following link <span></span></p>
                                                              
                                                                <a href="#" class="btn btn-lg" style="color: #fff; text-decoration: none;
			    background: #2aaebf !important; border:0; box-shadow:0; display: inline-block;
			 border-radius: 4px;padding: 10px 60px;  margin-top: 20px; margin-bottom: 20px; clear: both">Confirm Registration</a>
                                                                <p  style="font-size: 13px; letter-spacing: 0px; line-height: 1.3; font-family: sans-serif;">Or copy the following link into your browser<span></span></p>
                                                                <a href="#" style="font-size: 13px; margin-bottom:40px; display: inline-block; color:#2aaebf !important">https://www.whetstone-oxbridge.com/register=1?succesd=1</a> -->
                                                            <div class="footer">
                            <!--<p>© 2018| All Rights Reserved</p>-->
                            <ul style="list-style: none; margin: 0; padding: 0">
                                <li style="list-style: none; display: inline-block; color:#3b3b3b; font-size:12px;"><a style=" color:#888; text-decoration: none; font-size:12px;" href="#">Ignore this email, if this email does not belong to.</a>.</li>
                               
                            </ul>
                        </div>
							</div>
						</div>
					</div>
                                   
                                    <div>
                                        
				</div>
				<div style="text-align: left; border-top: 1px solid #eee; margin-top: 40px; padding: 10px 15px">
					<!--<p style="margin-top: 15px; font-size: 25px;">Stay in touch</p>-->
                                        <img src="https://www.whetstone-oxbridge.com/assets/images/logo.jpg" style="height:135px"/>
                                        <p style="margin-top: 12px; font-size: 12px;"><a href="mailto:info@whetstone-oxbridge.com" style="color:#888">Reply to this email</a> <br/> </p>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
