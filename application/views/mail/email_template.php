<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        body
        {
            margin: 0px;
        }
        .wrapper
        {
            width: 100%;
            height: auto;
            background-color: #edf1f7;
            padding: 55px 300px;
            box-sizing: border-box;
        }
        .wrapper-content
        {
            background-color: white;
            border-top: 5px solid #0084ff;
        }
        .content-header
        {
            height: auto;
            width: 100%;
            background-color: white;
            border-bottom: 1px solid #eeeeee;
            padding: 20px;
            box-sizing: border-box;
        }
        .content-body
        {
            width: 100%;
            height: auto;
            padding: 20px 20px 10px 20px;
            box-sizing: border-box;
        }
        .text-gray
        {
            font-size: 16px;
            font-weight: 600;
            color: #818e94;
            line-height: 1.2;
        }
        .text-dark
        {
            font-size: 16px;
            font-weight: 600;
            color: #2d3039;
            line-height: 1.2;
        }
        a
        {
            color: #0084ff;
        }
        .text-regard
        {
            font-size: 16px;
            font-weight: 600;
            color: #818e94;
            line-height: 0;
        }
        .content-footer
        {
            width: 100%;
            height: auto;
            padding: 0px 20px 20px 20px;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="wrapper-content">
        <div class="content-header">
            <!--LOGO-->
            <?php $image='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAABZCAYAAAC5Ui/8AAASuklEQVR4Xu2de2xTV57HPzXBsULipKmbBicyJITghmAyaQNZYHZb6M6q7O6UaiQqjcSIUekgLQtSKqpGG4mKSulQFW0lUCsxomoF0qyK1F2qaqi2W6g6AyxJWiaEJONJjZN6HROCa0ISLNs47upcX8fPvGwj03Lun+Sex/2ez/m9zvXlobr//OJ75CUVyIECfc8/xUMSwBwoL4dUFJAAShByqoAEMKfyy8ElgJKBnCogAcyp/HJwCaBkIKcKSABzKr8cXAIoGcipAhLAnMovB5cASgZyqoAEMKfyy8ElgJKBnCogAcyp/HJwCaBkIKcKSABzKr8cXAIoGcipAhLAnMovB5cASgZyqoAEMKfyy8ElgJKBnCogAcyp/HJwCaBkIKcKSABzKr8cXAIoGcipAhLAnMovB5cASgZyqoAEMKfyy8ElgJKBnCogAcyp/HJwCaBkIKcK/CAALDIYaa0to2qxBqaCWJ1DHBoaJ5BT6eTg2VDgBwGgscLMsSfKqV4kHjmEdbCfnd1uJrKhgOwjpwqkB6CmkO1PmNll0JIvpj8VoPPaAG3XZrJKeTStNHOgqpCJUTv7u0dxpXxsHZvXmGmpLECv/D2I3WnniMfA6xLAnIJyrwZPD0A0WFbUc9RSikGd2fhNB/s67HTdTTHV/FJeba7jV6V5+O+4OdTRy6nbKe4rKKN9vZltJZrwH6e8nO7u5Z0pk7SA94qAHPebJoBAYTlvNdeytUiFJTDJh5d7ef26L+mRHimr4WhTJWu14k8Bzvb0su/aeNJ9VSYzRxoirhbGx1y0dQxgfVi64Bxzcs+GTx9AdGxuqKe9qlB1lyHs3w6w7/IIg3HT1fGsep/irgH3TQf7k6xlAc811fNaZUHYrRPkykA/u/s8FOUyBlxcwvbVJp4tCHBuwMZJdzDjxaiqqOZflunJn3Dx9tXRBL0y7v4H1UEGAEJRWTXHmkyqZQPuuDnY1c+pW6GoCIluVTGC4xzv6uXt0Zg8ttjIkfU1bFmiWlTfGP/e1ct77iC5SkJE9n1gTTVbS/IAH2e+7uYVR7KFn/+Kixi3ltaqUioWwbjHwb4/2emKkWv+ff047swIQDR6djTX0/qY4lsV93q+t5e930STkUS3Gr4viHXQym+63XyntgzHlIbpmNJ9w8beS056QiQBaHe66Fmsx1Kow7A4j3xCTISCjE9Mct7h4F3H+IwZsra4jD21RjY/XBBuuyjExN0g7tsePhwY4pRbbIo8LMtraH28nLW66EL7AwEmVFjcHgcHu8T88rCYTOyqMrC2QEvRYg3+qSCBUAj3xDid10c4ec2Dq9BAi6WGHY/pVAsvYtwg43dD4XLSlJczV628GRPCPGIwsmdFOU3FOvSL8yhaFMJ/N4TbN0mP08Xxa24G4+DV0LSyjgM1evSEcLsdvHOzkG2VesxFYm555E8Flecdvj3G6WuR500Fs5amFdXsEm1jnmvC7+XKt07eTRo7vQ2RGYBAlamOIw1laokkvKv3X7JzwS8mlOhWYxZzbIS2DiufeiEJ5CkfZ3p6eWVoUmkQbwHnetAgV761cbB7hL/GLY4QtIZWcxnmyH5J7CowyemrVtqcebT8tJ5dpcLypb4U63VhBKPFTOsyseAzX26Pi2OuPHbWl1Ex420xFlZTyHOWGl42lWBQSk8zzGFslHf77JwcjVjlPDausXBYAXAeV8DLGesAr18bi9uwYpO2rKnmhUdjNktcdyGGbzg5+HVknecx1gy3ZAwgBQYOrK/jhUjmGpuMJLjV8TsB8pdESjfhDLfN4aXIYOJIUzXrVGvjnxih7aIK54IBFE/q42xPP/uny0IaLMvNtFuiG2UmyfwToxz6+joVltVzAvhbu5aXfhJNmmaG1clv/0/Lv66dB4BODc811NG6LBJbz764oqrw9uUBTqqWe0EAKlKNhcOhSGxbYODVxlp+9ehMuzQynyBXbP3sveqZ9mLpYJg5gOSxcXU9h2tLkpIR7fJ6jv7EoO56H2dtY1SYylULFGLYaWN31wiG1RaOTLdPds/JFjDIsMfD2W9dHB8aY0K4VfNythsj9UNhiZ3sv2RTLLG22Mjh2PhyKsAVcZpyVbjyUloaamNcY5DOgV52903SFGdNEmNALZsbLByuKlRdaoDzA1b+rc/DdxodG6uWs6uqlLX5QT692k+bY1KpHBzZYGbLkvBSpYoBVy2v46ilTIkRlWvKx5Xro5y0OTh7V88LNUa2VRjirLj7hp39XQ667qawgFMBrDfcnB50KJbSuNREi9mkxrViAJE8Wtl9WdRmtWxcUx9nQccn3Jzqs/P29UA4LFldPh3zK4bikpVPw44qrSsLAIL24UqONtWwSRVWJCOHuhxQW0erMWzW/HdGOdg1gtkSrgcql5K0uKiw1EWtTYoEJTEJiQoW88yJyY6SxPTwnhuaVlo4XF+ixpfJ2boy/+YaNqkWWMSfv7kU3hhRd5YIoFjsBo7WRAAM4fa4OTEwxMnrXvWYUIOxUIff6+U7EQ7MBWBMvTQM3ySnuwW8Ik6JXo8YTLzRWB3VOzDOia5e3hwNJbhgH2f/3Ms+NZSJ9PBIWTVHY5LH6c26qIz2DWa2RUprMYlguG3Cppua5MOu1KW3+dKYFQCTY70AnbYRApUmdVFD2J0D7OsaxRAHg7AaboqWG6d3lfumnf0dYjdHH2F+WbCOZxsttC9TyziiiP11D23X89jRVD+9EUQGHl6smAw8YeHDlsmBdlYAwbi0hsONkfpmdL5+nxfr7TE+tYWtzvQ1B4CJVYXYRCx+QRMt1XytttpLIujCEHT0craght81Vk5b12HXALs7XHFlolUJXi3TykCWABSJQi1HG4zTkx+f8EKR6hIjMAz7wtYy1tpMeNFG7puhSD0/ABMtkmqxruto2TB7QpG4W8dv2tl90UXRHAAKi7BxZS2vPW6Iusy4zkIM3xzhnT4bH4vS1BwAGk1mjk0X4sMx1u6rnpQZfbImvezsHscya9igTk6j58UNFl5+VPVEPg+HLvZwvriOYw0x7n8uMxazrnPdOtPfswYgibsqZkR/XMZbyHZRcFZdc+zEFDfd0c/HCcd08wcwhct06ubMaBPFETt/r7DWSQD28EqCOxRtRblk14pythj0VKSI3cc9Ltq6BjinSREDXoweX8YDGH7pIrZUFTvPJE1svey8ugAAm+t5OVI+UwE8W1zHB0/MliglKJXKmyyQxOwBKM6HV9ZztD56PhyeixDSyt6YFxCSAm31vpSxXVIWPNPbMIkBuGoBnQkumBDDY5O4Zyr+Tr/uNcnauOQoucaZrLWWpuUmtleWsq60IKaEIrLyXvbdKIw7vvSPudjfMcA5NcSLP7KMT6QSXXB8AqT2f82XFAOmdJHCAqYA8EyCC8bn5Yp35pMfv3eMk3326fkvkD3l9iwCCBSX89b6WrZGTjPECKl2iUgYYoNd5b5JPuzu5fXh5JOGjCygI8TmNRbaa6JHhsMukTWGi9yJV1G+Fr8/oCQRq1bU87vp4niyRdI+XE5LlR6XMyHWS9qMqjsdQAkHpktWwvJc6uVk5OQosaRFgCs2G20Jx3VVFbW0N0TjZu54ONQl+tFkBOCHiGpB7XSWrqzd5f644vi0XhotxkVBXHczO8bJLoCIRKCe9mWRzHCmXZx8PhxbNkmEIjMAfclHhsIKejyc9/hQ6uVoMBQVYi4upIJx3lGy5xCJFgnfJGevj3Da5uLcpDbmjDuI3TNOj8fDmUEXgwVG9jxuYltp9IQo/AJGkO3rLTHhh4gRR/nUMcIJxxjfKW8Z1XHYEildhT2Ie2yMzlte3FNinnrWGQpjrGuQTls/+5V63AxeIPH4cAYLePJ2ioODgJfO62NYVdDyF+uoKi7EXJSH2zmglm/SsX3hNlkGEOJTfJGd9bOvLzmQTnxDprO3n93fjKV8yzlTABEbo6FeeR9x7hMCH2f+3MMrQ15EXNvSHFMiUnUWrrPV6uWf1oiz67nFF8Xtg11WPr4dInX4EePe1XctWyrnM1dRXbDT1u2kR6kaZAogFBkqOayUeNQz+VkeT6kWxMSwcyuRfEfWAYw7VguMcbwjpsoeO/7iElrW17FLVNynXUhqc545gMBiPdvX1LCnUj/r8Zawcqf7rOHCsUgwyky80bA8bkHC5QkPxpXVvFxjwKybebFEIfd4j433IuUYTUHySUdimKIexe2pKEmZ1CgTC/g477BzqG805jw4cwBF18al1by22simopmPIoVlzsZxXPYBFLGTycxr5hLyb4nzwtSxlnB7lhVmXluhx+8eoi3pNa4orfP7TYiGVaZq9pjCgPnvejk3YFePqKJ9PWIoZ1dVGWsLdVTohMAh3N4A43d9WG+Mcmow8YAfyC/huZpytjyswyCSKscQhxyRFy7CLyPsqNRTIV4YyNfCXR8TdwNYnU7eGRKuNfHSULXUyI7KUqoKNOD1cLzPwYX4ejPawlLl5GNzsRZDvg49QYa9PoZvezg96OLCZOKGnZ8GwiNsXl3NDkP4rDdlMqEpYPOKSrY9VkhVgQ69omkAty+Ie3Kcs04nHyvHf5ld9wTAzKYkWz9ICkgAH6TVvg+fVQJ4Hy7KgzQlCeCDtNr34bNKAO/DRXmQpiQBfJBW+z58VgngfbgoD9KUJIAP0mrfh88qAbwPF2XmKRWysUJH4JabroSidXqPke3+Fj4LCeDCNcu8RXEpL1aVY14MAa841RiZJ1BmPnnegLvnPL++Np9p6Gl/ppFnp5w0fmFL0WCh/c1nzIXdIwFcmF4Z3l3Iixvq2fOYDv8dr/JOYn5+ARVa8dqVlV9e9czR/8KB2bjSzFZGafsmVd8L7y9DAZKaSwCzregs/e3YtInWYi8nui7z5mjkRh3PNllor9Rypecrfn1tti8vZBuYbPe3cDElgAvXLL0WBbV89A/l+Hv/yC+/SexCx4Gnm3lh0Qh///kQWzY9SWvxJIe+6OYklbz/dA0V7n52dpRy7Pky9HdCFC0RX4SA8YlR3r7YzykvbN/QzB7NJNaCUjYtCXL20kXc5r9lk9/Gzy6GP4hnFD/7fLwUs/IihriCdEZcelkN7zcYWae8ihXEPhHEUKShp/MiuydNHFtvYtOS8AscdpdN+cFS6s/szV8iCeD8tcrszhUN/K9Fy7n/6qQtVU+N6+hbGuDQHwR0pbz1s3o2eYc4jok9Ojd7P7dyAWGxytGLjztdttO12MT7m6pZO2mn8UsHioV9NE/5+NMhp5fxoRG2PvMUm302/ua8E0wWvnxCz/Cg+uUIpb9oTPnq00+x7a6NX5x34iqr47ONJQx+fZHdjhLan7Gw2TfEzvMOJirq+WCdnkEB5nBmskgAM9Nv/q0VAHWc/+9LvJIigzU2NfM/ZT4O/qGbU6LXMjOfNJdTzSQnPv+KN5U2KVxmDLgoLn5ShTg8tfYYAAWgLYtcNH5pV+c9C4DFtXyy2YBLQOav57Of6hnssXFaiRD07Ggwku/4I7+4On8JUt0pAcxMvwW0NvH7n1dT4bbydxdHEtoJi2dhiz9syZRLWKBm8Qs1L6cvddKmxIwpAFzzJH2moAJuvgBQ52b159bp/mMBjFg4xRoqV2x/lbz/j8sxk4defAQJGL6hutkKC1+uK4EJH9GvOgbp+ctl2qQFXAADOb51Y+M6ji7T4XbZ2Nvh4q9iPsXlHGio4YVCNeYTlq4gGvedXFxLS4GHvZ/1qy44pgxTHHbB1bfCUCsueBYAm5qa+WBpkBNffsWbt8Vvuev5j3Ul2EUMSNhCd/XaODmcWGcMbx7DDRGHqp9XLi5k1e3J8DNkcEkLmIF46TQVHw9qNZVSrQX/VIj8RZpwIiG+q6j8HlrHjk1P0hKJ+1QYzbdFbFbIseeFWw7hn4L8RcJKDbH3okMBYS4AoZQDT4c/JCXai0RjAg32PlFXLKX9mfroZzkIMe7zcv4v4itl4ueejbxRo8cwJayjhnxhmf/USdutdFSItpEAZqZf+q0LSthY4OOCO40PXmbSVsw4ZfsaPvp5Ge5ukXSA0VBOS6OZrSEXqz8fmH7OVWWlMOrJ2PJFOpQApo/Qj6xlDR89X0mRy85B6yiBYhMta8oput7DP18eu2fPKgG8Z9L+8DoWPxl94/FS9T8E8tHlGOLg1Zn+S43sPJ8EMDs6yl7SVEACmKZwsll2FJAAZkdH2UuaCkgA0xRONsuOAhLA7Ogoe0lTAQlgmsLJZtlRQAKYHR1lL2kqIAFMUzjZLDsKSACzo6PsJU0FJIBpCiebZUcBCWB2dJS9pKmABDBN4WSz7CggAcyOjrKXNBWQAKYpnGyWHQUkgNnRUfaSpgIKgC+99NL3abb/UTT7/vvsP/5DDz1EJv2K9uKK7SPyb4n/PtsiJM4jto97vXjzff7/B8M7Y7s95SOOAAAAAElFTkSuQmCC';?>
            <?php // $image='../../../assets/images/core.png';?>
            <?php // $image='core.png';?>
            <img src="<?php echo $image;?>" alt="logo">
        </div>
        <div class="content-body">
            <?php echo $msg;?> 
        </div>
        <div class="content-footer">
            <p class="text-regard" >Many thanks,</p>
            <p class="text-regard" >Whetstone Oxbridge Team </p>
        </div>

    </div>
</div>
</body>
</html>