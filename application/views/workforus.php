<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Work For Us | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?>
  <link  href="<?php echo base_url();?>assets/js/imageuploader/css/fineCrop.css" rel="stylesheet">
  <link  href="<?php echo base_url();?>assets/js/imageuploader/css/layout.css" rel="stylesheet">
  <style>
  .single-team-member > p{min-height: 160px; max-height: 160px; overflow: hidden}
  .testimonial-content ul li{ text-align: left; margin-bottom: 20px}
  .testimonial-content ul li i{color:#2aaebf; margin-right: 10px;}
  .hiw{padding-top: 40px;  margin-top: 40px}
  .hiw h4{font-size: 24px; color:#333; font-weight: bold;}
  .hiw p{font-size: 14px; color:#888}
  .hiw-img{margin-top: 40px}
  .hiw-img img{width: 100%}
  #croppedImg{width: 128px; height:128px;}
</style>
</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU -->

  <!-- Start single page header -->
  <section id="single-page-header" style="background:url(<?php echo base_url();?>assets/images/bg-counter.jpg); background-position:0 -400px">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Work For Us</h2>
              <p>From students for students, you know better than anyone what is going on behind the interview doors at the moment. </p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Work for us</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->


  <section style="background: #f1f3f4; padding:60px 0; display: inline;float: left;width: 100%;">

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">How it Works?</h2>
            <span class="line"></span>
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
      </div>
      <?php  $i=0;
           foreach ($work_for as $row) 
           { 
             $number=$i++;
            if($number % 2 == 0)
              {$a=''; $b='';}
            else
              { $a='col-md-push-6';$b='col-md-pull-6';}
            ?>
          
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 <?php   echo $a;?>">
                <div class="hiw">
                    <h4><?php echo $row->title;?></h4>
                     <?php echo $row->body;?>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 <?php   echo $b;?>">
                <div class="hiw-img">
                <img src="<?= FILE_PATH_WORK.$row->image;?> "/>
                </div>
            </div>
        </div> 
      <?php  } ?>

      <div class="row">
       <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="hiw">

        </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="hiw-img">
          <!--<img src="<?php echo base_url();?>assets/images/hiw7.png"/>-->
        </div>
      </div>
    </div>

  </div>

</section>

<!-- end about -->

<!-- End Service -->
<!-- Start subscribe us -->
<section id="subscribe">
  <div class="subscribe-overlay">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="subscribe-area">
            <h2 class="wow fadeInUp">Already applied, check your Application status here</h2>
            <a href='<?php echo base_url();?>check-request-status' class="btn btn-primary" style='background: #2baec0; border-radius: 0; padding: 18px 25px'>Check Status</a>
          </div>
          <br>
          <br>
          <div class="subscribe-area" style="margin-top:40px">
            <h2 class="wow fadeInUp">Become an Interviewer</h2>
            <div class="col-md-10" style="float: none; margin: 0px auto">
             <div class="contact-area-right">

               <?php
               if($this->session->flashdata('error')) {
                 $message = $this->session->flashdata('error');
                 echo'
                 <div class="alert alert-danger alert-dismissible error_already_applied" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <i class="fa fa-check-circle"></i>'.$message['message'].
                 '</div>';
               }?>

               <form style="text-align:left !important" id="request_join_tutor"  onsubmit="return getpicture()" method="post" class="comments-form contact-form" role="form"  action="<?php echo base_url();?>interviewer/add_join_request" enctype="multipart/form-data" accept-charset="utf-8" novalidate="novalidate">
                 <div class="col-md-6">
                   <div class="form-group">
                    <label style="color:#fff; font-weight: normal">Name</label>
                    <input name="name" class="form-control" placeholder="" type="text">
                  </div>
                  <div class="form-group">
                   <label style="color:#fff; font-weight: normal">  University email address </label>
                   <input name="email_address" class="form-control" placeholder="" type="email">
                 </div>
                 <div class="form-group">
                   <label style="color:#fff; font-weight: normal">University</label>
                   <!-- <input type="text" name="university" class="form-control" placeholder=""> -->
                   <select class="form-control" name="university" id="university" onchange="return get_college(this.value)" required="">
                    <option value="">Select University</option>
                    <option value="1">Cambridge</option>                        <option value="2">Oxford</option>                      </select>
                  </div>
                  <div class="form-group">
                   <label style="color:#fff; font-weight: normal">College</label>
                   <!--  <input type="text" name="college" class="form-control" placeholder=""> -->
                   <select class="form-control" name="college" id="college_id">
                    <option value="">Select University first </option>
                  </select>
                </div>
                <div class="form-group">
                 <label style="color:#fff; font-weight: normal">Course</label>
                 <input name="subject" class="form-control" placeholder="" required="" type="text">
               </div>

               <div class="form-group">
                 <label style="color:#fff; font-weight: normal"> Year of study</label>
                 <!-- <input type="text" name="university" class="form-control" placeholder=""> -->
                 <select class="form-control" name="year_of_study" id="year_of_study" required="">
                  <option value="">Year of study</option>
                  <option value="1st Year">1st Year</option> 
                  <option value="2nd Year">2nd Year</option> 
                  <option value="3rd Year">3rd Year</option> 
                  <option value="4th Year">4th Year</option> 
                  <option value=" 5th Year"> 5th Year</option> 
                  <option value="6th Year">6th Year</option> 
                </select>
              </div>

              <div class="form-group">
               <label style="color:#fff; font-weight: normal">Mobile Number</label>
               <input name="mobile_number" class="form-control" placeholder="" required="" type="text">
             </div>

           </div>

           <div class="col-md-6">
             <div class="form-group">
              <label style="color:#fff; font-weight: normal">Main academic interests</label>
              <!-- <input name="working_as" class="form-control" placeholder="" required="" type="text"> -->
              <textarea class="form-control" name="working_as" style="height: 60px" required></textarea>
            </div>
            <div class="form-group">
             <label style="color:#fff; font-weight: normal">What do you wish you’d known before your Oxbridge interview?</label>
             <!--  <input name="wish_to" class="form-control" placeholder="" required="" type="text"> -->
             <textarea class="form-control" name="wish_to" style="height: 60px" required></textarea>
           </div>
           <div class="form-group">
             <label style="color:#fff; font-weight: normal">Extra curricular interest</label>
             <!-- <input name="extra_curricular" class="form-control" placeholder="" required="" type="text"> -->
             <textarea class="form-control" name="extra_curricular" style="height: 60px" required></textarea>
           </div>
           <div class="col-lg-12" style=" padding: 0px">
             <div class="form-group">
              <label style="font-weight: normal">Your Display Picture</label>
            </div>
            <img id="croppedImg" src="<?php echo base_url();?>assets/images/default.png">
            <input type="file" id="upphoto" name="img" style="display:none;">
            <input type="hidden" id="upphoto1" name="picture" >
            <label for="upphoto">
              <div class="inputLabel">
                click here to upload an image
              </div>
            </label>

          </div>
          <div class="col-lg-12" style=" padding: 0px">
           <div class="form-group" style="color:#fff; margin-top: 10px">
            <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a href="/privacy-policy" target="_blank" style="color:#fff;">GDPR Policy</a>.
          </div>
        </div>
        <button class="comment-btn" type="submit">Submit</button>
        <!--   <button class="btn signin-btn" type="submit">Login</button> -->
      </div>
    </form>
  </div>
</div>
<!--<a href='<?php echo base_url();?>become-tutor' class="btn btn-primary" style='background: #2baec0; border-radius: 0; padding: 18px 25px'>Start Today</a>-->
</div>
</div>
</div>
</div>
</div>
</section>
<!-- End subscribe us -->

<!-- Start footer -->
<?php $this->load->view('common/footer');?>

<div class="cropHolder">
  <div id="cropWrapper">
    <img id="inputImage" src="images/face.jpg">
  </div>
  <div class="cropInputs">
    <div class="inputtools">
      <p>
        <span>
          <i class="fa fa-arrows-h"></i>
        </span>
        <span>horizontal movement</span>
      </p>
      <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
    </div>
    <div class="inputtools">
      <p>
        <span>
          <i class="fa fa-arrows-v"></i>
        </span>
        <span>vertical movement</span>
      </p>
      <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
    </div>
    <br>
    <button class="cropButtons" id="zplus">
      <i class="fa fa-search-plus"></i>
    </button>
    <button class="cropButtons" id="zminus">
      <i class="fa fa-search-minus"></i>
    </button>
    <br>
    <button id="cropSubmit">submit</button>
    <button id="closeCrop">Close</button>
  </div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {
    setTimeout(function() {
      $('.alert').fadeOut('slow');}, 5000
      );
  });
</script>

<script type="text/javascript">
  function get_college(university_id) {
          //alert(university_id);
          $.ajax({
            url: '<?php echo base_url();?>interviewer/get_college/' + university_id ,
            success: function(response)
            {
                  //alert(response);
                  jQuery('#college_id').html(response);
                }
              });
        }

        /* script for text area */
        $('#request_join_tutor').find('textarea').on('keydown', function(e){
          var that = $(this);
          if (that.scrollTop()) {
            $(this).height(function(i,h){
              return h + 20;
            });
          }
        });
      </script>
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/imageuploader/js/fineCrop.js"></script>

      <script>
        $("#upphoto").finecrop({
          viewHeight: 500,
          cropWidth: 200,
          cropHeight: 200,
          cropInput: 'inputImage',
          cropOutput: 'croppedImg',
          zoomValue: 50
        });

        function getpicture(){
          $("#upphoto1").val($("#croppedImg").attr("src"));
          return true;
        }

      </script>
    </body>
    </html>
