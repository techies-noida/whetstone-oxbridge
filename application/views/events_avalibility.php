<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Event Availability | Whetstone Oxbridge</title>
        <?php $this->load->view('common/header_assets');?>
        <!-- <link rel="shortcut icon" type="image/icon" href="<?php echo base_url();?>assets/images/favicon.ico"/>
        <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slick.css"/> 
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css"/> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-progressbar-3.3.4.css"/>
        <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet"> 
        <link id="switcher" href="<?php echo base_url();?>assets/css/theme-color/default-theme.css" rel="stylesheet"> -->

        <link href="<?php echo base_url();?>assets/js/fullcalendar-3.9.0/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            html{overflow-x:hidden}
          .widget{background: #fff; border:1px solid #eee; margin-bottom: 10px; display: block}
          .widget .w-header{font-size: 13px; padding: 8px; background: #2aaebf !important; color:#fff}
          .widget .w-header i{opacity: 0.5}
          .widget .w-body{padding: 10px}
          .widget .w-body img{border-radius: 50%; border:1px solid #eee}
          .addperiodbutton{background: #2aaebf; color:#fff !important; display: block; text-align: center; height: 92px; }
          .addperiodbutton i{margin-top: 30px}
          .addperiodbutton:hover{background: #2aaebf; color:#fff !important; text-decoration: none; opacity: 0.7}
          #event-detail .table-striped > tbody > tr:nth-of-type(odd){border:0; background: rgba(42,174,191,0.05)}
          #event-detail .table-striped > tbody > tr th{border:0; }
          #event-detail .table-striped > tbody > tr td{border:0; }
          #event-detail .btn-primary{background: #2aaebf; border-color:#2aaebf; border-radius: 0}
          #event-detail .btn-danger{background: red; border-color:red; border-radius: 0}
          .fc-center h2{color:#2aaebf !important}
        </style>
    </head>
    <body>
        <!-- BEGAIN PRELOADER -->
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- END PRELOADER -->

        <!-- SCROLL TOP BUTTON -->
        <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
        <!-- END SCROLL TOP BUTTON -->

        <!-- Start header -->
       <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
        <!-- END MENU --> 
        <!-- Start Pricing table -->
        <section id="our-team">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title" style="text-align: left">Schedule</h3>
                    </div>
                </div>
                <br>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="w-header"><i class="fa fa-clock-o"></i> 10:00 - 13:00</div>
                         <?php if (isset($this->session->userdata['logged_in']))
          $user_name=($this->session->userdata['logged_in']['user_name']);
          $profile_image=($this->session->userdata['logged_in']['profile_image']); ?>
                        <div class="w-body">
                            <!-- <img src="<?php echo base_url().$profile_image;?>assets/images/team-member-2.png" height="36"/><?php echo $user_name;?> -->
                            <img src="<?php echo base_url().$profile_image.'" height="36"/>'.$user_name;?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <a class="addperiodbutton" href="#" data-toggle="modal" data-target="#addtime">
                       
                            <i class="fa fa-plus"></i><br>
                            Add Time Period
                      
                    </a>
                </div>
                <div class="col-md-12">
                    <div class="our-team-content" style="margin-top: 0">
                        <div class="row">
                            
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                
                <div class="single-team-member" style="margin-top: 0">
                 <div class="team-member-img">
                      <?php $image=$user[0]->profile_image;
                       if($image=='')
                   
                  echo '<img src="'.base_url().'assets/images/default.png" alt="team member img">';
                  
                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img">';
                     ?>
                 </div>
                 <div class="team-member-name" style="float:none; display: block; margin-bottom: 0">
                    <?php $user_name=$user[0]->first_name.' '.$user[0]->last_name;?>
                   <p><?php echo $user_name;?></p>
                   <span><?php echo $user[0]->name;?></span>
                 </div>
                    <div>  <hr style="height: 1px; background: #eee; width: 100%"></div>
                 <div class="sidebar-widget" style="float:none; display: block; margin-bottom: 0">
                    <ul class="widget-catg" style="text-align: left">                      
                        <li><a href="#"><i class="fa fa-book"></i>Sociology</a></li>
                      <li><a href="#"><i class="fa fa-map-marker"></i>Manhetton, NY</a></li>
                      <li><a href="#"><i class="fa fa-phone"></i>+91 <?php echo $user[0]->phone_number;?></a></li>
                      <li><a href="#"><i class="fa fa-envelope"></i><?php echo $user[0]->email_address;?></a></li>
                      <li><a href="#"><i class="fa fa-bank"></i>MIT University</a></li>
                       <!-- <li><a href="#"><?php echo $user[0]->user_id;?></a></li> -->
                    </ul>
                  </div>
                    <div>    <hr style="height: 1px; background: #eee; width: 100%"></div>
                <a href="<?php echo base_url().'interviewer/edit_profile/'.$user[0]->user_id;?>" class="btn comment-btn" style="display: block; margin-bottom: 10px">Edit Profile</a>
                 <a href="<?php echo base_url();?>change_password" class="btn comment-btn" style="display: block">Change Password</a>
                </div>
              
                            </div>
                            
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <div class="inter-more">
                                    <div class="availability">
                                        <h4>Availability</h4>
                                        <div id='calendar'></div>
                                    </div>
                                    
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
<div class="modal fade" id="addtime" tabindex="-1" role="dialog" aria-labelledby="addtime">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Time Period</h4>
      </div>
      <div class="modal-body">
         <form id="set_interviewer_schedule"  class="comments-form contact-form">
                       <div class=" col-md-8" style="float:none; margin: auto">
                         <div id="schedule_message" >
                           
                         </div>
                        <div class="form-group">
                             <label style="color:#444; font-weight: normal">Start Time</label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="start_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                            <div class="form-group">
                                 <label style="color:#444; font-weight: normal">End Time</label>
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text'  name="end_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                            <div class="form-group">
                                 <label style="color:#444; font-weight: normal">Select Date</label>
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' name="schedule_date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <button class="comment-btn" type="submit">Confirm and Set  </button>
                       </div>
                  </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="event-detail" tabindex="-1" role="dialog" aria-labelledby="event-detail">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 8px">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Appointment Detail</h4>
      </div>
      <div class="modal-body" style="padding: 15px; font-size: 13px">
          <table class="table table-striped">
              <tr>
                  <th>Appointment Id</th><td><span class="app-color"><i class="fa fa-square"></i></span>&nbsp;&nbsp;<span class="app-desc"></span></td>
              </tr>
              <tr>
                  <th>Appointment Time</th><td><span class="app-time"></span></td>
              </tr>
              <tr>
                  <th>Appointment Date</th><td><span class="app-date"></span></td>
              </tr>
              <tr>
                  <th>Student Name</th><td><span class="stu-name">John Doe</span></td>
              </tr>
             
          </table>
          <button class="btn btn-xs btn-primary">Confirm Appointment</button>
          <button class="btn btn-xs btn-danger">Cancel Appointment</button>
      </div>
    </div>
  </div>
</div>  

        <!-- Start footer -->
      
        <?php  $this->load->view('common/footer');?>
        <!-- End footer -->

        <!-- jQuery library -->
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>  
        <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->
        <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>-->
        <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script> 
        <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  
        
        <script src="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/fullcalendar-3.9.0/fullcalendar.js"></script>  
        <script>
            $(function() {
                $('#calendar').fullCalendar({
                    selectable: true,
                    header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'month,agendaWeek,agendaDay'
                    },
                    dayClick: function(date) {
                      alert('clicked ' + date.format());
                    },
                    select: function(startDate, endDate) {
                      alert('selected ' + startDate.format() + ' to ' + endDate.format());
                    },
                     events: [
                        {
                          title: '10:00-13:00 Slot',
                          start: '2018-10-16',
                          description: 'WO20181016',
                          backgroundColor: 'red'
                        },
                         {
                          title: '04:00-08:00 Slot',
                          start: '2018-10-10',
                          description: 'WO20181010',
                          backgroundColor: 'green'
                        },
                         {
                          title: '09:00 AM-08:00PM Slot',
                          start: '2018-10-11',
                          description: 'WO20181010',
                          backgroundColor: 'green'
                        },
                        {
                          title: '09:00 AM-08:00PM Slot',
                          start: '2018-10-25',
                          description: 'WO20181010',
                          backgroundColor: 'green'
                        },
                        // more events here
                      ],
                       eventClick: function(calEvent, jsEvent, view) {

//                            alert('Event: ' + calEvent.title);
//                            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                            alert('View: ' + view.name);

//                            $(this).css('border-color', 'red');
                            $('.app-time').text(calEvent.title);
                            $('.app-date').text(calEvent.start);
                            $('.app-desc').text(calEvent.description);
                            $('.app-color').find('i').css('color',calEvent.backgroundColor);
                            $('#event-detail').modal();

                          }
                         
                });
                
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker3').datetimepicker({
                    format: 'L',
                    //format:'Y-m-d',
                    minDate:new Date()
                });
            });
        </script>
        <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script> -->

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
         <script>
 $(function myFunction(){
   //alert('hiiii');
    $('#set_interviewer_schedule').validate({
        rules:{
            start_time:{
                required:true,
               
            },
            end_time:{
                required:true
            },
            schedule_date:{
                required:true
            },
        },
        messages:{
            start_time:{
                required:"  Please enter start time ",
                
            },
            end_time:{
                required: "Please enter end time",
            },
            schedule_date:{
                required: "Please select schedule date",
            }
        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>interviewer_schedule/add_schedule', 
            $('#set_interviewer_schedule').serialize(), 
            function(data){
          // alert (data); 
                if(data == 1)
                {
                    
                   // window.location='<?=base_url()?>profile-details'; 
                    responseText = '<span style="color:green;font-size: 18px;font-weight: normal;margin-left: 40px;">Your Schedule added</span>';
                    $("#schedule_message").html(responseText);
                }
                 if(data == 0)
                {
                   responseText = '<span style="color:red;font-size: 18px;font-weight: normal;margin-left: 40px;">Schedule already exist </span>';
                    $("#schedule_message").html(responseText);
                }
               
                

            });
       }
   });
});
</script>

    </body>
</html>