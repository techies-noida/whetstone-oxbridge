<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Order Success | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?>
  <style>
  .btn-submit{background: #2aaebf !important; color:#fff; border-radius: 0; display: inline-block; margin-top: 20px; padding: 12px 25px}
  .table-highlight{border:0 !important}
  .table-highlight tr{border:0 !important; background: #f3fef6}
  .table-highlight tr td{border:0 !important; vertical-align: middle !important}
  .table-highlight tr th{border:0 !important; vertical-align: middle !important}
  .table-striped tr td{border:0 !important}
  .table-striped tr th{border:0 !important; font-weight: bold !important; color:#333 !important}
  .small-head{text-align: left; text-transform: uppercase; font-weight: 800; font-size: 14px; color:#23527c; margin-bottom: 20px}
</style>

</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  

  <!-- end about -->
  <!-- Start Pricing table -->
  <section id="our-team" style="background: #f6fef7; padding-top: 40px">
    <div class="container">
      <div class="row">
<!--        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title" style="font-size:24px">Booking Review</h2>
            <span class="line"></span>
          </div>
        </div>-->
        <div class="col-md-12">
          <div class="errror-page-area">
            <h1 class="error-title" style="      font-size: 30px;  padding: 28px 32px;"><span class="fa fa-thumbs-up"></span></h1>
            <div class="error-content">
              <span>Payment Done</span>
              <p><?=$msg;?></p>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content" style="margin-top: 20px">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-6 col-sm-6 col-xs-12" style="float:none; margin:auto">
                <div class="single-team-member">

                  <?php if ($this->uri->segment('2')==1 ) { ?>
                    <h4 class="small-head">Booking Detail</h4>
                    <table class="table table-highlight">
                      <tr>
                        <!-- <td><img src="<?= base_url();?>assets/images/main-testimonials-img-4.png" height="36" alt="team member img"></td> -->
                        <td style="width: 64px"><img src="<?php if($this->session->userdata['logged_in']['profile_image']!='') { echo base_url().$this->session->userdata['logged_in']['profile_image'];} else { echo base_url().'assets/images/default.png';}?>" height="36" style="border-radius:50%" alt="team member img"></td>
                        <td class="text-left">
                         <b style="color:#2aaebf"><?=$this->session->userdata['logged_in']['user_name']?></b><br><small style="font-size:12px"><?=$this->session->userdata('invoice_session')['start']?> - <?=$this->session->userdata('invoice_session')['end']?>, <?=date('d-m-Y',strtotime($this->session->userdata('invoice_session')['booking_date']))?></small>
                       </td>
                     </tr>

                   </table>
                   

                   <h4 class="small-head">Payment Detail</h4>
                   <table class="table table-striped">

                    <tr>
                      <td class="text-left">Total</td>
                      <td class="text-right">£<?=round($this->session->userdata('invoice_session')['main_amount'])?></td>
                    </tr>
                    <tr>
                      <td class="text-left">Discount</td>
                      <td class="text-right">-£<?=round($this->session->userdata('invoice_session')['discount_amount'])?></td>
                    </tr>
                    <tr>
                      <td class="text-left">Grand Total</td>
                      <td class="text-right">£<?=round($this->session->userdata('invoice_session')['amount'])?></td>
                    </tr>
                    <tr>
                      <td class="text-left">Payment Method</td>
                      <td class="text-right">Card</td>
                    </tr>
                    <tr>
                      <td class="text-left">Payment Status</td>
                      <td class="text-right">Done</td>
                    </tr>

                  </table>
                <?php } else { ?>
                  <?=$msg?>
                <?php } ?>
                <!-- <button class="btn btn-submit">Go Home</button> -->
                <a href="<?php echo base_url();?>" class="btn btn-submit">Go Home</a>
                
              </div>
            </div>
            <!-- Start single team member -->

          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="discount_coupon" id="discount_coupon" value="<?php echo $this->session->userdata('invoice_session')['discount_coupon'];  ?>">
  <input type="hidden" name="pay_status" id="pay_status" value="<?php echo $this->uri->segment('2')  ?>">
</section>
<!-- Start Service -->
<!-- End Service -->
<!-- Start subscribe us -->
 <!--  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2 class="wow fadeInUp">Subscribe Newsletter</h2>
              <form action="" class="subscrib-form wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <?php $this->load->view('common/footer');?>
  <!-- <script type="text/javascript">
    $(document).ready(function(){
      var discount_coupon = $('#discount_coupon').val();
      var pay_status = $('#pay_status').val();
        // alert(pay_status);
        if(pay_status == 1) {
          $.ajax({
           url:"<?php echo base_url() . 'oxbridge/update_coupn_usage'?>", 
           method:'POST',  
           data:{discount_coupon:discount_coupon}, 
           success:function(data)  
           { 
          // alert(data);
        }
      });
        }
        
      });
    </script> -->
    <script type="text/javascript">
      history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});
    </script>
  </body>
  </html>