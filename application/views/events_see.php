<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Set Interviewer Availability | Whetstone Oxbridge </title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <?php $this->load->view('common/header_assets');?>
        <link href="<?php echo base_url();?>assets/js/fullcalendar-3.9.0/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            html{overflow-x:hidden}
          .widget{background: #fff; border:1px solid #eee; margin-bottom: 10px; display: block}
          .widget .w-header{font-size: 13px; padding: 8px; background: #2aaebf !important; color:#fff}
          .widget .w-header i{opacity: 0.5}
          .widget .w-body{padding: 10px}
          .widget .w-body img{border-radius: 50%; border:1px solid #eee}
          .addperiodbutton{background: #2aaebf; color:#fff !important; display: block; text-align: center; height: 92px; }
          .addperiodbutton i{margin-top: 30px}
          .addperiodbutton:hover{background: #2aaebf; color:#fff !important; text-decoration: none; opacity: 0.7}
          #event-detail .table-striped > tbody > tr:nth-of-type(odd){border:0; background: rgba(42,174,191,0.05)}
          #event-detail .table-striped > tbody > tr th{border:0; }
          #event-detail .table-striped > tbody > tr td{border:0; }
          #event-detail .btn-primary{background: #2aaebf; border-color:#2aaebf; border-radius: 0}
          #event-detail .btn-danger{background: red; border-color:red; border-radius: 0}
          .fc-center h2{color:#2aaebf !important}
        </style>
    </head>
    <body>
        <!-- BEGAIN PRELOADER -->
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- END PRELOADER -->

        <!-- SCROLL TOP BUTTON -->
        <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
        <!-- END SCROLL TOP BUTTON -->

        <!-- Start header -->
       <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
       $this->load->view('common/subheader');
       ?>
        <!-- END MENU --> 
        <!-- Start Pricing table -->
        <section id="our-team" style='background: #f6fef7; padding-top: 0px'>
            <div class="container">
               <!--  <div class="row">
                    <div class="col-md-12">
                        <h3 class="title" style="text-align: left">Schedule</h3>
                    </div>
                </div> -->
              
                <div class="row">
               <br>
                <div class="col-md-12" style="padding: 0">
                  <div class="our-team-content" style="margin-top: 0">
                      
                    <div class="row">
                       <?php   $this->load->view('common/interviewer_sidebar');?>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                                
                       <!--         <div class="row">
                                    
                              <div class="col-md-4">
                    <div class="widget">
                        <div class="w-header"><i class="fa fa-clock-o"></i> 10:00 - 13:00</div>
                         <?php if (isset($this->session->userdata['logged_in']))
          $user_name=($this->session->userdata['logged_in']['user_name']);
          $profile_image=($this->session->userdata['logged_in']['profile_image']); ?>
                        <div class="w-body">
                             <img src="<?php echo base_url().$profile_image;?>assets/images/team-member-2.png" height="36"/><?php echo $user_name;?> 
                            <img src="<?php echo base_url().$profile_image?>" height='36'/><?php echo $user_name;?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <a class="addperiodbutton" href="#" data-toggle="modal" data-target="#addtime">
                       
                            <i class="fa fa-plus"></i><br>
                            Add Time Period
                      
                    </a>
                </div>
                        
                                </div> -->
                              <div class="row">
                                <div class="inter-more">
                                    <div class="availability">
                                        <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px;">Availability</h4><div id='event_success'></div>
                                        <div id='calendar'></div>
                                    </div> 
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
<div class="modal fade" id="addtime" tabindex="-1" role="dialog" aria-labelledby="addtime">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Time Period</h4>
      </div>
      <div class="modal-body">
         <form id="set_interviewer_schedule"  class="comments-form contact-form">
                       <div class=" col-md-8" style="float:none; margin: auto">
                         <div id="schedule_message" >
                           
                         </div>
                        <div class="form-group">
                             <label style="color:#444; font-weight: normal">Start Time</label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="start_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                            <div class="form-group">
                                 <label style="color:#444; font-weight: normal">End Time</label>
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text'  name="end_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                            <div class="form-group">
                                 <label style="color:#444; font-weight: normal">Select Date</label>
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' name="schedule_date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <button class="comment-btn" type="submit">Confirm and Set  </button>
                       </div>
                  </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="event-detail" tabindex="-1" role="dialog" aria-labelledby="event-detail">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding: 8px">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Appointment Detail</h4>
      </div>
      <div class="modal-body" style="padding: 15px; font-size: 13px">
          <table class="table table-striped">
              <tr>
                  <th>Appointment Id</th><td><span class="app-color"><i class="fa fa-square"></i></span>&nbsp;&nbsp;<span class="app-desc"></span></td>
              </tr>
              <tr>
                  <th>Appointment Time</th><td><span class="app-time"></span></td>
              </tr>
              <tr>
                  <th>Appointment Date</th><td><span class="app-date"></span></td>
              </tr>
              <tr>
                  <th>Student Name</th><td><span class="stu-name">John Doe</span></td>
              </tr>
             
          </table>
          <button class="btn btn-xs btn-primary">Confirm Appointment</button>
          <button class="btn btn-xs btn-danger">Cancel Appointment</button>
      </div>
    </div>
  </div>
</div>  

        <!-- Start footer -->
      
        <?php  $this->load->view('common/footer');?>
        <!-- End footer -->

        <!-- jQuery library -->
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>  
         
        <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->
        <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>-->
        <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script> 
        <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
        <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  
        
        <script src="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.js"></script>
 <script src="<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.js"></script>

<script>

    $(document).ready(function() {
        var currentMousePos = {
            x: -1,
            y: -1
        };
        jQuery(document).on("mousemove", function (event) {
            currentMousePos.x = event.pageX;
            currentMousePos.y = event.pageY;
        });

        var myDate1 = new Date();
        //How many days to add from today?
        var daysToAdd1 = 2;
        myDate1.setDate(myDate1.getDate() + daysToAdd1);
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var interviewer_id='<?=$this->session->userdata['logged_in']['user_id']?>';
        var calendar = $('#calendar').fullCalendar({
            editable: true,
            header: {
                left: 'next today',
                center: 'title',
                right: 'agendaWeek,agendaDay'
            },
            
            defaultView: 'agendaDDay',
            views: {
                agendaDDay: {
                    type: 'agenda',
                    duration: { days: 7 },
                    buttonText: '7 day'
                }
            },
            ignoreTimezone: false,
            events: {
                url: '<?php echo base_url();?>Interviewer_schedule/get_available_data_ajax',
                data: {interviewer_id:interviewer_id},
                dataType: 'json',

                success: function (response) {

                },
                error: function() {
                    //$('#script-warning').show();
                }
            },
             timezone: 'local',//America/Caracas
            eventRender: function(event, element, view) {
                if (event.allDay === 'true') {
                    event.allDay = false;
                } else {
                    event.allDay = false;
                }
            },
            selectable: true,
            selectHelper: true,
            selectOverlap: false,
            eventOverlap: false,
            editable: true,
            expandThrough: false,
            select: function(start,end, jsEvent, view) {
                var myDate = new Date();
                
                var daysToAdd = 1;
                myDate.setDate(myDate.getDate() + daysToAdd);
                (start.local());
                (end.local());
               
                var start_full=(start.format());
                var end_full=(end.format());
               
                   /* var date=$.fullCalendar.formatDate(start, "YYYY-MM-DD");
                    var slot_start = $.fullCalendar.formatDate(start, "HH:mm:ss");
                    var slot_end = $.fullCalendar.formatDate(end, "HH:mm:ss");
                    var date_start = $.fullCalendar.formatDate(start, "YYYY-MM-DD HH:mm:ss");
                    var date_end = $.fullCalendar.formatDate(end, "YYYY-MM-DD HH:mm:ss");*/
                     var date=$.fullCalendar.formatDate(start, "DD-MM-YYYY");
                    var slot_start = $.fullCalendar.formatDate(start, "HH:mm:ss");
                    var slot_end = $.fullCalendar.formatDate(end, "HH:mm:ss");
                    var date_start = $.fullCalendar.formatDate(start, "DD-MM-YYYY HH:mm:ss");
                    var date_end = $.fullCalendar.formatDate(end, "DD-MM-YYYY HH:mm:ss");
                    if (date!='' && start!='' && end!='') {
                        $.ajax({
                            url: '<?php echo base_url();?>Interviewer_schedule/add_session_availablety_ajax',
                            data: {date:date,start:slot_start,end:slot_end,start_full:start_full,end_full:end_full},
                            type: "POST",
                            success: function(json) {
                                $('#event_success').html('<div class="alert alert-success alert-dismissable">\n' +
                                    '                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                                    '                                          <strong>Success!</strong> Session time added successfully.\n' +
                                    '       </div>');
                                
                                var id=json;
                                $('#calendar').fullCalendar('rerenderEvents');
                                $('#calendar').fullCalendar('refetchEvents');
                            }
                        });

                 }
            },

            editable: true,
            eventDrop: function(event, delta) {
                var date = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD");
                var start = $.fullCalendar.formatDate(event.start, "HH:mm:ss");
                var end = $.fullCalendar.formatDate(event.end, "HH:mm:ss");
                (event.start.local());
                (event.end.local());
                //(start.utcOffset()/60);
                var start_full=(event.start.format());
                var end_full=(event.end.format());
                $.ajax({
                    url: '<?php echo base_url();?>Interviewer_schedule/update_session_availablety_ajax',
                    data: {date:date,start:start,end:end,start_full:start_full,end_full:end_full,id:event.id} ,
                    type: "POST",
                    success: function(json) {
                        $('#event_success').html('<div class="alert alert-success alert-dismissable">\n' +
                            '                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                            '                                          <strong>Success!</strong> Session time Update successfully.\n' +
                            '       </div>');
                    }
                });
            },
            eventResize: function(event) {
                var date = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD");
                var start = $.fullCalendar.formatDate(event.start, "HH:mm:ss");
                var end = $.fullCalendar.formatDate(event.end, "HH:mm:ss");
                var start_full=(event.start.format());
                var end_full=(event.end.format());
                $.ajax({
                    url: '<?php echo base_url();?>Interviewer_schedule/update_session_availablety_ajax',
                    data: {date:date,start:start,end:end,start_full:start_full,end_full:end_full,id:event.id},
                    type: "POST",
                    success: function(json) {
                        $('#event_success').html('<div class="alert alert-success alert-dismissable">\n' +
                            '                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                            '                                          <strong>Success!</strong> Session time Update successfully.\n' +
                            '       </div>');
                    }
                });

            },
            eventClick: function(event) {

                if(event.url!='')
                {

                }
                else
                {
                    var decision = confirm("Do you really want to Delete?");
                    if (decision) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url();?>Interviewer_schedule/delete_session_availablety_ajax",
                            data: "&id=" + event.id,
                            success: function(ddx) {
                            $('#calendar').fullCalendar('removeEvents', event.id);
                            $('#event_success').html('<div class="alert alert-success alert-dismissable">\n' +
                                        '                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                                        '                                          <strong>Success!</strong> Session time deleted successfully.\n' +
                                        '       </div>');
                               
                            }
                        });


                    } else {
                    }
                }

            },


        });

    });

</script>




        <script>
            $(function() {
                $('#calendar111').fullCalendar({
                    selectable: true,
                      defaultView: 'agendaWeek',
                    header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'agendaWeek,agendaDay'
                    },
                    dayClick: function(date) {
                      alert('clicked ' + date.format());
                    },
                    select: function(startDate, endDate) {
                      alert('selected ' + startDate.format() + ' to ' + endDate.format());
                    },

                    eventSources: [
                    {
                             events: function(start, end, timezone, callback) {
                                 $.ajax({
                                 url: '<?php echo base_url() ?>interviewer_schedule/availibility_list',
                                 dataType: 'json',
                                 data: {
                                 // our hypothetical feed requires UNIX timestamps
                                 start: start.unix(),
                                 end: end.unix()
                                 },
                                 success: function(msg) {
                                     var events = msg.events;
                                     callback(events);
                                 }
                                 });
                             }
                         },
                     ],
                     /*events: [
                        {
                          title: '10:00-13:00 Slot',
                          start: '2018-10-16',
                          description: 'WO20181016',
                          backgroundColor: 'red'
                        },
                         {
                          title: '04:00-08:00 Slot',
                          start: '2018-10-10',
                          description: 'WO20181010',
                          backgroundColor: 'green'
                        },
                         {
                          title: '09:00 AM-08:00PM Slot',
                          start: '2018-10-11',
                          description: 'WO20181010',
                          backgroundColor: 'green'
                        },
                        {
                          title: '09:00 AM-08:00PM Slot',
                          start: '2018-10-25',
                          description: 'WO20181010',
                          backgroundColor: 'green'
                        },
                        // more events here
                      ],*/
                       eventClick: function(calEvent, jsEvent, view) {

//                            alert('Event: ' + calEvent.title);
//                            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                            alert('View: ' + view.name);

//                            $(this).css('border-color', 'red');
                            $('.app-time').text(calEvent.title);
                            $('.app-date').text(calEvent.start);
                            $('.app-desc').text(calEvent.description);
                            $('.app-color').find('i').css('color',calEvent.backgroundColor);
                            $('#event-detail').modal();

                          }
                         
                });
                
            });

            $('#addtime').on('hidden.bs.modal', function () {
     location.reload();
});
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker3').datetimepicker({
                    format: 'L',
                    format: 'YYYY-MM-DD',
                    minDate:new Date()
                });
            });
        </script>
        <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script> -->

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
         <script>
 $(function myFunction(){
   //alert('hiiii');
    $('#set_interviewer_schedule').validate({
        rules:{
            start_time:{
                required:true,
               
            },
            end_time:{
                required:true
            },
            schedule_date:{
                required:true
            },
        },
        messages:{
            start_time:{
                required:"  Please enter start time ",
                
            },
            end_time:{
                required: "Please enter end time",
            },
            schedule_date:{
                required: "Please select schedule date",
            }
        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>interviewer_schedule/add_schedule', 
            $('#set_interviewer_schedule').serialize(), 
            function(data){
          // alert (data); 
                if(data == 1)
                {
                    
                   // window.location='<?=base_url()?>profile-details'; 
                    responseText = '<span style="color:green;font-size: 18px;font-weight: normal;margin-left: 40px;">Your Schedule added</span>';
                    $("#schedule_message").html(responseText);
                }
                 if(data == 0)
                {
                   responseText = '<span style="color:red;font-size: 18px;font-weight: normal;margin-left: 40px;">Schedule already exist </span>';
                    $("#schedule_message").html(responseText);
                }
               
                

            });
       }
   });
});
</script>

    </body>
</html>
