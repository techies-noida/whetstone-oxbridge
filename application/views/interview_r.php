<!DOCTYPE html>
<?php $room= $this->uri->segment('3'); ?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Applicant Session | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?>
  <link href="<?php echo base_url();?>assets/js/Monthly-Event-Calendar-pbcalendar/pb.calendar.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  <script src="<?=base_url()?>assets/ckeditor/ckeditor.js"></script>
  <script src="<?=base_url()?>assets/ckeditor/samples/js/sample.js"></script>
 <!-- <link rel="stylesheet" href="<?=base_url()?>assets/ckeditor/samples/css/samples.css"> -->
  <link href="<?=base_url()?>assets/ckeditor/toolbarconfigurator/lib/codemirror/neo.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/index.css">
  <style>
    #block{position: fixed;  z-index:99999; display:none; top:0; bottom:0; left:0; right:0; text-align: center; color:#fff; background: #2aaebf !important; vertical-align:middle}
    #block table {width:100%}
    #block table tr td{vertical-align: middle; height:100%; height:100vh}
    #block table tr td i{font-size: 48px !important; margin-bottom: 20px}
    .nav-tabs.nav-justified > .active > a, .nav-tabs.nav-justified > .active > a:hover, .nav-tabs.nav-justified > .active > a:focus{border-top:0}
    .quicktext a{padding: 5px; font-size:14px; margin-top: 5px; text-transform:none;}
    .pb-calendar .schedule-dot-item.blue{
     background-color: blue;
   }

   .pb-calendar .schedule-dot-item.red{
     background-color: red;
   }

   .pb-calendar .schedule-dot-item.green{
     background-color: green;
   }
   .rating {
    overflow: hidden;
    display: inline-block;
  }

  .rating-input {
    float: right;
    width: 16px;
    height: 16px;
    padding: 0;
    margin: 0 0 0 -16px;
    opacity: 0;
  }


  .rating-star:hover {
    background-position: 0 0;
  }

  .rating-star {
    position: relative;
    float: right;
    display: block;
    width: 16px;
    height: 16px;
    background: url('<?php echo base_url();?>assets/images/star.png') 0 -16px;
  }

  .rating:hover .rating-star:hover,
  .rating:hover .rating-star:hover ~ .rating-star,
  .rating-input:checked ~ .rating-star {
    background-position: 0 0;
  }

  .rating-star,
  .rating:hover .rating-star {
    position: relative;
    float: right;
    display: block;
    width: 16px;
    height: 16px;
    background: url('<?php echo base_url();?>assets/images/star.png') 0 -16px;
  }

  .quicktext a{padding: 5px; font-size:14px; margin-bottom: 5px; text-transform:none;}
  #feedback-form{visibility: hidden;}
  /* rating css*/
  .text-center {text-align:center;}

  a {
    color: tomato;
    text-decoration: none;
  }

  a:hover {
    color: #2196f3;
  }

  pre {
    display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #F5F5F5;
    border: 1px solid #CCC;
    border-radius: 4px;
  }

  .header {
    padding:20px 0;
    position:relative;
    margin-bottom:10px;

  }

  .header:after {
    content:"";
    display:block;
    height:1px;
    background:#eee;
    position:absolute;
    left:30%; right:30%;
  }

  .header h2 {
    font-size:3em;
    font-weight:300;
    margin-bottom:0.2em;
  }

  .header p {
    font-size:14px;
  }



  #a-footer {
    margin: 20px 0;
  }

  .new-react-version {
    padding: 20px 20px;
    border: 1px solid #eee;
    border-radius: 20px;
    box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);

    text-align: center;
    font-size: 14px;
    line-height: 1.7;
  }

  .new-react-version .react-svg-logo {
    text-align: center;
    max-width: 60px;
    margin: 20px auto;
    margin-top: 0;
  }

  .success-box {
    margin:50px 0;
    padding:10px 10px;
    border:1px solid #eee;
    background:#f9f9f9;
  }

  .success-box img {
    margin-right:10px;
    display:inline-block;
    vertical-align:top;
  }

  .success-box > div {
    vertical-align:top;
    display:inline-block;
    color:#888;
  }



  /* Rating Star Widgets Style */
  .rating-stars ul {
    list-style-type:none;
    padding:0;

    -moz-user-select:none;
    -webkit-user-select:none;
  }
  .rating-stars ul > li.star {
    display:inline-block;

  }

  /* Idle State of the stars */
  .rating-stars ul > li.star > i.fa {
    font-size:2.5em; /* Change the size of the stars */
    color:#ccc; /* Color on idle state */
  }

  /* Hover state of the stars */
  .rating-stars ul > li.star.hover > i.fa {
    color:#FFCC36;
  }

  /* Selected state of the stars */
  .rating-stars ul > li.star.selected > i.fa {
    color:#FF912C;
  }

  /* chat panel ui */
  .inbox_people {
    float: left;
    overflow: hidden;
    width: 24%; border-right:1px solid #E0E0E0;
  }
  .inbox_msg {
    background: #fff;
    position: relative;
    clear: both;
    overflow: hidden;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.08);
  }
  .top_spac{ margin: 20px 0 0;}
  .recent_heading {float: left; width:40%;}
  .srch_bar {
    display: inline-block;
    text-align: right;
    width: 60%;
  }
  .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #E0E0E0;}

  .recent_heading h4 {
    color: #05728f;
    font-size: 21px;
    margin: auto;
  }
  .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
  .srch_bar .input-group-addon button {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
    padding: 0;
    color: #707070;
    font-size: 18px;
  }
  .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

  .chat_ib h5{ font-size:14px; color:#3B3B3B; margin:0 0 8px 0; font-weight: 700}
  .chat_ib h5 span{ font-size:11px; float:right; color:#727272; font-weight: normal}
  .chat_ib p{ font-size:12px; color:#989898; margin:auto; white-space: nowrap; overflow: hidden; text-overflow: ellipsis}
  .chat_img {
    float: left;
    width: 16%;
    border-radius: 50%;
  }
  .chat_img img{
    border-radius: 50%;
  }
  .chat_ib {
    float: left;
    padding: 0 0 0 15px;
    width: 82%;
  }

  .chat_people{ overflow:hidden; clear:both;}
  .chat_list {
    border-bottom: 1px solid #E0E0E0;
    margin: 0;
    padding: 18px 16px 10px;
    cursor: pointer;
  }
  .chat_list:hover {cursor: pointer; background: rgba(0,0,0,0.04)}
  .chat_list.active_chat {background: rgba(73,161,239,0.08)}
  .inbox_chat { height: 550px; overflow-y: auto;}

  .active_chat{ }
  .incoming_msg{margin-bottom: 8px}
  .incoming_msg_img {
    display: inline-block;
    width: 13%;
  }
  .outcoming_msg_img {
    display: inline-block;
    width: 16%; float: right;
    margin: 15px 0 0 10px;
  }
  .outcoming_msg_img img {
    border-radius: 50%
  }
  .incoming_msg_img img{
    border-radius: 50%
  }
  .received_msg {
    display: inline-block;
    padding: 0 0 0 10px;
    vertical-align: top;
    width: 80%;
  }
  .received_withd_msg p {
    background: #f1f5f1 none repeat scroll 0 0;
    border-radius: 3px;
    color: #fff;
    font-size: 12px !important;
    margin: 0;
    padding: 8px 10px 8px 12px;
    width: 100%;
    font-weight: normal;
  }


  .time_date {
    color: #ACACAC;
    font-size: 10px;
    margin: 8px 0 0;
    right: 0;

  }
  .received_withd_msg { width: 100%; position: relative}
  .mesgs {
    float: left;
    padding: 0px 0px 0px 0px;
    width: 100%;
  }

  .sent_msg p {
    background: #2aaebf !important;
    border-radius: 3px;
    font-size: 12px !important;
    margin: 0; color:#fff;
    padding: 8px 10px 8px 12px;
    width:100%; position: relative;
    color:#fff !important;
  }

  .outgoing_msg{ overflow:hidden; margin:20px 0 20px;}
  .sent_msg {
    float: right;
    width: 62%; position: relative;
    margin: 15px 0;
  }
  .sent_msg .time_date {
    color: #747474;
    font-size: 10px;
    margin: 8px 0 0;
    left: 0;

  }
  .input_msg_write input {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: medium none;
    color: #A7A7A7;
    font-size: 13px;
    min-height:36px;
    padding: 10px 65px 10px 15px;
    width: 100%;
  }
  .input_msg_write i{position: absolute; color: #09BC8A; left:0;    left: 28px;    top: 28px;}
  .type_msg {border-top: 1px solid #e0e0e0; position: relative; padding: 15px}
  .msg_send_btn {
    background: #fff none repeat scroll 0 0;
    border: medium none;
    color: #058ED9;
    cursor: pointer;
    font-size: 15px;
    height: 33px;
    position: absolute;
    right: 35px;
    top:20px;

  }
  .input_msg_write{border:1px solid #ededed}
  .messaging { padding: 0 0 50px 0;}
  .msg_history {
    height: 404px;
    padding: 30px 15px 0 25px;
    overflow-y: auto;
  }
  .msg_history1 {
    height: 404px;
    padding: 30px 15px 0 25px;
    overflow-y: auto;
  }
  .msg_history img{height: 36px; width: 36px}
  .msg_history1 img{height: 36px; width: 36px}

  .comments-form .form-group{padding: 0 0px; margin-bottom: 40px}
  .comments-form .form-group h4{color:#333; margin-bottom: 15px}
  .comments-form .form-group .radio input{height: 12px !important; margin-right: 0 !important; display: table-cell; vertical-align: top; margin-left:0px !important}
  .comments-form .form-group .radio span{padding-left: 25px !important; display: table-cell; vertical-align: top; float: left}
  .comments-form .form-group .radio {font-size: 13px; display: table}

  #remote-media{background: #fff; min-height: 525px; max-height:525px; overflow: hidden; border: 1px solid #cececc;box-sizing: border-box;background-position: center;background-repeat: no-repeat;margin: 0 auto;}
  #local-media{position: absolute;z-index: 9999 !important;box-shadow: 0px 2px 5px rgba(0,0,0,0.1);top: 25px;right: 25px;width: 96px;height: 96px;overflow: hidden; max-width: 96px; display: inline-block;background: #000;background: transparent;overflow: visible;height: auto;box-shadow: none;}
  div#controls div#preview div#local-media video{width: 100%; height: 100%; max-width: 100%; max-height: 100%; border:none}
</style>
</head>
<body>
  <input type="hidden" name="p_id"  id="p_id" value="">
  <input type="hidden" name="session_time"  id="session_time">
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->
  <div id='block'><table>
    <tr>
      <td>
        <i class="fa fa-desktop"></i>
        <p>You can’t do an interview on your mobile or tablet, you must be on a desktop or laptop to get the most out of your interview.</p>
      </td>
    </tr>
  </table></div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <h3 class="title" style="text-align: left">Applicant</h3>

        </div>
        <?php
        if($this->session->flashdata('success')) {
         $message = $this->session->flashdata('success');
         echo'
         <div class=" alert alert-success alert-dismissible" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
         <i class="fa fa-check-circle"></i>'.$message['message'].
         '</div>';
       }?>
     </div>
     <br>
     <div class="row">
      <div class="col-md-12">
        <div class="our-team-content" style="margin-top: 0">
          <div class="row">
            <!-- Start single team member -->
            <div class="col-md-8 col-sm-6 col-xs-12">
              <div class="interview">
                <div class="interview-screen">
                  <!-- <img src="<?php echo base_url();?>assets/images/Panasonic-screen-ready2.png"/> -->
                  <div id="controls" style="padding:0; margin: 0">
                    <div id="preview" style="padding:0; width:100%; margin: 0">
                      <!--<p class="instructions">Hello Beautiful</p> -->
                      <div id="local-media" ></div>
                      <!--<button id="button-preview">Preview My Camera</button>-->
                    </div>
                    <div id="room-controls" style="width: auto; margin:0">
                      <!--<p class="instructions">Room Name:</p> -->
                      <input id="room-name" type="hidden" placeholder="Enter a room name" value="<?= $room;?>"  />
                      <input id="appointment_id" type="hidden"  name="appointment_id" value="<?= $appointment[0]->appointment_id;?>">
                      <input id="interviewer_id" type="hidden"  name="interviewer_id" value="<?= $appointment[0]->interviewer_id;?>">

                      <!-- <input id="room-name" type="hidden" placeholder="Enter a room name"/>  -->

                      <?php if ($interview_status == 'active') { ?>
                        <button id="button-join"  class="btn btn-success" style="position: absolute; right: 0px; top: -70px;">Join Interview</button>
                        <button id="button-leave" class="btn btn-danger" style="position: absolute; right: 0px; top: -70px;">Leave Interview</button>
                      <?php } elseif ($interview_status == 'feedback') { ?>
                        <button id="feedback_buttom" class="btn btn-danger" style="position: absolute; right: 0px; top: -70px;">Give Feedback</button>  
                      <?php } elseif ($interview_status == 'completed') { ?>  
                        <span class="label label-success"  style="position: absolute; right: 15px; top: -50px; font-size: initial;"> Completed</span>  
                      <?php } ?>  
                    </div>
                    <div id="log" style="display:none"></div>
                  </div>
                  <a href="#" class="interviewee">
                    <!-- <img src="<?php echo base_url();?>assets/images/Screen-Shot-2018-03-29-at-12.10.48-PM.png"> -->
                    <div id="remote-media"></div>
                  </a>
                  <span id="console" >
                    <ul id="interview_timer">
                      <li><div class="timer"></div></li>
                    </ul>
                  </span>
                </div>


              </div>
              <!-- ends single team member -->

             <!--  <div class="about-content" style='padding: 0px 0px; margin-top: 20px'>

                <div class="our-skill">
                 <button class="btn comment-btn" onclick="return send_mail()" style="text-transform:none; display: block; text-align: center; width: 100%">Interviewer not arrived? Click here to message them      </button>    <input type="hidden" name="interviewer_id" value="<?php echo $appointment[0]->interviewer_id;?>" id="interviewer_id">
                   <span id="msg_mail" style="color:green;font-size: 16px;font-weight: normal;margin-left: 240px;"></span>
                  <div class="our-skill-content">

                  </div>
                </div>
              </div> -->

            </div>


            <div class="col-md-4 col-sm-6 col-xs-12">

             <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
              <div class="panel panel-default">
                <div class="panel-heading">

                  <h4 class="panel-title">
                    <!-- <button class="btn comment-btn" onclick="return send_connection_mail()" style="text-transform:none; display: block; text-align: center; width: 100%">Having trouble connecting? Click here</     </button> -->
                    <button class="btn comment-btn" data-toggle="modal" data-target="#myModal" style="text-transform:none; display: block; text-align: center; width: 100%">Having trouble connecting? Click here</     </button>
                    <input type="hidden" style="display: none" name="interviewer_ids" value="<?php echo $appointment[0]->interviewer_id;?>" id="interviewer_ids">

                    <!-- Start for send message on connect call -->

                    <input id="sender_id" type="hidden" value="<?php echo $appointment[0]->interviewer_id;?>" class="form-control input-sm"/>
                    <input id="receiver_id" type="hidden" value="<?php echo $appointment[0]->student_id;?>" class="form-control input-sm"/>
                    <input id="room_name" type="hidden" value="<?php echo $appointment[0]->room_name;?>" class="form-control input-sm"/>
                    <input id="appointment_id" type="hidden" value="<?php echo $appointment[0]->appointment_id;?>" class="form-control input-sm"/>
                    <?php
                    $id=$appointment[0]->interviewer_id;
                    $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);
                    $user_name= $result[0]->first_name;
                    $profile_image= $result[0]->profile_image;
                    ?>
                    <input id="nickname" value="<?php echo ucwords($user_name);?>" type="hidden" class="form-control input-sm"  />
                    <input id="profile_image" value="<?php echo $profile_image;?>" type="hidden" class="form-control input-sm"  />

                    <!-- End for send message on connect call -->
                  </h4>

                </div>
                <!--  <span id="connect_mail" style="color:green;font-size: 16px;font-weight: normal;margin-left: 80px; display: "></span> -->


              </div>

            </div>


            <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
              <div class="panel panel-default">

                <div class="panel-heading">
                        <!--  <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                              Having trouble connecting? <small>Send a message here</small>
                          </a>
                        </h4> -->

                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                           Chat Panel     
                         </a>

                       </h4>

                     </div>
                     <span id="msg_mail" style="color:green;font-size: 16px;font-weight: normal;margin-left: 80px;"></span>
                     <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="panel-body" style="padding: 0">
                        <div class="rushed-box">
                          <div class="mesgs">

                            <div class="msg_history two-way-chat" style="height: 335px">
                              <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg">
                                    <p>Are you available? Should we start?</p>
                                    <span class="time_date"> 11:01 AM</span></div>
                                  </div>
                                </div>

                                <div class="incoming_msg">
                                  <div class="incoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
                                  <div class="received_msg">
                                    <div class="received_withd_msg">
                                      <p>Are you available? Should we start?</p>
                                      <span class="time_date"> 11:01 AM</span></div>
                                    </div>
                                  </div>

                                  <div class="outgoing_msg">
                                   <div class="outcoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
                                   <div class="sent_msg">
                                    <p>There.....yes i am</p>
                                    <span class="time_date"> 11:01 AM</span> </div>
                                  </div>

                                  <div class="incoming_msg">
                                    <div class="incoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
                                    <div class="received_msg">
                                      <div class="received_withd_msg">
                                        <p>Are you available? Should we start? There ?</p>
                                        <span class="time_date"> 11:01 AM <i class="fa fa-check-double color-primary"></i></span></div>
                                      </div>
                                    </div>



                                  </div>
                                  <div class="type_msg">
                                    <div class="input_msg_write">

                                      <input type="text"  id='msg_txt'class="write_msg" placeholder="Message your interviewer for help..." />
                                      <button class="msg_send_btn" id="send_chat" type="button">Send</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>




                    </div>





             <!--  <div class="col-md-4 col-sm-6 col-xs-12">

                       <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a>
                              Rushed Messages <small></small>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body" style="padding: 0">
                          <div class="rushed-box">
                            <div class="mesgs">
                                <div class="msg_history1 one-way-chat" >
                                  <div class="">
                                            <ul class="chat" id="received">
                                            </ul>
                                          </div>

                                     <input id="sender_id" type="hidden" value="<?php echo $appointment[0]->interviewer_id;?>" class="form-control input-sm"/>
                                          <input id="receiver_id" type="hidden" value="<?php echo $appointment[0]->student_id;?>" class="form-control input-sm"/>
                                          <?php $user_name= $this->session->userdata['logged_in']['user_name'];
                                                $interviewer_id=$appointment[0]->interviewer_id;
                                          $result=$this->Twilio_model->get_profile_pic($interviewer_id);
                                            $profile_image=$result[0]->profile_image;

                                          ?>
                                          <input id="profile_image" value="<?php echo $profile_image;?>" type="hidden" class="form-control input-sm" />
                                          <input id="nickname" value="<?php echo ucwords($user_name);?>" type="hidden" class="form-control input-sm"  />
                                </div>
                               <div class="type_msg">

                            </div>
                              </div>
                          </div>
                      </div>
                    </div>

                  </div>

                </div>-->

              </div>
              <div class="clearfix"></div>
              <br>
              <div class="row">
                <div class="col-md-8">
                  <div class="about-content"  style='background: #fff; border:1px solid #eee; padding: 15px 25px; margin-top: 0px'>

                    <div class="our-skill">
                      <div class="row">
                       <div class="col-md-8">
                          <h3>Shared Whiteboard</h3>
                       </div>
                       <div class="col-md-4">                         
                      <button onclick="fetch_whiteboard_data()" style="float: right; margin: 12px;" class="btn btn-primary"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                       </div>
                      </div>
                      <div class="our-skill-content" style="min-height: 340px">
                        <form id="updated_shared_whiteboard">
                          <textarea rows="15" class="form-control" name="shared_whiteboard" id="shared_whiteboard"></textarea>
                          <input type="hidden" name="shared_whiteboard_value" id="shared_whiteboard_value">
                          <input type="hidden" name="room_id" id="room_id" value="<?php echo $this->uri->segment('3'); ?>">
                          <div style="float: right; margin-top: 15px; margin-right: 6px;">
                            <button type="submit" id="submit_bt" class="btn btn-primary"> Save Changes</button>
                            <button id="processing_bt" disabled="disabled" style="display: none;" class="btn btn-primary"><i class="fa fa-spinner fa-spin"></i> Processing...</button>
                          </div>
                        </form>
                      </div> 
                      <!-- <h5 style="margin-top:30px;font-size:16px;">Login to your google account to access this.</h5>
                      <div class="our-skill-content">
                        <div class="contact-area-right" style="min-height: 280px">
                          <iframe src="https://docs.google.com/document/d/1mneIesA2iCKwZ6JjYbnp6tDHSZEp6ZDtRzr07It0tZc/edit?usp=sharing?embedded=true" style="width:100%; height:100%; border:0; min-height:600px"></iframe>
                        </div>
                      </div> -->
                    </div>
                  </div>
                </div>
                <div class="col-md-4">



                 <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                          Previous Feedback
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="panel-body" style="padding: 0">
                        <div>

                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="active"><a href="#pdf" aria-controls="pdf" role="tab" data-toggle="tab">PDF</a></li>
                            <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">VIDEO</a></li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="pdf">

                             <div class="pre-feedback" style="max-height: 308px;">

                              <ul>
                                <?php foreach ($feedback as $row ) {
                                  $originalDate = $row->review_date;
                                  $newDate = date("d M, Y", strtotime($originalDate));
                                  ?>
                                  <li>
                                    <a href="#">
                                      <table>
                                        <tr>
                                          <td>
                                            <!-- <img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/> -->
                                            <?php $image= $row->profile_image; if($image=='')
                                            /* echo '<img src="'.base_url().'assets/images/main-testimonials-img-2.png" alt="team member img">';*/
                                            echo '<img  class="int-img" src="'.base_url().'assets/images/default.png" alt="team member img">';

                                            else
                                             echo '<img  class="int-img" src="'.base_url().$image.'" alt="team member img">';
                                           ?>

                                         </td>
                                         <td>
                                          <h4><?php echo ucwords($row->first_name);?></h4>
                                          <small><?php echo $newDate;?> </small>
                                        </td>
                                        <td>
                                          <input type="hidden" id="downlad_pdf_file_link" value="<?php echo base_url().'Student/previous_feedback/'.$row->review_id;?>" >
                                          <a href="javascript:void(0)" onclick="req_download_pdf()">
                                            <img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png">
                                          </a>
                                        </td>
                                      </tr>
                                    </table>
                                  </a>
                                </li>
                              <?php  } ?>
                            </ul>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="video">
                         <div class="pre-feedback" style="max-height: 308px;">
                          <ul>
                            <?php 
                            if($video) {
                              foreach ($video as $row) {
                               $originalDate = $row->created_date;
                               $newDate = date("d M, Y", strtotime($originalDate));
                               $id=$row->interviewer_id;
                               $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);
                               /* if($result){*/
                                ?>

                                <li>
                                  <a href="#">
                                    <table>
                                      <tr>
                                        <td>

                                          <?php $image= $result[0]->profile_image; if($image=='')
                                          echo '<img class="int-img" src="'.base_url().'assets/images/default.png" alt="team member img">';
                                          else
                                            echo '<img class="int-img" src="'.base_url().$image.'" alt="team member img">';
                                          ?>
                                        </td>
                                        <td>
                                          <h4><?php echo ucwords($result[0]->first_name);?></h4>
                                          <small><?php echo $newDate;?></small>
                                        </td>
                                        <td>
                                          <a href="https://whetstone-oxbridge.com/compose_video/composed/<?php  echo $row->video_url;?>" download><img class="pdficon" src="<?php echo base_url();?>assets/images/video-icon.png"> </a>

                                        </td> 
                            <!--  <td>
                              <a href="#" onclick="download_file()" ><img class="pdficon" src="<?php echo base_url();?>assets/images/video-icon.png"> </a>
                              
                            </td> -->
                            
                          </tr>
                        </table>
                      </a>
                    </li>
                  <?php  } }  ?>

                </ul>
              </div>
            </div>

          </div>

        </div>


      </div>
    </div>
  </div>

</div>


</div>
</div>

</div>
</div>
</div>
</div>

</section>



<div aria-hidden="false" role="dialog" tabindex="-1" id="please-feedback" class="modal leread-modal fade in">
  <div class="modal-dialog">
    <!-- Start login section -->
    <div id="login-content" class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <h4 class="modal-title">Review</h4>
      </div>
      <div class="modal-body clearfix">
        <p style='color:red'>Please provide review otherwise you won't be able to receive feedback and video.</p>
        <div class="about-content"  id="feedback-form" style='visibility:visible !important; background: #fff; border:1px solid #eee; padding: 15px 25px; margin-top: 20px'>

          <div class="our-skill">
            <h3>Review Form</h3>
            <div class="our-skill-content">
              <div class="contact-area-right">
                   <!-- <form action="" class="comments-form contact-form">

                    <div class="form-group">
                      <textarea placeholder="Comment" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">

                    </div>
                        <button class="comment-btn">Submit</button>
                      </form>  -->

                      <div><span id="review_msg" style="color:green;font-size: 18px;font-weight: normal;margin-left: 40px;"></span></div>
                      <form method="post" action="<?php echo site_url('oxbridge/ratings'); ?>"  id="session_review"  class="comments-form contact-form">
                        <!--  <form   id="session_review"  class="comments-form contact-form"> -->

                          <div class="form-group">
                            <h4>How useful was the interview practice?</h4>
                            <div class="radio" >
                              <input type="radio" name="q1" value="I didn't learn anything from this practice interview." required>  <span>I didn't learn anything from this practice interview.</span>
                            </div>
                            <div class="radio" >
                              <input type="radio" name="q1" value="The interview practice was useful but the feedback wasn't very clear." > <span>The interview practice was useful but the feedback wasn't very clear.</span>
                            </div>
                            <div class="radio" >
                              <input type="radio" name="q1" value="Everything about this Interviewer and the feedback was excellent and I would use them again to hone my interview skills further."> <span>Everything about this Interviewer and the feedback was excellent and I would use them again to hone my interview skills further.</span>
                            </div>
                          </div>

                          <div class="form-group">
                            <h4>How challenging were the questions?</h4>
                            <div  class="radio">
                              <input type="radio" name="q2" value="I wasn't really stretched" required> <span>I wasn't really stretched.</span>
                            </div>
                            <div  class="radio">
                              <input type="radio" name="q2" value="I found it too challenging"> <span>I found it too challenging.</span>
                            </div>
                            <div class="radio" >
                              <input type="radio" name="q2" value="I was adequately stretched"> <span>I was adequately stretched.</span>
                            </div>

                          </div>

                          <div class="form-group">
                            <h4>How useful was the face-to-face feedback given?</h4>
                            <div class="radio">
                              <input type="radio" name="q3" value="It was not made clear how I need to improve"> <span>It was not made clear how I need to improve.</span>
                            </div>
                            <div  class="radio">
                              <input type="radio" name="q3" value="The interviewer was overly harsh in their feedback"> <span>The interviewer was overly harsh in their feedback.</span>
                            </div>
                            <div  class="radio">
                              <input type="radio" name="q3" value="Feedback was useful and I know the areas to work on before my next practice"> <span>Feedback was useful and I know the areas to work on before my next practice.</span>
                            </div>

                          </div>

                          <div class='rating-stars '>
                            <ul id='stars'>
                              <li class='star' title='Poor' data-value='1'>

                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Fair' data-value='2'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Good' data-value='3'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Excellent' data-value='4'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='WOW!!!' data-value='5'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                            </ul>
                          </div>
                          <div class='text-message'></div>
                          <div class="form-group">
                            <input type="hidden"  id="ratings" name="rating" required readonly>
                            <input type="hidden"  name="user_type" value="student">
                            <input type="hidden"  name="interviewer_id" value="<?= $appointment[0]->interviewer_id;?>">
                            <input type="hidden"  name="student_id" value="<?= $appointment[0]->student_id;?>">
                            <input type="hidden"  name="room_name" value="<?= $appointment[0]->room_name;?>">
                            <input type="hidden"  name="appointment_id" value="<?= $appointment[0]->appointment_id;?>">

                          </div>
                          <div class="form-group">
                            <h4 style="color:#888; font-size: 12px">Any other comments? (What you thought was particularly useful and what could have been better)</h4>
                            <textarea placeholder="Comment" name="comment" rows="2" class="form-control" required></textarea>
                          </div>
                          <div class="form-group">

                          </div>
                          <button  type="submit" class="comment-btn">Submit</button>
                          <!-- <button class="btn signin-btn" type="submit">submit</button> --><br>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>

        <!-- alert  Modal content-->
        <div class="container">
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header " style="width: 100%;
                background-color:#15585F;
                height: 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Message to interviewer</h4>
              </div>
              <div class="modal-body " >
                <span id="connect_mail" style="color:green;font-size: 16px;font-weight: normal;margin-left: 100px;"></span>  
                <!--  <form id="send_mail" > -->
                 <input type="hidden" name="interviewer_id" value="<?php echo $appointment[0]->interviewer_id;?>" id="interviewer_id">
                 <div class="form-group">                        
                  <textarea placeholder="Message to interviewer..." id="email_message"  rows="2" col="50" class="form-control" required></textarea>
                </div>
                <div id="required_message">
                </div>

                <div class="form-group"> 
                 <div style="display:none" class="loading">
                  <img src="<?php echo base_url();?>assets/images/loader.gif"  style="margin-left: 100px;" />
                </div>                       
                <button  type="submit" onclick="return send_connection_mail()" class="btn btn-info pull-right">Send</button> 
              </div>
              <br>
              <!--  </form> -->  
            </div>
            <div class="modal-footer">

              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    </div>

    <!-- model  -->

     <!-- alert  Modal content-->
      <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="web_cam_model" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header " style="width: 100%;
             background-color:#15585F;
             height: 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Session help Message</h4>
              </div>
              <div class="modal-body " >
                <p> You must be accessing this screen from a desktop or laptop with a web cam.</p>  
              </div>
              <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>

      <!-- model  -->



    <!-- Start footer -->

    <?php $this->load->view('common/footer');?>


    <!--  send Message on connect call  -->
    <script>
     /*$('#button-join1').on('click',function(){
      setTimeout(function(){ 
        var msg='Welcome';
          // alert(msg);
          $.getJSON('<?php echo base_url(); ?>api/send_message_connect?message=' + msg + '&nickname=' + $('#nickname').val() + '&sender_id=' + $('#sender_id').val() + '&receiver_id=' + $('#receiver_id').val() + '&appointment_id=' + $('#appointment_id').val() + '&room_name=' + $('#room_name').val(),
            function (data){

            });
          
          setInterval(function (){
            var height = 0;
            $('.outgoing_msg').each(function(i, value){
              height += parseInt($(this).height());
            });
            height += '';
            $('.msg_history1').animate({scrollTop: height}); 
          }, 2000);

        }, 2000);
    });*/
    $('#button-join').on('click',function(){
      setTimeout(function(){ 
        //alert('join button is click by student');
        var msg='Welcome';
         //alert(msg);
         $.getJSON('<?php echo base_url(); ?>api/send_message?message=' + msg + '&nickname=' + $('#nickname').val() + '&sender_id=' + $('#sender_id').val() + '&receiver_id=' + $('#receiver_id').val() + '&appointment_id=' + $('#appointment_id').val() + '&room_name=' + $('#room_name').val() + '&guid=' + getCookie('user_guid'),
          function (data){

          });
          setInterval(function (){
            var height = 0;
            $('.outgoing_msg').each(function(i, value){
              height += parseInt($(this).height());
            });
            height += '';
            $('.msg_history1').animate({scrollTop: height}); 
          }, 2000);
      }, 2000);
    });


  </script>

  <script>
   $('#button-leave').on('click',function(){
     job=confirm("Are you sure to leave session?");
     if(job==true)
     {
            //return false;
            //alert('yes');

            $.getJSON('<?php echo base_url(); ?>Appointment/update_Call_status?message=' + msg + '&nickname=' + $('#nickname').val() + '&sender_id=' + $('#sender_id').val() + '&receiver_id=' + $('#receiver_id').val() + '&appointment_id=' + $('#appointment_id').val() + '&room_name=' + $('#room_name').val(),
              function (data){
             // alert(data);

           });
          }





        });

      </script>

      <!--  start js for chat server -->

      <script type="text/javascript">
        var request_timestamp = 0;

        var setCookie = function(key, value) {
          var expires = new Date();
          expires.setTime(expires.getTime() + (5 * 60 * 1000));
          document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        var getCookie = function(key) {
          var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
          return keyValue ? keyValue[2] : null;
        }

        var guid = function() {
          function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
          }
          return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }

        if(getCookie('user_guid') == null || typeof(getCookie('user_guid')) == 'undefined'){
          var user_guid = guid();
          setCookie('user_guid', user_guid);
        }


// https://gist.github.com/kmaida/6045266
var parseTimestamp = function(timestamp) {
  var d = new Date( timestamp * 1000 ), // milliseconds
  yyyy = d.getFullYear(),
    mm = ('0' + (d.getMonth() + 1)).slice(-2),  // Months are zero based. Add leading 0.
    dd = ('0' + d.getDate()).slice(-2),     // Add leading 0.
    hh = d.getHours(),
    h = hh,
    min = ('0' + d.getMinutes()).slice(-2),   // Add leading 0.
    ampm = 'AM',
    timeString;

    if (hh > 12) {
      h = hh - 12;
      ampm = 'PM';
    } else if (hh === 12) {
      h = 12;
      ampm = 'PM';
    } else if (hh == 0) {
      h = 12;
    }

  //timeString = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
  timeString = dd + '-' + mm + '-' + yyyy + ', ' + h + ':' + min + ' ' + ampm;

  return timeString;
}

</script>
<script type="text/javascript">
  var sendChat = function (message, callback) {
    $.getJSON('<?php echo base_url(); ?>api/send_message?message=' + message + '&nickname=' + $('#nickname').val() + '&sender_id=' + $('#sender_id').val() + '&receiver_id=' + $('#receiver_id').val() + '&guid=' + getCookie('user_guid'), function (data){
      callback();
    });
  }

  var append_chat_data = function (chat_data) {
    chat_data.forEach(function (data) {
      var is_me = data.guid == getCookie('user_guid');
      var profile_image = $('#profile_image').val();
      if(is_me){


        var html='<div class="incoming_msg">';
        html += '<div class="incoming_msg_img">';
        html += '<img src="<?php echo base_url();?>'+ profile_image +'" ></div><div class="sent_msg"> ';
        html+= '</div><div class="received_msg"><div class="received_withd_msg">';
        html+= '<p>' + data.message + '</p>';
        html+= '<span class="time_date">'+ parseTimestamp(data.timestamp) +'</span></div> </div></div>';
      }else{


        var html='<div class="outgoing_msg">';
        html += '<div class="outcoming_msg_img">';
        html += '<img src="<?php echo base_url();?>'+ profile_image +'" ></div><div class="sent_msg">';
        html+= '<p>' + data.message + '</p>';
        html+= '<span class="time_date">'+ parseTimestamp(data.timestamp) +'</span> </div></div>';
      }
      $("#received").html( $("#received").html() + html);
    });

    $('#received').animate({ scrollTop: $('#received').height()}, 1000);
  }

  var update_chats = function () {
    if(typeof(request_timestamp) == 'undefined' || request_timestamp == 0){
    var offset = 60*15; // 15min
    request_timestamp = parseInt( Date.now() / 1000 - offset );
  }
  $.getJSON('<?php echo base_url(); ?>api/get_messages?timestamp=' + request_timestamp + '&sender_id=' + $('#sender_id').val() + '&receiver_id=' + $('#receiver_id').val(), function (data){
    append_chat_data(data);

    var newIndex = data.length-1;
    if(typeof(data[newIndex]) != 'undefined'){
      request_timestamp = data[newIndex].timestamp;
    }
  });
}
</script>

<script type="text/javascript">
  $('#submit').click(function (e) {
    e.preventDefault();

    var $field = $('#message');
    var $sender = $('#sender_id');
    var $receiver = $('#receiver_id');
    var data = $field.val();

    $field.addClass('disabled').attr('disabled', 'disabled');
    sendChat(data, function (){
      $field.val('').removeClass('disabled').removeAttr('disabled');
    });
  });

  $('#message').keyup(function (e) {
    if (e.which == 13) {
      $('#submit').trigger('click');
    }
  });

  setInterval(function (){
    update_chats();
  }, 4500);

</script>



<!--  End js for chat server -->

<!-- End footer -->

<!-- jQuery library -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>     -->


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>

<!-- Bootstrap -->
    <!-- <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script> -->

    <!-- counter -->
    <!-- <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  -->


    <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>-->
<!--    <script src="<?php echo base_url();?>assets/js/Mini-Event-Calendar-Plugin-jQuery/src/mini-event-calendar.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".calendar").MEC();
        });
      </script>-->
      <!-- Custom js -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/js/Monthly-Event-Calendar-pbcalendar/pb.calendar.js"></script>
      <script src="<?php echo base_url();?>assets/js/timer/dist/ez.countimer.js"></script>

      <script>
        function get_chat() {
         var id='<?=$this->uri->segment('3')?>';
         $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>twilioController/get_user_chat_ajax",
          data:{id:id},
          dataType: "json",
          success: function (result) {
            if (result['status'] == true) {
                        // alert(result['design']);
                        $('.msg_history').html(result['design']);

                        $(".msg_history").scrollTop($(".msg_history").prop("scrollHeight"));
                      }
                      else {

                        $('.msg_history').html(result['design']);

                        $(".msg_history").scrollTop($(".msg_history").prop("scrollHeight"));
                      }
                    }
                  });


       }


       $(document).ready(function () {
        if (localStorage.getItem("webcam_status") == null) {
          $('#web_cam_model').modal('show');
        localStorage.setItem("webcam_status", "yes");
        }
        
        $(document).bind('keypress', function(e) {
          if(e.keyCode==13){
            $('#send_chat').trigger('click');
          }
        });


        setInterval(get_chat, 500 * 15);
        get_chat();

        $(document).on('click', '#send_chat', function (e) {
         var message=$('#msg_txt').val();
         var id='<?=$this->uri->segment('3')?>';
         if (message) {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>twilioController/send_chat_ajax",
            data: {id: id,message:message},
            dataType: "json",
            success: function (result) {
              if (result['status'] == true) {

               get_chat();
               $('#msg_txt').val('');
             }
             else {

              get_chat();
              $('#msg_txt').val('');
            }
          }
        });
        }
        else
        {
          if(message=='')
          {
                        //$('#chat_text').css({"background-color": "red"});
                      }
                    }



                  });

      });



    </script>


    <script type="text/javascript">

     $(function(){
      if ($(window).width() < 768) {
        $('#block').show();
      }
    });

     $(document).ready(function(){

      //setTimeout(function(){ alert('hiii'); }, 3000);
    //$("#button-join").click();
  });
</script>

<script>
  $( "body" ).on( "click", "#button-leave", function(e) {
  //$('#button-leave').on('click',function(){
    console.log('levebutton is click by me');
    $('#feedback-form').css('visibility','visible');
    $('.timer').countimer('stop');
    $('#please-feedback').modal();
  });
  $('#feedback_buttom').on('click',function(){
    $('#feedback-form').css('visibility','visible');
    $('#please-feedback').modal();
  });
  $('#button-join').on('click',function(){
    window.setInterval(function(){
    //var session_time = $('#p_id').val();
    var session_time = $('#session_time').val();
    // alert(session_time);
    if (session_time == '') {
      return;
    }
    // alert('atul');
    $('.timer').countimer({
      enableEvents: true
    }).on('second', function(evt, time){

      if(time.original.seconds === 300)
      {
        alert(" Session Over");
      }
    });
    }, 10000);


//
//$.screentime({
//  fields: [
//    { selector: '#interview_timer',
//      name: 'Top'
//    },
//    { selector: '#middle',
//      name: 'Middle'
//    },
//    { selector: '#bottom',
//      name: 'Bottom'
//    }
//  ],
//  reportInterval: 1,
//  callback: function(data) {
//    $.each(data, function(key, val) {
//      var $elem = $('#console li[data-field="' + key + '"]');
//      var current = parseInt($elem.data('time'), 10);
//
//      $elem.data('time', current + val);
//      $elem.find('span').html(current += val);
//    });
//  }
//});

});
</script>
<!--  send mail to inerviewer -->
<script type="text/javascript">
  function send_mail() {
    var interviewer_id = $('#interviewer_id').val();
      //alert(interviewer_id);
      $.ajax({
        url: '<?php echo base_url();?>Student/send_mail_interviewer/' + interviewer_id ,
        success: function(response)
        {
              //alert(response);
              $('#msg_mail').text(response);
            }
          });
      setTimeout(function(){
        $("#msg_mail").hide();
      }, 30000);
    }
  </script>

  <!--  send mail to inerviewer -->
  <script type="text/javascript">
    $(document).ready(function() {

  $('#email_message').keydown(function(event) {
    if (event.keyCode == 13) {
      send_connection_mail();
      return;
    }
  });

});
  </script>
  <script type="text/javascript">
    function send_connection_mail() {
      var interviewer_connect = $('#interviewer_ids').val();
      var message = $('#email_message').val();
      if(!message ==''){
        $(".loading").show();

        // $.ajax({
        //   url: '<?php echo base_url();?>Student/send_mail_connection_issues/' + interviewer_connect +'/'+ message  ,
        //   success: function(response)
        //   {
        //       //alert(response);
        //       $('#connect_mail').text(response);
        //       $(".loading").hide();
        //       setTimeout(function(){
        //         $("#connect_mail").text("");
        //         $("#email_message").val("");
        //         $('#myModal').modal('hide');
        //       }, 1000);
        //     }
        //   });

        $.ajax({
          url:"<?php echo base_url() . 'Student/send_mail_connection_issues'?>", 
          method:'POST',  
          data:{interviewer_connect:interviewer_connect,message:message},
          success: function(response)
          {
              //alert(response);
              $('#connect_mail').text(response);
              $(".loading").hide();
              setTimeout(function(){
                $("#connect_mail").text("");
                $("#email_message").val("");
                $('#myModal').modal('hide');
              }, 1000);
            }
          });


        
      }
      else{

        responseText = '<span style="color:red;font-size: 12px;font-weight: normal;margin-left: 40px;"> Please type your message</span>';

        $("#required_message").html(responseText);
      }
    }
  </script>



  <script>
    $('#button-leave').on('click',function(){
      $('#feedback-form').css('visibility','visible');
      $('#please-feedback').modal();
    });
    $('#button-join').on('click',function(){
      $.screentime({
        fields: [{ 
          selector: '#interview_timer',
          name: 'Top'
          },
          { selector: '#middle',
            name: 'Middle'
          },
          { selector: '#bottom',
          name: 'Bottom'
        }],
        reportInterval: 1,
        callback: function(data) {
          $.each(data, function(key, val) {
            var $elem = $('#console li[data-field="' + key + '"]');
            var current = parseInt($elem.data('time'), 10);

            $elem.data('time', current + val);
            $elem.find('span').html(current += val);
          });
        }
      });
    });
  </script>
  <script>
    $(document).ready(function(){
      var current_yyyymm_ = moment().format("YYYYMM");
      $("#pb-calendar").pb_calendar({

  // is date selectable?
  'day_selectable' : false,

  // callbacks
  'callback_selected_day' : $.noop,
  'callback_changed_month' : $.noop,

  // min/max dates
  'min_yyyymm' : null,
  'max_yyyymm' : null,

  // navigation arrows
  'next_month_button' : '<img src="<?php echo base_url();?>assets/images/arrow-right.png" class="icon">',
  'prev_month_button' : '<img src="<?php echo base_url();?>assets/images/arrow-left.png" class="icon">',

  // custom label format
  'month_label_format' : "MMM",
  'year_label_format' : "YYYY",

  schedule_list : function(callback_, yyyymm_){
    var temp_schedule_list_ = {};

    temp_schedule_list_[current_yyyymm_+"03"] = [
    {'ID' : 1, style : "red"}
    ];

    temp_schedule_list_[current_yyyymm_+"10"] = [
    {'ID' : 2, style : "red"},
    {'ID' : 3, style : "blue"},
    ];

    temp_schedule_list_[current_yyyymm_+"20"] = [
    {'ID' : 4, style : "red"},
    {'ID' : 5, style : "blue"},
    {'ID' : 6, style : "green"},
    ];
    callback_(temp_schedule_list_);
  },
  schedule_dot_item_render : function(dot_item_el_, schedule_data_){
    dot_item_el_.addClass(schedule_data_['style'], true);
    return dot_item_el_;
  }

});
    });
  </script>
  <!--  script for star rating  -->
  <script type="text/javascript">

    $(document).ready(function(){

      /* 1. Visualizing things on Hover - See next part for action on click */
      $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });

  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });


  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }

    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
      msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
      msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg,ratingValue);

  });


});


    function responseMessage(msg,ratingValue) {
 // $('.success-box').fadeIn(200);
 $('#ratings').val(ratingValue);
 $('div.text-message').html("<span>" + msg + "</span>");
}

</script>
<script type="text/javascript">

  function download_file()
  {
    alert('hiii');
    var type= $("#select").val();
    var date = $("#dateIpone").val();
    var combine = type+date;
    var path = "logfile/"+combine+".txt"; //relative-path
    $('location').attr('href',path);
  }
</script>










<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<!-- <script src="//media.twiliocdn.com/sdk/js/video/releases/2.0.0-beta1/twilio-video.min.js"></script> -->
<!-- <script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script> -->
<script src="<?=base_url()?>assets/js/twilio-video.min.js"></script>
<script src="<?=base_url()?>assets/js/index.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>

<!-- <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script> -->

<script type="text/javascript">
        $( document ).ready(function() {
          console.log( "ready!" );
          fetch_whiteboard_data();          
        });
        $(document).on('submit', '#updated_shared_whiteboard', function(event){  
          event.preventDefault();
         $('#shared_whiteboard_value').val(CKEDITOR.instances.shared_whiteboard.getData());
          $('#processing_bt').show();
          $('#submit_bt').hide();
          $.ajax({  
            url:"<?php echo base_url() . 'TwilioController/updated_shared_whiteboard'?>", 
            method:'POST',  
            data:new FormData(this),  
            contentType:false,  
            processData:false,  
            success:function(data)  
            {                 
              $('#processing_bt').hide();
              $('#submit_bt').show();
            }
          });
        });
        function fetch_whiteboard_data() {
          var room_id = $('#room_id').val();
          $.ajax({  
            url:"<?php echo base_url() . 'TwilioController/fetch_shared_whiteboard'?>", 
            method:'POST',  
            data:{ room_id:room_id},  
            success:function(data)  
            {
              if (data != 0) {
                CKEDITOR.instances.shared_whiteboard.setData(data);
              }else{
                
              }
            }
          });
        }
      </script>
      <script type="text/javascript">
        function req_download_pdf() {
          window.open($('#downlad_pdf_file_link').val(),'_blank');
        }
        $(document).ready(function() {
          CKEDITOR.replace( 'shared_whiteboard', {
            extraPlugins: 'easyimage',
            removePlugins: 'image',
            cloudServices_tokenUrl: 'https://41245.cke-cs.com/token/dev/ggMSZPgRqvPslGyrY1x3OIOy575wAVurfd1ctVRh402fGP0KvmDStr7nQO2n',
            cloudServices_uploadUrl: 'https://41245.cke-cs.com/easyimage/upload/'
          });
        });
      </script>
</body>
</html>
