<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> New Home</title>
    <?php $this->load->view('common/header_assets');?>
    <style>
        .single-team-member > p{min-height: 160px; max-height: 160px; overflow: hidden}
        .testimonial-content ul li{ text-align: left; margin-bottom: 20px}
        .testimonial-content ul li i{color:#2aaebf; margin-right: 10px;}
    </style>
     <style>
        #schools_features ul{list-style-image: url(<?php echo base_url();?>assets/images/check.png)}
        #schools_features p{color:#fff; font-size: 16px; font-weight: bold}
        .title-area p{color:#fff; font-size: 16px; font-weight: bold}
       
    </style>
  </head>
  <body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  
  <!-- Start slider -->
  <section id="slider">
    <div class="main-slider">
      <?php foreach ($banner as $row) {?>
        
      <div class="single-slide">
        <img src="<?php echo base_url().$row->banner_image;?>" alt="img">
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center">
                <div class="slide-article text-center">
                  <h1 class="wow fadeInUp text-center" style="color:#2aaebf" data-wow-duration="0.5s" data-wow-delay="0.5s"><?= $row->title;?></h1>
                  <p class="wow fadeInUp text-center" style="font-size:19px; margin-bottom: 90px" data-wow-duration="0.5s" data-wow-delay="0.75s"><?= $row->sub_title;?></p>
                  <a class="read-more-btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" href="<?php echo base_url();?>interviewers-list">Read More</a>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php }?>
   <!--    <div class="single-slide">
        <img src="<?php echo base_url();?>assets/images/slider-3.jpg" alt="img" style>
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center">
                <div class="slide-article text-center">
                  <h1 class="wow fadeInUp text-center" style="color:#2aaebf" data-wow-duration="0.5s" data-wow-delay="0.5s">Unparalleled Student Network</h1>
                  <p class="wow fadeInUp text-center" style="font-size:19px; margin-bottom: 90px" data-wow-duration="0.5s" data-wow-delay="0.75s">From Oxbridge students to school students…</p>
                  <a class="read-more-btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" href="<?php echo base_url();?>interviewers-list">Read More</a>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->          
    </div>
  </section>
  <!-- End slider -->
  
  <!-- Start about  -->
  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">What we do</h2>
            <span class="line"></span>
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="about-content">
            <div class="row">
              <div class="col-md-6">
                <div class="our-skill">
                               
                  <div class="our-skill-content">
                  <p>
                    <?php  echo $home[0]->what_we_do;?>
                  <!-- Oxbridge interviews are constantly changing as competition for these two leading universities ever increases.
Whetstone is an Oxbridge applications advisory guaranteeing the highest quality interview practice with successful interviewees currently studying at Oxford and Cambridge.<br> </p><p>We have found that the advice given to applicants all too often comes from graduates and reflects the out-of-date interview experience that they had. Therefore, Whetstone has formed a network of current students covering Oxford and Cambridge who are dedicated to properly prepareapplicants for what is going on behind the interview doors right now. --> 
</p>
                  
                    <a class="read-more-btn wow fadeInUp" style="border:1px solid #2aae8f; color:#2aae8f" data-wow-duration="1s" data-wow-delay="1s" href="<?php echo base_url();?>about-us">More Info</a>
                  </div>                  
                </div>
              </div>
              <div class="col-md-6">
             <!--   <div class="our-skill">
                  <p>% of students placed</p>
                  <div class="our-skill-content">
                    <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="70">
                        <span class="progress-title">Cambridge</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="85">
                        <span class="progress-title">Oxford</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="70">
                        <span class="progress-title">Velley</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="60">
                        <span class="progress-title">MacLarn</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="40">
                        <span class="progress-title">The Khan's</span>
                      </div>
                  </div>
                 
                  </div>       -->
                 
               
            
                  <img src="<?php  echo base_url().$home[0]->what_we_do_image;?>" class="img img-responsive">
                      <div class="single-feature wow zoomIn">
                  <i class="fa fa-thumbs-o-up feature-icon"></i>
                  <h4 class="feat-title">Internationally trusted </h4>
                  <p>Our network of current Oxbridge students have successfully helped applicants from these countries. </p>
                </div>         
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end about -->
<section id="testimonial">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" style='color:#fff'>How it works</h2>
                <span class="line"></span>           
              </div>
            </div>
            <div class="col-md-6">
              <!-- Start testimonial slider -->
              <div class="">
                <!-- Start single slider -->
                <div style="margin-top:80px">
                
                  <div class="testimonial-content">
                      <ul style="list-style-image: url(<?php echo base_url();?>assets/images/check.png); padding-left: 35px; margin-top: 120px ">
                          <li ><?= $home[0]->how_it_work_content1;?></li>
                          <li ><?= $home[0]->how_it_work_content2;?></li>
                          <li ><?= $home[0]->how_it_work_content3;?></li>
                      </ul>  
               
                  </div>
                </div>
              </div>
            </div>
              <div class="col-md-6" style="padding-top:40px">
                  <div class="inner">
                  <!-- <iframe id="video" src="https://player.vimeo.com/video/301238869" style="width:100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                   <iframe id="video" src="<?= $home[0]->how_it_work_url;?>" style="width:100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
              </div>
              </div>
          </div>
        </div>
        <div class="col-md-6"></div>        
      </div>
    </div>
  </section>
  
  <div class="clearfix"></div>
  <section id="" style="background: #f6fef7; padding: 40px 0 ">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >Real-Time Video Feedback</h2>
                <span class="line"></span>           
              </div>
            </div>
            <div class="col-md-6" style="margin-top:40px">
                  
                <img src="<?= $home[0]->real_time_video_image1;?>" style="width: 100%"/>
                
              </div>
              <div class="col-md-6" style="margin-top:40px">
                  
                <img src="<?= $home[0]->real_time_video_image2;?>"  style="width: 100%"/>
                
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12" style="margin-top:40px" id="schools_features">
                  <h4  style="text-align:center; color:#333; font-weight:bold">
                      <!-- Part of what makes Whetstone Oxbridge so unique is the style of feedback we offer at the end of your practice interview. -->
                      <?= $home[0]->real_time_video_heading;?>
                  </h4>
				  <br>
                  <!-- <ul style="text-align:center; padding-left: 35px; background: white; padding:20px 20px 20px 55px; list-style-image: url(<?php echo base_url();?>assets/images/check.png)"> -->
                     <ul  style="list-style-image: url(<?php echo base_url();?>assets/images/check.png)">
                    <?= $home[0]->real_time_video_content;?>
                      <!-- <li style="text-align:left; margin-bottom:20px">
                          <span style="float: left"> At the end of the session you will be sent a video recording of your interview with real- time feedback comments.</span>
                          
                      </li>
                      <li style="text-align:left; margin-bottom:20px">
                          <span style="float: left">This means that when the interviewer makes a note about you during the interview, it is saved and will appear on your screen in the recorded version of the interview that you receive afterwards.</span>
                          
                      </li>
                      <li style="text-align:left; margin-bottom:20px">
                          <span style="float: left">You will also be sent comprehensive written notes detailing the questions that were asked, as well as any reading recommendations and areas to improve on.</span>
                           
                      </li> -->
                  </ul>
              </div>
            <div class="col-md-12">
              <!-- Start testimonial slider -->
              <div class="testimonial-slider">
                <!-- Start single slider -->
                <div class="single-slider">
                
                    <div class="testimonial-content">
                        
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
         
      </div>
    </div>
  </section>
  
  
  <!-- Start Feature -->
  <section id="feature">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Why Choose Us?</h2>
            <span class="line"></span>
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="feature-content">
            <div class="row">
              <?php foreach ($choose as $row) {?>
               
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="<?= $row->icon;?>"></i>
                  <h4 class="feat-title"><?= $row->heading;?> </h4>
                  <p><?= $row->content;?></p>
                </div>
              </div><?php } ?>
               <!-- <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-star feature-icon"></i>
                  <h4 class="feat-title">High Quality Feedback </h4>
                  <p>At the end of your practice interview you will receive the video of yourself during the interview with real time feedback comments. You will also have a 15 minute de-brief in which your interviewer will outline where you need to improve. You can then take their notes and the video and go over it in your own time before coming back for another practice.</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-support feature-icon"></i>
                  <h4 class="feat-title">Additional Support </h4>
                  <p>We work with schools and teachers to provide them with additional support in their students' applications. Schools can only provide a limited number of practice interviews. We encourage teachers to help their students use Whetstone Oxbridge as another preparation tool. If requested, we send interview feedback to both teacher and student so that they can work on the necessary improvements together.</p>
                </div>
              </div> -->
             
                <div class="clearfix"></div>
                
              <!-- <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-gears feature-icon"></i>
                  <h4 class="feat-title">Largest student network </h4>
                 <p> You can select interviewers based on university, course and even college. Spanning all courses and colleges, at both Oxford and Cambridge, we will help connect you with students who successfully went through the exact same interviews that you will be taking.</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-bank feature-icon"></i>
                  <h4 class="feat-title">Current students </h4>
                  <p>We only employ current Oxbridge students. Results have shown that this leads to greater chances of success because no one can shed better light on the nature of the Oxbridge interview at the moment than those who successfully completed the process only a year or two before you.</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-smile-o feature-icon"></i>
                  <h4 class="feat-title">Practice makes Perfect </h4>
                  <p>We offer practice interview packages which enable you to mix-and-match and practice with as many different Oxbridge students as you like to get experience with different interview styles and questions.</p>
                </div>
              </div> -->
                
             
                
            </div>
          </div>
        </div>
      </div>
        
        <div class="row">
            
<!--      <div class="map-container">
        <div id="worldmap" style="width: 600px; height: 400px"></div>
    </div>-->
        </div>
        
    </div>
  </section>
  <!-- End Feature -->


  <!-- 
  <section id="counter">
    <div class="counter-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="counter-area">
              <div class="row">
            
                <div class="col-md-4 col-sm-6">
                  <div class="single-counter">
                    <div class="counter-icon">
                      <i class="fa fa-suitcase"></i>
                    </div>
                    <div class="counter-no counter">
                     <?php echo $this->db->where('user_role_id',2)->count_all_results('users');?>
                    </div>
                    <div class="counter-label">
                      Interviewers
                    </div>
                  </div>
                </div>
               
                <div class="col-md-4 col-sm-6">
                  <div class="single-counter">
                    <div class="counter-icon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="counter-no counter">
                      <?php echo $this->db->count_all_results('appointment');?>
                    </div>
                    <div class="counter-label">
                      Interviews
                    </div>
                  </div>
                </div>
             
                <div class="col-md-4 col-sm-6">
                 <div class="single-counter">
                    <div class="counter-icon">
                      <i class="fa fa-users"></i>
                    </div>
                    <div class="counter-no counter">
                    <?php echo $this->db->where('user_role_id',3)->count_all_results('users');?>
                    </div>
                    <div class="counter-label">
                      Student
                    </div>
                  </div>
                </div>
               
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
   -->
  
  <!-- 
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Top Ranking Interviewers</h2>
            <span class="line"></span>
          
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
            
               <?php foreach ($interviewer as $row) {?>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   
                    <?php $image=$row->profile_image;
                     if($image=='')
                    echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="filter:grayscale(100%)"  style="filter:grayscale(100%)">';
                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="filter:grayscale(100%)">';
                     ?>
                 </div>
                 <div class="team-member-name">
                 
                   <p><?php echo $row->first_name.''.$row->last_name;?></p>
                   <span><?php echo $row->university_name;?></span>
                 </div>
                 <?php echo '<p>'.$row->about_us.'</p>';?>
                 <div class="team-member-link">
                   <a href="<?php echo base_url().'interviewer/profile/'.$row->user_id;?>">View</a>
                   
                 </div>
                </div>
              </div>
            <?php }?>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
   -->


  <!-- Start Service -->
  <section id="service">
    <div class="container-fluid">
      
        <div class="row">
            <div class="side">
                <div class="side-content">
                    <div>
                        <h4 style="color:#fff; opacity: 0.5"> <?= $home[0]->school_heading;?></h4>
                        <h2><span style="color: #ffffff;"><?= $home[0]->school_content;?></span></h2>
                    <a itemprop="url" href="<?= $home[0]->school_link;?>" target="_self" style="color: #ffffff" class="eltdf-btn eltdf-btn-medium eltdf-btn-simple eltdf-btn-arrow">
                        <span class="eltdf-btn-text">Read More</span>
                    </a>		
                    </div>
                </div>
                <div class="side-img" style="background-image: url(<?php echo base_url().$home[0]->school_image;?>)">
                </div>
            </div>
      </div>
    </div>
  </section>
  <!-- End Service -->



<section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title">Contact Us</h2>
              <span class="line"></span>
            </div>
         </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               <div class="col-md-5">
                 <div class="contact-area-left">
                   <h4>Contact Info</h4>
                   <p><?= $home[0]->contact_content;?></p>
                   <br>
                   <table style="width: 100%; margin-top: 10px">
                       <tr>
                           <td style="vertical-align:top"> <div style="max-height: 140px;overflow: hidden;"> 
                                   <img src='<?php echo base_url().$home[0]->contact_image;?>' height="200"/> </div></td>
                           <td style="vertical-align:top; padding-left: 20px">
                                <address class="single-address">
                    
                                <p style="color:#333; font-weight: bold; font-size:21px;"><?= $home[0]->contact_title;?></p>
                                <p style="color:#2baec0; font-weight: bold; font-size:18px;">Whetstone Education Ltd</p>
                                <p><?= $home[0]->contact_email;?></p>
                                <p>+44(0)20 8133 8277</p>
                                <p></p>
                              </address> 
                           </td>
                       </tr>
                   </table>
                  
                              
                 </div>
               </div>
               <div class="col-md-7">
                 <!-- <div class="contact-area-right">
                   <form action="" class="comments-form contact-form">
                    <div class="form-group">                        
                      <input type="text" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">                        
                      <input type="email" class="form-control" placeholder="Email">
                    </div>
                   
                    <div class="form-group">                        
                      <textarea placeholder="Comment" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">                        
                   <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a style="color:#888" target="_blank" href="<?php echo base_url();?>privacy-policy">GDPR Policy</a>
                    </div>
                        <button class="comment-btn">Submit</button>
                  </form>
                 </div> -->

                 <div class="contact-area-right">
                  <div id="contact_message"></div>
                   <form action="" id="contact_us" class="comments-form contact-form">
                    <div class="form-group">                        
                      <input type="text" name="contact_name" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">                        
                      <input type="email" name="contact_email" class="form-control" placeholder="Email">
                    </div>
                   
                    <div class="form-group">                        
                      <textarea placeholder="Comment"  name="contact_comment"rows="3" class="form-control"></textarea>
                    </div>
                   <!--  <div class="form-group">                        
                      <input type="checkbox" name="agree_terms" style='height: auto'> I agree with GDPR Policy
                    </div> -->
                     <div class="form-group">                        
                   <input name="agree_terms" style="height: auto;" type="checkbox"> I agree with <a style="color:#888" target="_blank" href="<?php echo base_url();?>privacy-policy">GDPR Policy</a>
                    </div>
                        <button class="comment-btn" type="submit">Submit</button>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  
  <!-- Start subscribe us -->
 <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
  
  <script type="text/javascript">
    $(function myFunction(){
  
    $('#contact_us').validate({
        rules:{
            contact_name:{
                required:true,
                minlength:4,
                maxlength:60,
               
            },
            contact_email:{
                required:true,
                email:true,
            },
            contact_comment:{
                required:true,
                 minlength: 6,
                maxlength: 650,
            },
            agree_terms:{
                required:true,
                
            },
            
        },
        messages:{
            contact_name:{
                required:"  Please enter  name ",
                minlength:"Please enter name minmum 4 character",
                maxlength:"Name should not more than 60 character",
                
            },

            contact_email:{
                required:"  Please enter email address ",
                email:"Please enter valid email id ",
            },
            contact_comment:{
                required: "Please enter comment",
                minlength: "Please write comment minmum 15 characters",
                maxlength:"Comment should not more than 650 characters",


            },
            agree_terms:{
                required: "Please check I agree with GDPR Policy",
               


            },
            
        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>oxbridge/contact_us', 
            $('#contact_us').serialize(), 
            function(data){
          
                if(data == 1)
                { 
                     responseText = '<span style="color:green;font-size: 16px;font-weight: normal;margin-left: 40px;">Thank you for contact us, Whetstone Oxbridge Team will reply soon</span>';
                    $("#contact_message").html(responseText);
                }
                 if(data == 0)
                {
                  responseText = '<span style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;">Your mail id already exist</span>';
                  //$('#message1').text('Your email Id already exist');
                  $("#contact_message").html(responseText);
                }
               
                

            });
       }
   });
});
  </script>
  <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/froogaloop.js"></script>
  <script src="https://player.vimeo.com/api/player.js"></script>
  <script>
  var players = [];
$('.inner').each(function() {
  players.push({
      player: new Vimeo.Player($(this).find("iframe").get(0)),
      top: $(this).offset().top,
      status: "paused"
  });
});
var viewportHeight = $(window).height();

$(window).on('scroll', function() {
    var scrollPos = $(window).scrollTop();
//    console.log(scrollPos);
    for(var i=0; i<players.length;i++) {
//        console.log(players[i].top); 
        var elementFromTop = players[i].top - scrollPos;
        console.log(scrollPos+"|"+players[i].top+"|"+elementFromTop+"|"+viewportHeight);
      var status = (elementFromTop > 0 && elementFromTop < players[i].top + viewportHeight) ? "pause" : "play";   
      if(players[i].status !== status) {
          players[i].status = status;
          players[i].player[status]();
          console.log(i, status);      
      }
    }
});
  </script>
  </body>
</html>