<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> New Privacy-Policy</title>
      <?php $this->load->view('common/header_assets');?>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .tnc{background: #fff; width: 100%; padding: 40px 45px; margin-bottom: 10px; text-align:left}
    .tnc .org-logo{text-align: center}
    .tnc .org-logo > img{ margin-bottom: 20px}
    .tnc h1{text-align: center; font-size: 24px; }
    .tnc .tnc-category{font-size: 16px;  color: #333; margin-top: 60px; display: block}
    .tnc .tnc-subcategory{font-size: 14px;  color: #333; margin-top: 20px; display: block}
    .tnc p {font-size: 14px; color: #333; margin-top: 20px; line-height: 24px}
    .tnc p.uppercase{text-transform: uppercase}
    .uppercase{text-transform: uppercase}
    .tnc p.lowercase{text-transform: lowercase}
    .lowercase{text-transform: lowercase}
    .tnc capitalcase{text-transform: capitalize}
    .tnc p.bold{font-weight: bold}
    .tnc ul{margin-top: 20px; list-style: disc; list-style-position: outside; padding-left: 0 }
    .tnc ol{margin-top: 20px; list-style: decimal;list-style-position: outside; padding-left: 0 }
    .tnc ul li{margin-top: 20px; line-height: 24px; margin-left: 20px; font-size: 14px; color:#333}
    .tnc ol li{margin-top: 20px; line-height: 24px; margin-left: 20px; font-size: 14px; color:#333}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

 <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
 <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Privacy Policy</h2>
              <p></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Privacy Policy</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start error section  -->
  <section id="error">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="errror-page-area">
       
            <div class="error-content">
              <span>Privacy Policy</span>
         
                               
                                               
                            <div class="tnc">
                              <?php echo $privacy[0]->body;?>
                             <!--    <p>
                                    <span class="uppercase">W</span>e understand that we have a responsibility to protect and respect your privacy and look after your personal data. This Privacy Policy explains what personal data we collect, how we use and store it, and the reasons we may need to disclose it to other parties. Data Protection law will change on 25 May 2018 when the General Data Protection Regulations (GDPR) come into force and this Privacy Policy sets out your rights under these new laws.
                                </p>
                            
                            <a href="#" class="tnc-category">Who are we?</a>
                            <p>
                                <span class="uppercase">W</span>e are Whetstone Education Ltd T/As Whetstone Oxbridge. Registered in England & Wales No. 11596562 at Mill Green House, Mill Green Road, Haywards Heath, RH16 1XJ.
                            </p>
                            <p>
                                <span class="uppercase">F</span>or the data we collect and subsequently process, we are the “Data Controller”.  This is a legal term which means that we make decisions about how and why we use your personal data.  As the Data Controller, we are responsible for making sure that your personal data is used in accordance with applicable data protection laws and this Policy. When you provide your personal data to another party such as a finance broker who then discloses this information to us for the sole purpose of reviewing your finance application, we will process that data in accordance with this Policy. In these circumstances we are acting as the “Data Processor”.
                            </p>
                            
                            <a href="#" class="tnc-category">How the law protects you</a>
                            <p>Data protection laws state that we are only able to process personal data if we have valid reasons to do so. The reasons we process your personal data are:</p>
                             <ul>
                                 <li><b>Consent</b>: You have given clear consent for us to process your personal data for a specific purpose.</li>
                                 <li><b>Contract</b>: the processing is necessary for a contract you have with us, or because you have asked us to take specific steps before entering into a contract (e.g. to provide you with documentation regarding our products and services).</li>
                                 <li><b>Legal obligation</b>: the processing is necessary for us to comply with our legal and/or regulatory obligations (e.g. HMRC reporting or Credit Reference Agencies).</li>
                                 <li><b>Legitimate interests</b>: the processing is necessary for our legitimate interests or the legitimate interests of a third party (e.g. to market to you similar products and services in the future).</li>

                            </ul>
                            
                            <a href="#" class="tnc-category">How do we collect personal data from you?</a>
                             <ul>
                                <li>Personal information means information that identifies someone as an individual.
                                <li>	We receive and collect information about you when you use our website, complete our documents, communicate with us by phone, post or email.</li>
                                <li>	The personal information collected by Whetstone Education Ltd includes:</li>


                            </ul>
                            
                            <ol>
                                <li>
                                    Information you give us  <br>
-	personal details (name, age, email, phone number)<br>
-	financial details and credit card information <br>
-	education details (name of school or university and year of study)<br>
-	details of services booked and used on our website <br>
-	complaints, reviews, refunds and payments through our website <br>
                                </li>
                                
                                <li>
                                    Information we gather from your use of our website <br>
-	login information <br>
-	location (IP address) and time zone used to connect your computer to the internet <br>
-	video recordings of interview sessions (stored for five days after interview for quality monitoring purposes)<br>
-	Uniform Resource Locators (ULR)<br>
-	services reviewed or searched for <br>
-	pages navigated<br>
-	time spent on a given page <br>
-	download errors <br>
-	page interaction information<br>
                                </li>
                            </ol>
                            <p>Calls to our offices may be recorded and the recordings are used for quality and training purposes.</p>

	

                            
                            <a href="#" class="tnc-category">Cookie Usage on our website </a>
                            <p>
                                <span class="uppercase">T</span>here are instances where we may use cookies to gather information regarding our services in a mathematical collection for our website. Any information collected will not have any identifying data. It is statistical data about our visitors and how they have used our site. No personal details will be shared that could identify you.<br>
We may assemble information about your common internet use with a cookie file. When used, the cookies are downloaded to your computer automatically. The cookie is stored on the hard drive, with transferred information. The data sought by the cookie helps us improve our website and any service offered to you.
Your browser has the ability to decline cookies. This is done by setting your browser options to decline all cookies. Note: if you do decline the download of cookies, some aspects of our site may not work or allow you access.

                            </p>
                          
                            
                            <a href="#" class="tnc-category">Third party links on our website</a>
                            <p class="lowercase"><span class="uppercase">T</span>
                                hird party links may be discovered on our website. These third party links have their own privacy policy, which you agree to when you click on the link. We are not responsible nor do we accept responsibility for third party links. Our liability covers us only on our site, and thus we do not accept liability for third party links as we have no control over them.
</p>

   
                          <a href="#" class="tnc-category">What type of personal data do we collect from you?</a>
                          <p>We retain records of your queries and correspondence, in the event you contact us. The personal data that we may collect from you includes but is not limited to:</p>
                             
                            
                            <ol>
                                <li>
                                    Information you give us:<br>
-	personal details (name, age, email, phone number, address)<br>
-	financial details and credit card information <br>
-	education details (name of school or university and year of study)<br>
-	details of services booked and used on our website <br>
-	complaints, reviews, refunds and payments through our website<br>
-	public record certificates <br>

                                </li>
                                
                                <li>
                                    Information we gather from your use of our website: <br>
-	login information <br>
-	location (IP address) and time zone used to connect your computer to the internet <br>
-	video recordings of interview sessions (stored for five days after interview for quality monitoring purposes)<br>
-	Uniform Resource Locators (ULR)<br>
-	services reviewed or searched for <br>
-	pages navigated<br>
-	time spent on a given page <br>
-	download errors <br>
-	page interaction information<br>

                                </li>
                            </ol>
                           
                         
                         <a href="#" class="tnc-category">
What do we use your personal data for?
</a>
                          <ul>
                              <li>	To process enquiries that you or a third party acting with your consent have submitted to us</li>
                              <li>To comply with any contractual obligations such as administering and managing your account</li>
	<li>To meet our legal and regulatory requirements;</li>
	<li>To help us identify you, any accounts you hold with us and keep this information up to date</li>
	<li>To provide customer care, including responding to your requests if you contact us with a query</li>
	<li>To share your personal data with certain third party service providers such as payment services and credit reference agencies (CRAs). This information may be shared by other organisations by the CRA.</li>
	<li>To detect and prevent criminal activity such as fraud and to verify what you have told us is correct we may perform identify checks</li>
	<li>To notify you about changes to our website, services or terms and conditions</li>
	<li>To enable us to review, improve and provide you with information about our products and services which we feel may interest you</li>
	<li>To deal with requests from you to exercise your rights under data protection laws</li>
	<li>To contact you with administrative emails</li>
	<li>To contact you with future employment offers</li>
	<li>To contact you with relevant products and services (With your opt-in consent) N.B. If you decide that you don’t want to receive marketing content from Whetstone any longer, please note that we may still be required to send you emails regarding factual, transactional and / or servicing information in connection with services that we are providing to you. </li>

                          </ul>
                          
                            
                        <a href="#" class="tnc-category">
How long will we keep your personal data?
</a>    
                          <p>
                              <span class="uppercase">N</span>o personal data will be stored for longer than is necessary and all documents containing personal and sensitive data will be disposed of securely in accordance with Data Protection principles. In essence, we keep it as long as we need to, for the purposes it was collected in the first place. We do so in order to deliver a full and high quality service. We may have to retain information for longer when there is a legal reason to do so (for example to comply with Child Protection laws). 
                          </p><p>
<span class="uppercase">W</span>e will retain your personal information for up to seven years where you do not object or opt-out of receiving email marketing communication from us about similar products and services. In this case we will store your personal information until you object or opt-out.
                          </p><p>
<span class="uppercase">V</span>ideo recorded is treated differently and will be stored by Whetstone Education Ltd. for a maximum of five days after the completion of the service for quality monitoring purposes. After this time it will be deleted from our records, although the copy sent to the client will remain intact.

                          </p>  
                           
                          
                           <a href="#" class="tnc-category">
How old do you have to be to use our service?
</a>    
                          <p>In order to use the services offered by Whetstone Education Ltd. you must be over the age of 16. If we find that personal data has been submitted to us by anyone under the age of 16 we will remove it as soon as possible.</p>
                      
                           <a href="#" class="tnc-category">
Who has access to your personal data?
</a>    
                          <ul>
                              <li>We will never share your personal data with third parties for their marketing purposes. </li>
                              <li>	We will only share personal and sensitive information with relevant persons to enable them to undertake specific duties. 
                                  
                                  <br>
                                      -	Interviewers <br>
-	Employees<br>
-	Volunteers<br>
-	Business Partners<br>
-	Business associates and professional advisers <br>
-	Other voluntary and charitable organisations<br>
-	Analytics and search engine providers (for site improvement)<br>
-	Future subsidiaries, holding companies, or any other subsidiaries involved in our business<br>
-	If Whetstone Education Ltd or substantially all of its assets are acquired by a third party then personal data held by it about its customers will be one of the transferred assets.<br>
-	If Whetstone Education Ltd. is required to do so, it will share personal data with legal authorities as it is under duty to do. This could be to reinforce Terms and Conditions policies, as well as other rights and agreements.<br>

                                  
</li>
                          </ul>
                          
                          <a href="#" class="tnc-category">
Your rights
                          </a>    <p>
                              Under data protections laws you have a number of rights in relation to the collection and use of your personal data.   These are:
                          </p>
                          
                          <ul>
                              <li>The right to access to the information we hold about you;</li>
	<li>The right to rectification so you can have your personal data corrected if it is inaccurate and to have incomplete personal data completed;</li>
	<li>The right to erasure – You have the right to request that we delete your personal data from our records.</li>
	<li>The right to restrict how we use your personal information;</li>
	<li>The right to object to the collection and use of your personal information at any time;</li>
	<li>The right to obtain a copy of your personal data in a legible and compatible format such as Excel or Word.</li>

                          </ul>
                          
                          <p>
                              The majority of ways we process your personal data are not based on your consent and is instead based on other legal reasons (as set out above). Where our processing is based solely on your consent you have <b>the right to withdraw your consent</b> for future processing at any time. You should be aware that preventing the use or processing of your personal data may mean that we are unable to provide our services to you. If we are acting as the Data Processor for the data you provided to another Data Controller you should also contact them to withdraw your consent.
                          </p>
                          <p>
                              You can exercise your rights by writing to us at the email address below stating what information you require and we will respond to you within a month. Please bare in mind that if you require copies that may incur a fee.
                          </p>
                          
                           <a href="#" class="tnc-category">
Updating your personal data
                          </a>
                          <p>
                              You should notify us of any changes to your status, contact and payment details without delay in order that the data we hold for you is kept up to date at all times.
                          </p>
                          
                           <a href="#" class="tnc-category">
Where we store your personal data?
                          </a>
                          <p>
                             The transmission of information via the email and the internet is not completely secure, we cannot guarantee the security of your data transmitted to us and any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.
                             <br>
All personal data you provide to us is stored on our encrypted servers. 

                          </p>
                          
                           <a href="#" class="tnc-category">
How do I lodge a complaint about the use of my personal data?
                          </a>
                          <p>
                              You can lodge a complaint about the use of your personal data directly with us or with the Information Commissioner’s Office (ICO). The ICO are the regulator who makes sure that we use your personal information in a lawful way. You can do this at <a herf="www://ico.org.uk/concerns/">www://ico.org.uk/concerns/</a> or by calling 0303 123 1113.
                           

                          </p>
                          
                           <a href="#" class="tnc-category">
What happens if we make changes to this privacy policy?
                          </a>
                          <p>
                             Occasionally we may need to amend parts of this policy to comply with changes in legal and regulatory requirements. For any large alterations we will notify you via email, however we also recommend that you periodically review this policy for any changes.
                           

                          </p>
                          
                           <a href="#" class="tnc-category">
Contacting Us
                          </a>
                          <p>
                              Please feel free to contact us via email on <a href="mailto:info@whetstone-oxbridge.com">info@whetstone-oxbridge.com</a>. 
                           

                          </p> -->
                          
                          
                          
                          
                          
                        </div>
                    
                            
              <a class="error-home" href="<?php echo base_url();?>">Home Page</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End error section  -->

  

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
    
  </body>
</html>