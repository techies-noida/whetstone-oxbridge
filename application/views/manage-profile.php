<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manage Profile | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>

   <link href="<?php echo base_url();?>assets/js/Monthly-Event-Calendar-pbcalendar/pb.calendar.css" rel="stylesheet">

   <!--  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .pb-calendar .schedule-dot-item.blue{
			background-color: blue;
		}

		.pb-calendar .schedule-dot-item.red{
			background-color: red;
		}

		.pb-calendar .schedule-dot-item.green{
			background-color: green;
		}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU -->



  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
          <div class="col-md-12">

            <h3 class="title" style="text-align: left">Manage Profile</h3>

          </div>
          </div>
        <br>
        <div class="col-md-12">
          <div class="our-team-content" style="margin-top: 0">
            <div class="container">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member" style="margin-top: 0">
                 <div class="team-member-img">
                     <img src="<?php echo base_url().$user[0]->profile_image;?>" alt="team member img">
                 </div>
                 <div class="team-member-name">
                  <?php $user_name=$user[0]->first_name.' '.$user[0]->last_name;?>
                   <p><?php echo $user_name;?></p>
                   <span><?php echo $user[0]->name;?></span>
                 </div>
                    <hr style="height: 1px; background: #eee; width: 100%">
                 <div class="sidebar-widget">
                    <ul class="widget-catg" style="text-align: left">
                      <li><a href="#">Sociology</a></li>
                      <li><a href="#">Manhetton, NY</a></li>
                      <li><a href="#">+91 <?php echo $user[0]->phone_number;?></a></li>
                      <li><a href="#"><?php echo $user[0]->email_address;?></a></li>
                      <li><a href="#">MIT University</a></li>
                    </ul>
                  </div>
                      <hr style="height: 1px; background: #eee; width: 100%">
                 <a href="#" class="btn comment-btn">Boost Your Profile</a>
                </div>
              </div>
              <!-- ends single team member -->

              <div class="col-md-5 col-sm-12 col-xs-12">
                   <div class="inter-more">
                   <div class="availability">
                          <h4>Availability</h4>
                          <!--<div class="calender"></div>-->
                          <div id="pb-calendar" class="pb-calendar"></div>

                      </div>
                      </div>
              </div>

              <div class="col-md-4 col-sm-6 col-xs-12">



                <div class="why-choose-us">
                  <div class="panel-group why-choose-group" id="accordion">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Reviews <span class="fa fa-minus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                             <div class="reviews">

                          <div class="row">
                              <div class="col-md-12">
                                    <blockquote>
                                        <div class="">
                                            <a href="#">
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                            </a>
                                        </div>
                                        <p>"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from"</p>
                                        <span> - John Doe</span>
                                    </blockquote>
                              </div>
                              <div class="col-md-12">
                                    <blockquote>
                                        <div class="">
                                            <a href="#">
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                            </a>
                                        </div>
                                        <p>"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from"</p>
                                        <span> - John Doe</span>
                                    </blockquote>
                              </div>
                              <div class="col-md-12">
                                    <blockquote>
                                        <div class="">
                                            <a href="#">
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                            </a>
                                        </div>
                                        <p>"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from"</p>
                                        <span> - John Doe</span>
                                    </blockquote>
                              </div>
                          </div>



                      </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default ">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Earnings <span class="fa fa-plus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                         <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Appointments <span class="fa fa-plus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>




  <!-- Start footer -->
 <!--  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="footer-left">
              <p>&copy; Oxbridge 2018 | All Rights Reserved </p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="footer-right">
            <a href="index.html"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer> -->
   <?php $this->load->view('common/footer');?>
  <!-- End footer -->

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<!--    <script type="text/javascript" src="<?php echo base_url();?>assets/js/pg-calendar-master/dist/js/pignose.calendar.full.min.js"></script>
    <script>
        $(document).ready(function() {
              $('.calendar').pignoseCalendar();
        });
    </script>-->
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <!-- Slick Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>
    <!-- mixit slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
   <!-- counter -->
    <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
    <!-- Wow animation -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script>
    <!-- progress bar   -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>
    <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>-->
<!--    <script src="<?php echo base_url();?>assets/js/Mini-Event-Calendar-Plugin-jQuery/src/mini-event-calendar.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".calendar").MEC();
        });
    </script>-->
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/Monthly-Event-Calendar-pbcalendar/pb.calendar.js"></script>
    <script>
        $(document).ready(function(){
            var current_yyyymm_ = moment().format("YYYYMM");
           $("#pb-calendar").pb_calendar({

  // is date selectable?
  'day_selectable' : false,

  // callbacks
  'callback_selected_day' : $.noop,
  'callback_changed_month' : $.noop,

  // min/max dates
  'min_yyyymm' : null,
  'max_yyyymm' : null,

  // navigation arrows
  'next_month_button' : '<img src="<?php echo base_url();?>assets/images/arrow-right.png" class="icon">',
  'prev_month_button' : '<img src="<?php echo base_url();?>assets/images/arrow-left.png" class="icon">',

  // custom label format
  'month_label_format' : "MMM",
  'year_label_format' : "YYYY",

   schedule_list : function(callback_, yyyymm_){
    var temp_schedule_list_ = {};

    temp_schedule_list_[current_yyyymm_+"03"] = [
      {'ID' : 1, style : "red"}
    ];

    temp_schedule_list_[current_yyyymm_+"10"] = [
      {'ID' : 2, style : "red"},
      {'ID' : 3, style : "blue"},
    ];

    temp_schedule_list_[current_yyyymm_+"15"] = [
      {'ID' : 4, style : "red"},
      {'ID' : 5, style : "blue"},
      {'ID' : 6, style : "green"},
    ];
    temp_schedule_list_[current_yyyymm_+"20"] = [
      {'ID' : 7, style : "red"},
      {'ID' : 8, style : "blue"},
      {'ID' : 9, style : "green"},
    ];
    callback_(temp_schedule_list_);
  },
  schedule_dot_item_render : function(dot_item_el_, schedule_data_){
    dot_item_el_.addClass(schedule_data_['style'], true);
    return dot_item_el_;
  }

});
        });
    </script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
  </body>
</html>
