<header id="header">
    <!-- header top search -->
    <div class="header-top">
      <div class="container">
        <form action="">
          <div id="search">
          <input type="text" placeholder="Type your search keyword here and hit Enter..." name="s" id="m_search" style="display: inline-block;">
          <button type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
        </form>
      </div>
    </div>
    <!-- header bottom -->
    <div class="header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="header-contact">
              <ul>
                <li>
                  <a class="mail">
                    <i class="fa fa-twitter"></i>
              
                  </a>
                </li>
                <li>
                  <a class="mail" href='index.html#contact'>
                    Contacts
              
                  </a>
                </li>
              </ul>
            </div>
          </div>
         

          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="header-login">
               <?php  if (!isset($this->session->userdata['logged_in'])){
                echo '<a class="login modal-form" data-target="#login-form" data-toggle="modal" href="#">Login / Sign Up</a>';
                }else{
                   $user_name=($this->session->userdata['logged_in']['user_name']);
                  echo ' <a class="login modal-form" data-target="" data-toggle="modal" href="'.base_url().'profile-details"> Hi.. '.ucwords($user_name).'</a>';
                }?>

            </div>
          </div>
       
          
           

          <!--   user login section   -->
         <!--  <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="header-login">
                <a class="login modal-form" data-target="#login-form" data-toggle="modal" href="#">Hi Devendra </a> 
            </div>
          </div> -->


        </div>
      </div>
    </div>
  </header>
  <!-- End header -->
  
  <!-- Start login modal window -->
  <?php // $this->load->view('login_signup.php');?>
  <!-- End login modal window -->

  <!-- BEGIN MENU -->
  <section id="menu-area">      
    <nav class="navbar navbar-default" role="navigation">  
      <div class="container">
        <div class="navbar-header">
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- LOGO -->              
          <!-- TEXT BASED LOGO -->
          <a class="navbar-brand" href="<?php echo base_url();?>">Whetstone <small>Oxbridge</small> </a>              
          <!-- IMG BASED LOGO  -->
           <!-- <a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
            <li><a href="<?php echo base_url();?>about-us">About Us</a></li>
            <li><a href="<?php echo base_url();?>interviewers-list">Interviewers</a></li>
            <li><a href="<?php echo base_url();?>work-for-us">Work for Us</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Resources <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                 <li><a href="<?php echo base_url();?>resources/personal-statement">Personal Statements</a></li>                
                <li><a href="resources.html#enttes">Entrance Tests</a></li>
                <!--<li><a href="resources.html#colsel">College Selections</a></li>-->
                <li><a href="blog-archive.html">Blog</a></li>           
              </ul>
            </li>
            <li><a href="<?php echo base_url();?>schools">Schools</a></li> 
            <li><a href="<?php // echo base_url();?>become-tutor">Become Tutor</a></li> 
             <li><a href="<?php echo base_url();?>check-request-status">Check status</a></li>               
            <!-- <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="statements.html">0 Items</a></li>                
                        
              </ul>
            </li>               
            <li><a href="#" id='dosearch'><i class="fa fa-search"></i></a></li>   -->
            <?php  if (!isset($this->session->userdata['logged_in'])){
            /*echo '<li><a href="#"><i class="fa fa-file-text-o"></i></a></li>';*/
            }else{
              echo '<li><a href="'.base_url().'loginSignup/logout'.'" title="Logout"><i class="fa fa-sign-out"></i> </a></li>';
            } ?>            
            <!-- <li><a href="#" title="Logout"><i class="fa fa-sign-out"></i> </a></li>  -->   
            <!-- <li><a href="#"><i class="fa fa-file-text-o"></i></a></li>  -->          
          </ul>                     
        </div><!--/.nav-collapse -->
       
      </div>     
    </nav>
  </section>
  

