<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Our Pricing | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
   
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

 <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Our Pricing</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Our Pricing</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
   <!-- Start Pricing table -->
  <section id="pricing-table">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Our Prices</h2>
            <span class="line"></span>
            <p>Whetstone Oxbridge has extensive experience in the Oxbridge interview process. We have found that chances of interview success can in increase by as much as 20% for every interview practice session booked and completed. </p>
            <p><b style="color:#2aaebf">Individually booked interviews are priced at £50</b>. For this you receive a <span style="text-decoration: underline">full mock interview</span> with written <span style="text-decoration: underline">feedback</span> from the interviewer. You also receive a <span style="text-decoration: underline">video copy of your interview</span> with real-time feedback comments which pop up to show you what your interviewer was thinking at the time of your response.</p>

            <div style="border:1px solid #ddd; padding-bottom: 20px; margin-top: 20px">
            <p style="font-weight: bold">When you buy interview practice sessions in bulk:</p>
            <ul style="list-style-image: url(<?php echo base_url();?>assets/images/check.png); list-style-position: inside; margin: 0 35px; text-align: left">
                      <li style="text-align: left">We will credit your account with the hours you have bought. </li>
                      <li style="text-align: left">You can then use these hours as you wish. </li>
                      <li style="text-align: left">You can use the same interviewer or chose a selection of different interviewers.</li>
                      <li style="text-align: left">You can book individual sessions as and when you’re ready to practice again. </li>
                     
                    </ul>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="pricing-table-content">
            <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                  <div class="price-header">
                    <span class="price-title">Basic</span>
                    <div class="price">
                      <sup class="price-up">£</sup>
                      145
                      <span class="price-down">(£5 saving)</span>
                    </div>
                  </div>
                  <div class="price-article">
                    <ul>
                    
                      <li>3 Practice Interviews</li>
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="javascript:void(0)">Purchase</a>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">
                  <div class="price-header">
                    <span class="price-title">Standard</span>
                    <div class="price">
                      <sup class="price-up">£</sup>
                      240
                      <span class="price-down">(£10 saving)</span>
                    </div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li> 5 practice interviews</li>
                      <!--<li> 50% off yearly subscription to Whetstone resources </li>-->
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="javascript:void(0)">Purchase</a>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-table-price featured-price wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                  <div class="price-header">
                    <span class="price-title">Premium </span>
                    <div class="price">
                      <sup class="price-up">£</sup>
                      335
                      <span class="price-down"> (£15 saving)</span>
                    </div>
                  </div>
                  <div class="price-article">
                    <ul>
                        <li>7 practice interviews</li>
                        <!--<li>15 minute general interview diagnosis with a member of the Whetstone team who will assess the areas that need particular work</li>-->
                        <!--<li>unlimited 1 year access to Whetstone resources </li>-->
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="javascript:void(0)">Purchase</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table -->  
  <!-- Start Service -->
   <section id="contact" >
     <div class="container">
       <div class="row">
           <div class="col-md-12">
           <div class="title-area">
           <h2 class="title">School Prices</h2>
            <span class="line"></span>
           </div>
           <p>If you represent a school and are looking for bulk discount pricing for your students then please complete the form below and a member of our team will be in contact to talk through a bespoke solution. </p>
           </div>
           <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               
               <div class="col-md-6" style='float: none; margin: auto'>
                 <div class="contact-area-right">
                   <form  id="bulk_request"action="" class="comments-form contact-form">
                    <div class="form-group">                        
                        <input type="text" required="" name="contact_person" class="form-control" placeholder="Name of school contact">
                    </div>
                    <div class="form-group">                        
                      <input type="text" required=""  name="school_name"class="form-control" placeholder="Name of school">
                    </div>
                    <div class="form-group">                        
                      <input type="text"required=""  name="school_email" class="form-control" placeholder="Contact email address">
                    </div>
                    <div class="form-group">                        
                      <input type="text" required=""  name="school_contact" class="form-control" placeholder="Contact telephone number">
                    </div>
                    <div class="form-group">                        
                      <input type="text" required=""  name="student_number" class="form-control" placeholder="Estimated number of Oxbridge applicants">
                    </div>
                    <div class="form-group"> 
                        <textarea name="comments" placeholder="Additional notes" rows="2" class="form-control"></textarea>
                    </div>
                   
                  <div id="intrested_user_message"></div>
                  <br>
                    <button class="comment-btn" type="submit">Submit</button>
                       <!--  <button class="comment-btn">Submit</button> -->
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  <!-- alert  Modal content-->
      <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="alert_model" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header " style="width: 100%;
             background-color:#15585F;
             height: 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bulk Request for Interview</h4>
              </div>
              <div class="modal-body" >
                <p>Thank you for submitting your bulk request.A member of our team will be in contact with you within the 48 hours (Mon to Friday).</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>

      <!-- model  -->
 
 <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
   <script type="text/javascript">
    $(function myFunction(){

      $.validator.addMethod(
      "regexcontactperson",
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
      "Contact person name should be character"
      );

      $.validator.addMethod(
      "regexapplicantsrange",
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
      "Applicants range should be in between 10 to 5000"
      );
  
    $('#bulk_request').validate({
        rules:{
            contact_person:{
                required:true,
                minlength:3,
                maxlength:25,
                regexcontactperson: "^[A-Za-z ]+$",
               
            },
            school_email:{
                required:true,
                email:true,
            },
            school_name:{
                required:true,
                 minlength: 4,
                maxlength: 35,
                regexschool: "^[A-Za-z ]+$",
            },
            school_contact:{
                required:true,
                digits: true,
                minlength: 11,
                maxlength: 25, 
            },
           student_number: {
            required: true,
            digits: true,
            regexapplicantsrange: "^((?:[1][0-9])|(?:[1-9][0-9])|(?:[1-9][0-9][0-9])|(?:[1-4][0-9][0-9][0-9])|(?:5000))$",
           },
           comments: {
            required: true,
            minlength: 10,
            maxlength: 600, 
           },
            
        },
        messages:{
            contact_person:{
                required:"Please enter conatct person name ",
                minlength:"Contact person name should be minimum 3 character",
                maxlength:"Contact person name should not more than 25 character",
                
            },

            school_email:{
                required:"  Please enter email address ",
                email:"Please enter valid email id ",
            },
            school_name:{
                required: "Please enter school name",
                minlength: "School name should be minimum 4 characters",
                maxlength:"School name should not more than 35 characters",


            },
            school_contact:{
                required: "Please enter phone number",
                digits: "Please enter valid phone number",
                minlength: "Please enter 11 digit phone number",
                maxlength:"Phone number should not more than 25 digit",


            },

           student_number: {
             required: "Please enter number of applicant",
             digits: "Please enter valid number of applicant",
          },
           comments: {
             required: "Please enter comments", 
                minlength: "Comments should be minimum 10 characters",
                maxlength:"Comments should not more than 600 characters",
          },
            
        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>oxbridge/bulk_request', 
            $('#bulk_request').serialize(), 
            function(data){
              // alert(data);
          
                if(data == 1)
                { 
                  //alert('thanks ');
                  $('#bulk_request').trigger("reset");
                  $("#intrested_user_message").html('');
                   $('#alert_model').modal('show');
                    /* responseText = '<span style="color:green;font-size: 14px;font-weight: normal;margin-left: 40px;">Thank you for registration, Whetstone Oxbridge Team will reply soon</span>';
                    $("#intrested_user_message").html(responseText);*/
                }
                else{
                 // alert('you have requested');
                  responseText = '<span style="color:red;font-size: 16px;font-weight: normal;">Your have already  requested </span>';
                  $("#intrested_user_message").html(responseText);
                }
                 
                setTimeout(function(){
             $("#alert_model").hide();
              window.location.reload();
            }, 20000);
               
                

            });
       }
   });
});

  </script>
    
  </body>
</html>