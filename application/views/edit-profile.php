<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Interviewer Profile | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?>
  <link  href="<?php echo base_url();?>assets/js/imageuploader/css/fineCrop.css" rel="stylesheet">
  <link  href="<?php echo base_url();?>assets/js/imageuploader/css/layout.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
  $this->load->view('common/subheader');
  ?>
  <style>
    .sidebar-widget{float:none; display: block;}
    .team-member-name{float:none; display: block;}
    .single-team-member{float:none; display: block;}
  </style>
  <!-- END MENU -->
  <!-- end about -->
  <section id="testimonial" style='background: #f6fef7; padding-top: 0px'>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding: 0">
          <div class="container">
            <div class="row">
            <!-- <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >Edit Profile</h2>
                <span class="line"></span>
              </div>
            </div> -->

            <br>
            <div class="clearfix"></div>

            <?php  $this->load->view('common/interviewer_sidebar');?>

            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="content-wrapper clearfix">

                <!-- Tab panes -->

                <?php
                if($this->session->flashdata('success')) {
                 $message = $this->session->flashdata('success');
                 echo'
                 <div class=" alert alert-success alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <i class="fa fa-check-circle"></i>'.$message['message'].
                 '</div>';
               }?>
               <?php
               if($this->session->flashdata('error')) {
                 $message = $this->session->flashdata('error');
                 echo'
                 <div class="alert alert-danger alert-dismissible" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <i class="fa fa-check-circle"></i>'.$message['message'].
                 '</div>';
               }?>

               <div class="contact-area-right" style="margin-top: 0px">
                <!--  <form method="post" action="" class="comments-form contact-form" > -->
                  <form  id="request_join_tutor" method="post"  onsubmit="return getpicture()" class="comments-form contact-form clearfix" role="form"  method='post'<?php echo form_open_multipart(base_url().'interviewer/update_profile')?>
                  <div class="col-md-12">

                   <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Personal Information</h4>
                 </div>
                 <div class="clearfix"></div>
                 <div class="col-md-5">
                  <div class="form-group">
                    <label style="color:#444; font-weight: normal">Name</label>
                    <input type="text" name="first_name" class="form-control" placeholder="" value="<?php echo $user[0]->first_name;?>">
                    <input type="hidden" name="user_id" class="form-control" placeholder="" value="<?php echo $user[0]->user_id;?>">
                  </div>
                     <!-- <div class="form-group">
                      <label style="color:#444; font-weight: normal">last Name</label>
                      <input type="text" name="last_name" class="form-control" placeholder="" value="<?php echo $user[0]->last_name;?>">
                    </div> -->
                    <div class="form-group">
                      <label style="color:#444; font-weight: normal"> University email address </label>
                      <input type="text" readonly="readonly" name="email_address" class="form-control" placeholder="" value="<?php  echo $user[0]->email_address;?>">
                    </div>
                    <div class="form-group">
                     <label style="color:#444; font-weight: normal">Phone number</label>
                     <input type="text" name="phone_number" value="<?php echo $user[0]->phone_number;?>" class="form-control" placeholder="">
                   </div>

                   <div class="form-group">
                    <label style="color:#444; font-weight: normal">Gender</label>
                    <select class="form-control" name="gender" style="border-radius: 0">

                      <option <?php if($user[0]->gender == 'Male'){ echo 'selected'; } ?> value="Male">Male</option>
                      <option <?php if($user[0]->gender == 'Female'){ echo 'selected'; } ?> value="Female">Female</option>
                      <option <?php if($user[0]->gender == 'Transgender'){ echo 'selected'; } ?> value="Transgender">Transgender</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label style="color:#444; font-weight: normal">DOB</label>
                    <input type="date" name="dob"  class="form-control" value="<?php echo $user[0]->date_of_birth;?>">
                  </div>

                  <div class="form-group">
                   <label style="color:#444; font-weight: normal">Country</label>
                   <select class="form-control" name="country" style="border-radius: 0">
                    <option>Select</option>
                       <!--  <?php foreach($country as $row){
                         echo '<option value="'.$row->country_id.'">'.$row->country_name.'</option>';
                       } ?> -->
                       <?php foreach ($country as $row) {?>
                        <option <?php if($row->country_id == $user[0]->country_id){ echo 'selected="selected"'; } ?> value="<?php echo $row->country_id;?>"><?php echo $row->country_name;?></option><?php }?>

                      </select>
                    </div>
                    <!--  <div class="form-group">
                         <label style="color:#444; font-weight: normal">About us</label>
                         <textarea class="form-control" name="about_us" style="height: 60px"><?php  echo $user[0]->about_us;?></textarea>
                       </div> -->

                       <div class="col-lg-12" style="background: #f8f8f8; border:1px solid #f5f5f5; padding: 25px">
                         <div class="form-group">
                          <label style="color:#444; font-weight: normal">Your Display Picture</label>
                        </div>
                        <input type="file" id="upphoto" name="img" style="display:none;">
                        <input type="hidden" id="upphoto1" name="picture" >
                        <input type="hidden"  name="img1" value="<?php echo $user[0]->profile_image;?>" style="display:none;">
                        <label for="upphoto">
                          <div class="inputLabel">
                            click here to upload an image
                          </div>
                        </label>

                      </div>

                      <div class="col-lg-12">
                       <!--  <img id="croppedImg" src="<?php echo base_url();?>assets/images/default.png"> -->
                       <?php  $image=$user[0]->profile_image;
                       if($image=='')
                         echo '<img id="croppedImg" src="'.base_url().'assets/images/default.png" alt="team member img">';

                       else
                        echo '<img id="croppedImg" src="'.base_url().$image.'" alt="team member img">';
                      ?>
                    </div>


                  </div>

                  <div class="col-md-offset-1 col-md-6">




                    <div class="form-group">
                     <label style="color:#444; font-weight: normal">University</label>
                     <!-- <input type="text" name="university" class="form-control" placeholder="" value="<?php echo $user[0]->university_name;?>" > -->
                     <select class="form-control" name="university" id="change_university" style="border-radius: 0">
                       <?php foreach ($university as $row) {?>
                        <option <?php if($row->university_name == $user[0]->university_name){ echo 'selected="selected"'; } ?> value="<?php echo $row->university_id;?>"><?php echo $row->university_name;?></option><?php }?>

                      </select>
                    </div>

                    <div class="form-group">
                     <label style="color:#444; font-weight: normal">College</label>
                     <!-- <input type="text" name="college" class="form-control" placeholder="" value="<?php echo $user[0]->college_name;?>"> -->
                     <select class="form-control" name="college" id="change_college" style="border-radius: 0">
                       <?php foreach ($college as $row) {?>
                        <option <?php if($row->college_name == $user[0]->college_name){ echo 'selected="selected"'; } ?> value="<?php echo $row->college_id;?>"><?php echo $row->college_name;?></option><?php }?>

                      </select>
                    </div>

                    <div class="form-group">
                     <label style="color:#444; font-weight: normal">Course</label>
                     <input type="text" name="subject" class="form-control" placeholder="" value="<?php   echo $user[0]->subject;?>">
                   </div>

                   <div class="form-group">
                     <label style="color:#444; font-weight: normal">Current year of study</label>
                     <input type="text" name="study_year" class="form-control" placeholder="" value="<?php  echo $user[0]->course_year;?>">
                   </div>

                   <div class="form-group">
                     <label style="color:#444; font-weight: normal">Previous interviewing and tutoring experience (only fill in if you have relevant experience)</label>
                     <!-- <input type="text" name="interview_exprience" class="form-control" placeholder="" value="<?php echo $user[0]->experience;?>"> -->
                     <textarea class="form-control" name="interview_exprience" style="height: 60px"><?php  echo $user[0]->experience;?></textarea>
                   </div>

                   <div class="form-group">
                     <label style="color:#444; font-weight: normal">Main academic interests</label>
                     <textarea class="form-control" name="acadmic_intrests" style="height: 60px"><?php  echo $user[0]->academic_interest;?></textarea>
                   </div>
                   <div class="form-group">
                     <label style="color:#444; font-weight: normal">What do you wish to told before your Oxbridge interview</label>
                     <textarea class="form-control" name="wish_to" style="height: 60px"><?php  echo $user[0]->wish_to_told_oxbridge;?></textarea>
                   </div>
                   <div class="form-group">
                     <label style="color:#444; font-weight: normal">Extra-curricular interests</label>
                     <textarea class="form-control" name="extra_curicular" style="height: 60px"> <?php  echo $user[0]->extra_curricular_interest;?></textarea>
                   </div>



                   <button class="comment-btn" type="submit">Update </button>
                 </div>
               </form>

             </div> <!--student form ends-->
           </div>

         </div>
       </div>
     </div>
   </div>

 </div>
</div>
</section>
<!-- End Service -->
<!-- Start footer -->

<?php  $this->load->view('common/footer');?>
<!-- End footer -->

<div class="cropHolder">
  <div id="cropWrapper">
    <img id="inputImage" src="images/face.jpg">
  </div>
  <div class="cropInputs">
    <div class="inputtools">
      <p>
        <span>
          <i class="fa fa-arrows-h"></i>
        </span>
        <span>horizontal movement</span>
      </p>
      <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
    </div>
    <div class="inputtools">
      <p>
        <span>
          <i class="fa fa-arrows-v"></i>
        </span>
        <span>vertical movement</span>
      </p>
      <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
    </div>
    <br>
    <button class="cropButtons" id="zplus">
      <i class="fa fa-search-plus"></i>
    </button>
    <button class="cropButtons" id="zminus">
      <i class="fa fa-search-minus"></i>
    </button>
    <br>
    <button id="cropSubmit">submit</button>
    <button id="closeCrop">Close</button>
  </div>
</div>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<!-- Slick Slider -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
<!-- counter -->
<script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
<!-- Wow animation -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script>
<!-- progress bar   -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>


<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/imageuploader/js/fineCrop.js"></script>

<script>
  $("#upphoto").finecrop({
    viewHeight: 500,
    cropWidth: 200,
    cropHeight: 200,
    cropInput: 'inputImage',
    cropOutput: 'croppedImg',
    zoomValue: 50
  });

  function getpicture(){
    $("#upphoto1").val($("#croppedImg").attr("src"));
    return true;
  }
</script>
<script>
  $(document).ready(function(){

    $('.submenu-toggle').on('click',function(){
      console.log('submenu clicked');
      if($('.secondheader-menu').hasClass('hiddenn')){
       $('.secondheader-menu').removeClass('hiddenn');
       $('.secondheader-menu').addClass('shownn');
       $(this).html("<i class='fa fa-times'></i>");
     } 
     else if($('.secondheader-menu').hasClass('shownn')){
       $('.secondheader-menu').removeClass('shownn');
       $('.secondheader-menu').addClass('hiddenn');
       $(this).html("My Pages");
     } 
   });
    
  });
</script>
<script type="text/javascript">
  $( "#change_university" ).change(function() {
    var university_id = $('#change_university').val();
    $.ajax({  
      url:"<?php echo base_url() . 'Interviewer/fetch_collage_list'?>", 
      method:'POST', 
      data:{university_id:university_id},
      success:function(data)  
      {
        var new_data = JSON.parse(data);
        console.log(new_data);
        var change_college = "<option value=''>Select College name</option>";
        for (var i = 0; i < new_data.length; i++) {
          change_college += "<option value='" +  new_data[i]['college_id'] + "'>" +  new_data[i]['college_name'] + "</option>";
        }
        $('#change_college option').remove();
        $("#change_college").append(change_college);
      }
    });
  });
</script>
</body>
</html>
