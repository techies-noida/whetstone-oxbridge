<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Checkout | Whetstone Oxbridge</title>
    <?php $this->load->view('common/header_assets');?> 
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]--> <style>
                        .btn-submit{background: #2aaebf !important; color:#fff; border-radius: 0; display: inline-block; margin-top: 20px; padding: 12px 25px}
                        .table-highlight{border:0 !important}
                        .table-highlight tr{border:0 !important; background: #f3fef6}
                        .table-highlight tr td{border:0 !important; vertical-align: middle !important}
                        .table-highlight tr th{border:0 !important; vertical-align: middle !important}
                        .table-striped tr td{border:0 !important}
                        .table-striped tr th{border:0 !important; font-weight: bold !important; color:#333 !important}
                        .small-head{text-align: left; text-transform: uppercase; font-weight: 800; font-size: 14px; color:#23527c; margin-bottom: 20px}
    </style>
    
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
 <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
 <!-- Start single page header -->
 
  <!-- End single page header -->
  <!-- Start error section  -->
  <section id="our-team" style="background: #f6fef7; padding-top: 40px">
    <div class="container">
      <div class="row">
<!--        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title" style="font-size:24px">Payment</h2>
            <span class="line"></span>
          </div>
        </div>-->
        <div class="col-md-12">
          <div class="our-team-content" style="margin-top: 20px">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-6 col-sm-6 col-xs-12" style="float:none; margin:auto">
                <div class="single-team-member text-center">
                
                   
                  
                    
                    <h4 class="small-head text-center" style="text-align: center">We Accept</h4>
                    <img src="<?=base_url()?>assets/images/cards.png" style="margin-bottom: 10px">
                    <p>We accept Visa, MasterCard and Amex, Its secured!</p>
                    
                    <h4 style="color:#2aaebf !important; font-weight: bold">Payable to whetstone education ltd</h4>
                    <?php
                      $checkout_session = $this->session->userdata('checkout_session');
                      //print_r($checkout_session);
                      if(!empty($checkout_session)){
                        $amount = $checkout_session['amount'];
                      }else{
                        $amount = $this->session->purchase_plan['new_amount'];
                      }
                     ?>
                    <table class="table table-striped">
                       
                     
                        <tr>
                            <td >Payable Amount:
                        
                            £<?=$amount?></td>
                        </tr>
                        
                    </table>
                    
                    
                    <form action="" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_rkqUBNs86mfmdWZ7kMv6oui6"
    data-amount="<?=round($amount*100);?>"
    data-name="test"
    data-description=""
    data-image=""
    data-locale="auto"
  data-currency="GBP"
    data-label="Make Payment"
      >
  </script>
</form>
                
                </div>
              </div>
              <!-- Start single team member -->
         
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- End error section  -->

  <!-- Start subscribe us -->
  <!-- <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2>Subscribe Newsletter</h2>
              <form action="" class="subscrib-form">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
    
  </body>
</html>




