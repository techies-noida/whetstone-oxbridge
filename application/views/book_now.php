<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Order Details | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?> 
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .btn-submit{background: #2aaebf !important; color:#fff; border-radius: 0; display: inline-block; margin-top: 20px; padding: 12px 25px}
    .table-highlight{border:0 !important}
    .table-highlight tr{border:0 !important; background: #f3fef6}
    .table-highlight tr td{border:0 !important; vertical-align: middle !important}
    .table-highlight tr th{border:0 !important; vertical-align: middle !important}
    .table-striped tr td{border:0 !important}
    .table-striped tr th{border:0 !important; font-weight: bold !important; color:#333 !important}
    .small-head{text-align: left; text-transform: uppercase; font-weight: 800; font-size: 14px; color:#23527c; margin-bottom: 20px}
  </style>
</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->

  <!-- End single page header -->
  <!-- Start error section  -->
  <section id="our-team" style="background: #f6fef7; padding-top: 40px">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title" style="font-size:24px">Booking Review</h2>
            <span class="line"></span>
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content" style="margin-top: 20px">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-6 col-sm-6 col-xs-12" style="float:none; margin:auto">
                <div class="single-team-member">


                  <h4 class="small-head">Booking Details</h4>
                  <table class="table table-highlight">
                    <tr>
                      <td style="width: 64px"><img src="<?php if($this->session->userdata['logged_in']['profile_image']!='') { echo base_url().$this->session->userdata['logged_in']['profile_image'];} else { echo base_url().'assets/images/default.png';}?>" height="36" style="border-radius:50%" alt="team member img"></td>
                      <td class="text-left">
                       <b style="color:#2aaebf"><?=$this->session->userdata['logged_in']['user_name']?></b><br><small style="font-size:12px"><?=$this->session->purchase_plan['session_start_time']?> - <?=$this->session->purchase_plan['session_end_time']?>, <?=date('d-m-Y',strtotime($this->session->purchase_plan['appointment_date']))?></small>
                     </td>
                   </tr>

                 </table>
                 <form action="" method="POST">
                  <table class="table table-highlight">
                   <tr>
                    <td class="text-left">Coupon</td>
                    <td class="text-right">
                      <input placeholder="coupon code here" style="color:#ddd; border:1px solid #ddd" type="text" id="discount_coupon" name="discount_coupon">
                      <input type="hidden" name="real_amount" id="real_amount" value="<?=$this->session->purchase_plan['amount']?>">
                      <button class="btn btn-xs" id="apply" style="background: #16bf9d; color:#fff; margin-left: 5px; padding: 3px 15px">Apply</button>

                    </td>
                  </tr>
                  <div id="discount_coupon_mesg"></div>

                </table>

                <h4 class="small-head">Payment Detail</h4>
                <table class="table table-striped">

                  <tr>
                    <td class="text-left">Total</td>
                    <td class="text-right">£<?=$this->session->purchase_plan['amount']?></td>
                  </tr>
                  <tr>
                    <td class="text-left">Discount</td>
                    <td class="text-right" style="color:#16bf9d" id="discount_coupon_per">-£0</td>
                  </tr>
                  <tr>
                    <th class="text-left">Grand Total</th>
                    <th class="text-right" id="new_amount">£<?=$this->session->purchase_plan['amount']?></th>
                  </tr>

                </table>
                <span style='display:none;'>
                  User:<input type='text' name="user_id" value="<?=$this->session->userdata['logged_in']['user_id']?>"><br>
                  <input type='text' name="interviewer_id" value="<?=$id?>"><br>
                  <input type='text' name="amount" id="set_new_amount" value="<?=$this->session->purchase_plan['amount']?>"><br>
                  <input type='text' name="booking_date" value="<?=$this->session->purchase_plan['appointment_date']?>"><br>
                  <input type='text' name="start" value="<?=$this->session->purchase_plan['session_start_time']?>"><br>
                  <input type='text' name="end" value="<?=$this->session->purchase_plan['session_end_time']?>"><br>
                  <input type='text' name="package" value="<?=$this->session->purchase_plan['package']?>"><br>
                  <input type='text' name="message" value="<?=$this->session->purchase_plan['message']?>"><br>
                  <input type='text' name="room_name" id="room_id" value=""><br>
                </span>

                <button class="btn btn-submit" type="submit" name="submit" value="Proceed to Payment">Proceed to Payment</button>
              </form>
            </div>
          </div>
          <!-- Start single team member -->

        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- End error section  -->

<!-- Start subscribe us -->
  <!-- <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2>Subscribe Newsletter</h2>
              <form action="" class="subscrib-form">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
  <script type="text/javascript">
   $(document).ready(function(){
    $('#apply').text("Apply"); 
    $.ajax({
     type: 'post',
     url: '<?php echo base_url();?>TwilioController/create_room/',


     success: function(response)
     {
                   //jQuery('#slot_id').html(response);
                   $('#room_id').val(response);
                 }
               });
  });
   $(document).on('click', '#apply', function(event){
    event.preventDefault();
    $('#apply').text("Wait");
    $('#apply').prop('disabled',true); 
    var discount_coupon = $('#discount_coupon').val(); 
    var real_amount = $('#real_amount').val(); 
    // alert(real_amount);
    if (discount_coupon == '') {
      $('#discount_coupon_mesg').html('<p style="color: red;">Please enter coupon code</p>');
      $('#apply').text("Apply");
      $('#apply').prop('disabled',false); 
      setInterval(function (){
        $("#discount_coupon_mesg").html('');

      }, 3000); 
      return;
    }
    $.ajax({
      url:"<?php echo base_url() . 'oxbridge/discount_coupon'?>", 
      method:'POST',  
      data:{discount_coupon:discount_coupon}, 
      success:function(data)  
      {  
        // alert(data); 
        if( data == '0') {
          $('#discount_coupon_mesg').html(''); 
          $('#discount_coupon_mesg').html('<p style="color: red;">Coupon expired or invalid</p>');
          $('#apply').text("Apply");
          $('#apply').prop('disabled',false); 
          setInterval(function (){
            $("#discount_coupon_mesg").html('');

          }, 3000); 
          return; 
        }
        $('#discount_coupon_mesg').html('');  
        var discount_price = ( data * real_amount) / 100;
        var new_price  = real_amount - discount_price;
       // alert(new_price);
       $('#discount_coupon_per').html('<p>-£' + discount_price +'</p>');
       $('#new_amount').html('<p>£' + new_price +'</p>');
       $('#set_new_amount').val(new_price);
       $.ajax({
         url:"<?php echo base_url() . 'oxbridge/discount_amount'?>", 
         method:'POST',  
         data:{new_price:new_price, discount_coupon:discount_coupon}, 
         success:function(data)  
         { 
          // alert(data);
         }
       });
       $('#discount_coupon_mesg').html('<p style="color: blue;">Coupon applied successfully</p>');
       $('#apply').text("Apply");
       $('#apply').prop('disabled',false);  
     } 
   });

  });
</script>
<script type="text/javascript">
      history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});
    </script>


</body>
</html>