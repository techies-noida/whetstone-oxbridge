<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Interviewers | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?>
  <style>
  .single-team-member > p{min-height: 160px; max-height: 160px; overflow: hidden}
  #req-btn{border:1px solid #fff; background: transparent; color:#fff;}
  #req-btn:hover{border:1px solid #fff; background: #fff; color:rgb(42, 174, 191);}
</style>
</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- Start about  -->
  <section id="about" style='padding-top: 0; padding-bottom: 0'>
   <div class="container-fluid" style='background:#1d1d1d; padding-top: 60px; padding-bottom: 60px'>
    <div class="row">
      <div class="col-md-12">
        <div class="subscribe-area">
          <h2 class="wow fadeInUp" style='color:#2aaebf'>Start preparing for your interviews today</h2>
        </div>
      </div>
      <div class="col-md-6" style="float:none; margin:auto; display: none">
        <div class="row">
         <div class="col-md-4" style="padding-right:0">
           <input type="text" placeholder="University.." style="height:42px; width:100%; border-radius:4px 0 0 4px; border:0; border-right:1px solid #eee; padding-left:15px">
         </div>
         <div class="col-md-4" style="padding-right:0; padding-left:0">
          <input type="text" placeholder="College.. " style="height:42px; width:100%; border-radius:0px 0 0 0px; border:0; border-right:1px solid #eee; padding-left:15px">
        </div>
        <div class="col-md-4" style="padding-left:0">

          <input type="text" placeholder="Course.. " style="height:42px; width:100%; border-radius:0px 4px 4px 0px; border:0; padding-left:15px">


        </div>
      </div>
      <div class="row text-center">
       <button class="btn btn-info" type="submit" style="margin-top:20px; padding:10px 25px"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
     </div>
   </div>
 </div>
</div>

</section>
<!-- end about -->

<section id="testimonial" style="background:#2aaebf !important; padding: 20px 0">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            <div class="title-area">
              <p style="color:#fff">Request an interviewer</p>
            </div>
            <div class="title-area">
              <p style="color:#fff">Fill in this form with the University, Course and College you are applying to and we'll find a perfect match for you.</p>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-12" style="float: none; margin: auto">


            <div style="margin-top: 0px">




              <div class="tab-content">


                <div role="tabpanel" class="tab-pane active" id="student">
                 <div class="contact-area-right" style="margin-top: 20px">
                   <form action="" class="comments-form contact-form" id="interviewer_contact_form" novalidate="novalidate">

                     <div class="col-md-3">

                       <div class="form-group">
                        <label style="color:#fff; font-weight: normal">University</label>
                        <input type="text" class="form-control" placeholder="" value="" name="university_name" id="university_name">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                       <label style="color:#fff; font-weight: normal">College</label>
                       <input type="text" class="form-control" placeholder="" value="" name="college_name" id="college_name">
                     </div>
                   </div>
                   <div class="col-md-3">
                    <div class="form-group">
                     <label style="color:#fff; font-weight: normal">Course</label>
                     <input type="text" class="form-control" placeholder="" name="cource_name" id="cource_name">
                   </div>
                 </div>
                 <div class="col-md-3">
                  <div class="form-group">
                   <label style="color:#fff; font-weight: normal">Your Email Address</label>
                   <input type="email" class="form-control" placeholder="" name="email_address" id="email_address">
                 </div>
               </div>
               <div class="col-lg-12" style="text-align: center;">
                 <div class="form-group" style="color:#fff;">
                  <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a href="/privacy-policy" target="_blank" style="color:#fff;">GDPR Policy</a>.
                </div>
              </div>
              <div class="col-md-12 text-center">
                <button class="comment-btn add_btn_css" id="submit_details"  type="submit" value="Submit">Submit</button>
              </div>




            </form>
          </div>
        </div> <!--student form ends-->


      </div>



    </div>

  </div>
</div>
</div>

</div>
</div>
</section>



<!-- Start Pricing table -->
<section id="our-team">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-area">
          <h2 class="title">Looking for a practice interview?</h2>
          <span class="line"></span>
          <p>Book a slot with one of our Top Listed Interviewers below.</p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="our-team-content">
          <div class="row" id="shuffle-this">
           <!-- Start single team member -->
           <?php foreach ($interviewer as $row) {?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <a href="<?php echo base_url().'interviewer/profile/'.$row->user_id;?>" class="single-team-member">
               <div class="team-member-img" style="border-radius: 4px; background: #f2f2f2; margin-top: 20px">
                 <?php $image=$row->profile_image;
                 if($image=='')
                  echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="filter:grayscale(100%); height:100%; width:100%">';
                else
                 echo '<img src="'.base_url().$image.'" alt="team member img" style="filter:grayscale(100%); height:100%; width:100%">';
               ?>

             </div>
             <div class="team-member-name">
               <p><?php echo $row->first_name.''.$row->last_name;?></p>
               <span><table><tr><td style="width: 28px"><i class="fa fa-bank"></i></td><td><?php echo $row->university_name;?></td></tr></table></span>

               <span><table><tr><td style="width: 28px"><i class="fa fa-graduation-cap"></i></td><td><?php echo $row->college_name;?></td></tr></table></span>
               <span><table><tr><td style="width: 28px"><i class="fa fa-book"></i></td><td><?php echo $row->subject;?></td></tr></table></span>

             </div>
             <?php // echo '<p>'.$row->about_us.'</p>';?>
             <!-- <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p> -->
             <div class="team-member-link" style="margin-bottom: 50px;">
              Read More
            </div>
          </a>
          </div> <?php }?>
          <!-- Start single team member -->

        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- code written by ravindra 11-01-2019 -->
<!-- <script type="text/javascript">
  function submit_contact_frm(){
    alert("Thank you for your request. Hold tight and your Interviewer will contact you in the next 24 hours to arrange a session.");
    $("#university_name").val("");
    $("#cource_name").val("");
    $("#email_address").val("");
    $("#college_name").val("");
    return true;
  }
</script> -->
<div class="modal" tabindex="-1" id="success_model" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #2aaebf; height: 50px;">
       <!--  <h5 class="modal-title">Modal title</h5> -->
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        <p style="margin-top: 15px;">Thank you for your request. Hold tight and your interviewer will contact you in the next 24 hours to arrange a session. In the meantime, if you have any queries please do not hesitate to <a href="<?php echo base_url(); ?>#contact">contact us</a>.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- End Service -->
<!-- Start subscribe us -->
<?php  $this->load->view('common/newsletter');?>
<!-- End subscribe us -->

<!-- Start footer -->
<?php $this->load->view('common/footer');?>

<script type="text/javascript">
  // $('#submit_details').click(function(e) {
  //   e.preventDefault();

  //  var university_name = $("#university_name").val();
  //  alert(university_name);
  // });
  $( document ).ready(function() {
    $('#submit_details').text("Submit"); 
    $('#interviewer_contact_form').validate({
      rules:{

        university_name:{
          required:true,
          minlength: 4,
          maxlength: 25,
        },
        college_name:{
          required:true,
          minlength: 4,
          maxlength: 25,

        },
        cource_name:{
          required:true,
          minlength: 4,
          maxlength: 25,
        },
        email_address: {
          required:true,
        },
        terms_condition: {
          required:true,
        },


      },
      messages:{

        university_name:{
          required: "Please enter university name",
          minlength: "Please enter minimum 4 character",
          maxlength:"University name should not more than 25 character",


        },
        college_name:{
          required: "Please enter College name",
          minlength: "Please enter minimum 4 character",
          maxlength:"College name should not more than 25 character",



        },
        cource_name:{
          required: "Please enter Course name",
          minlength: "Please enter minimum 4 character",
          maxlength:"Course name should not more than 25 character",
        },
        email_address:{
          required:"  Please enter email_address ",
          email:"Please enter valid email id ",
        },
        terms_condition:{
          required: "Please Check policy",
        },
      },
      unhighlight: function (element) {

        $(element).parent().removeClass('has_error')
      },
        submitHandler: function(form) {
          $('#submit_details').text("Processing...");
          $('#submit_details').prop('disabled',true);
           $.post('<?=base_url()?>interviewer/interviewer_contact_form', 
            $('#interviewer_contact_form').serialize(), 
            function(data){
              // alert(data);
              if (data == 1) {
                $("#interviewer_contact_form").trigger('reset')
                $("#success_model").modal();
              }

          $('#submit_details').text("Submit");
          $('#submit_details').prop('disabled',false);
            });
        }

    });
  });  
  
  $(function () {
    var parent = $("#shuffle-this");
    var divs = parent.children();
    while (divs.length) {
        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }
});
  
</script>
</body>
</html>
