<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interviewer Dashbaord | Whetstone Oxbridge</title>
    <?php  $this->load->view('common/header_assets');?>

          <link href="<?php echo base_url();?>assets/js/Super-Simple-Calendar/css/dncalendar-skin.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .pb-calendar .schedule-dot-item.blue{
			background-color: blue;
		}

		.pb-calendar .schedule-dot-item.red{
			background-color: red;
		}

		.pb-calendar .schedule-dot-item.green{
			background-color: green;
		}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');$this->load->view('common/subheader');?>
  <!-- END MENU -->
  <!-- Start Pricing table -->
  <section id="our-team" style="background: #f6fef7; padding-top: 0">
    <div class="container">
    <div class="row">
     <!--  <div class="row">
          <div class="col-md-12">

            <h3 class="title" style="text-align: left">Manage Profile</h3>

          </div>
          </div> -->
     <br>
        <div class="col-md-12" style="padding: 0">
          <div class="our-team-content" style="margin-top: 0">
            <div class="container">
            <div class="row">
              <!-- Start single team member -->
             <?php  $this->load->view('common/interviewer_sidebar');?>
              <!-- ends single team member -->

              <div class="col-md-9 col-sm-6 col-xs-12">
<!--                   <div class="inter-more">
                   <div class="availability">
                          <h4>Availability</h4>

                      </div>

                      <?php  if (isset($this->session->userdata['logged_in'])){
                        $role_id=($this->session->userdata['logged_in']['user_role_id']);
                        if($role_id==2){?>
                       <div class="col-sm-6">  <a href="<?php echo base_url();?>interviewer_schedule" class="btn comment-btn" style="display: block">Set Available Schedule</a> </div>
                       <br>
                     <?php }}?>
                       <div class="col-sm-6">  <a href="<?php echo base_url();?>interviewer_schedule/bookings" class="btn comment-btn" style="display: block"> View All Bookings</a> </div>
                      </div>
             -->


                  <div class="inter-more">
                   <div class="availability">
                    <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message'].
                          '</div>';
                      }?>
                          <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Upcoming Appointments</h4>


                       <div class="table-responsive">
                           <table id="bookings-1" class="display bookings" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Applicant</th>
                                    <th>Appointment Date</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                   <th>Personal Statement</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($appointment as $row) {
                                $id=$row->student_id;
                                $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);
                                ?>

                                <tr>
                                    <td>
                                         <?php $image= $result[0]->profile_image; if($image=='')
                   /* echo '<img src="'.base_url().'assets/images/main-testimonials-img-2.png" alt="team member img">';*/
                  echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="height: 28px;width: 5opx;">';

                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="height: 28px;width: 5opx;">';
                     ?>
                                        <!-- <img src="<?php echo base_url();?>assets/images/team-member-2.png" height="36" class="img img-circle"/> -->
                                        <?php echo ucwords($result[0]->first_name);?>
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i>  <?php echo date('d-m-Y', strtotime($row->booking_date));?>
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $row->start_time;
                                        //echo $st= date('h:i A',strtotime('-0 hour -10 minutes',strtotime($row->start_time)));
                                        ?>
                                    </td>
                                     <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $row->end_time;?>
                                        <?php $date = date('Y-m-d H:i:s', time());
                                        // echo $newDateTime = date('h:i A', strtotime($date));
                                         ?>
                                    </td>

                                    <td>
                                      <?php if($row->attachment) {  ?>
                                         <a href="<?php  echo base_url().'uploads/attachment/'.$row->attachment;?>" target="_blank" download><i class="fa fa-file-text-o" style="font-size:24px;margin-left: 50px;"></i></a> 
                                        <?php  } ?>
                                    </td>
                                    <td>
                                        
                                        <!-- <a href="<?= base_url().'session/student/'.$row->room_name;?>" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a> -->
                                         <!-- <?php $time = date('Y-m-d H:i:s', time()); 
                                               $date = date('Y-m-d'); 
                                            $real_time = date('h:i A', strtotime($time));
                                            $start_time=$row->start_time;
                                            $end_time=$row->end_time;
                                            $booking_date=$row->booking_date;
                                            $schedule_time= date('h:i A',strtotime('-0 hour -15 minutes',strtotime($start_time)));
                                            $schedule_end= date('h:i A',strtotime('-0 hour -0 minutes',strtotime($end_time)));
                                          //echo '<pre>';print_r($result);echo '</pre>'; exit();
                                            if($row->call_status=='Active'){

                                              if($real_time >= $schedule_time && $real_time <= $schedule_end && $date==$booking_date) { 
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a>';
                                              }/*else if( $date>=$booking_date &&  $real_time > $schedule_end && $real_time > $schedule_time )*/
                                              else if( $date>$booking_date || $date==$booking_date && $real_time >= $schedule_time && $real_time >= $schedule_end)
                                              {
                                                 echo '<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-phone"></i> Expire</a>';

                                              }

                                              else{
                                                echo '<a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i> Call</a>';

                                              }
                                            }
                                             else if($row->call_status=='Started'){
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Started</a>';
                                            }
                                            else if($row->call_status=='Completed'){
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Completed</a>';
                                            }
                                            else if($row->call_status=='Expire'){
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Expire</a>';
                                            }


                                         ?>  -->
                                         <?php $time = date('Y-m-d H:i:s', time()); 
                                               $date = date('Y-m-d'); 
                                            $real_time = date('h:i A', strtotime($time));
                                            $start_time=$row->start_time;
                                            $end_time=$row->end_time;
                                            $booking_date=$row->booking_date;
                                            $schedule_time= date('h:i A',strtotime('-0 hour -15 minutes',strtotime($start_time)));
                                            $schedule_end= date('h:i A',strtotime('-0 hour -0 minutes',strtotime($end_time)));
                                          //echo '<pre>';print_r($result);echo '</pre>'; exit();
                                            if($row->call_status=='Active'){
                                            echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a>';                                             
                                            }
                                             else if($row->call_status=='Started'){
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Started</a>';
                                            }
                                            else if($row->call_status=='Completed'){
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Completed</a>';
                                            }
                                            else if($row->call_status=='Expire'){
                                              echo '<a href="'.base_url().'session/student/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Expire</a>';
                                            }


                                         ?> 
                                    </td>
                                </tr>
                            <?php }?>






                            </tbody>
                           </table>
                       </div>

                      </div>


                      </div>
              </div>

       <!--       <div class="col-md-5 col-sm-6 col-xs-12">

                <div class="why-choose-us">
                  <div class="panel-group why-choose-group" id="accordion">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Reviews <span class="fa fa-minus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body" style="padding: 0">
                             <div class="reviews">

                          <div class="row">
                              <div class="col-md-12">
                                    <blockquote>
                                        <div class="">
                                            <a href="#">
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                            </a>
                                        </div>
                                        <p>"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from"</p>
                                        <span> - John Doe</span>
                                    </blockquote>
                              </div>
                              <div class="col-md-12">
                                    <blockquote>
                                        <div class="">
                                            <a href="#">
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                            </a>
                                        </div>
                                        <p>"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from"</p>
                                        <span> - John Doe</span>
                                    </blockquote>
                              </div>
                              <div class="col-md-12">
                                    <blockquote>
                                        <div class="">
                                            <a href="#">
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star" style="color:#f7c80a"></i>
                                                <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                            </a>
                                        </div>
                                        <p>"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from"</p>
                                        <span> - John Doe</span>
                                    </blockquote>
                              </div>
                          </div>



                      </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default ">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Earnings <span class="fa fa-plus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                         <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Appointments <span class="fa fa-plus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


                  <div class="single-team-member" style="margin-top: 0">

                 <div class="">
                  <iframe style="width:100%" height="200" src="https://www.youtube.com/embed/ev0begDFc20">
                    </iframe>
                 </div>
                    <hr style="height: 1px; background: #eee; width: 100%">
                  <a href="#" class="btn comment-btn">Boost Your Profile</a>

                </div>

              </div> -->
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>


  <!-- alert  Modal content-->
      <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header " style="width: 100%;
             background-color:#15585F;
             height: 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Session help Message</h4>
              </div>
              <div class="modal-body " >
                <p> Session will be started before 15 minute from schedule time. If scheduled time is passed then, please refresh the page.</p>  
              </div>
              <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>

      <!-- model  -->




  <!-- Start footer -->

  <?php  $this->load->view('common/footer');?>
  <!-- End footer -->

    <!-- jQuery library -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<!--    <script type="text/javascript" src="<?php echo base_url();?>assets/js/pg-calendar-master/dist/js/pignose.calendar.full.min.js"></script>
    <script>
        $(document).ready(function() {
              $('.calendar').pignoseCalendar();
        });
    </script>-->
   <!--  <!-- Bootstrap -->



  <script type="text/javascript" src="<?php echo base_url();?>assets/js/Super-Simple-Calendar/js/dncalendar.min.js"></script>
    <script>
		$(document).ready(function() {
			var my_calendar = $("#dncalendar-container").dnCalendar({
				minDate: "2018-01-15",
				maxDate: "2018-12-02",
				defaultDate: "2018-05-10",
				monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
				monthNamesShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
				dayNames: [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                dayNamesShort: [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ],
                dataTitles: { defaultDate: 'Selected', today : 'hari ini' },
                notes: [
                		{ "date": "2018-05-25", "note": ["Not Available"] },
                		{ "date": "2018-05-12", "note": ["Not Available"] }
                		],
                showNotes: true,
                startWeek: 'Monday',
                dayClick: function(date, view) {
                	alert("You'r going to un-available on: "+date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear());
                        my_calendar.update({
                            defaultDate: date.getFullYear()+"-"+(date.getMonth() + 1)+"-"+date.getDate()
			});
                }
			});

			// init calendar
			my_calendar.build();

			// update calendar
			// my_calendar.update({
			// 	minDate: "2016-01-05",
			// 	defaultDate: "2016-05-04"
			// });
		});
		</script>
                  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
                  <script>

        $(document).ready(function() {
            $('#bookings-1').DataTable({
                searching:false,
                 sorting:false,
                "paging": false,
                "bInfo" : false
            });
        } );
    </script>

   
    <!-- <script type="text/javascript" src="<?php // echo base_url();?>assets/js/custom.js"></script> -->
  </body>
</html>
