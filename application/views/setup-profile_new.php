<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Setup Profile | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
     

    <link  href="<?php echo base_url();?>assets/js/imageuploader/css/fineCrop.css" rel="stylesheet">
    <link  href="<?php echo base_url();?>assets/js/imageuploader/css/layout.css" rel="stylesheet">
   
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> 
   
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  <!-- end about -->
<section id="testimonial" style='background: #f6fef7'>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >Setup account</h2>
                <span class="line"></span>           
              </div>
            </div>
              <div class="clearfix"></div>
                  <div class="col-md-10" style="float: none; margin: auto">
                      <div style="margin-top: 40px">
  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="student">
           <div class="contact-area-right" style="margin-top: 40px">
                  <!--  <form method="post" action="" class="comments-form contact-form" > -->
                    <form  method="post" action="<?php echo base_url();?>interviewer/create_account" onsubmit="return getpicture()" class="comments-form contact-form" id="create_tutor_account">
                       <div class="col-md-5">
                           <h4 style="color:#2aaebf !important; margin-bottom: 30px">Personal Information</h4>
                    <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Name</label>
                      <input type="text" name="first_name" class="form-control" placeholder="" value="<?php echo $status[0]->name;?>">
                    </div>
                     <!-- <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">last Name</label>
                      <input type="text" name="last_name" class="form-control" placeholder="" value="">
                    </div> -->
                    <div class="form-group">                        
                      <label style="color:#444; font-weight: normal"> University Email Address </label>
                      <input type="text" name="email_address" class="form-control" placeholder="" value="<?php echo $status[0]->email_address;?>" style="color: gray;" readonly>
                    </div>
                     <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Password</label>
                      <input type="password" name="passwords" id="passwords" class="form-control" placeholder="" value="">
                    </div>
                     <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Confirm Password</label>
                      <input type="password" name="confirm_passwords" id="confirm_passwords" class="form-control" placeholder="" value="">
                    </div>
                    <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Phone Number</label>
                      <input type="text" name="mobile_number" class="form-control" placeholder="" value="<?php echo $status[0]->phone_number;?>">
                    </div>
                    <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Gender</label>
                      <select class="form-control" name="gender" style="border-radius: 0">
                         <option value="">Select Gender</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Transgender">Transgender</option>
                      </select>
                    </div>
                            <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">DOB</label>
                       <input type="date" name="dob" class="form-control" >
                    </div>
<!-- 
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">About us</label>
                         <textarea class="form-control" name="about_us" style="height: 60px"></textarea>
                    </div> -->

                     <div class="col-lg-12" style="background: #f8f8f8; border:1px solid #f5f5f5; padding: 25px">
                               <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Your Display Picture</label>
                               </div>
                                <input type="file" id="upphoto" name="img" style="display:none;">
                                 <input type="hidden" id="upphoto1" name="picture" > 
                                 <input type="hidden"  name="img1" value="<?php echo $status[0]->profile_image;?>" style="display:none;">
                                <label for="upphoto">
                                    <div class="inputLabel">
                                        click here to upload an image
                                    </div>
                                </label>

                            </div>

                             <div class="col-lg-12">
                                <?php  $image=$status[0]->profile_image;
                                  if($image=='')
                             echo '<img id="croppedImg" src="'.base_url().'assets/images/default.png" alt="team member img">';
                  
                              else
                          echo '<img id="croppedImg" src="'.base_url().$image.'" alt="team member img">';
                              ?>
                            </div>  
                           
                           
                       </div>
                       
                       <div class="col-md-offset-1 col-md-6">
                           
                           <h4 style="color:#2aaebf !important; margin-bottom: 30px">Academic Information</h4>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Nationality</label>
                      <select class="form-control" name="country" style="border-radius: 0">
                        <option value="">Select nationality</option>
                         <?php foreach($country as $row){ 
                         echo '<option value="'.$row->country_id.'">'.$row->country_name.'</option>';
                       } ?>
                     </select>
                    </div>
                    
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">University</label>
                      <input type="text" name="university" class="form-control" placeholder="" value="<?php echo $status[0]->university_name;?>" style="color: gray;" readonly>
                      <input type="hidden" name="university_id"  value="<?php echo $status[0]->university_id;?>">
                    </div>
                     <div class="form-group">  
                         <label style="color:#444; font-weight: normal">College</label>
                      <input type="text" name="college" class="form-control" placeholder="" value="<?php echo $status[0]->college_name;?>" style="color: gray;" readonly>
                      <input type="hidden" name="college_id"  value="<?php echo $status[0]->college_id;?>">
                    </div>

                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Course</label>
                      <input type="text" name="subject" class="form-control" placeholder="" value="<?php  echo $status[0]->subject;?>">
                    </div>

                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Current year of study</label>
                      <input type="text" name="study_year" class="form-control" placeholder="" value="<?php  echo $status[0]->year_of_study;?>" style="color: gray;" readonly>
                    </div>

                   <!--  <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Subject</label>
                      <input type="text" name="subject" class="form-control" placeholder="" value="<?php echo $status[0]->subject;?>">
                    </div> -->

                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Previous interviewing and tutoring experience (only fill in if you have relevant experience)</label>
                      <!-- <input type="text" name="interview_exprience" class="form-control" placeholder="" value="<?php //echo $status[0]->subject;?>"> -->
                      <!-- <select class="form-control" name="interview_exprience" style="border-radius: 0">
                        <option value="">Select year of experience</option>
                        <?php for ($i=0; $i < 51; $i++) { ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?> year</option> 
                        <?php } ?>
                        
                     </select> -->
                     <textarea class="form-control" name="interview_exprience" style="height: auto;"></textarea>
                    </div>
                   
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Main academic interests</label>
                         <textarea class="form-control" name="acadmic_intrests" style="height: auto;"><?php echo $status[0]->working_interest;?></textarea>
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">What do you wish to told before your Oxbridge interview</label>
                         <textarea class="form-control" name="wish_to" style="height: auto;"><?php echo $status[0]->wish_to_know;?></textarea>
                    </div>
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Extra-curricular interests</label>
                         <textarea class="form-control" name="extra_curicular" style="height: auto;"> <?php echo $status[0]->extra_curricular;?></textarea>
                    </div>
                   

                   <!--  <div class="col-lg-12" style="background: #f8f8f8; border:1px solid #f5f5f5; padding: 25px">
                               <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">Your Display Picture</label>
                               </div>
                                <input type="file" id="upphoto" name="img" style="display:none;">
                                 <input type="hidden"  name="img1" value="<?php echo $status[0]->profile_image;?>" style="display:none;">
                                <label for="upphoto">
                                    <div class="inputLabel">
                                        click here to upload an image
                                    </div>
                                </label>

                            </div>

                             <div class="col-lg-12">
                                <?php  $image=$status[0]->profile_image;
                                  if($image=='')
                             echo '<img id="croppedImg" src="'.base_url().'assets/images/default.png" alt="team member img">';
                  
                              else
                          echo '<img id="croppedImg" src="'.base_url().$image.'" alt="team member img">';
                              ?>
                            </div>  -->
                            <br><br>
                        <div class="form-group" style="color:#888">                        
                          <input type="checkbox" name="terms" style="height: auto;"> I agree with GDPR Policy
                        </div>    
                        <button class="comment-btn" type="submit">Update </button>
                       </div>
                  </form>
                 </div>
      </div> <!--student form ends-->
</div>
                 </div>
              </div>
          </div>
        </div>
             
      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
 
  <?php  $this->load->view('common/footer');?>
  <!-- End footer -->
  
   <div class="cropHolder">
        <div id="cropWrapper">
            <img id="inputImage" src="images/face.jpg">
        </div>
        <div class="cropInputs">
            <div class="inputtools">
                <p>
                    <span>
                        <i class="fa fa-arrows-h"></i>
                    </span>
                    <span>horizontal movement</span>
                </p>
                <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
            </div>
            <div class="inputtools">
                <p>
                    <span>
                        <i class="fa fa-arrows-v"></i>
                    </span>
                    <span>vertical movement</span>
                </p>
                <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
            </div>
            <br>
            <button class="cropButtons" id="zplus">
                <i class="fa fa-search-plus"></i>
            </button>
            <button class="cropButtons" id="zminus">
                <i class="fa fa-search-minus"></i>
            </button>
            <br>
            <button id="cropSubmit">submit</button>
            <button id="closeCrop">Close</button>
        </div>
    </div>
   <!-- start script for validation  -->
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
   <!-- End  script for validation  -->
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <!-- Slick Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
    <!-- mixit slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
    <!-- Add fancyBox -->        
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
   <!-- counter -->
    <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
    <!-- Wow animation -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
    <!-- progress bar   -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/imageuploader/js/fineCrop.js"></script>
    
     <script>
        $("#upphoto").finecrop({
            viewHeight: 500,
            cropWidth: 200,
            cropHeight: 200,
            cropInput: 'inputImage',
            cropOutput: 'croppedImg',
            zoomValue: 50
        });
        function getpicture(){
          $("#upphoto1").val($("#croppedImg").attr("src"));
          return true;
        }
    </script>
    
  </body>
</html>