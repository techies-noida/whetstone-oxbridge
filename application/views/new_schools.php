<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> New Schools</title>
    <?php $this->load->view('common/header_assets'); ?>
    <style>
        #schools_features ul{list-style-image: url(<?php echo base_url();?>assets/images/check.png)}
        #schools_features p{color:#fff; font-size: 16px; font-weight: bold}
        .title-area p{color:#fff; font-size: 16px; font-weight: bold}
        #schools_features ul li{font-size:16px; color:#eee;}
        #schools_features ul li a{font-size:16px; color:#eee;}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU -->

  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Schools</h2>
              <p>We are proud to be trusted by top performing schools not only in the UK, but across the world.</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Schools</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->

  <!-- Start Feature -->
  <section id="service" style="background: #fff; padding: 60px 0">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Interview practice</h2>
            <span class="line"></span>
          </div>
        </div>
       <div class="col-md-12">
          <div class="service-content">
            <div class="row">
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">
                <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; margin-top: 20px;">
                  <i class="fa fa-check service-icon"></i>
                  <?php   echo $school[0]->body_1;?>
                 <!--  <p>While many schools are very adept at supporting their students through Oxbridge applications, there is only a limited amount of practice that they can offer. We have found that, on average, applicants have only one practice interview, and this is usually with a teacher that they are already familiar with.</p> -->
                </div>
                <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
                  <i class="fa fa-check service-icon"></i>
                   <?php   echo $school[0]->body_2;?>
                 <!--  <p>Whetstone offers schools interview practice packages where, depending on the number of applicants they have that year, they can purchase interview hours for their pupils. We provide schools with a series of codes which they can distribute to their applicants to enable them to find, book and practice their interview skills with a current Oxbridge student.</p> -->
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
              	<img src="<?= FILE_PATH_SCHOOL.$school[0]->image_1;?>" class="img img-responsive">
              </div>
              <!-- End single service -->
              <div class="clearfix"></div>
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6 col-sm-push-6">
                 <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; margin-top: 40px;">
                  <i class="fa fa-check service-icon"></i>
                   <?php  echo $school[0]->body_3;?>
                 <!--  <p>At the end of the practice will we then send the feedback given to both the pupil and the school co-ordinator. This enables teacher and pupil to work on the comments made together. Teachers are an invaluable resource of experience and advice. Our interview practice tool therefore supports teachers in preparing their pupils for Oxbridge interview.</p> -->
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-sm-pull-6">
              	<img src="<?=FILE_PATH_SCHOOL.$school[0]->image_2;?>" class="img img-responsive">
              </div>
              <!-- End single service -->
            </div>
            <div class="row">
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">

              </div>
              <!-- End single service -->
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">

              </div>
              <!-- End single service -->
            </div>
          </div>
        </div>
            <div class="col-md-12">
          <div class="title-area">
             <i class="fa fa-check service-icon" style="margin-left: 100px;"></i>
             <?php  echo $school[0]->body_4;?>
               <!-- <p>If you are interested in this service and would like to find out more then please <a href="#contact">fill in the form</a> below today and our <b>CEO Fenella Chesterfield</b> will be in touch to talk through a bespoke solution for your school.</p>  -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Feature -->
  <section id="latest-news" style="background-image: url(<?= FILE_PATH_SCHOOL.$school[0]->upcoming; ?>); background-size: cover;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title" style="color:#fff"><?php  echo $school[0]->center_body;?></h2>
            <span class="line"></span>
            <p style="color:#fff; font-size: 16px; font-weight: bold"><?php echo  $school[0]->main_title;?> </p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="latest-news-content">
            <div class="row">
              <!-- start single latest news -->
           <!--   <div class="col-md-4" style='border-right: 1px solid #ccc'>
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-1.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">Whetstone Roadshow 2019</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|10 Jan 2019</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div> -->
              <!-- start single latest news -->
              <!-- start single latest news -->
              <div class="col-md-12">
                 <div class="contact-area-left text-center" id="schools_features">
                     <ul style="list-style-image: url(<?php echo base_url();?>assets/images/check.png)">
                       <?php  echo $school[0]->body_5;?>
                     <!-- <li style="color:#fff; font-size: 16px; text-align: center"> Free of charge, we organise for one of our current student teams to come and offer a range of talks and workshops designed to support prospective applicants.</li>
                     <li style="color:#fff; font-size: 16px; text-align: center"> From breaking down the myths surrounding what makes an Oxbridge student, to detailed interview preparation workshops, we adapt our services to each individual school.</li>
                     <li style="color:#fff; font-size: 16px; text-align: center"> Register your interest by filling in the <a href="#contact" style="color: #fff">form</a> below</li> -->
                     <br>
                     <br>
                     <!--<h4>Upcoming</h4>-->



                 </div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="contact" style='padding-top: 40px'>
     <div class="container">
       <div class="row">
      <div class="title-area">
            <h2 class="title">Register Interest</h2>
            <span class="line"></span>

          </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">

               <div class="col-md-6" style='float: none; margin: auto'>
                 <div class="contact-area-right">
                   <div id="intrested_user_message"></div>
                   <form action="" id="intrested_user"class="comments-form contact-form">
                    <div class="form-group">
                      <input type="text"  name="interested_user_name"class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                      <input type="text"  name="school_name"class="form-control" placeholder="School Name">
                    </div>
                    <div class="form-group">
                      <input type="email" name="interested_user_email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <input type="text" name="interested_user_phone" class="form-control" placeholder="Phone Number">
                    </div>

                    <div class="form-group">
                      <textarea placeholder="Message" name="interested_user_message" rows="2" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        Interested in
                       <input type="checkbox" name="interview"   value ="interviewer" style="height: auto"> Interview Practice
                      <input type="checkbox" name="roadshow"  value ="roadshow" style="height: auto"> Roadshow
                    </div>
                    <div class="col-lg-12" style=" padding: 0px">
                     <div class="form-group" style="color:#; margin-top: 10px">
                      <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a href="/privacy-policy" target="_blank" style="color:#;">GDPR Policy</a>.
                    </div>
                    </div>
                        <button class="comment-btn" type="submit">Submit</button>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>

  <!-- Start subscribe us -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <?php $this->load->view('common/footer');?>

   <script type="text/javascript">
    $(function myFunction(){

    $('#intrested_user').validate({
        rules:{
            interested_user_name:{
                required:true,
                minlength:4,
                maxlength:60,

            },
            interested_user_email:{
                required:true,
                email:true,
            },
            interested_user_message:{
                required:true,
                 minlength: 6,
                maxlength: 650,
            },
            interested_user_phone:{
                required:true,
                digits: true,
                minlength: 12,
                maxlength:12,
            },
           interview: {
            required: true,
           },
           roadshow: {
            required: true,
           },
            terms_condition: {
            required: true,
           },

        },
        messages:{
            interested_user_name:{
                required:"  Please enter  name ",
                minlength:"Please enter name minmum 4 character",
                maxlength:"Name should not more than 60 character",

            },

            interested_user_email:{
                required:"  Please enter email address ",
                email:"Please enter valid email id ",
            },
            interested_user_message:{
                required: "Please enter comment",
                minlength: "Please write comment minmum 15 characters",
                maxlength:"Comment should not more than 650 characters",


            },
            interested_user_phone:{
                required: "Please enter phone number",
                digits: "Only numeric digits allow",
                minlength: "Please enter 12 digit phone number",
                maxlength:"Please enter only 12 digit phone number",


            },

           interview: {
             required: "Please select",
          },
           roadshow: {
             required: "Please select",
          },
           terms_condition: {
             required: "Please accept GDPR olicy",
          },

        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>oxbridge/interested_user',
            $('#intrested_user').serialize(),
            function(data){

                if(data == 1)
                {
                     responseText = '<span style="color:green;font-size: 14px;font-weight: normal;margin-left: 40px;">Thank you very much for submitting your interest in, a member of our team will be in touch with you very soon to discuss your requirements and options.</span>';
                    $("#intrested_user_message").html(responseText);
                     $("#intrested_user")[0].reset();
                }
                 if(data == 0)
                {
                  responseText = '<span style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;">Your have already  requested </span>';
                  //$('#message1').text('Your email Id already exist');
                  $("#intrested_user_message").html(responseText);
                   $("#intrested_user")[0].reset();
                }



            });

           setTimeout(function(){
            $("#intrested_user_message").hide();
              }, 20000);
       }
   });
});

  </script>

  </body>
</html>
