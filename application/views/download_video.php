<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Download Video</h2>
              <!-- <p>As a professional teacher…</p> -->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Download Video</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
   <!-- Start about  -->
  <section id="about" style="border-bottom:1px solid #eee; padding-bottom: 0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Download Video</h2>
              <?php if(!empty($message)){?>
              <p style="font-size: 25px;margin-bottom: 35px;"><?=$message?></p>
              <?php }else{
              ?>
              <p style="font-size: 25px;margin-bottom: 35px;"><a href="<?=$download_link?>" download>Click here to download video</a></p>
              <?php }
              ?>
            <!-- <span class="line"></span> -->
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end about -->
   <?php  $this->load->view('common/newsletter');?>
  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
    
  </body>
</html>