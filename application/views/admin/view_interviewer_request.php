

<!doctype html>
<html lang="en">
<head>
	<title>Interviewers | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<div class="subheader">
				<ul>
					<li>Title</li>
				</ul>
			</div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">

						<div class="panel-heading">
							<h3 class="panel-title">Interviewer Profile Information </h3>
							<div class="right">
								<a href="https://whetstone-oxbridge.com/admin/interviewer-dashboard/8891446811500288"> Interviewer Dashbaord</a>
                        <!-- <a href="https://whetstone-oxbridge.com/admin/interviewer-dashboard/<?=$interviewers[0]->request_id?>"> Interviewer Dashbaord</a> -->
							</div>
						</div>



						
						<div class="panel-body" style="padding-top: 20px">
							<form id="request_join_tutor" method="post" onsubmit="return getpicture()" class="comments-form contact-form clearfix" role="form" <form="" action="https://whetstone-oxbridge.com/Admin/Interviewer_admin/update_profile" enctype="multipart/form-data" accept-charset="utf-8">
								<div class="col-md-12">

									<h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Personal Information</h4>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-5">
									<div class="form-group">
										<label style="color:#444; font-weight: normal">Request Id</label>
										<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->request_id ?>"  readonly="">
									</div>
									<div class="form-group">
										<label style="color:#444; font-weight: normal">Name</label>
										<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->name ?>" readonly="">
									</div>
                     <!-- <div class="form-group">
                      <label style="color:#444; font-weight: normal">last Name</label>
                      <input type="text" name="last_name" class="form-control" placeholder="" value="">
                  </div> -->
                  <div class="form-group">
                  	<label style="color:#444; font-weight: normal"> Email address </label>
                  	<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->email_address ?>" readonly="">
                  </div>
                  <div class="form-group">
                  	<label style="color:#444; font-weight: normal">Phone number</label>
                  	<input type="text" value="<?php echo $interviewers[0]->phone_number ?>" class="form-control" placeholder="" readonly="">
                  </div>
                  <?php $image = $interviewers[0]->profile_image; ?>   
                  <div class="col-lg-12">
                  	<!--  <img id="croppedImg" src="https://whetstone-oxbridge.com/assets/images/default.png"> -->
                  	<img id="croppedImg" src="<?php echo base_url() . $image; ?>" style="width: 200px;">
                  </div>


                  </div>

                  <div class="col-md-offset-1 col-md-6">

                  	
                  <div class="form-group">
                  		<label style="color:#444; font-weight: normal">University</label>
                  		<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->university_name ?>" readonly="">
                  	</div>

                  	<div class="form-group">
                  		<label style="color:#444; font-weight: normal">College</label>
                  		<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->college_name ?>" readonly="">
                  	</div>

                  	<div class="form-group">
                  		<label style="color:#444; font-weight: normal">Course</label>
                  		<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->subject ?>" readonly="">
                  	</div>

                  	<div class="form-group">
                  		<label style="color:#444; font-weight: normal">Current year of study</label>
                  		<input type="text" class="form-control" placeholder="" value="<?php echo $interviewers[0]->year_of_study ?>" readonly="">
                  	</div>
                  	<div class="form-group">
                  		<label style="color:#444; font-weight: normal">Main academic interests</label>
                  		<p><?php echo $interviewers[0]->working_interest ?></p>
                  	</div>
                  	<div class="form-group">
                  		<label style="color:#444; font-weight: normal">What do you wish you’d known before your Oxbridge interview?</label>
                  		<p><?php echo $interviewers[0]->wish_to_know ?></p>
                  	</div>
                  	<div class="form-group">
                  		<label style="color:#444; font-weight: normal">Extra-curricular interests</label>
                  		<p><?php echo $interviewers[0]->extra_curricular ?></p>
                  	</div>
                  </div>
              </form>
          </div>
      </div>
      <!-- END OVERVIEW -->
  </div>
</div>
<!-- END MAIN CONTENT -->
<?php $this->load->view('admin/common/footer');?> 

</body>

</html>
