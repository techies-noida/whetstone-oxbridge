<!doctype html>
<html lang="en">

<head>
    <title>Home Page | Whetstone Oxbridge</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/chartist/css/chartist-custom.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/main.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/demo.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url();?>assets/admin/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url();?>assets/admin/assets/img/favicon.png">
    <link rel="shortcut icon" type="image/icon" href="https://whetstone-oxbridge.com/assets/images/favicon.ico"/>

        <style>
            .section-group{background: #f2f6fa; position: relative; padding-top: 20px; margin-bottom: 44px}
            .section-group .section-group-title{background: #f2f6fa; position: absolute; content:""; top:-32px; border-radius: 4px 4px 0 0; padding: 6px 15px}
            .why-choose{background: #f2f6fa; position: relative; padding-top: 20px; margin-bottom: 44px}
            .why-choose .why-choose-title{background: #f2f6fa; position: absolute; content:""; top:-32px; border-radius: 4px 4px 0 0; padding: 6px 15px}
             .banner-cms{background: #fff; border:1px solid #eee; position: relative; padding-top: 20px; margin-bottom: 44px}
            .banner-cms .banner-cms-title{cursor:pointer; right:0; background: #f2f6fa; position: absolute; content:""; top:-34px; border-radius: 4px 4px 0 0; padding: 6px 15px; border:1px solid #eee; border-bottom: 0}
        </style>
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
       <?php $this->load->view('admin/common/navbar_sidebar');?>
        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
                    <div class="subheader">
                        <ul>
                            <li>CMS Home Page</li>
                        </ul>
                    </div>
                     <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?>
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    
                    <!-- END OVERVIEW -->
                    <div class="row">
                        <div class="col-md-12">
                        <form class="col-md-12 form-panel" style="border:0">
                        <div class="row">
                            <div class="panel new-panel">
                                <div class="panel-heading">
                               
                                        <h3 class="panel-title">Banner Area</h3>
                                
                                    <br>
                                </div>
                                <div class="panel-body no-padding">
                                    <div class="banner-cms">
                                        <div class="banner-cms-title"  data-toggle="modal" data-target="#myModal2" id="addsection">Add New Banner </div>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>S.N.</th>
                                                <th>Banner Image</th>
                                                <th>Heading</th>
                                                <th>Subheading</th>
                                                <th>Link</th>
                                                <th>Action</th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $i=1;foreach ($banner as $row) { ?>
                                               
                                             <tr>
                                                <td class="title-index" style="vertical-align: middle"><?php echo $i++;?></td>
                                                <td><img src="<?php echo base_url().$row->banner_image;?>" height="60"/></td>
                                                <td>
                                                  
                                                    <p><?php echo $row->title;?></p>
                                                </td>
                                                <td>
                                                    
                                                    <p><?php echo $row->sub_title;?></p>
                                                </td>
                                                <td>
                                                 
                                                    <a href=""><?php echo $row->link;?></a>
                                                </td>
                                                <td style="vertical-align: middle">
                                                    <a href="#" data-toggle="modal" data-target="#<?php echo 'editmyModal'.$row->serial_number;?>" class="btn btn-xs btn-info">Edit</a>
                                                </td>
                                                 <td style="vertical-align: middle">
                                                    <a class="btn btn-xs btn-danger" href="<?php echo base_url().'Web_home_page/deleteBanner/'.$row->serial_number;?>" onClick="return doconfirm();"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr> 



                                             <!-- Modal -->
                                                <div class="modal fade" id="<?php echo 'editmyModal'.$row->serial_number;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Banner</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="row">
                                                          <form  role="form"  method="post" class="col-md-12 form-panel" style="border:0" <?php  echo form_open_multipart(base_url().'Web_home_page/update_banner')?>   

                                                       <div class="col-md-12">
                                                        <fieldset class="form-group">
                                                            <label>Upload Image</label>
                                                            <input type="file" name="img">
                                                             <input type="hidden" name="img1" value="<?php echo $row->banner_image;?>">
                                                            <img src="<?php echo base_url().$row->banner_image;?>" height="60"/>
                                                        </fieldset>
                                                        <div class="preview-images-zone">

                                                        </div>
                                                    <!-- </div> -->
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12">
                                                        <label>Heading</label>
                                                        <input class="form-control" name="title" value="<?php echo $row->title;?>" type="text">
                                                         <input class="form-control" name="id" value="<?php echo $row->serial_number;?>" type="hidden">
                                                        <br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Sub-Heading</label>
                                                        <textarea class="form-control" name="subtitle"> <?php echo $row->sub_title;?></textarea>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Read More Link</label>
                                                        <input class="form-control" name="link" value="<?php echo $row->link;?>" type="text">
                                                        <br>
                                                    </div>

                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-info"> Submit </a> -->
                                                                 <button class="btn btn-primary" type="submit">Update</button>
                                                            </div>
                                                        </div>
                                                   </div>


                                                          </div>
                                                      </form>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                       <!--  <button type="button" class="btn btn-primary">Save changes</button>
                                                        <button type="button" class="btn btn-danger">Delete</button> -->
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                        <?php }?>
                                           
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                                    <div class="panel-footer">
                                            <div class="row">
                                                
                                                <a href="#" class="btn btn-primary"> Update & Publish </a>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </form>

                        </div>
                    </div>
                    <!-- END OVERVIEW -->
                    <div class="row">
                        <div class="col-md-12">
                            
                      <!--<form class="col-md-12 form-panel" style="border:0"> -->
                              <form  role="form" id="" method="post"  class="col-md-12 form-panel" style="border:0" action="<?= base_url();?>Web_home_page/what_we_do" <?php  echo form_open_multipart();?>  
                                 
                            <div class="row">
                                <div class="panel new-panel">
                                        <div class="panel-heading">
                                       
                                                <h3 class="panel-title">What we do Area</h3>
                                               <span id="what_message"></span>
                                            <br>
                                        </div>
                                        <div class="panel-body no-padding">
                                            <div class="section-group">
                                                <div class="section-group-title">What we do  </div>
                                                <div class="col-md-12">
                                                    <fieldset class="form-group">
                                                        <label>Upload Image</label>
                                                        <input type="file" name="img" value="<?php echo $home[0]->what_we_do_image;?>">
                                                         <input type="hidden" name="img1" value="<?php echo $home[0]->what_we_do_image;?>">
                                                         <img src="<?php echo base_url().$home[0]->what_we_do_image;?>" height="60"/>
                                                    </fieldset>
                                                    <div class="preview-images-zone">

                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                
                                                <div class="col-md-12">
                                                    <label>Content</label>
                                                    <textarea name="what_we_do" class="form-control"><?php echo $home[0]->what_we_do;?></textarea>
                                                    <br>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>More Info Link</label>
                                                    <input class="form-control" name="what_we_do_link" value="<?php echo $home[0]->what_we_do_link;?>" type="text">
                                                    <br>
                                                </div>
                                                <div class="clearfix"></div>
                                                
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                                <div class="row">
                                                   <!--  <a href="#" class="btn btn-primary"> Update & Publish </a>-->
                                                    <button class="btn btn-primary" type="submit">Update & Publish</button>
                                                </div>
                                        </div>
                                </div>
                            </div>
                        </form>
                            
                        </div>
                    </div>
                    <!-- END OVERVIEW -->
                    <div class="row">
                        <div class="col-md-12">
                     <form  role="form"  method="post" class="col-md-12 form-panel" style="border:0" <?php  echo form_open_multipart(base_url().'Web_home_page/how_it_work')?> 
                        <div class="row">
                            <div class="panel new-panel">
                                    <div class="panel-heading">
                                   
                                            <h3 class="panel-title">How it works Area</h3>
                                    
                                        <br>
                                    </div>
                                    <div class="panel-body no-padding">
                                        <div class="section-group">
                                          
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <label>Content 1</label>
                                                <textarea name="how_it_work_content1" class="form-control"> <?php  echo $home[0]->how_it_work_content1;?> </textarea>
                                                <br>
                                            </div>
                                           
                                            <div class="clearfix"></div>
                                            
                                        </div>
                                        
                                        <div class="section-group">
                                            
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <label>Content 2</label>
                                                <textarea class="form-control" name="how_it_work_content2"> <?php  echo $home[0]->how_it_work_content2;?></textarea>
                                                <br>
                                            </div>
                                           
                                            <div class="clearfix"></div>
                                            
                                        </div>
                                        
                                        <div class="section-group">
                                          
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <label>Content 3</label>
                                                <textarea class="form-control" name="how_it_work_content3"><?php  echo $home[0]->how_it_work_content3;?></textarea>
                                                <br>
                                            </div>
                                           
                                            <div class="clearfix"></div>

                                             <div class="col-md-12">
                                                <label>Video Url</label>
                                                <textarea class="form-control" name="how_it_work_url"><?php   echo $home[0]->how_it_work_url;?></textarea>
                                                <br>
                                            </div>


                                            
                                        </div>
                                    
                                    
                                    </div>
                                    <div class="panel-footer">
                                            <div class="row">
                                                <!-- <a href="#" class="btn btn-primary"> Update & Publish </a> -->
                                                <button class="btn btn-primary" type="submit">Update & Publish</button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </form>
                            
                        </div>
                    </div>

                    <!--  Real time video feedback  -->

                    <div class="row">
                        <div class="col-md-12">
                     <form  role="form"  method="post" class="col-md-12 form-panel" style="border:0" <?php  echo form_open_multipart(base_url().'Web_home_page/real_time_video')?> 
                        <div class="row">
                            <div class="panel new-panel">
                                    <div class="panel-heading">
                                   
                                            <h3 class="panel-title">Real time video feedback</h3>
                                    
                                        <br>
                                    </div>
                                    <div class="panel-body no-padding">
                                        
                                        
                                        <div class="section-group">
                                            
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <fieldset class="form-group">
                                                        <label>Upload Image 1</label>
                                                        <input type="file" name="img" value="<?php echo $home[0]->real_time_video_image1;?>">
                                                         <input type="hidden" name="img1" value="<?php echo $home[0]->what_we_do_image;?>">
                                                         <img src="<?php echo base_url().$home[0]->real_time_video_image1;?>" height="60"/>
                                                    </fieldset>
                                                    <div class="preview-images-zone">

                                                    </div>
                                            </div>
                                           
                                            <div class="clearfix"></div>
                                            
                                        </div>
                                        
                                        <div class="section-group">
                                          
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <fieldset class="form-group">
                                                        <label>Upload Image 2</label>
                                                        <input type="file" name="img2" value="<?php echo $home[0]->real_time_video_image2;?>">
                                                         <input type="hidden" name="img3" value="<?php echo $home[0]->what_we_do_image;?>">
                                                         <img src="<?php echo base_url().$home[0]->real_time_video_image2;?>" height="60"/>
                                                    </fieldset>
                                                    <div class="preview-images-zone">

                                                    </div>
                                            </div> 
                                            <div class="clearfix"></div> 
                                        </div>
                                        <div class="section-group">
                                          
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <label>Heading</label>
                                                <textarea name="heading"  class="form-control"> <?php  echo $home[0]->real_time_video_heading;?> </textarea>
                                                <br>
                                            </div>
                                           
                                            <div class="clearfix"></div>
                                            
                                        </div>

                                        <div class="section-group">
                                          
                                            <div class="clearfix"></div>
                                            
                                            <div class="col-md-12">
                                                <label>Content </label>
                                                <textarea name="editor1" id="editor1" rows="10" cols="80" class="form-control"> <?php  echo $home[0]->real_time_video_content;?> </textarea>
                                                <br>
                                            </div>
                                           
                                            <div class="clearfix"></div>
                                            
                                        </div>
                                    
                                    
                                    </div>
                                    <div class="panel-footer">
                                            <div class="row">
                                                <!-- <a href="#" class="btn btn-primary"> Update & Publish </a> -->
                                                <button class="btn btn-primary" type="submit">Update & Publish</button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </form>
                            
                        </div>
                    </div>

                                        
                    <div class="row">               
                        <div class="col-md-12">
                       <!--  <form class="col-md-12 form-panel" style="border:0"> -->
                            <div class="row">
                                <div class="panel new-panel">
                                        <div class="panel-heading">
                                       
                                                <h3 class="panel-title">Why Choose Us Area</h3>
                                        
                                            <br>
                                        </div>

                                         <div class="panel-body no-padding">
                                    <div class="banner-cms">
                                        <div class="banner-cms-title"  data-toggle="modal" data-target="#myModal233" id="addsection">Add New</div>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>S.N.</th>
                                               <!--  <th> Image</th> -->
                                                <th>Heading</th>
                                                <th>Content</th>
                                                <th>Edit</th>  
                                                <th>Delete</th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $i=1;foreach ($choose_us as $row) { ?>
                                               
                                             <tr>
                                                <td class="title-index" style="vertical-align: middle"><?php echo $i++;?></td>
                                               <!--  <td><img src="<?php // echo base_url().$row->image;?>" height="60"/></td> -->
                                                <td>
                                                  
                                                    <p><?php echo $row->heading;?></p>
                                                </td>
                                                <td>
                                                    
                                                    <p><?php echo $row->content;?></p>
                                                </td>
                                               
                                                <td style="vertical-align: middle">
                                                    <a href="#" data-toggle="modal" data-target="#<?php echo 'editmyModalchoose'.$row->serial_number;?>" class="btn btn-xs btn-info">Edit</a>
                                                </td>

                                                <td style="vertical-align: middle">
                                                    <a class="btn btn-xs btn-danger" href="<?php echo base_url().'Web_home_page/delete_choose_us/'.$row->serial_number;?>" onClick="return doconfirm();"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr> 



                                             <!-- Modal -->
                                                <div class="modal fade" id="<?php echo 'editmyModalchoose'.$row->serial_number;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Why choose Us</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="row">
                                                          <form  role="form"  method="post" class="col-md-12 form-panel" style="border:0" <?php  echo form_open_multipart(base_url().'Web_home_page/update_choose_us')?>   

                                                       <div class="col-md-12">
                                                        <fieldset class="form-group">
                                                            <label>Upload Image</label>
                                                            <input type="file" name="img">
                                                             <input type="hidden" name="img1" value="<?php echo $row->image;?>">
                                                            <img src="<?php echo base_url().$row->image;?>" height="60"/>
                                                        </fieldset>
                                                        <div class="preview-images-zone">

                                                        </div>
                                                    <!-- </div> -->
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12">
                                                        <label>Heading</label>
                                                        <input class="form-control" name="heading" value="<?php echo $row->heading;?>" type="text">
                                                         <input class="form-control" name="id" value="<?php echo $row->serial_number;?>" type="hidden">
                                                        <br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Content</label>
                                                        <textarea class="form-control" name="content"> <?php echo $row->content;?></textarea>
                                                        <br>
                                                    </div>
                                                   

                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-info"> Submit </a> -->
                                                                 <button class="btn btn-primary" type="submit">Update</button>
                                                            </div>
                                                        </div>
                                                   </div>


                                                          </div>
                                                      </form>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                       <!--  <button type="button" class="btn btn-primary">Save changes</button>
                                                        <button type="button" class="btn btn-danger">Delete</button> -->
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                        <?php }?>
                                           
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                                       
                                </div>
                            </div>
                        
                        </div>
                    </div>
                                        
                    <div class="row">
                        <div class="col-md-12">
                            
                           <!--  <form class="col-md-12 form-panel" style="border:0"> -->
                            <form  role="form" id="" method="post"  class="col-md-12 form-panel" style="border:0" <?php echo form_open_multipart(base_url().'Web_home_page/school_section');?>  
                                <div class="row">
                                    <div class="panel new-panel">
                                            <div class="panel-heading">
                                           
                                                    <h3 class="panel-title">Schools Area</h3>
                                            
                                                <br>
                                            </div>
                                            <div class="panel-body no-padding">
                                                <div class="section-group">
                                                    <div class="section-group-title">1  </div>
                                                    <div class="col-md-12">
                                                        <fieldset class="form-group">
                                                            <label>Upload Image</label>
                                                            <input type="file" name="img">
                                                            <input type="hidden" name="img1" value="<?php echo $home[0]->school_image;?>">
                                                        </fieldset>
                                                        <div class="preview-images-zone">
                                                            <img src="<?php echo base_url().$home[0]->school_image;?>" height="60"/>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    
                                                    <div class="col-md-12">
                                                        <label>Heading</label>
                                                        <input type="text" name="school_heading" value="<?php echo $home[0]->school_heading;?>" class="form-control" required>
                                                        <br>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>Content</label>
                                                        <textarea class="form-control" name="school_content" required><?php echo $home[0]->school_content;?></textarea>
                                                        <br>
                                                    </div>
                                                     <div class="col-md-12">
                                                        <label>Read More Link</label>
                                                        <input type="text" class="form-control" name="school_link" value="<?php echo $home[0]->school_link;?>">
                                                        <br>
                                                    </div>
                                                   
                                                    <div class="clearfix"></div>
                                                    
                                                </div>
                                                
                                            
                                            
                                            </div>
                                            <div class="panel-footer">
                                                    <div class="row">
                                                        <!-- <a href="#" class="btn btn-primary"> Update & Publish </a> -->
                                                         <button class="btn btn-primary" type="submit">Update & Publish</button>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <div class="row">                  
                        <div class="col-md-12">
                            
                        <!-- <form class="col-md-12 form-panel" style="border:0"> -->
                            <form  role="form" id="" method="post"  class="col-md-12 form-panel" style="border:0" <?php echo form_open_multipart(base_url().'Web_home_page/contact_section');?>
                            <div class="row">
                                <div class="panel new-panel">
                                        <div class="panel-heading">
                                       
                                                <h3 class="panel-title">Contact us Area</h3>
                                        
                                            <br>
                                        </div>
                                        <div class="panel-body no-padding">
                                            <div class="section-group">
                                                <div class="section-group-title">Contact Info  </div>
                                                <div class="col-md-12">
                                                    <fieldset class="form-group">
                                                        <label>Upload Contact Image</label>
                                                        <input type="file" name="img">
                                                        <input type="hidden" name="img1" value="<?php echo $home[0]->contact_image;?>">
                                                    </fieldset>
                                                    <div class="preview-images-zone">
                                                         <img src="<?php echo base_url().$home[0]->contact_image;?>" height="60"/>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12">
                                                    <label>Content</label>
                                                    <textarea class="form-control" name="contact_content" required ><?php echo $home[0]->contact_content;?></textarea>
                                                    <br>
                                                </div>
                                                
                                                <div class="col-md-12">
                                                    <label>Title of Contact</label>
                                                    <input type="text" name="contact_title" class="form-control" value="<?php echo $home[0]->contact_title;?>" required >
                                                    <br>
                                                </div>
                                                 <div class="col-md-12">
                                                    <label>Contact Email</label>
                                                    <input type="email" name="contact_email" class="form-control" value="<?php echo $home[0]->contact_email;?>" required>
                                                    <br>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Contact Number</label>
                                                    <input type="text" name="contact_number" class="form-control" value="<?php echo $home[0]->contact_number;?>" required>
                                                    <br>
                                                </div>
                                               
                                                <div class="clearfix"></div>
                                                
                                            </div>
                                            
                                        
                                        
                                        </div>
                                        <div class="panel-footer">
                                                <div class="row">
                                                    <!-- <a href="#" class="btn btn-primary"> Update & Publish </a> -->
                                                    <button class="btn btn-primary" type="submit">Update & Publish</button>
                                                </div>
                                        </div>
                                </div>
                            </div>
                        </form>
                            
                        </div>
                                        
                                        </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
                        
        </div>
        <!-- END MAIN -->
        <div class="clearfix"></div>
        
    </div>




       
        
         <!-- Modal for add new banner -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Add New Home page Banner</h4>
      </div>
      <div class="modal-body">
          <div class="row">
           
                  <form  role="form"  method="post" class="col-md-12 form-panel" style="border:0" <?php  echo form_open_multipart(base_url().'Web_home_page/add_banner')?> 
            <div class="col-md-12">
                <fieldset class="form-group">
                    <label>Upload Image</label>
                    <input type="file" name="img" required>
                </fieldset>
                <div class="preview-images-zone">

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <label>Heading</label>
                <input class="form-control" name="title" value="" type="text" required>
                <br>
            </div>
            <div class="col-md-12">
                <label>Sub-Heading</label>
                <textarea class="form-control" name="sub_title" required></textarea>
                <br>
            </div>
            <div class="col-md-12">
                <label>Read More Link</label>
                <input class="form-control" value="" name="link" type="text">
                <br>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-info"> Submit </a> -->
                         <button class="btn btn-primary" type="submit">Add Now</button>
                    </div>
                </div>
           </div>
          </div>
      </form>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"</button>
      </div>
    </div>
  </div>

  <!--  Model -->

  <div class="modal fade" id="myModal23" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Add New Home page Banner</h4>
      </div>
      <div class="modal-body">
          <div class="row">
           
                  <form  role="form"  method="post" class="col-md-12 form-panel" style="border:0" <?php  echo form_open_multipart(base_url().'Web_home_page/add_banner')?> 
            <div class="col-md-12">
                <fieldset class="form-group">
                    <label>Upload Image</label>
                    <input type="file" name="img" required>
                </fieldset>
                <div class="preview-images-zone">

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <label>Heading</label>
                <input class="form-control" name="title" value="" type="text" required>
                <br>
            </div>
            <div class="col-md-12">
                <label>Sub-Heading</label>
                <textarea class="form-control" name="sub_title" required></textarea>
                <br>
            </div>
            <div class="col-md-12">
                <label>Read More Link</label>
                <input class="form-control" value="" name="link" type="text">
                <br>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-info"> Submit </a> -->
                         <button class="btn btn-primary" type="submit">Add Now</button>
                    </div>
                </div>
           </div>
          </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"</button>
      </div>
    </div>
  </div>

           <!-- Modal for add new why choose us -->

</div>
     <?php $this->load->view('admin/common/footer');?>
    <!-- END WRAPPER -->

    <!-- Javascript -->
    <script src="<?= base_url();?>assets/admin/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url();?>assets/admin/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>assets/admin/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url();?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>assets/admin/assets/scripts/klorofil-common.js"></script>
     <script src="<?= base_url(); ?>assets/admin/assets/scripts/ckeditor/ckeditor/ckeditor.js"></script>
        <script>
                $("#addsection").click(function(e){
                    e.preventDefault();
                    var lastindex = $('.banner-area.section-group:last').find('.title-index').html();
                    lastindex = parseInt(lastindex) + 1;
                    $('.banner-area.section-group:last').clone().insertAfter('.banner-area.section-group:last');
                    $('.banner-area.section-group:last').find('.title-index').html(lastindex);
               });
               
                $(".delselection").on('click',function(e){
                    e.preventDefault();
                    console.log("aagya");
                    $(this).parent().parent().remove();
               });
               
               $("#addwhychoose").click(function(e){
                    e.preventDefault();
                    var lastindex = $('.why-choose:last').find('.title-index').html();
                    lastindex = parseInt(lastindex) + 1;
                    $('.why-choose:last').clone().insertAfter('.why-choose:last');
                    $('.why-choose:last').find('.title-index').html(lastindex);
               });
               
                $(".delwhychoose").on('click',function(e){
                    e.preventDefault();
                    console.log("aagya");
                    $(this).parent().parent().remove();
               });
        </script>
<script type="text/javascript">
    $(document).ready(function (e) {

         
$("#what_we_doss").on('submit',(function(e) {
//e.preventDefault();

$.ajax({
url: "<?=base_url()?>Web_home_page/what_we_do", // Url to which the request is send
type: "POST",             // Type of request to be send, called as method
data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
contentType: false,       // The content type used when sending data to the server.
cache: false,             // To unable request pages to be cached
processData:false,       // To send DOMDocument or non processed data file it is set to false
success: function(data)   // A function to be called if request succeeds
{
alert(data);
//$('#msg').text(' Please verify your email address'); 
if(data == 1)
{
    
    responseText = '<span style="color:green;font-size: 16px;font-weight: normal;margin-left: 40px;">Content Updated</span>';
                    $("#what_message").html(responseText);
   // window.location='<?=base_url()?>profile-details'; 
}
 if(data == 2)
{
    responseText = '<span style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;">Please Try agin </span>';
  $("#what_message").html(responseText);
   
}
}
});
}))
});
</script>
<script>  // script for Delete Confirmation
        function doconfirm()
        {
            job=confirm("Are you sure to delete banner permanently?");
            if(job!=true)
            {
                return false;
            }
        }
</script>
<script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
           // CKEDITOR.replace('editor2');
           // CKEDITOR.replace('editor3');
        </script>
</body>

</html>
