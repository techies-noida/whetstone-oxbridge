<!doctype html>
<html lang="en">

<head>
	<title>Request a specific time | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Request a specific time </li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Request a specific time </h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Applicant Name</th>
												<th>Interviewer Name</th>
												<th>Interviewer Email</th>
												<th>Interviewer Contact</th>
												<th>Applointment Date</th>
												<th>Start Time</th>
												<th>End Time</th>
												<th>Requested Date</th> 
												<th>Requested Status</th>  
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($session_list as $row) {
												 $id=$row->interviewer_id;
                                                  $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id); 
                                                  $name=$result[0]->first_name;
                                                  ?>
											<tr>
												<td><?= $i++.'</td>
												<td>'.$row->first_name.'</td>
												<td>'.ucfirst($name).'</td>
												<td>'.$result[0]->email_address.'</td>
												<td>'.$result[0]->phone_number.'</td>
												<td>'.$row->schedule_date.'</td>
												<td>'.$row->start_time.'</td>
												<td>'.$row->end_time.'</td>
												<td>'.date('d-m-Y h:m:s', strtotime($row->request_date)).'</td>';

												?>
												<td>
													<?php if($row->request_status=='Waiting'){
														echo '<span style="margin-right: 5px;" class="label label-warning">Waiting</span><span id="'.$row->appointment_request_id.'" style="margin-right: 5px;" type="button" class="label label-success verify_btn"><i class="fa fa-check" aria-hidden="true"></i></span><span type="button" id="'.$row->appointment_request_id.'" class="label label-danger reject_btn"><i class="fa fa-ban" aria-hidden="true"></i></span>';
													}else if($row->request_status =='reject'){
													echo '<span class="label label-danger">Rejected</span>';	
													}else{
														echo '<span class="label label-success">Verified</span>';
													}
													?>
												</td>
												
											</tr>
											<?php } ?>
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>            
	
</body>
<script type="text/javascript">
	$(document).on('click', '.verify_btn', function(){		
		var id = $(this).attr("id");
		$.ajax({
			url: "<?php echo base_url() . 'Admin/Admin_panel/verify_req'?>",
			method:'POST',
			data: {id:id},  
			success: function(data) {
				// alert(data);
				var url      = window.location.href;
				$(location).attr('href', url);
				return;
			}
		});
	});
	$(document).on('click', '.reject_btn', function(){		
		var id = $(this).attr("id");
		$.ajax({
			url: "<?php echo base_url() . 'Admin/Admin_panel/reject_req'?>",
			method:'POST',
			data: {id:id},  
			success: function(data) {
				// alert(data);
				var url      = window.location.href;
				$(location).attr('href', url);
				return;
			}
		});
	});
</script>
</html>
