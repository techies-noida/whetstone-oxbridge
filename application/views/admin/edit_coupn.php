<!doctype html>
<html lang="en">

<head>
	<title>Coupons | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
<style type="text/css">
	.error{
		color:red;
	}
</style>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Admin</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Edit Coupons</h3>
								</div>
								<?php
			                      if($this->session->flashdata('success')) {
			                         $message = $this->session->flashdata('success');
			                         echo'
			                          <div class=" alert alert-success alert-dismissible" role="alert">
			                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			                              <i class="fa fa-check-circle"></i>'.$message['message']. 
			                          '</div>';
			                      }?> 
			                      <?php
			                      if($this->session->flashdata('error')) {
			                         $message = $this->session->flashdata('error');
			                         echo'
			                          <div class="alert alert-danger alert-dismissible" role="alert">
			                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			                              <i class="fa fa-check-circle"></i>'.$message['message']. 
			                          '</div>';
			                      }?> 
								<div class="panel-body no-padding">
									<form class="col-md-12 form-panel" style="border:0" method="POST" action="<?=base_url()?>admin/edit-coupn-action">
										<input type="hidden" name="id" value="<?= $coupn?$coupn[0]->serial_number:''?>">
										<div class="col-md-3">
	                                        <label>Name<span class="required">*</span></label>
	                                        <input class="form-control"  type="text" name="name" value="<?= $coupn?$coupn[0]->name:''?>">
	                                         <?php echo form_error('name', '<div class="error">', '</div>'); ?>
	                                        <br>
	                                    </div>
                                        <div class="col-md-3">
                                            <label>Description<span class="required">*</span></label>
                                            <input class="form-control"  type="text" name="description" value="<?= $coupn?$coupn[0]->description:''?>">
                                             <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Start Date<span class="required">*</span></label>
                                            <input class="form-control"  type="text" name="start_date" id="start_date" value="<?= $coupn?$coupn[0]->start_date:''?>" placeholder="yyyy-mm-dd">
                                             <?php echo form_error('start_date', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                         <div class="col-md-3">
                                            <label>End Date<span class="required">*</span></label>
                                            <input class="form-control"  type="text" name="end_date" id="end_date" value="<?= $coupn?$coupn[0]->end_date:''?>" placeholder="yyyy-mm-dd">
                                             <?php echo form_error('end_date', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Code<span class="required">*</span></label>
                                            <input class="form-control"  type="text" name="code" value="<?= $coupn?$coupn[0]->code:''?>">
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Limit<span class="required">*</span></label>
                                            <input class="form-control"  type="text" name="limit" value="<?= $coupn?$coupn[0]->coupn_limit:''?>">
                                             <?php echo form_error('limit', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Percentage<span class="required">*</span></label>
                                            <input class="form-control"  type="text"  name="percentage" value="<?= $coupn?$coupn[0]->percentage:''?>">
                                             <?php echo form_error('percentage', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-12 text-right">
                                        	<button type="submit" class="btn btn-new"> Submit </button>
                                        </div>
									</form>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>
           <script type="text/javascript">
           	$(function () {
		        $('#start_date').datetimepicker({format: 'YYYY-MM-DD'});
		        $('#end_date').datetimepicker({format: 'YYYY-MM-DD'});
		    });
           </script>            
	
</body>

</html>
