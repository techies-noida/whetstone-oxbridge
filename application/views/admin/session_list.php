<!doctype html>
<html lang="en">

<head>
	<title>Appointment List | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<div class="subheader">
				<ul>
					<li>Appointments </li>
				</ul>
			</div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Appointments</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
									<div class="table-responsive">
										<table class="table table-striped datatable">
											<thead>
												<tr>
													<th>S.N.</th>
													<th>Applicant Name</th>
													<th>Interviewer Name</th>
													<th>Applointment Date</th>
													<th>Start Time</th>
													<th>End Time</th>
													<th>Booking Date</th>  
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												<?php  $i=1; foreach ($session_list as $row) {
													$id=$row->interviewer_id;
													$result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id); 
													$name=$result[0]->first_name;
													?>
													<tr>
														<td><?= $i++.'</td>
														<td>'.$row->first_name.'</td>
														<td>'.ucfirst($name).'</td>
														<td>'.$row->booking_date.'</td>
														<td>'.$row->start_time.'</td>
														<td>'.$row->end_time.'</td>
														<td>'.date('d-m-Y h:m:s', strtotime($row->created_date)).'</td>';?>
														<td>
															<?php $time = date('Y-m-d H:i:s', time()); 
															$date = date('Y-m-d'); 
															$real_time = date('h:i A', strtotime($time));
															$start_time=$row->start_time;
															$end_time=$row->end_time;
															$booking_date=$row->booking_date;
															$schedule_time= date('h:i A',strtotime('-0 hour -5 minutes',strtotime($start_time)));
															$schedule_end= date('h:i A',strtotime('-0 hour -0 minutes',strtotime($end_time)));
                                          //echo '<pre>';print_r($result);echo '</pre>'; exit();
															if($row->call_status=='Active'){

																if($real_time >= $schedule_time && $real_time <= $schedule_end && $date==$booking_date) { 
																	echo '<span class="label label-success"> Active</span>';
																}/*else if( $date>=$booking_date &&  $real_time > $schedule_end && $real_time > $schedule_time )*/
																else if( $date>$booking_date || $date==$booking_date && $real_time >= $schedule_time && $real_time >= $schedule_end)
																{
																	echo '<span class="label label-danger"> Expired</span>';

																}

																else{
																	echo '<span class="label label-success"> Active</span>';

																}
															}
															else if($row->call_status=='Started'){
																echo '<span class="label label-success"> In progress</span>';
															}
															else if($row->call_status=='Completed'){
																$query = $this->db->select('review_id')->from('reviews')->where('appointment_id', $row->appointment_id)->where('user_type', 'student')->get()->result();
																$query1 = $this->db->select('review_id')->from('reviews')->where('appointment_id', $row->appointment_id)->where('user_type', 'interviewer')->get()->result();
																if (empty($query)) {
																	echo '<span class="label label-success"> Waiting for Applicant feedback</span>';
																} else if (empty($query1)) {
																	echo '<span class="label label-success"> Waiting for Interviewer feedback</span>';
																} else { ?>
																	<a href="#" id="<?php echo $row->appointment_id; ?>" class="completed_button"><span class="label label-success"> Completed</span></a>
																<?php }
															}
															else if($row->call_status=='Expire'){
																echo '<span class="label label-danger"> Expired</span>';
															}


															?> 
														</td>

													</tr>
												<?php } ?>

											</tbody>
										</table>
									</div>
								</div>
								<!-- <div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div> -->
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php $this->load->view('admin/common/footer');?>  
			<script type="text/javascript">
				$( ".completed_button" ).on( "click", function() {
					var id = $(this).attr('id');
					var base_url = '<?php echo base_url()?>';
					var url = base_url + 'admin/sesssion/review/' + id;
					$(location).attr('href', url);
				});
			</script>          

		</body>

		</html>
