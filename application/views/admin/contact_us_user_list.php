<!doctype html>
<html lang="en">

<head>
	<title>Contact Us Request | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
	<style type="text/css">
		.btn-verify {
    background-color: #2aaebf;
    border-color: #2aaebf;
}
	</style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Contact us request</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->

			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Contact us request</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												
												<th>Contact Person Name</th>
												<th>Contact Person Email Id</th>
												<th>Message</th>
                                                <th>Request Date</th>
												
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($request as $row) {?>
											
											<tr>
												<td><?= $i++;?> </td>
												
												<td><?= ucwords($row->user_name);?></td>
												<td><?= $row->email_address;?></td>
												<td><?= $row->comments;?></td>
                                                <td><?= $row->contact_date;?></td>
											</tr>
											<?php  } ?>
											
										</tbody>
									</table>
								</div>
								</div>
								<!-- <div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div> -->
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>   

                  
	
</body>

</html>
