<!doctype html>
<html lang="en">

<head>
	<title>Work for Us | Whetstone Oxbridge </title>
	<?php $this->load->view('admin/common/header_assets');?>
	<style type="text/css">
		.btn-verify {
    background-color: #2aaebf;
    border-color: #2aaebf;
}
	</style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Work for Us</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->

			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Work for Us</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<?php
                                        if($this->session->flashdata('success')) {
                                           $message = $this->session->flashdata('success');
                                           echo'
                                            <div class=" alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <i class="fa fa-check-circle"></i>'.ucfirst($message['message']). 
                                            '</div>';
                                        }?> 
                                        <?php
                                        if($this->session->flashdata('error')) {
                                           $message = $this->session->flashdata('error');
                                           echo'
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <i class="fa fa-check-circle"></i>'.ucfirst($message['message']). 
                                            '</div>';
                                        }?>  
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Request Id</th>
												<th>Name</th>
												<th>Email</th>
												<th>University</th>
												<th>College</th>
												<!-- <th>Subject</th>
                                                <th>Acadimic Intrest</th> -->
                                                <th>Request Date</th>
												<th>Status</th>
												<th>View</th>
                                                 <th>Action</th>
                                                <!-- <th>Edit</th>
                                                <th>Delete</th>  -->
												
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($request as $row) {?>
												
											
											<tr>
												<td><?= $i++;?> </td>
												<td><?= $row->request_id;?></td>
												<td><?= ucwords($row->name);?></td>
												<td><?= $row->email_address;?></td>
												<td><?= $row->university_name;?></td>
												<td><?= $row->college_name;?></td>
												<!-- <td><?= $row->subject;?></td>
                                                <td><?= $row->working_interest;?> </td> -->
                                                <td><?= date('d-m-Y h:m:s', strtotime($row->request_date));?></td>
												<td>
													<?php if($row->request_status=='waiting'){
														echo '<span class="label label-warning">Waiting</span>';
													}else if($row->request_status =='reject'){
													echo '<span class="label label-danger">Rejected</span>';	
													}else{
														echo '<span class="label label-success">Verified</span>';
													}
													?>
												</td>
												<td><a type="button" href="<?php echo base_url() . 'view-interviewer-request/' . $row->request_id?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a></td>
												<td>
													 <?php if($row->request_status =='waiting'){?>
												<div class="col-md-2">
														<button class="btn btn-verify dropdown-toggle" type="button" data-toggle="dropdown"><?php echo ucfirst($row->request_status);?>
														    <span class="caret"></span></button>
															<ul class="dropdown-menu">
																 <?php if($row->request_status =='waiting'){?>
														      		<li><a href="<?php echo base_url().'Admin/Admin_panel/verify_request/'.$row->request_id.'/verified';?>" onClick="return verify();">Verify</a></li>
														      		<li><a href="<?php echo base_url().'Admin/Admin_panel/verify_request/'.$row->request_id.'/reject';?>" onClick="return reject();">Reject</a></li>
														      		<?php }else{?>
														      		<li><a href="<?php echo base_url().'Admin/Admin_panel/verify_request/'.$row->request_id.'/reject';?>" onClick="return reject();">Reject</a></li>
														      	<?php  } ?>
														  </ul>
                                                    </div>
                                                <?php } ?>
												</td>
												<!-- <td><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button></td>
												<td><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
												<td><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td> -->
											</tr>
											<?php  } ?>
											
																						
											
										</tbody>
									</table>
								</div>
								</div>
								<!-- <div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div> -->
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>   

           <script type="text/javascript">
           	function verify()
		{
		    pen=confirm("Are you sure to Verify request ?");
		    if(pen!=true)
		    {
		        return false;
		    }
		}
	function reject()
		{
		    pen=confirm("Are you sure to reject request ?");
		    if(pen!=true)
		    {
		        return false;
		    }
		}
           </script>         
	
</body>

</html>
