<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Admin Login | Whetstone Oxbridge </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/main.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/demo.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url();?>assets/admin/assets/img/favicon.png">
	<link rel="shortcut icon" type="image/icon" href="https://whetstone-oxbridge.com/assets/images/favicon.ico"/>
</head>

<body style="background: #F4F5F7; background-image: url(<?= base_url();?>assets/admin/assets/img/quizbg.png); background-size: cover">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="col-lg-4 col-md-8 col-sm-10 col-xs-12" style="float: none; margin: auto">
					<div class="login-header">
						<div class="logo text-center" style="color:#fff">
							<img src="<?= base_url();?>assets/admin/assets/img/oxbridge_logo.png" height="52"/>
						</div>
                        <br>
                        <br>
					</div>
					<div class="login-box">
						<div class="content">

							<p class="lead text-center">Login to your account</p>
							<div><span id="msg" style="color:red; font-size: 18px;font-weight: normal"></span></div>
							<form class="form-auth-small" id="login" action="">
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input type="email" name="email" class="form-control" id="signin-email"  placeholder="Email">
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input type="password" name="password" class="form-control" id="signin-password"  placeholder="Password">
								</div>
								<div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox">
										<span>Remember me</span>
									</label>
								</div>
								<button type="submit" class="btn btn-new btn-lg btn-block">LOGIN</button>
								<div class="bottom text-center">
									<br>
									<span class="helper-text"> <a href="#">Forgot password?</a></span>
								</div>
							</form>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
                            
				<div class="col-sm-12">
                    <ul style="margin: 0; padding: 0; list-style: none">
						<li><p class="copyright text-center" style="text-align: center; margin-top: 30px; color:#666">© 2018 | &nbsp; <img src="<?= base_url();?>assets/admin/assets/img/oxbridge_logo.png" height="19"/></p></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
 <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
 <script>
 $(function myFunction(){
 		//alert('hiiii');
 	
    $('#login').validate({
        rules:{
            email:{
                required:true,
                email:true
            },
            password:{
                required:true
            }
        },
        messages:{
            email:{
                required:"  Please enter email id ",
                email:"Please enter valid email id "
            },
            password:{
                required: "Please enter password"
            }
        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>Admin/Login/userlogin', 
            $('#login').serialize(), 
            function(data){
           // alert (data); 
                if(data == 1)
                {
                    //alert('login sucessfull');
                    window.location='<?=base_url()?>admin-dashboard'; 
                }
                 if(data == 2)
                {
                	
                   $('#msg').text(' Please verify your email address'); 
                }
               if(data==0)
                {
                	//alert('login again ');
                   $('#msg').text('Invalid email id password '); 
                }
                

            });
       }
   });
});
 </script>

</body>

</html>
