<!doctype html>
<html lang="en">
<head>
	<title>Request an Interviewer | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Request an Interviewer</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Request an Interviewer</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<?php
                                        if($this->session->flashdata('success')) {
                                           $message = $this->session->flashdata('success');
                                           echo'
                                            <div class=" alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <i class="fa fa-check-circle"></i>'.ucfirst($message['message']). 
                                            '</div>';
                                        }?> 
                                        <?php
                                        if($this->session->flashdata('error')) {
                                           $message = $this->session->flashdata('error');
                                           echo'
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <i class="fa fa-check-circle"></i>'.ucfirst($message['message']). 
                                            '</div>';
                                        }?>  
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>University Name</th>
												<th>College Name</th>
												<th>Course Name</th>
												<th>Email</th>
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($interviewers as $row) { ?>
											
											<tr>
												<td><?= $i++.'</td>
												<td>'.$row->university_name.'</td>
												<td>'.$row->college_name.'</td>
												<td>'.$row->course_name.'</td>
												<td>'.$row->interviewer_email_address.'</td>';?>
                                                
                                                 
												
											
												<!-- <td><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button></td>
												<td><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
												<td><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td> -->
											</tr>
										<?php } ?>
											
																						
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>             
	
</body>

</html>
