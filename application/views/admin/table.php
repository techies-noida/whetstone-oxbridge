<!doctype html>
<html lang="en">

<head>
	<title>Dashboard | Gym Data Trak</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Title</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Members</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>Member Id</th>
												<th>Name</th>
												<th>Email</th>
												<th>Contact</th>
												<th>Plan</th>
												<th>Amount</th>
                                                <th>Join Date</th>
                                                <th>Expiry Date</th>
												<th>Status</th>
                                                                                                <th>View</th>
                                                                                                <th>Edit</th>
                                                                                                <th>Delete</th>
												
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><a href="#">GDTM10001</a></td>
												<td>Arvind</td>
												<td>arvind@coretechies.com</td>
												<td>8562846127</td>
												<td>Monthly</td>
												<td>5000 USD</td>
                                                <td>Oct 21, 2016</td>
                                                <td>Oct 21, 2016</td>
												<td><span class="label label-success">Active</span></td>
												<td><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button></td>
												<td><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
												<td><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>
											</tr>
											
																						
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>            
	
</body>

</html>
