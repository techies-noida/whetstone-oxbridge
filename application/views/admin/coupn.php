<!doctype html>
<html lang="en">

<head>
	<title>Coupons | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Admin</li>
                        </ul>
                    </div>
                    <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
                      <?php
                      if($this->session->flashdata('error')) {
                         $message = $this->session->flashdata('error');
                         echo'
                          <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Update Package Price</h3>
								</div>
								<div class="panel-body no-padding">
									<form id="update_package_price" style="">
										<div class="form-row align-items-center">
											<div class="col-md-4">
												<label>Enter Package Price</label>
												<input type="text" class="form-control mb-2" name="package_price" id="package_price" value="<?php echo $package_price; ?>">
												<span id="package_price_fb" style="color: red;"></span>
											</div>
											<div class="col-md-4" style="margin-top: 25px; padding-bottom: 31px;">
												<button type="submit" class="btn btn-primary mb-2">Update</button>
											</div>
										</div>
									</form>
								</div>
								<!-- <div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div> -->
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>


					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Coupons List</h3>
									<div class="right">
										<a href="<?=base_url()?>admin/add-coupn"><i class="lnr lnr-plus-circle"></i>Add Coupon</a>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Name</th>
												<th>Code</th>
												<th>Limit</th>
												<th>Percentage</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php 
											if(count($coupn_list)){
												$i = 1;
												foreach ($coupn_list as $key => $value) {
													echo '<tr><td>'.$i.'</td><td>'.$value->name.'</td>'
													.'<td>'.$value->code.'</td>'
													.'<td>'.$value->coupn_limit.'</td>'
													.'<td>'.$value->percentage.'</td>'
													.'<td>'.$value->start_date.'</td>'
													.'<td>'.$value->end_date.'</td>'
													.'<td><a href="'.base_url().'admin/edit-coupn?id='.$value->serial_number.'"><span class="label label-success">Edit</span>
													<a href="'.base_url().'delete/coupn?id='.$value->serial_number.'" onclick="return confirm_click();"><span class="label label-danger">Delete</span></a></td></tr>';
													$i++;
												}
											}
										?>	
										</tbody>
									</table>
								</div>
								</div>
								<!-- <div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div> -->
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>   
           <script type="text/javascript">
			function confirm_click()
			{
				return confirm("Are you sure want to delete?");
			}
			</script>  
			<script type="text/javascript">
			$(document).on('submit', '#update_package_price', function(event){
			event.preventDefault();
			var package_price  =  $('#package_price').val();
			var flag1 = false;

			if (flag1 == false) {
				if (package_price == '') {
					$('#package_price_fb').text("Please enter package price.");
					$('#package_price').focus();
				} else if (package_price < 1) {
					$('#package_price_fb').text("Please enter valid package price.");
					$('#package_price').focus();
				} else if(/^[0-9]*$/.test(package_price) == false){
					$('#package_price_fb').text("Please enter valid package price.");
					$('#package_price').focus();
			    } else{
					$('#package_price_fb').text("");
					flag1 = true;
				}
			}
			if ((flag1 == true)) {
				$.ajax({  
					url:"<?php echo base_url() . 'Admin/Admin_panel/update_price'?>", 
					method:'POST',  
					data:new FormData(this),  
					contentType:false,  
					processData:false,  
					success:function(data)  
					{
						if (data == 1) {
							alert('Package Price Update');
						} else {
							alert('Oops, Went Something Wrong!')
							var url      = window.location.href;
							$(location).attr('href', url);
							return;
						}
					}
				});
			}
		});
			</script>       
	
</body>

</html>
