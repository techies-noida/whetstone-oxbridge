<!doctype html>
<html lang="en">
<head>
	<title>Interviewers | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Title</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Interviewers</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<?php
                                        if($this->session->flashdata('success')) {
                                           $message = $this->session->flashdata('success');
                                           echo'
                                            <div class=" alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <i class="fa fa-check-circle"></i>'.ucfirst($message['message']). 
                                            '</div>';
                                        }?> 
                                        <?php
                                        if($this->session->flashdata('error')) {
                                           $message = $this->session->flashdata('error');
                                           echo'
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <i class="fa fa-check-circle"></i>'.ucfirst($message['message']). 
                                            '</div>';
                                        }?>  
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>User Id</th>
												<th>Name</th>
												<th>Email</th>
												<th>Contact</th>
												<th>University</th>
												<th>College</th>
												<!-- <th>Subject</th> -->
                                                <th>Join Date</th>
                                               <th> View Status</th>
												<th>Status</th>
												 
                                                <!-- <th>View</th>
                                                <th>Edit</th>
                                                <th>Delete</th> -->
												
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($interviewers as $row) { ?>
											
											<tr>
												<td><?= $i++.'</td>
												<td>'.$row->user_id.'</td>
												<td><a href="'.base_url().'admin/interviewer-dashboard/'.$row->user_id.'">'.$row->first_name.'<a></td>
												<td>'.$row->email_address.'</td>
												<td>'.$row->phone_number.'</td>
												<td>'.$row->university_name.'</td>
												<td>'.$row->college_name.'</td>
                                               
                                                <td>'.date('d-m-Y h:m:s', strtotime($row->created_date)).'</td>';?>

                                                <td>
												<div class="col-md-2">
														<button class="btn btn-verify dropdown-toggle" type="button" data-toggle="dropdown"><?php echo ucfirst($row->account_view_status);?>
														    <span class="caret"></span></button>
															<ul class="dropdown-menu">
																 <?php if($row->account_view_status =='Activate'){?>
														      		<li><a href="<?php echo base_url().'Admin/Admin_panel/change_account_view_status/'.$row->user_id.'/Deactivate';?>" onClick="return deactivate();">Deactivate</a></li>
														      		
														      		<?php }else{?>
														      		<li><a href="<?php echo base_url().'Admin/Admin_panel/change_account_view_status/'.$row->user_id.'/Activate';?>" onClick="return activate();">Activate</a></li>
														      	<?php  } ?>
														  </ul>
                                                    </div>
												
											</td>

                                                <?php 
                                                if($row->account_view_status=='Activate'){
														echo '<td><span class="label label-success">Activate</span></td>';
													}else {
													echo '<td><span class="label label-danger">Deactivate</span></td>';	
													}

                                                ?>
                                                
                                                 
												
											
												<!-- <td><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button></td>
												<td><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
												<td><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td> -->
											</tr>
										<?php } ?>
											
																						
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>  
            <script type="text/javascript">
           	function activate()
			{
			    pen=confirm("Are you sure to activate interviewer account for display on website ?");
			    if(pen!=true)
			    {
			        return false;
			    }
		    }
	function deactivate()
		{
		    pen=confirm("Are you sure to deactivate interviewer account for hide from website  ?");
		    if(pen!=true)
		    {
		        return false;
		    }
		}
           </script>            
	
</body>

</html>
