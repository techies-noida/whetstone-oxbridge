<!doctype html>
<html lang="en">
<head>
	<title>News Letter Subscriber | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
	<style type="text/css">
	.btn-verify {
		background-color: #2aaebf;
		border-color: #2aaebf;
	}
</style>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<div class="subheader">
				<ul>
					<li>News Letter Subscriber</li>
				</ul>
			</div>
			<!-- MAIN CONTENT -->

			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">

									<div class="row">
										<div class="right">
											<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
											<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
										</div>
									</div>
									<div class="row">
										<h3 class="panel-title">News Letter Subscriber</h3>
										<div class=" text-right"><a href="<?php echo base_url() . 'admin/send-newsletter-mail'?>" class="btn btn-info">Send mail to subscribe</a></div>
									</div>

								</div>
								<div class="panel-body no-padding">
									<div class="table-responsive">
										<table class="table table-striped datatable">
											<thead>
												<tr>
													<th>S.N.</th>
													<th>Subscriber Email Id</th>
													<th>Subscribe Date</th>
													<th>Email Verify Status</th>

												</tr>
											</thead>
											<tbody>
												<?php  $i=1; foreach ($request as $row) {?>
													<tr>
														<td><?= $i++;?> </td>
														<td><?= $row->email_address;?></td>
														<td><?= $row->subscribe_date;?></td>
														<td>
															<?php if($row->verify_status==0){
																echo '<span class="label label-warning">Waiting</span>';
															}else{
																echo '<span class="label label-success">Verified</span>';	
															}
															?>
														</td>
													</tr>
												<?php  } ?>



											</tbody>
										</table>
									</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<form method="post" action="<?php echo base_url(); ?>Admin/Admin_panel/export_action">
											<input type="submit" name="export" class="btn btn-success" value="Export" />
										</form>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php $this->load->view('admin/common/footer');?>   



		</body>

		</html>
