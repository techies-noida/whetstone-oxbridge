<!doctype html>
<html lang="en">

<head>
	<title> Interviewer Dashboard | Whetstone Oxbridge</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	
	<?php $this->load->view('admin/common/header_assets');?>
        <style>
            .profile h4{font-weight: 600; font-size: 16px; color:#000; border-bottom: 1px solid #eee; padding-bottom: 10px}
            .stars{color: #FFCC36}
            .action-btns .btn{padding: 2px 5px;}
            
            .menu .menu-category{color:#4cb050; text-transform: uppercase; font-weight: 600}
            .menu .menu-item {border-bottom: 1px solid rgba(76,176,80,0.1)}
            .menu .menu-item h4{color:#333; font-weight: 600}
            .menu .menu-item p{font-style: italic}
            
            .listname{list-style: none; padding: 0; margin: 0}
            .listname li{display: block; line-height: 24px; font-size: 13px; color:#333}
            .listname li i{color:#2aaebf;}
            
            .post-date{font-size: 12px}
            .feedback-title{background: #f2f6fa; padding: 8px 15px}
            
            .ques {border-bottom: 1px dotted #ccc; padding: 10px 0}
            .ques h4{font-size: 14px; font-weight: bold}
            .ques p{font-size: 14px; color:#555; font-style: italic}
            
            .other-details{width: 100%}
            .other-details tr td:last-of-type{text-align: right}
            #interview-happen{display: none}
        </style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Interviewer</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
                        <div class="panel-heading">
                  <h3 class="panel-title">Interviewer Dashboard </h3>
                  <div class="right">
                    <a href="<?=base_url()?>interviewers">All Interviewer List</a>
                  </div>
                </div>
						
						<div class="panel-body" style='padding-top: 20px'>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                             <?php $image= $profile[0]->profile_image; if($image=='')
                                                             echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="border-radius: 4px; height: 160px">';
                  
                                                                else
                                                                   echo '<img src="'.base_url().$image.'" alt="team member img" style="border-radius: 4px; height: 160px">';
                                                                 ?>
                                                             <div class="profile">
                                                                <h4><?php echo $profile[0]->first_name;?></h4>
                                                                <ul class="listname">
                                                                        <li><span><i class="fa fa-book" style='width:16px; margin-right: 5px'></i><?php echo $profile[0]->subject;?></span></li>
                                                                        <li><span><i class="fa fa-bank" style='width:16px; margin-right: 5px'></i><?php echo $profile[0]->university_name;?></span></li>
                                                                        <li><span><i class="fa fa-book" style='width:16px; margin-right: 5px'></i><?php echo $profile[0]->college_name;?></span></li>
                                                                        <li><span><i class="fa fa-envelope" style='width:16px; margin-right: 5px'></i><?php echo $profile[0]->email_address;?></span></li>
                                                                         <li><span><i class="fa fa-phone" style='width:16px; margin-right: 5px'></i><?php echo $profile[0]->course_year;?></span></li>
                                                                        <?php /*$country_id=$profile[0]->country_id;
                                                                        if($country_id>0){
                                                                           $result=$this->Common_model->getDataByID($table='country',$fname='country_id',$country_id);
                                                                            $country_name=$result[0]->country_name;
                                                                       echo '<li><span><i class="fa fa-map-marker" style="width:16px; margin-right: 5px"></i>&nbsp;'.$country_name.'</span></li>';
                                                                       }*/ ?>
                                                                </ul>  
                                                               <!--  <hr>
                                                                <table class="other-details">
                                                                    <tr>
                                                                        <td>Gender</td>
                                                                        <td><?php // echo $profile[0]->gender;?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>DOB</td>
                                                                        <td><?php echo $profile[0]->date_of_birth;?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Phone Number</td>
                                                                        <td><?php echo $profile[0]->phone_number;?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>About</td>
                                                                        <td><?php echo $profile[0]->about_us;?></td>
                                                                    </tr>
                                                                   
                                                                </table> -->
                                                                <hr>
                                                                <a href="<?php echo base_url().'admin/interviewer/edit-profile/'.$profile[0]->user_id;?>"><button class="btn btn-sm btn-info">Edit Profile</button></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                           
								<div class="table-responsive">
									<table class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Interviewer Name</th>
                                            <th> Applicant Name</th>
                                            
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th>Schedule Date</th>
                                            <th>Booking Date </th>
                                            <th>Status</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;
                                         foreach ($appointment as $row) {

                                        $id=$row->student_id;
                                        $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);
                                       
                                         $student_name=$result[0]->first_name;
                                            ?>
                                           
                                        <tr>
                                           <td><?= $i++.'</td>
                                            <td>'.$row->first_name.'</td>
                                            <td>'.$student_name.'</td>
                                            <td>'.$row->start_time.'</td>
                                            <td>'.$row->end_time.'</td>
                                            <td>'.$row->booking_date.'</td>
                                            <td>'.$row->created_date.'</td>';?>

                                             

                                            <!-- <td><button class="btn btn-xs btn-success">Done</button></td> -->

                                            <td>
                                             <?php $time = date('Y-m-d H:i:s', time()); 
                                               $date = date('Y-m-d'); 
                                            $real_time = date('h:i A', strtotime($time));
                                            $start_time=$row->start_time;
                                            $end_time=$row->end_time;
                                            $booking_date=$row->booking_date;
                                            $schedule_time= date('h:i A',strtotime('-0 hour -5 minutes',strtotime($start_time)));
                                            $schedule_end= date('h:i A',strtotime('-0 hour -0 minutes',strtotime($end_time)));
                                          //echo '<pre>';print_r($result);echo '</pre>'; exit();
                                            if($row->call_status=='Active'){

                                              if($real_time >= $schedule_time && $real_time <= $schedule_end && $date==$booking_date) { 
                                              echo '<a href="#" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Active</a>';
                                              }/*else if( $date>=$booking_date &&  $real_time > $schedule_end && $real_time > $schedule_time )*/
                                              else if( $date>$booking_date || $date==$booking_date && $real_time >= $schedule_time && $real_time >= $schedule_end)
                                              {
                                                 echo '<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-phone"></i> Expire</a>';

                                              }

                                              else{
                                                echo '<a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i> Active</a>';

                                              }
                                            }
                                             else if($row->call_status=='Started'){
                                              echo '<a href="#" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Started</a>';
                                            }
                                            else if($row->call_status=='Completed'){
                                              echo '<a href="#" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Completed</a>';
                                            }
                                            else if($row->call_status=='Expire'){
                                              echo '<a href="#" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Expire</a>';
                                            }


                                         ?> 
                                    </td>
                                          <?php if($row->call_status=='Completed') {?>

                                            <td><button value="<?php echo $row->appointment_id;?>" class="btn btn-xs btn-info feedback" id="view-happens"><i class="fa fa-eye"></i></button></td>
                                            <?php }?>
                                        </tr>
                                    <?php }?>
                                       
                                    </tbody>
									</table>
								</div>
	
                            </div>
                        </div>
						</div>
					</div>
					<hr>
					<!-- END OVERVIEW -->
					<div class="row" id="interview-happen">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel" style='margin-top: -48px'>
								<div class="panel-heading" style='padding-top: 0'>
									<h3 class="panel-title"></h3>
									
								</div>
								<div class="panel-body no-padding">
									<div class="tab_padding">
										<div id="content">
											<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
												<li class="active"><a href="#red" data-toggle="tab">Feedback</a>
												</li>
												<li><a href="#green" data-toggle="tab">Review</a>
												</li>
												<!-- <li><a href="#orange" data-toggle="tab">Video</a> -->
												</li>

											</ul>
										</div> 
										<div id="my-tab-content" class="tab-content">
                                            <div class="tab-pane active" id="red">
                                                <div class="row">
                                                    <div class='col-md-12'>                    
                                                        <div class="comment-container" style='margin-top: 0; padding-top: 0'>
                                                            <div class="comment-header">
                                                                <span>Feedback</span>
                                                            </div>
                                                            <div class="comment-body">
                                                                <ul>
                                                                    <li>
                                                                        <table>
                                                                            <tr>
                                                                                <!-- <td>
                                                                                    <img src="<?php echo base_url();?>assets/images/default.png" alt="user"/>
                                                                                </td> -->
                                                                                <td>
                                                                                    <div class="clearfix feedback-title">
                                                                                        <div class="pull-left" >
                                                                                            <a href="#" class="link user-name" style='font-size: 16px; font-weight: 600; color:#333'>  Applicant Name:  &nbsp;&nbsp;&nbsp;<span id="applicant_name"></span></a> 
                                                                                        </div>
                                                                                        <div class="pull-right">
                                                                                        <span class="post-date" id="feedback_date"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="comment-text">
                  
                                                                                        <div class="ques">
                                                                                            <h4>1. Questions and content covered in the interview.</h4>
                                                                                            <p id="question1"></p>
                                                                                        </div>
                                                                                        <div class="ques">
                                                                                            <h4>2. Area in which the applicant performed well (this can be both knowledge and interview techniques).</h4>
                                                                                            <p id="question2"></p>
                                                                                        </div>
                                                                                        <div class="ques">
                                                                                            <h4>3. Areas to improve upon: (Knowledge, including any recommended readings here)</h4>
                                                                                            <p id="question3"></p>
                                                                                        </div>
                                                                                        <div class="ques">
                                                                                            <h4>4. Areas to improve upon: (Interview techniques).</h4>
                                                                                            <p id="question4"></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                  
                                                                        </table>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="green">
                                              
                                                <div class="row">
                                                    
                                                    <div class='col-md-12'>
                                                        
                                                        <div class="comment-container" style='margin-top: 0; padding-top: 0'>
                                                            <div class="comment-header">
                                                                <span>Review</span>
                                                            </div>
                                                            <div class="comment-body">
                                                                <ul>
                                                                    <li>
                                                                        <table>
                                                                            <tr>
                                                                                <!-- <td>
                                                                                     <img src="assets/img/1.jpg" alt="user"/>  
                                                                                     <img src="<?php echo base_url();?>assets/images/default.png" alt="user" style="width: 50px;height: 28px;"/> 
                                                                                </td> -->
                                                                                <td>
                                                                                    <div class="clearfix">
                                                                                        <div class="pull-left">
                                                                                             <a href="#" class="link user-name" style='font-size: 16px; font-weight: 600; color:#333'> Applicant Name:  &nbsp;&nbsp;&nbsp;    <span id="applicant_name2"></span></a> 
                                                                                        </div>
                                                                                         <div class="pull-right">
                                                                                            <span class="post-date" id="review_date"></span>
                                                                                        </div> 
                                                                                        
                                                                                    </div>
                                                                                    <div class="comment-text">
                                                                                        <div class="stars">
                                                                                            <i class="fa fa-star"></i>
                                                                                            <i class="fa fa-star"></i>
                                                                                            <i class="fa fa-star"></i>
                                                                                            <i class="fa fa-star-o"></i>
                                                                                            <i class="fa fa-star-o"></i>
                                                                                        </div>
                                                                                      
                                                                                        <p id="review_comment"><!-- <i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</i> --></p>
                                                                                    
                                                                                       
                                                                                        
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                       
                                                                        </table>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                
                                                    </div>
                                                </div>
                                            
                                            </div>
                                            <div class="tab-pane" id="orange">
                                                <div class='row'>
                                                        <div class="col-md-3">
                                                            <video></video>
                                                        </div>
                                                </div>
                                               
                                                
                                            </div>
											</div>

										</div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			 <?php $this->load->view('admin/common/footer');?>   
                        
		<!-- </div>
		
		<div class="clearfix"></div>

		
	</div> -->
	
	<script>
            $(document).ready(function(){
                $('.datatable').DataTable();
            });
	</script>
	<script>
        $(document).ready(function(){

            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                    $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                var msg = "";
                if (ratingValue > 1) {
                    msg = "Thanks! You rated this " + ratingValue + " stars.";
                }
                else {
                    msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
                }
                responseMessage(msg);

            });


        });

	</script>
	<script>
        jQuery('.barChart').barChart({easing: 'easeOutQuart'});
	</script>
	</div>
</body><script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
<script>
$('#view-happen').on('click',function(){

    //var msg=$(this).attr("value");
    //alert(msg);
   $('#interview-happen').show(); 
});


$(".feedback").on("click",function(){
           var appointment_id=$(this).attr("value");
           $('#interview-happen').show();
            //alert(appointment_id);
           $.ajax({
            url: '<?php echo base_url();?>Admin/Interviewer_admin/interviewer_appointment_feedback/' + appointment_id,
            success: function(response)
            {
              //alert(response);
              var data = jQuery.parseJSON(response);
             //alert(data.review[0]['appointment_id']);
            review_comment=data.review[0]['comments'];
            // $('#review_comment').text(review_comment);
             
               $("#review_comment").append('<i>'+data.review[0]['comments']+'<i>');
              $('#applicant_name').text(data.review[0]['first_name']);
               $('#applicant_name2').text(data.review[0]['first_name']);

             $("#review_date").append('<b>Review on : </b>'+moment(data.review[0]['review_date']).format('YYYY-MM-DD'));
             $("#feedback_date").append('<b>Feedback on : </b>'+moment(data.feedback[0]['review_date']).format('YYYY-MM-DD'));

             
             $('#question1').text(data.feedback[0]['comments']);
             $('#question2').text(data.feedback[0]['comments2']);
             $('#question3').text(data.feedback[0]['comments3']);
             $('#question4').text(data.feedback[0]['comments4']);
             
            //alert(review_comment);
                //$('#msg_mail').text(response);
            }
            });

         
        });
</script>
</body>

</html>
