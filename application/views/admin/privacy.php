<?php
/**
 * Created by PhpStorm.
 * User: Pranav Mehra
 * Date: 27/11/2018
 * Time: 13:11
 */
?>
<!doctype html>
<html lang="en">
<head>
    <title>Privacy Policy | Whetstone Oxbridge</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/chartist/css/chartist-custom.css">
    <link rel="stylesheet"
          href="<?= base_url(); ?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/css/main.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/css/demo.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>assets/admin/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url(); ?>assets/admin/assets/img/favicon.png">
    <link rel="shortcut icon" type="image/icon" href="https://whetstone-oxbridge.com/assets/images/favicon.ico"/>

    <style>
        .section-group {
            background: #f2f6fa;
            position: relative;
            padding-top: 20px;
            margin-bottom: 44px
        }

        .section-group .section-group-title {
            background: #f2f6fa;
            position: absolute;
            content: "";
            top: -32px;
            border-radius: 4px 4px 0 0;
            padding: 6px 15px
        }

        .why-choose {
            background: #f2f6fa;
            position: relative;
            padding-top: 20px;
            margin-bottom: 44px
        }

        .why-choose .why-choose-title {
            background: #f2f6fa;
            position: absolute;
            content: "";
            top: -32px;
            border-radius: 4px 4px 0 0;
            padding: 6px 15px
        }

        .banner-cms {
            background: #fff;
            border: 1px solid #eee;
            position: relative;
            padding-top: 20px;
            margin-bottom: 44px
        }

        .banner-cms .banner-cms-title {
            cursor: pointer;
            right: 0;
            background: #f2f6fa;
            position: absolute;
            content: "";
            top: -34px;
            border-radius: 4px 4px 0 0;
            padding: 6px 15px;
            border: 1px solid #eee;
            border-bottom: 0
        }
    </style>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <?php $this->load->view('admin/common/navbar_sidebar'); ?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <div class="subheader">
            <ul>
                <li>CMS Privacy Policy Page</li>
            </ul>
        </div>
        <?php
        if ($this->session->flashdata('success')) {
            $message = $this->session->flashdata('success');
            echo '
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>' . $message['message'] .
                '</div>';
        } ?>
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- END OVERVIEW -->
                <div class="row">
                    <div class="col-md-12">
                        <form class="col-md-12 form-panel" style="border:0" method="post">
                            <div class="row">
                                <div class="panel new-panel">
                                    <div class="panel-heading">
                                        <label> Privacy Policy Content </label>
                                        <textarea name="editor1" id="editor1" rows="10" cols="80"><?php if ($privacy) {
                                                echo $privacy->body;
                                            } else {
                                                echo NULL;
                                            } ?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <input type="submit" class="btn btn-success" name="content_1">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/common/footer'); ?>
        <!-- END WRAPPER -->

        <!-- Javascript -->
        <script src="<?= base_url(); ?>assets/admin/assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/scripts/klorofil-common.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/scripts/ckeditor/ckeditor/ckeditor.js"></script>
        <script>
            $("#addsection").click(function (e) {
                e.preventDefault();
                var lastindex = $('.banner-area.section-group:last').find('.title-index').html();
                lastindex = parseInt(lastindex) + 1;
                $('.banner-area.section-group:last').clone().insertAfter('.banner-area.section-group:last');
                $('.banner-area.section-group:last').find('.title-index').html(lastindex);
            });

            $(".delselection").on('click', function (e) {
                e.preventDefault();
                console.log("aagya");
                $(this).parent().parent().remove();
            });

            $("#addwhychoose").click(function (e) {
                e.preventDefault();
                var lastindex = $('.why-choose:last').find('.title-index').html();
                lastindex = parseInt(lastindex) + 1;
                $('.why-choose:last').clone().insertAfter('.why-choose:last');
                $('.why-choose:last').find('.title-index').html(lastindex);
            });

            $(".delwhychoose").on('click', function (e) {
                e.preventDefault();
                console.log("aagya");
                $(this).parent().parent().remove();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function (e) {


                $("#what_we_doss").on('submit', (function (e) {
//e.preventDefault();

                    $.ajax({
                        url: "<?=base_url()?>Web_home_page/what_we_do", // Url to which the request is send
                        type: "POST",             // Type of request to be send, called as method
                        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData: false,       // To send DOMDocument or non processed data file it is set to false
                        success: function (data)   // A function to be called if request succeeds
                        {
                            alert(data);
//$('#msg').text(' Please verify your email address');
                            if (data == 1) {

                                responseText = '<span style="color:green;font-size: 16px;font-weight: normal;margin-left: 40px;">Content Updated</span>';
                                $("#what_message").html(responseText);
                                // window.location='<?=base_url()?>profile-details';
                            }
                            if (data == 2) {
                                responseText = '<span style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;">Please Try agin </span>';
                                $("#what_message").html(responseText);

                            }
                        }
                    });
                }))
            });
        </script>
        <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
        </script>
</body>

</html>