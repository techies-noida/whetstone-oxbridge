<?php
/**
 * Created by PhpStorm.
 * User: Pranav Mehra
 * Date: 27/11/2018
 * Time: 14:39
 */
?>
<!doctype html>
<html lang="en">
<head>
    <title>School | Whetstone Oxbridge</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/vendor/chartist/css/chartist-custom.css">
    <link rel="stylesheet"
          href="<?= base_url(); ?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/css/main.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/admin/assets/css/demo.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>assets/admin/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url(); ?>assets/admin/assets/img/favicon.png">
    <link rel="shortcut icon" type="image/icon" href="https://whetstone-oxbridge.com/assets/images/favicon.ico"/>

    <style>
        .section-group {
            background: #f2f6fa;
            position: relative;
            padding-top: 20px;
            margin-bottom: 44px
        }

        .section-group .section-group-title {
            background: #f2f6fa;
            position: absolute;
            content: "";
            top: -32px;
            border-radius: 4px 4px 0 0;
            padding: 6px 15px
        }

        .why-choose {
            background: #f2f6fa;
            position: relative;
            padding-top: 20px;
            margin-bottom: 44px
        }

        .why-choose .why-choose-title {
            background: #f2f6fa;
            position: absolute;
            content: "";
            top: -32px;
            border-radius: 4px 4px 0 0;
            padding: 6px 15px
        }

        .banner-cms {
            background: #fff;
            border: 1px solid #eee;
            position: relative;
            padding-top: 20px;
            margin-bottom: 44px
        }

        .banner-cms .banner-cms-title {
            cursor: pointer;
            right: 0;
            background: #f2f6fa;
            position: absolute;
            content: "";
            top: -34px;
            border-radius: 4px 4px 0 0;
            padding: 6px 15px;
            border: 1px solid #eee;
            border-bottom: 0
        }
    </style>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <?php $this->load->view('admin/common/navbar_sidebar'); ?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <div class="subheader">
            <ul>
                <li>CMS School Page</li>
            </ul>
        </div>
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <!-- END OVERVIEW -->
                <div class="row">
                    <div class="col-md-12">
                        <form class="col-md-12 form-panel" style="border:0" method="post" id="save_data_1">
                            <div class="row">
                                <div class="panel new-panel">
                                    <div class="panel-heading">
                                        <label>Content 1</label>
                                        <textarea name="content1" id="content1" rows="5" class="form-control ckeditor"><?php  echo $school[0]->body_1;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <label>Content 2</label>
                                        <textarea name="content2" id="content2" rows="5" class="form-control ckeditor"><?php  echo $school[0]->body_2;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <label>Image 1</label>
                                        <input type="file" name="image_1" class="form-control">
                                         <img style="width: 200px" src="<?php echo base_url() . $school[0]->image_1;?>" class="img img-responsive">
                                        <input type="hidden" name="hidden_image_1" class="form-control" value="<?php  echo $school[0]->image_1;?>">
                                    </div>
                                    <div class="panel-heading">
                                        <label>Content 3</label>
                                        <textarea name="content3" id="content3" rows="5" class="form-control ckeditor"><?php  echo $school[0]->body_3;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <label>Content 4</label>
                                        <textarea name="content4" id="content4" rows="5" class="form-control ckeditor"><?php  echo $school[0]->body_4;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <label>Image 2</label>
                                        <input type="file" name="image_2" class="form-control">
                                        <img style="width: 200px" src="<?php echo base_url() . $school[0]->image_2;?>" class="img img-responsive">
                                        <input type="hidden" name="hidden_image_2" class="form-control" value="<?php  echo $school[0]->image_2;?>">
                                    </div>
                                    <div class="panel-heading">
                                        <label>Footer Text</label>
                                        <textarea name="footer_text" id="footer_text" rows="5" class="form-control ckeditor"><?php  echo $school[0]->footer_text;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <input type="submit" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form class="col-md-12 form-panel" style="border:0" method="post" id="save_data_2">
                            <div class="row">
                                <div class="panel new-panel">
                                    <div class="panel-heading">
                                        <label>Heading</label>
                                        <input type="text" name="center_body" id="center_body" class="form-control" value="<?php  echo $school[0]->center_body;?>">
                                    </div>
                                    <div class="panel-heading">
                                        <label>Title</label>
                                        <textarea name="main_title" id="main_title" rows="2" class="form-control"><?php  echo $school[0]->main_title;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <label>Content</label>
                                        <textarea name="body_5" id="body_5" rows="5" class="form-control ckeditor"><?php  echo $school[0]->body_5;?></textarea>
                                    </div>
                                    <div class="panel-heading">
                                        <input type="submit" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/common/footer'); ?>
        <!-- END WRAPPER -->

        <!-- Javascript -->
        <script src="<?= base_url(); ?>assets/admin/assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/scripts/klorofil-common.js"></script>
        <script src="<?= base_url(); ?>assets/admin/assets/scripts/ckeditor/ckeditor/ckeditor.js"></script>

        <script type="text/javascript">
            $(document).on('submit', '#save_data_1', function(event){
            event.preventDefault();
            $.ajax({
                      type: "POST",
                      url: "<?php echo base_url() . 'Admin/Add_Content_Controller/save_school_data'?>",
                      data:new FormData(this),  
                      contentType:false,  
                      processData:false, 
                      success: function(data) {
                        // alert(data);
                        if (data == 1) {
                            alert('Content Updated Successfully')
                        } else {
                            alert('Went Something Wrong!')
                        }
                        var url      = window.location.href;
                            $(location).attr('href', url);
                            return;
                      }
            });
        });
            $(document).on('submit', '#save_data_2', function(event){
            event.preventDefault();
            $.ajax({
                      type: "POST",
                      url: "<?php echo base_url() . 'Admin/Add_Content_Controller/save_school_data2'?>",
                      data:new FormData(this),  
                      contentType:false,  
                      processData:false, 
                      success: function(data) {
                        // alert(data);
                        if (data == 1) {
                            alert('Content Updated Successfully')
                        } else {
                            alert('Went Something Wrong!')
                        }
                        var url      = window.location.href;
                            $(location).attr('href', url);
                            return;
                      }
            });
        });
        </script>
        
</body>

</html>