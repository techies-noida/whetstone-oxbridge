<!doctype html>
<html lang="en">
<head>
	<title>Interested School | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Interested School</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Interested School</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>User Name</th>
												<th>School Name</th>
												<th>Email</th>
												<th>Contact</th>
												<th>Intrested In I</th>
												<th>Intrested In II</th>
												<th>Message</th>
                                                <th>Join Date</th>
												
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($intrested_user as $row) {?>
												
											<tr>
												<td><?= $i++.'</td>
												<td>'.$row->user_name.'</td>
												<td>'.$row->school_name.'</td>
												<td>'.$row->email_address.'</td>
												<td>'.$row->phone_number.'</td>
												<td>'.$row->interview.'</td>
												<td>'.$row->road_show.'</td>
												<td>'.$row->message.'</td>
												
												';?>
												 <td><?php  echo $row->created_date;?></td>
												
                                               
                                              <!--   <td>Oct 21, 2016</td> -->
												<!-- <td><span class="label label-success">Active</span></td>
												<td><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button></td>
												<td><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
												<td><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td> -->
											</tr>
											<?php } ?>
																						
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>            
	
</body>

</html>
