<!doctype html>
<html lang="en">
<head>
	<title>Send mail | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<div class="subheader">
				<ul>
					<li>Send Newsletter Mail</li>
				</ul>
			</div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-body no-padding">
									<form class="col-md-12 form-panel"  style="border:0" id="mail_form">
										<div class="row">
											<div class="panel new-panel">
												<div class="panel-heading">
													<div class="col-md-12">
														<h3 class="panel-title">Send Newsletter Mail</h3>
													</div>
													<br>
												</div>
												<div class="panel-body no-padding">
													<div class="col-md-6">
														<label>Email Address List<span class="required">*</span></label>
														<select id="email_address" name="email_address[]" multiple class="form-control" >
															<?php foreach ($email_lists as $key => $value) { ?>
																<option value="<?php echo $value->email_address; ?>"><?php echo $value->email_address; ?></option>
															<?php } ?>
														</select>
														<div class="invalid-feedback" id="email_address_fb" style="color: red;"></div>
														<br>
													</div>
													<div class="col-md-12">
														<label>Subject<span class="required">*</span></label>
														<input class="form-control"  type="text" name="subject" id="subject">
														<div class="invalid-feedback" id="subject_fb" style="color: red;"></div>
														<br>
													</div>
													<div class="col-md-12">
														<label>Message<span class="required">*</span></label>
														<textarea class="form-control ckeditor" id="message" name="message"></textarea>
														<div class="invalid-feedback" id="message_fb" style="color: red;"></div>
														<br>
													</div>
													<div class="clearfix"></div>
													<hr>
												</div>
												<div class="panel-footer">
													<div class="row">
														<div class="col-md-12 text-right">
															<!-- <?php $data = array(
																'type'  => 'submit',
																'id'    => 'submit',
																'value' => 'Send',
																'class' => 'btn btn-info'
															);

															echo form_input($data); ?> -->
															<button type="submit" class="btn btn-info" value="Submit" id="submit"> Send </button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<div class="modal" tabindex="-1" id="success_model" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="background-color: #2aaebf; height: 50px;">
						</div>
						<div class="modal-body">
							<p style="margin-top: 15px;">Your mail send successfully</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view('admin/common/footer');?>     

			<script src="https://whetstone-oxbridge.com/assets/admin/assets/scripts/ckeditor/ckeditor/ckeditor.js"></script>       
			<script>
				$(document).ready(function(){
					$('#email_address').multiselect({
						nonSelectedText: 'Select Email Address',
						enableFiltering: true,
						enableCaseInsensitiveFiltering: true,
						buttonWidth:'400px'
					});

					$(document).on('submit', '#mail_form', function(event) {
						event.preventDefault();
						var email_address = $('#email_address').val();
						var subject = $('#subject').val();
						var message = CKEDITOR.instances['message'].getData();
						// alert(email_address);

						var flag1 = false;
						var flag2 = false;
						var flag3 = false;

						if (flag3 == false) {
							if (message == '') {
								$('#message_fb').text("Please enter message.");
								$('#message').focus();
							}else{
								$('#message_fb').text("");
								flag3 = true;
							}
						}
						if (flag2 == false) {
							if (subject == '') {
								$('#subject_fb').text("Please enter subject.");
								$('#subject').focus();
							}else{
								$('#subject_fb').text("");
								flag2 = true;
							}
						}
						if (flag1 == false) {
							if (email_address == null) {
								$('#email_address_fb').text("Please select email address.");
								$('#email_address').focus();
							}else{
								$('#email_address_fb').text("");
								flag1 = true;
							}
						}
						if ((flag1 == true) && (flag2 == true) && (flag3 == true)) {
							$('#submit').text("Processing...");
							$('#submit').prop('disabled',true);
							$.ajax({
								type: "GET",
								url: "<?php echo base_url() . 'Admin/Admin_panel/send_multiple_mail'?>",
								data: $('#mail_form').serialize(),  
								contentType:false,  
								processData:false, 
								success: function(data) {
									$('#email_address option:selected').each(function(){
										$(this).prop('selected', false);
									});
									$('#mail_form').trigger("reset");
									$('#email_address').multiselect('refresh');
									for (instance in CKEDITOR.instances){
										CKEDITOR.instances[instance].setData(" ");
									}
									if (data == 1) {
										$("#success_model").modal();
									}
									$('#submit').text("Send");
									$('#submit').prop('disabled',false);
								}
							});
						}

					});
 // $('#mail_form').on('submit', function(event){
 // 	event.preventDefault();
 // 	var form_data = $(this).serialize();
 // 	alert(form_data)
 // 	$.ajax({
 // 		url:"insert.php",
 // 		method:"POST",
 // 		data:form_data,
 // 		success:function(data)
 // 		{
 // 			$('#email_address option:selected').each(function(){
 // 				$(this).prop('selected', false);
 // 			});
 // 			$('#email_address').multiselect('refresh');
 // 			alert(data);
 // 		}
 // 	});
 // });
 
 
});
</script>
</body>

</html>
