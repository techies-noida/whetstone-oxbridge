<!doctype html>
<html lang="en">
<head>
	<title>Interviewers | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Session Details</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php $query = $this->db->select('first_name')->from('users')->where('user_id', $reviews_student_details[0]['student_id'])->get()->result_array(); echo $query[0]['first_name']; ?> (Applicant Review)</h3>
								</div>
								<div style="margin-left: 30px;">
									<?php if ($reviews_student_details[0]['comments']) { ?>
											<p><b>Review :- </b><?php echo $reviews_student_details[0]['comments']; ?></p>
									<?php } ?>
									<?php if ($reviews_student_details[0]['rating']) { ?>
											<p><b>Rating :- </b><?php echo $reviews_student_details[0]['rating'];?> Star</p>
									<?php } ?>
									<?php if ($download_video_url) { ?>
											<p><b>Video Link :- </b><a href="<?php echo base_url() . 'compose_video/composed/' . $download_video_url[0]['video_url'];?>" target="_blank">Click to see video</a></p>
									<?php } ?>
								</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php $query = $this->db->select('first_name')->from('users')->where('user_id', $reviews_student_details[0]['student_id'])->get()->result_array(); echo $query[0]['first_name']; ?> (Applicant Feedback)</h3>
								</div>
								<div style="margin-left: 30px;">
									<?php if ($reviews_student_details[0]['question1']) { ?>
									<p><b>Ques:- How useful was the interview practice?</b></p>
									<p><b>Ans:- </b><?php echo $reviews_student_details[0]['question1']; ?></p>
								    <?php } if ($reviews_student_details[0]['question2']) {  ?>
									<p><b>Ques:- How challenging were the questions?</b></p>
									<p><b>Ans:- </b><?php echo $reviews_student_details[0]['question2']; ?></p>
                                    <?php } if ($reviews_student_details[0]['question3']) {  ?>
									<p><b>Ques:- How useful was the face-to-face feedback given?</b></p>
									<p><b>Ans:- </b><?php echo $reviews_student_details[0]['question3']; ?></p>
								    <?php } ?>
								</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
						<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?php $query1 = $this->db->select('first_name')->from('users')->where('user_id', $reviews_student_details[0]['interviewer_id'])->get()->result_array(); echo $query1[0]['first_name']; ?> (Interviewer Feedback)</h3>
								</div>
								<div style="margin-left: 30px;">
									<?php if ($reviews_interviewer_details[0]['comments']) { ?>
									<p><b>Ques:- Questions and content covered in the interview.</b></p>
									<p><b>Ans:- </b><?php echo $reviews_interviewer_details[0]['comments']; ?></p>
								    <?php } if ($reviews_interviewer_details[0]['comments2']) {  ?>
									<p><b>Ques:- Area in which the applicant performed well (this can be both knowledge and interview techniques).</b></p>
									<p><b>Ans:- </b><?php echo $reviews_interviewer_details[0]['comments2']; ?></p>
                                    <?php } if ($reviews_interviewer_details[0]['comments3']) {  ?>
									<p><b>Ques:- Areas to improve upon: (Knowledge, including any recommended readings here).</b></p>
									<p><b>Ans:- </b><?php echo $reviews_interviewer_details[0]['comments3']; ?></p>
									<?php } if ($reviews_interviewer_details[0]['comments4']) {  ?>
									<p><b>Ques:- Areas to improve upon: (Interview techniques).</b></p>
									<p><b>Ans:- </b><?php echo $reviews_interviewer_details[0]['comments4']; ?></p>
								    <?php } ?>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>             
	
</body>

</html>
