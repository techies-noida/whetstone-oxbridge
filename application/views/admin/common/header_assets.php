<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/linearicons/style.css">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/vendor/chartist/css/chartist-custom.css">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/scripts/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/main.css">
<link rel="stylesheet" href="<?= base_url();?>assets/admin/assets/css/demo.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url();?>assets/admin/assets/img/apple-icon.png">
<!-- <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url();?>assets/admin/assets/img/favicon.png"> -->
<link rel="shortcut icon" type="image/icon" href="https://whetstone-oxbridge.com/assets/images/favicon.ico"/>
