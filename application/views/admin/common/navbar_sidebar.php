NAVBAR -->
<?php if (isset($this->session->userdata['admin_logged_in']))
  {
    $user_name=($this->session->userdata['admin_logged_in']['user_name']);
   $user_type=($this->session->userdata['admin_logged_in']['user_type']);
   /* $profile_image=($this->session->userdata['logged_in']['profile_image']);
   $lastlog=($this->session->userdata['logged_in']['last_login']);*/
  }?>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
                            <a href="<?=base_url();?>"> <img src="<?= base_url();?>assets/admin/assets/img/oxbridge_logo.png" height="25"/> </a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-menu"></i></button>
				</div>
                                <form class="navbar-form navbar-left">
                                    <ul>
                                        <!--<li>GENOVA PTE. LTD.</li>-->
                                        <li></li>
                                        <!-- <li class="hidden-xs">Last Login : <span>19/04/2018 05:05 PM</span></li> -->
                                    </ul>
				</form>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<!-- <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="lnr lnr-alarm"></i>
								<span class="badge bg-danger"> 0</span>
							</a> -->
							 <?php
                                         $query= $this->db->select('message')->from('notification')->where('notification_status',0)->get();

                                          $notifi=$query->result();
                                         // print_r($notifi); exit();
                                         $notification=count($notifi);
                                         if($notification>0){
							echo '<a href="#" class="dropdown-toggle icon-menu read_notification" data-toggle="dropdown">
								<i class="lnr lnr-alarm"></i>
								<span class="badge bg-danger" id="unread">'.$notification.'</span>
							</a>';}
							else{
								echo '<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="lnr lnr-alarm"></i>
								<span class="badge bg-danger" id="read"> </span>
							</a>';}

							
							?>
							 <ul class="dropdown-menu notifications">
							 	 <?php 
				                 $query= $this->db->select('notification_id,interviewer_id,applicant_id,message,n.created_date,first_name')->from('notification as n')->join('users as u','n.applicant_id=u.user_id')->order_by('notification_id','DESC')->limit(10)->get();
				                $messages=$query->result();
				                //echo '<pre>';print_r($messages); echo '</pre>';exit();
				               $total=count($messages);
				                $messages[0]->message;

				                for($i=0;$i<$total;$i++){
				               
								echo '<li><a href="#" class="notification-item"><span class="dot bg-success"></span><b>'.$messages[$i]->first_name.'</b>  '.$messages[$i]->message.'</a></li>'; 
							    }?>
							    <li style="text-align: center;"><a href="<?php echo base_url(). 'admin/all-notifications' ?>" class="notification-item">Get all notifications...</a></li>
							</ul> 
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle users" data-toggle="dropdown"><img src="<?= base_url();?>assets/admin/assets/img/user2.png" class="img-circle" alt="Avatar"> <span><?= $user_name;?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<!-- <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile <?= $user_type;?></span></a></li>
								<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Change Password</span></a></li>
								<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li> -->
								<li><a href="<?= base_url();?>Admin/Login/logout"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="<?= base_url();?>Admin/Login/logout" class=" icon-menu" >
								<i class="lnr lnr-power-switch"></i>
							</a>
							
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->



		<!-- LEFT SIDEBAR -->
		 <div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<?php if($title=='dashboard'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>admin-dashboard" class="<?= $status;?>"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						<?php if($title=='session_list'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>admin/appointments" class="<?= $status;?>"><i class="lnr lnr-cog"></i> <span>Session</span></a></li>

						<?php if($title=='request_session_list'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>admin/appointment-request" class="<?= $status;?>"><i class="lnr lnr-cog"></i> <span> Request a specific time</span></a></li>
						<?php if($title=='interviewer-contact-form'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>interviewer-contact-form" class="<?= $status;?>"><i class="lnr lnr-mustache"></i> <span>Request an Interviewer</span></a></li>
						<?php if($title=='Interviewer-request'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>interviewer-request" class="<?= $status;?>"><i class="lnr lnr-mustache"></i> <span>Work for Us</span></a></li>
						<?php if($title=='Interviewers'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>interviewers" class="<?= $status;?>"><i class="lnr lnr-pencil"></i> <span>Interviewer</span></a></li>
					
						<?php if($title=='Students'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>students" class="<?= $status;?>"><i class="lnr lnr-book"></i> <span>Applicants</span></a></li>

						<?php if($title=='reviews'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>admin/reviews" class="<?= $status;?>"><i class="lnr lnr-pencil"></i> <span>Reviews</span></a></li>

						 <?php if($title=='intrested_user'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/school-intrested-user" class="<?= $status;?>"><i class="lnr lnr-users"></i> <span>Interested School </span></a></li>
						
						<!-- <?php if($title=='session_list'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>admin/appointments" class="<?= $status;?>"><i class="lnr lnr-cog"></i> <span>Session</span></a></li> -->

						<li><a href="<?=base_url()?>admin/coupn" class=""><i class="lnr lnr-list"></i> <span>Price & Coupons</span></a></li>

						<?php if($title=='home-page'){$status='active';}else{$status='';}?>
						<li><a href="<?= base_url();?>admin/home-page" class="<?= $status;?>"><i class="lnr lnr-book"></i> <span>Home-page</span></a></li>

						 <?php if($title=='Contact-Us'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/contact-us" class="<?= $status;?>"><i class="lnr lnr-users"></i> <span>Contact Us </span></a></li>
						 <?php if($title=='Subscriber'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/news-letter-subscriber" class="<?= $status;?>"><i class="lnr lnr-users"></i> <span>Newsletter signup</span></a></li>

						<!-- <li><a href="" class=""><i class="lnr lnr-cog"></i> <span>Session</span></a></li> -->

						

                        <?php if($title=='about-us'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/about-us" class="<?= $status;?>"><i class="lnr lnr-book"></i> <span>About Us</span></a></li>

						 <?php if($title=='work-for-us'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/work-for-us" class="<?= $status;?>"><i class="lnr lnr-briefcase"></i> <span>Work For Us</span></a></li>

						<?php if($title=='school'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/school" class="<?= $status;?>"><i class="lnr lnr-graduation-hat"></i> <span>Schools</span></a></li>

                        <?php if($title=='privacy'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/privacy" class="<?= $status;?>"><i class="lnr lnr-lock"></i> <span>Privacy Policy</span></a></li>

                        <?php if($title=='terms'){$status='active';}else{$status='';}?>
						<li><a href="<?=base_url()?>admin/terms" class="<?= $status;?>"><i class="lnr lnr-book"></i> <span>Terms and Condition</span></a></li>

                        
						

                       
					</ul>
				</nav>
			</div>
                    <footer>
			<div class="container-fluid">
				
			</div>
                    </footer>
		</div> 
		<!-- END LEFT SIDEBAR