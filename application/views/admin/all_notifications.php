<!doctype html>
<html lang="en">
<head>
	<title>Interviewers | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Title</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Notifications</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Notification</th>
												<th>Date</th>												
											</tr>
										</thead>
										<tbody>
											<?php 
				                 $query= $this->db->select('notification_id,interviewer_id,applicant_id,message,n.created_date,first_name')->from('notification as n')->join('users as u','n.applicant_id=u.user_id')->order_by('notification_id','DESC')->get();
				                $messages=$query->result();
				                //echo '<pre>';print_r($messages); echo '</pre>';exit();
				               $total=count($messages);
				                $messages[0]->message;

				                for($i=0;$i<$total;$i++){
				                	$sl_no = $i + 1;
				               
								echo '<tr><td>'.$sl_no.'</td><td><b>'.$messages[$i]->first_name.'</b>  '.$messages[$i]->message.'</td><td>'.$messages[$i]->created_date.'</td></tr>'; 
							    }?>
										</tbody>	
									</table>
								</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>             
	
</body>

</html>
