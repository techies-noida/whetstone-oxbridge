<!doctype html>
<html lang="en">

<head>
	<title>Applicants | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Applicants</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Applicants</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>User Id</th>
												<th>Name</th>
												<th>Email</th>
												<th>Contact</th>
												<!-- <th>School Name</th>
												<th>Desire University</th>
												<th>Desire College</th>
												<th>Desire Course</th>  -->
                                               
                                               <!--  <th>Expiry Date</th> -->
                                                <th>Join Date</th>
												<th>Status</th>
												<th>View</th>
												
                                                                                                <!-- <th>View</th>
                                                                                                <th>Edit</th>
                                                                                                <th>Delete</th> -->
												
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($students as $row) {?>
												
											<tr>
												<td><?= $i++.'</td>
												<td>'.$row->user_id.'</td>
												<td><a href="'.base_url().'admin/student/dashboard/'.$row->user_id.'">'.$row->first_name.'</a></td>
												<td>'.$row->email_address.'</td>
												<td>'.$row->phone_number.'</td>
												
												';?>
												<!-- <td>'.$row->university_id.'</td>
												<td>'.$row->desire_university.'</td>
												<td>'.$row->desire_college.'</td>
												<td>'.$row->desire_course.'</td> -->
												 <td><?php  echo $row->created_date;?></td>
												<td>
													<?php if($row->status==1)
													echo'<span class="label label-success">Active</span>';
												   else
													echo '<span class="label label-warning">Waiting</span>';
												     ?>
												</td>
												 <td><a href="<?php echo base_url().'admin/student/dashboard/'.$row->user_id;?>"><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button>
												 </a></td>
                                               
                                              <!--   <td>Oct 21, 2016</td> -->
												<!-- <td><span class="label label-success">Active</span></td>
												<td><button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button></td>
												<td><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
												<td><button class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td> -->
											</tr>
											<?php } ?>
																						
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>            
	
</body>

</html>
