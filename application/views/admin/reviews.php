<!doctype html>
<html lang="en">
<head>
	<title>Interviewers | Whetstone Oxbridge</title>
	<?php $this->load->view('admin/common/header_assets');?>
</head>
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Reviews & Rating</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Reviews & Rating</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Interviewer Name</th>
												<th>Applicant Name</th>	
												<th>comments</th>
												<th>Rating</th>
												<th>Status</th>	
												<th>Action</th>								
											</tr>
										</thead>
										<tbody>
										<?php for ($i=0; $i < sizeof($review); $i++) { ?>
											<tr>
												<td><?php echo $i + 1; ?></td>
												<td><?php $query1 = $this->db->select('first_name')->from('users')->where('user_id', $review[$i]['interviewer_id'])->get()->result_array(); echo $query1[0]['first_name']; ?></td>
												<td><?php $query1 = $this->db->select('first_name')->from('users')->where('user_id', $review[$i]['student_id'])->get()->result_array(); echo $query1[0]['first_name']; ?></td>
												<td><?php echo $review[$i]['comments'];?></td>
												<td><?php echo $review[$i]['rating'];?></td>
												<td><?php if ($review[$i]['status'] == 1){ ?>
													<span class="label label-success">Activate</span>
												<?php } else if ($review[$i]['status'] == 0){ ?>
													<span class="label label-danger">Deactivate</span>
												<?php } ?>
												</td>
												<td><?php if ($review[$i]['status'] == 1){ ?>
													<button type="button" class="btn btn-danger btn-sm" id="block" value="<?php echo $review[$i]['review_id'];?>"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
												<?php } else if ($review[$i]['status'] == 0){ ?>
													<button type="button" class="btn btn-success btn-sm"  id="active" value="<?php echo $review[$i]['review_id'];?>"><i class="fa fa-thumbs-up" aria-hidden="true"></i></button>
												<?php } ?>
												</td>
											</tr>
										<?php } ?>
										</tbody>	
									</table>
								</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
           <?php $this->load->view('admin/common/footer');?>
           <script type="text/javascript">
           	$(document).on('click', '#block', function(){
           		var id = $(this).attr("value");
            		$.ajax({
           			url:"<?php echo base_url() . 'Admin/Admin_panel/review_active'?>", 
           			method:'POST',  
           			data:{id:id}, 
           			success:function(data)  
           			    {  
							if (data == 1) {
								var url      = window.location.href; 
								$(location).attr('href', url);
                             	return;
							} 
							alert('Oops.. Something Went wrong...'); 
						} 
					});
           	});
           	$(document).on('click', '#active', function(){
           		var id = $(this).attr("value");
           		$.ajax({
           			url:"<?php echo base_url() . 'Admin/Admin_panel/review_block'?>", 
           			method:'POST',  
           			data:{id:id}, 
           			success:function(data)  
           			    {  
							// alert(data);  
							if (data == 1) {
								var url      = window.location.href; 
								$(location).attr('href', url);
                             	return;
							} 
							alert('Oops.. Something Went wrong...'); 
						} 
					});
           	});
           </script>          
	
</body>

</html>
