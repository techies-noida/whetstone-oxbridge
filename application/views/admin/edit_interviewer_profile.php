<!doctype html>
<html lang="en">

<head>
	<title>Interviewer Profile Information | Whetstone Oxbridge</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	 <link  href="<?php echo base_url();?>assets/js/imageuploader/css/fineCrop.css" rel="stylesheet">
     <link  href="<?php echo base_url();?>assets/js/imageuploader/css/layout.css" rel="stylesheet">
  <?php $this->load->view('admin/common/header_assets');?>

        <style>
            .profile h4{font-weight: 600; font-size: 16px; color:#000; border-bottom: 1px solid #eee; padding-bottom: 10px}
            .stars{color: #FFCC36}
            .action-btns .btn{padding: 2px 5px;}
            
            .menu .menu-category{color:#4cb050; text-transform: uppercase; font-weight: 600}
            .menu .menu-item {border-bottom: 1px solid rgba(76,176,80,0.1)}
            .menu .menu-item h4{color:#333; font-weight: 600}
            .menu .menu-item p{font-style: italic}
            
            .listname{list-style: none; padding: 0; margin: 0}
            .listname li{display: block; line-height: 24px; font-size: 13px; color:#333}
            .listname li i{color:#2aaebf;}
            
            .post-date{font-size: 12px}
            .feedback-title{background: #f2f6fa; padding: 8px 15px}
            
            .ques {border-bottom: 1px dotted #ccc; padding: 10px 0}
            .ques h4{font-size: 14px; font-weight: bold}
            .ques p{font-size: 14px; color:#555; font-style: italic}
        </style>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Edit Interviewer Profile</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">

            <div class="panel-heading">
                  <h3 class="panel-title" >Interviewer Profile Information </h3>
                  <div class="right">
                    <a href="<?=base_url().'admin/interviewer-dashboard/'.$user[0]->user_id;?>"> Interviewer Dashbaord</a>
                  </div>
                </div>

             <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
                      <?php
                      if($this->session->flashdata('error')) {
                         $message = $this->session->flashdata('error');
                         echo'
                          <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
						
						<div class="panel-body" style='padding-top: 20px'>
                    <form  id="request_join_tutor" method="post"  onsubmit="return getpicture()" class="comments-form contact-form clearfix" role="form"  method='post'<?php echo form_open_multipart(base_url().'Admin/Interviewer_admin/update_profile')?>
                           <div class="col-md-12">

                           <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Personal Information</h4>
                        </div>
                           <div class="clearfix"></div>
                       <div class="col-md-5">
                    <div class="form-group">
                      <label style="color:#444; font-weight: normal">Name</label>
                      <input type="text" name="first_name" class="form-control" placeholder="" value="<?php echo $user[0]->first_name;?>">
                      <input type="hidden" name="user_id" class="form-control" placeholder="" value="<?php echo $user[0]->user_id;?>">
                    </div>
                     <!-- <div class="form-group">
                      <label style="color:#444; font-weight: normal">last Name</label>
                      <input type="text" name="last_name" class="form-control" placeholder="" value="<?php echo $user[0]->last_name;?>">
                    </div> -->
                     <div class="form-group">
                      <label style="color:#444; font-weight: normal"> University email address </label>
                      <input type="text" name="email_address" class="form-control" placeholder="" value="<?php  echo $user[0]->email_address;?>" readonly>
                    </div>
                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">Phone number</label>
                      <input type="text" name="phone_number" value="<?php echo $user[0]->phone_number;?>" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                      <label style="color:#444; font-weight: normal">Gender</label>
                      <select class="form-control" name="gender" style="border-radius: 0">

                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Transgender">Transgender</option>
                      </select>
                    </div>
                            <div class="form-group">
                      <label style="color:#444; font-weight: normal">DOB</label>
                       <input type="date" name="dob"  class="form-control" value="<?php echo $user[0]->date_of_birth;?>">
                    </div>

                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">Country</label>
                       <select class="form-control" name="country" style="border-radius: 0">
                        <option>Select</option>
                       <!--  <?php foreach($country as $row){
                         echo '<option value="'.$row->country_id.'">'.$row->country_name.'</option>';
                       } ?> -->
                       <?php foreach ($country as $row) {?>
                        <option <?php if($row->country_id == $user[0]->country_id){ echo 'selected="selected"'; } ?> value="<?php echo $row->country_id;?>"><?php echo $row->country_name;?></option><?php }?>

                      </select>
                    </div>
                     <div class="form-group">
                         <label style="color:#444; font-weight: normal">About us</label>
                         <textarea class="form-control" name="about_us" style="height: 60px"><?php  echo $user[0]->about_us;?></textarea>
                    </div>

                     <div class="col-lg-12" style="background: #f8f8f8; border:1px solid #f5f5f5; padding: 25px">
                               <div class="form-group">
                      <label style="color:#444; font-weight: normal">Your Display Picture</label>
                               </div>
                                <input type="file" id="upphoto" name="img" style="display:none;">
                                 <input type="hidden" id="upphoto1" name="picture" >
                                 <input type="hidden"  name="img1" value="<?php echo $user[0]->profile_image;?>" style="display:none;">
                                <label for="upphoto">
                                    <div class="inputLabel">
                                        click here to upload an image
                                    </div>
                                </label>

                            </div>

                             <div class="col-lg-12">
                               <!--  <img id="croppedImg" src="<?php echo base_url();?>assets/images/default.png"> -->
                                 <?php  $image=$user[0]->profile_image;
                                  if($image=='')
                             echo '<img id="croppedImg" src="'.base_url().'assets/images/default.png" alt="team member img">';

                              else
                          echo '<img id="croppedImg" src="'.base_url().$image.'" alt="team member img">';
                              ?>
                            </div>


                       </div>

                       <div class="col-md-offset-1 col-md-6">




                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">University</label>
                      <input type="text" name="university" class="form-control" placeholder="" value="<?php echo $user[0]->university_name;?>" readonly>
                    </div>

                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">College</label>
                      <input type="text" name="college" class="form-control" placeholder="" value="<?php echo $user[0]->college_name;?>" readonly>
                    </div>

                     <div class="form-group">
                         <label style="color:#444; font-weight: normal">Course</label>
                      <input type="text" name="subject" class="form-control" placeholder="" value="<?php   echo $user[0]->subject;?>">
                    </div>

                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">Current year of study</label>
                      <input type="text" name="study_year" class="form-control" placeholder="" value="<?php  echo $user[0]->course_year;?>">
                    </div>

                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">Previous interviewer exprience</label>
                      <input type="text" name="interview_exprience" class="form-control" placeholder="" value="<?php echo $user[0]->experience;?>">
                    </div>

                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">Main academic interests</label>
                         <textarea class="form-control" name="acadmic_intrests" style="height: 60px"><?php  echo $user[0]->academic_interest;?></textarea>
                    </div>
                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">What do you wish to told before your Oxbridge interview</label>
                         <textarea class="form-control" name="wish_to" style="height: 60px"><?php  echo $user[0]->wish_to_told_oxbridge;?></textarea>
                    </div>
                    <div class="form-group">
                         <label style="color:#444; font-weight: normal">Extra-curricular interests</label>
                         <textarea class="form-control" name="extra_curicular" style="height: 60px"> <?php  echo $user[0]->extra_curricular_interest;?></textarea>
                    </div>



                        <button class="comment-btn" type="submit">Update </button>
                       </div>
                  </form>
                 </div>
					</div>
					<hr>
					<!-- END OVERVIEW -->
					</div>
				</div>
			</div>


      <div class="cropHolder">
        <div id="cropWrapper">
            <img id="inputImage" src="images/face.jpg">
        </div>
        <div class="cropInputs">
            <div class="inputtools">
                <p>
                    <span>
                        <i class="fa fa-arrows-h"></i>
                    </span>
                    <span>horizontal movement</span>
                </p>
                <input type="range" class="cropRange" name="xmove" id="xmove" min="0" value="0">
            </div>
            <div class="inputtools">
                <p>
                    <span>
                        <i class="fa fa-arrows-v"></i>
                    </span>
                    <span>vertical movement</span>
                </p>
                <input type="range" class="cropRange" name="ymove" id="ymove" min="0" value="0">
            </div>
            <br>
            <button class="cropButtons" id="zplus">
                <i class="fa fa-search-plus"></i>
            </button>
            <button class="cropButtons" id="zminus">
                <i class="fa fa-search-minus"></i>
            </button>
            <br>
            <button id="cropSubmit">submit</button>
            <button id="closeCrop">Close</button>
        </div>
    </div>
			<!-- END MAIN CONTENT -->
       <?php $this->load->view('admin/common/footer');?>       
	<!-- 	</div> -->
		<!-- END MAIN -->
		<div class="clearfix"></div>


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <!-- Slick Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>
    <!-- mixit slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
   <!-- counter -->
    <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
    <!-- Wow animation -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script>
    <!-- progress bar   -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>
     <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/imageuploader/js/fineCrop.js"></script>
    
     <script>
        $("#upphoto").finecrop({
            viewHeight: 500,
            cropWidth: 200,
            cropHeight: 200,
            cropInput: 'inputImage',
            cropOutput: 'croppedImg',
            zoomValue: 50
        });

        function getpicture(){
          $("#upphoto1").val($("#croppedImg").attr("src"));
          return true;
        }
    </script>
	
	
	<script>
            $(document).ready(function(){
                $('.datatable').DataTable();
            });
	</script>
	<script>
        $(document).ready(function(){

            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                    $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                var msg = "";
                if (ratingValue > 1) {
                    msg = "Thanks! You rated this " + ratingValue + " stars.";
                }
                else {
                    msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
                }
                responseMessage(msg);

            });


        });

	</script>
	<script>
       
	</script>
	
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/imageuploader/js/fineCrop.js"></script>
    
     <script>
        $("#upphoto").finecrop({
            viewHeight: 500,
            cropWidth: 200,
            cropHeight: 200,
            cropInput: 'inputImage',
            cropOutput: 'croppedImg',
            zoomValue: 50
        });
    </script>
</body>

</html>
