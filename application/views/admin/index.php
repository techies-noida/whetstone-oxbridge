<!doctype html>
<html lang="en">

<head>
	<title> Admin Dashboard | Whetstone Oxbridge</title>
	<?php  $this->load->view('admin/common/header_assets');?>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php $this->load->view('admin/common/navbar_sidebar');?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li>Dashbaord</li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Dashboard</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-mustache"></i></span>
										<p>
											<span class="number"><?php echo $this->db->where('request_status', 'waiting')->count_all_results('tutor_join_request');?></span>
											<span class="title">New Request</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-pencil"></i></i></span>
										<p>

											<span class="number"><?php echo $this->db->where('user_role_id',2)->count_all_results('users');?></span>
											<span class="title">Interviewer</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-book"></i></span>
										<p>
											<span class="number"><?php echo $this->db->where('user_role_id',3)->count_all_results('users');?></span>
											<span class="title">Student</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span class="number"><?php echo $this->db->count_all_results('appointment');?></span>
											<span class="title">Session</span>
										</p>
									</div>
								</div>
								
								
								
							</div>
						</div>
					</div>
                                        <hr>
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Recent Request</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Request Id</th>
												<th>Name</th>
												<th>Email</th>
												<th>University</th>
												<th>College</th>
												<!-- <th>Subject</th>
                                                <th>Acadimic Intrest</th> -->
                                                <th>Request Date</th>
												<th>Status</th>
												
											</tr>
										</thead>
										<tbody>
											<?php  $i=1; foreach ($request as $row) {?>
												
											
											<tr>
												<td><?= $i++;?> </td>
												<td><a href="#"><?= $row->request_id;?></a></td>
												<td><?= ucwords($row->name);?></td>
												<td><?= $row->email_address;?></td>
												<td><?= $row->university_name;?></td>
												<td><?= $row->college_name;?></td>
												<!-- <td><?= $row->subject;?></td>
                                                <td><?= $row->working_interest;?> </td> -->
                                                <td><?= date('d-m-Y h:m:s', strtotime($row->request_date));?></td>
                                                <td>
													<?php if($row->request_status=='waiting'){
														echo '<span class="label label-warning">Waiting</span>';
													}else if($row->request_status =='reject'){
													echo '<span class="label label-danger">Rejected</span>';	
													}else{
														echo '<span class="label label-success">Verified</span>';
													}
													?>
												</td>
											</tr>
										<?php } ?>
											
											
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="#" class="btn btn-new">View All</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<?php $this->load->view('admin/common/footer');?>
                        
	
</body>

</html>
