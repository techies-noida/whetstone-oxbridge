<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Payment Fail | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets');?>
  <style>
  .btn-submit{background: #2aaebf !important; color:#fff; border-radius: 0; display: inline-block; margin-top: 20px; padding: 12px 25px}
  .table-highlight{border:0 !important}
  .table-highlight tr{border:0 !important; background: #f3fef6}
  .table-highlight tr td{border:0 !important; vertical-align: middle !important}
  .table-highlight tr th{border:0 !important; vertical-align: middle !important}
  .table-striped tr td{border:0 !important}
  .table-striped tr th{border:0 !important; font-weight: bold !important; color:#333 !important}
  .small-head{text-align: left; text-transform: uppercase; font-weight: 800; font-size: 14px; color:#23527c; margin-bottom: 20px}
</style>

</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
   <section id="our-team" style="background: #f6fef7; padding-top: 40px">
    <div class="container">
      <div class="row">
<!--        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title" style="font-size:24px">Booking Review</h2>
            <span class="line"></span>
          </div>
        </div>-->
           <div class="col-md-12">
          <div class="errror-page-area">
            <h1 class="error-title" style="   background: #CCCC00; border-color: #CCCC00;  font-size: 30px;  padding: 28px 32px;"><span class="fa fa-exclamation-triangle"></span></h1>
            <div class="error-content">
              <span style="color:#CCCC00">Payment Error</span>
              <p>Unreported payment error occurred.</p>
              <a href="<?php echo base_url();?>checkout" class="btn btn-submit">please try again</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php  $this->load->view('common/newsletter');?>
  <?php $this->load->view('common/footer');?>
<script type="text/javascript">
      history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});
 </script>
  </body>
  </html>