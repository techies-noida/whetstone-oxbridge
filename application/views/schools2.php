<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Schools</title>
    <?php $this->load->view('common/header_assets'); ?>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Schools</h2>
              <p>We are proud to be trusted by top performing schools not only in the UK, but across the world.</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Schools</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
  <!-- Start Feature -->
  <section id="service" style="background: #fff; padding: 60px 0">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Interview practice</h2>
            <span class="line"></span>
          </div>
        </div>
       <div class="col-md-12">
          <div class="service-content">
            <div class="row">
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">
                <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; margin-top: 20px;">
                  <i class="fa fa-check service-icon"></i>
                  <p>While many schools are very adept at supporting their students through Oxbridge applications, there is only a limited amount of practice that they can offer. We have found that, on average, applicants have only one practice interview, and this is usually with a teacher that they are already familiar with.</p>
                </div>
                <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
                  <i class="fa fa-check service-icon"></i>
                  <p>Whetstone offers schools interview practice packages where, depending on the number of applicants they have that year, they can purchase interview hours for their pupils. We provide schools with a series of codes which they can distribute to their applicants to enable them to find, book and practice their interview skills with a current Oxbridge student.</p>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
              	<img src="<?php echo base_url();?>assets/images/Schools page 1.png" class="img img-responsive">
              </div>
              <!-- End single service -->
              <div class="clearfix"></div>
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">
              	<img src="<?php echo base_url();?>assets/images/Schools page 2.png" class="img img-responsive">
              </div>
              <div class="col-md-6 col-sm-6">
                 <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; margin-top: 40px;">
                  <i class="fa fa-check service-icon"></i>
                  <p>At the end of the practice will we then send the feedback given to both the pupil and the school co-ordinator. This enables teacher and pupil to work on the comments made together. Teachers are an invaluable resource of experience and advice. Our interview practice tool therefore supports teachers in preparing their pupils for Oxbridge interview.</p>
                </div>
               
              </div>
              <!-- End single service -->
            </div>
            <div class="row">
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">
               
              </div>
              <!-- End single service -->
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">
               
              </div>
              <!-- End single service -->
            </div>
          </div>
        </div>
            <div class="col-md-12">
          <div class="title-area">
              <p>If you are interested in this service and would like to find out more then please <a href="#contact">fill in the form</a> below today and our <b>CEO Fenella Chesterfield</b> will be in touch to talk through a bespoke solution for your school.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Feature -->
  <section id="latest-news">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Whetstone Roadshow</h2>
            <span class="line"></span>
            <p>As part of our outreach program, if you are a UK based school you can register interest in booking a slot on our annual Roadshow.</p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="latest-news-content">
            <div class="row">
              <!-- start single latest news -->
           <!--   <div class="col-md-4" style='border-right: 1px solid #ccc'>
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-1.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">Whetstone Roadshow 2019</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|10 Jan 2019</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div> -->
              <!-- start single latest news -->
              <!-- start single latest news -->
              <div class="col-md-12">
                 <div class="contact-area-left">
                     
                     <p><i class="fa fa-check" style='color:#2aaebf; margin-right:8px'></i> Free of charge, we organise for one of our current student teams to come and offer a range of talks and workshops designed to support prospective applicants.</p>
                     <p><i class="fa fa-check" style='color:#2aaebf; margin-right:8px'></i>From breaking down the myths surrounding what makes an Oxbridge student, to detailed interview preparation workshops, we adapt our services to each individual school.</p>
                     <p><i class="fa fa-check" style='color:#2aaebf; margin-right:8px'></i>Register your interest by filling in the <a href="#contact">form</a> below</p>
                     <br>
                     <br>
                     <h4>Upcoming</h4>
                     <p>
                       <a href='#contact'><img src='<?php echo base_url();?>assets/images/Bridgeofsighs.jpg'/></a>
                     </p>
                   
                              
                 </div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="contact" style='padding-top: 40px'>
     <div class="container">
       <div class="row">
      <div class="title-area">
            <h2 class="title">Register Interest</h2>
            <span class="line"></span>
            
          </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               
               <div class="col-md-6" style='float: none; margin: auto'>
                 <div class="contact-area-right">
                   <form action="" class="comments-form contact-form">
                    <div class="form-group">                        
                      <input type="text" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">                        
                      <input type="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">                        
                      <input type="email" class="form-control" placeholder="Phone Number">
                    </div>
                   
                    <div class="form-group">                        
                      <textarea placeholder="Message" rows="2" class="form-control"></textarea>
                    </div>
                    <div class="form-group">                  
                        Interested in 
                      <input type="checkbox" style="height: auto"> Interview Practice
                      <input type="checkbox" style="height: auto"> Roadshow
                    </div>
                        <button class="comment-btn">Submit</button>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  
  <!-- Start subscribe us -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <?php $this->load->view('common/footer');?>
    
  </body>
</html>