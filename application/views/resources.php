<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Resourse| Personal-statement</title>
    <?php $this->load->view('common/header_assets');?>
    <style>
        .slick-prev:before, .slick-next:before{color:#888}
        .slick-dots li button:before{font-size: 36px}
        *{outline: none}
        *:focus{outline: none}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Resources</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Resources</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
   <!-- Start about  -->
  <section id="about">
    <div class="container" id="colsel">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Want to study by reading what current Oxbridge students are working on right now</h2>
            <span class="line"></span>
            <p>Current students have given you accessible reading suggestions to help you figure out if the course material is right for you</p>
          </div>
        </div>
       <div class="col-md-12">
          <div class="latest-news-content">
            <div class="row">
            <div class="courses">
              <!-- start single latest news -->
              <div class="col-md-4">
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-1.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">All about writing story</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div>
              <!-- start single latest news -->
              <div class="col-md-4">
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-2.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">Best Mobile Device</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div>
              <!-- start single latest news -->
              <div class="col-md-4">
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-3.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">Personal Note Details</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div>
              <!-- start single latest news -->
              <div class="col-md-4">
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-3.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">Personal Note Details</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end about -->
  <!-- Start Service -->
  <section id="service">
    <div class="container-fluid">
      
        <div class="row">
            <div class="side">
                <div class="side-content">
                    <div>
                        <h4 style="color:#fff; opacity: 0.5">Whetstone for schools</h4>
                        <h2><span style="color: #ffffff;">Donec convallis tellus id ornare</span></h2>
                    <a itemprop="url" href="#" target="_self" style="color: #ffffff" class="eltdf-btn eltdf-btn-medium eltdf-btn-simple eltdf-btn-arrow">
                        <span class="eltdf-btn-text">Read More</span>
                    </a>		
                    </div>
                </div>
                <div class="side-img" style="background-image: url(<?php echo base_url();?>assets/images/become-a-teacher-slider.jpg)">
                </div>
            </div>
      </div>
    </div>
  </section>
  <!-- End Service -->
  
  
   <!-- Start Pricing table -->
  <section id="pricing-table">
    <div class="container" id="persta">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Subject specific personal statement advice</h2>
            <span class="line"></span>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="pricing-table-content">
            <div class="row">
            <div class="subjects">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                  <div class="price-header">
                    <span class="price-title">Theology</span>
                    <div class="price"> <sup class="price-up"></sup>
                      15K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">
                  <div class="price-header">
                    <span class="price-title">English</span>
                    <div class="price"> <sup class="price-up"></sup>
                      24K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                  <div class="price-header">
                    <span class="price-title">Commerce</span>
                    <div class="price"> <sup class="price-up"></sup>
                      12K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="1.15s" data-wow-delay="1.15s">
                  <div class="price-header">
                    <span class="price-title">Science</span>
                    <div class="price"> <sup class="price-up"></sup>
                      10K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="1.15s" data-wow-delay="1.15s">
                  <div class="price-header">
                    <span class="price-title">Maths</span>
                    <div class="price">
                      <sup class="price-up"></sup>
                      10K
                      <span class="price-down">students</span>
                    </div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table -->  
  
   <!-- Start Pricing table -->
  <section id="pricing-table" style="background: #fff">
    <div class="container" id="enttes">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Entrance test advice</h2>
            <span class="line"></span>
            <p>Admissions test advice from those who recently sat the exams</p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="pricing-table-content">
            <div class="row">
            <div class="subjects">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                  <div class="price-header" style="background: #ec9417">
                    <span class="price-title">Theology</span>
                    <div class="price"> <sup class="price-up"></sup>
                      15K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">
                  <div class="price-header" style="background: #ec173a">
                    <span class="price-title">English</span>
                    <div class="price"> <sup class="price-up"></sup>
                      24K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                  <div class="price-header" style="background: #1799ec">
                    <span class="price-title">Commerce</span>
                    <div class="price"> <sup class="price-up"></sup>
                      12K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="1.15s" data-wow-delay="1.15s">
                  <div class="price-header" style="background: #37c591">
                    <span class="price-title">Science</span>
                    <div class="price"> <sup class="price-up"></sup>
                      10K
                      <span class="price-down">students</span></div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-table-price wow fadeInUp" data-wow-duration="1.15s" data-wow-delay="1.15s">
                  <div class="price-header">
                    <span class="price-title">Maths</span>
                    <div class="price">
                      <sup class="price-up"></sup>
                      10K
                      <span class="price-down">students</span>
                    </div>
                  </div>
                  <div class="price-article">
                    <ul>
                      <li style="font-size:13px">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</li>
                      
                    </ul>
                  </div>
                  <div class="price-footer">
                    <a class="purchase-btn" href="#">Show</a>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table -->  

  
  
  <!-- Start subscribe us -->
  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2 class="wow fadeInUp">Subscribe Newsletter</h2>
              <form action="" class="subscrib-form wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="footer-left">
              <p>&copy; Oxbridge 2018 | All Rights Reserved </p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="footer-right">
            <a href="index.html"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End footer -->

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <!-- Slick Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
    <!-- mixit slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
    <!-- Add fancyBox -->        
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
   <!-- counter -->
    <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
    <!-- Wow animation -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
    <!-- progress bar   -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  
    
   
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script>
        $('.courses').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 3,
	  slidesToScroll: 1,
          arrows:true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
        $('.subjects').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 1,
          arrows:true,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
    </script>
  </body>
</html>