<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Work-for-us</title>
    <?php $this->load->view('common/header_assets');?>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header" style="background:url(<?php echo base_url();?>assets/images/bg-counter.jpg); background-position:0 -400px">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Work For Us</h2>
              <p>From students for students, you know better than anyone what is going on behind the interview doors at the moment. </p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Work for us</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
 <section id="latest-news">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Work for us</h2>
            <span class="line"></span>
            <p>Interviewing for Whetstone provides a unique opportunity to earn money whilst dramatically improving Oxbridge access. Our low cost interview practice tool is designed to be available to applicants from varied sociological backgrounds. </p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="latest-news-content">
            <div class="row">
             
        
              <!-- start single latest news -->
              <div class="col-md-6" style='float:none; margin:auto'>
                 <div class="contact-area-left">
                     
                     <p><i class="fa fa-check" style='color:#2aaebf; margin-right:8px'></i>Training in how to conduct interviews online via the Whetstone platform. </p>
                     <p><i class="fa fa-check" style='color:#2aaebf; margin-right:8px'></i>Guidance in setting up your online profiles to start receiving clients. </p>
                     <p><i class="fa fa-check" style='color:#2aaebf; margin-right:8px'></i>Flexibility to work as much or as little as your time commitments allow. </p>
                     <br>
                     <br>
                    
                   
                              
                 </div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 
  <!-- end about -->
<section id="testimonial" style='background: #fff'>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >How it works</h2>
                <span class="line"></span>           
              </div>
            </div>
            <div class="col-md-12">
              <!-- Start testimonial slider -->
              <div class="testimonial-slider">
                <!-- Start single slider -->
                <div class="single-slider">
                  <div class="testimonial-img">
                    <img src="<?php echo base_url();?>assets/images/testi1.jpg" alt="testimonial image">
                  </div>
                  <div class="testimonial-content">
                    <p style='color:#1d1d1d'>majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                    <h6><span>1</span></h6>
                  </div>
                </div>
                <!-- Start single slider -->
                <div class="single-slider">
                  <div class="testimonial-img">
                    <img src="<?php echo base_url();?>assets/images/testi3.jpg" alt="testimonial image">
                  </div>
                  <div class="testimonial-content">
                    <p style='color:#1d1d1d'>majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                    <h6><span>2</span></h6>
                  </div>
                </div>
                <!-- Start single slider -->
                <div class="single-slider">
                  <div class="testimonial-img">
                    <img src="<?php echo base_url();?>assets/images/testi2.jpg" alt="testimonial image">
                  </div>
                  <div class="testimonial-content">
                    <p style='color:#1d1d1d'>majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                    <h6><span>3</span></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6"></div>        
      </div>
    </div>
  </section>
 
  <!-- End Service -->
  <!-- Start subscribe us -->
  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2 class="wow fadeInUp">Already applied, check your Application status here</h2>
                <a href='<?php echo base_url();?>check-request-status' class="btn btn-primary" style='background: #2baec0; border-radius: 0; padding: 18px 25px'>Check Status</a>
            </div>
              <br>
              <br>
            <div class="subscribe-area" style="margin-top:40px">
              <h2 class="wow fadeInUp">Become an Interviewer</h2>
              <div class="col-md-10" style="float: none; margin: 0px auto">
                 <div class="contact-area-right">

                   
                       
                   <form style="text-align:left !important" id="request_join_tutor" method="post" class="comments-form contact-form" role="form"  action="http://167.99.231.76/interviewer/add_join_request" enctype="multipart/form-data" accept-charset="utf-8" novalidate="novalidate">
                       <div class="col-md-6">
                           <div class="form-group">                        
                      <label style="color:#fff; font-weight: normal">Name</label>
                      <input name="name" class="form-control" placeholder="" type="text">
                    </div>
                    <div class="form-group">  
                         <label style="color:#fff; font-weight: normal">  University email address </label>
                      <input name="email_address" class="form-control" placeholder="" type="email">
                    </div>
                    <div class="form-group">  
                         <label style="color:#fff; font-weight: normal">University</label>
                      <!-- <input type="text" name="university" class="form-control" placeholder=""> -->
                      <select class="form-control" name="university" id="university" onchange="return get_college(this.value)" required="">
                        <option value="">Select University</option>
                                                <option value="1">Cambridge</option>                        <option value="2">Oxford</option>                      </select>
                    </div>
                    <div class="form-group">  
                         <label style="color:#fff; font-weight: normal">College</label>
                     <!--  <input type="text" name="college" class="form-control" placeholder=""> -->
                     <select class="form-control" name="college" id="college_id">
                          <option value="">Select University first </option>
                      </select>
                    </div>
                    <div class="form-group">  
                         <label style="color:#fff; font-weight: normal">Course</label>
                      <input name="subject" class="form-control" placeholder="" required="" type="text">
                    </div>
                     <div class="form-group">                        
                      <label style="color:#fff; font-weight: normal">Main academic interests</label>
                      <input name="working_as" class="form-control" placeholder="" required="" type="text">
                    </div>
                       </div>
                       
                        <div class="col-md-6">
                          
                    <div class="form-group">  
                         <label style="color:#fff; font-weight: normal">What do you wish to told before your Oxbridge interview</label>
                    <textarea class="form-control" required=""></textarea>
                    </div>
                    <div class="form-group">  
                         <label style="color:#fff; font-weight: normal">Extra curricular interest</label>
                         <textarea class="form-control" required=""></textarea>
                    </div>
                   
                       <!--   <button class="btn signin-btn" type="submit">Login</button> -->
                       </div>
                       <div class="clearfix"></div>
                       <div class="col-md-12">
                             <div class="form-group" style="color:#fff">                 
                          <label style="color:#fff; font-weight: normal"> Upload picture</label>
                      <input name="image" style="height: auto;" type="file">
                    </div>
                     <div class="form-group" style="color:#fff">                        
                         <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a style="color:#fff" target="_blank" href="<?php echo base_url();?>privacy-policy">GDPR Policy</a>
                    </div>
                        <button class="comment-btn" type="submit">Submit</button>
                           
                       </div>
                  </form>
                 </div>
                 </div>
                <!--<a href='<?php echo base_url();?>become-tutor' class="btn btn-primary" style='background: #2baec0; border-radius: 0; padding: 18px 25px'>Start Today</a>-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End subscribe us -->

  <!-- Start footer -->
 <?php $this->load->view('common/footer');?>
    
  </body>
</html>