<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <?php $this->load->view('common/header_assets');?>
   

    <!-- <link id="switcher" href="<?php echo base_url();?>assets/css/theme-color/default-theme.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    
   
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .bookings tr td{color:#666}
        table.dataTable thead .sorting{background: none}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
   <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
 
  <!-- end about -->
<section id="testimonial" style='background: #f6fef7'>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >Bookings</h2>
                <span class="line"></span>           
              </div>
            </div>
              <div class="clearfix"></div>
                  <div class="col-md-10" style="float: none; margin: auto">
                      
                      
                      <div style="margin-top: 40px">

 <ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#upcoming" aria-controls="upcoming" role="tab" data-toggle="tab">Upcoming</a></li>
    <li role="presentation" class=""><a href="#past" aria-controls="past" role="tab" data-toggle="tab">Past</a></li>
  
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      
      
      <div role="tabpanel" class="tab-pane active" id="upcoming">
           <div class="" style="padding-top: 40px; background: #fff">
                   <form action="" class="comments-form contact-form">
                       <div class=" col-md-12" style="float:none; margin: auto">
                       <div class="table-responsive">
                           <table id="bookings-1" class="display bookings" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($booking as $row) {?>
                                   
                                <tr>
                                    <td>
                                         <?php $image= $booking[0]->profile_image; if($image=='')
                   /* echo '<img src="'.base_url().'assets/images/main-testimonials-img-2.png" alt="team member img">';*/
                  echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="height: 28px;width: 5opx;">';
                  
                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="height: 28px;width: 5opx;">';
                     ?>
                                        <!-- <img src="<?php echo base_url();?>assets/images/team-member-2.png" height="36" class="img img-circle"/> -->
                                        <?php echo $booking[0]->first_name;?>
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> <?php echo $booking[0]->schedule_date;?>
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $booking[0]->start_time.' - '.$booking[0]->end_time;?>
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                            <?php }?>
                                <!-- <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>

                                 <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/images/team-member-3.png" height="36" class="img img-circle"/>
                                        Stuart Johnson
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> 8018-10-16
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> 10:00 AM
                                    </td>
                                    <td>
                                        <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button>
                                    </td>
                                </tr> -->
                            </tbody>
                           </table>
                       </div>
                       </div>
                      
                   
                  </form>
                 </div>
      </div> <!--student form ends-->
      <div role="tabpanel" class="tab-pane" id="past" style="color:red">NO DATA</div>

</div>
                      
                      
                
                 </div>
         
              </div>
          </div>
        </div>
             
      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
  <!-- End footer -->
     <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
     <script>
      
        $(document).ready(function() {
            $('#bookings-1').DataTable({
                searching:false,
                sorting:false
            });
        } );
    </script>
    
  </body>
</html>