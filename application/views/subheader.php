<div class="clearfix"></div>
  <section class="submenu">
  <div class="container">
  <div class="row">
       <div class="box-topsubmenu clearfix">
                         <?php $user_id=$this->session->userdata['logged_in']['user_id'];?>
                        <ul class="secondheader-menu pull-left">
                            <?php  if($title=='dashboard'){$status='active';}else{$status='';}?>
                            <li><a href="<?= base_url();?>profile-details" class="<?= $status;?>">Dashboard</a></li>
                            <?php  if($title=='interviewer-edit-profile'){$status='active';}else{$status='';}?>
                            <li><a href="<?= base_url().'interviewer/edit_profile/'.$user_id;?>" class="<?= $status;?>" >Profile</a></li>
                             <?php  if($title=='calendar'){$status='active';}else{$status='';}?>
                            <li><a href="<?= base_url();?>interviewer_schedule" class="<?= $status;?>" >Calendar</a></li>

                             <?php  if($title=='interviewer-booking'){$status='active';}else{$status='';}?>
                            <li><a href="<?= base_url();?>interviewer/bookings" class="<?= $status;?>">Appointments</a></li>

                            <?php  if($title=='interviewer-rating'){$status='active';}else{$status='';}?>
                             <li><a href="<?= base_url();?>interviewer/ratings" class="<?php echo $status;?>">Ratings</a></li>
                              <li><a href="#" class="" >Transactions</a></li>
                              <?php  if($title=='interviewer-change-password'){$status='active';}else{$status='';}?>
                            <li><a href="<?= base_url();?>change_password" class="<?php echo $status;?>" >Change Password</a></li>
                            <li><a href="#">Settings</a></li>
                        </ul>
                                <div class="pull-right" style="padding: 0">
                                    <ul class="notif-icons">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="items">0</span></a>
              <ul class="dropdown-menu dropdown-menu-right" role="menu" >
                 <li><a href="#" class="notif new">
                                                            <table>
                                                                <tbody><tr>
                                                                    <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td>
                                                                    <td>
                                                                        <h4><b>John Smith</b>expressed interest in you</h4>
                                                                        <p>respond back</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                            <span class="notif-time"><i class="fa fa-clock-o"></i> 2 Mins ago</span>
                                                        </a></li>                
                 <li><a href="#" class="notif">
                                                            <table>
                                                                <tbody><tr>
                                                                    <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td>
                                                                    <td>
                                                                        <h4><b>John Smith</b>expressed interest in you</h4>
                                                                        <p>respond back</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                            <span class="notif-time"><i class="fa fa-clock-o"></i> 2 Mins ago</span>
                                                        </a></li>                
                 <li><a href="#" class="notif">
                                                            <table>
                                                                <tbody><tr>
                                                                    <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td>
                                                                    <td>
                                                                        <h4><b>John Smith</b>expressed interest in you</h4>
                                                                        <p>respond back</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                            <span class="notif-time"><i class="fa fa-clock-o"></i> 2 Mins ago</span>
                                                        </a></li>                
                      
              </ul>
            </li>
                                          <!--  <li>
                                            <a role="button"><i class="fa fa-sign-out"></i></a>
                                           </li> -->
                                    </ul>
                                </div>    
                    </div>
                    </div>
                    </div>
  </section>  