<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Notifications | Whetstone Oxbridge </title>
  <?php $this->load->view('common/header_assets');?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
  <style>
  .sidebar-widget{float:none; display: block;}
  .team-member-name{float:none; display: block;}
  .single-team-member{float:none; display: block;}
  .bookings tr td{color:#666}
  .bookings tr th{color:#666}
</style>
</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
  $this->load->view('common/student_header');
  ?>
  <!-- END MENU -->

  <!-- end about -->


  <section id="testimonial" style='background: #f6fef7; padding-top: 0'>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding: 0">
          <div class="container">
            <div class="row">

              <br>
              <div class="col-md-12" style="padding: 0" >
                <div style="margin-top: 0px">
                  <!-- Tab panes -->
                  <div class="tab-content">


                    <div role="tabpanel" class="tab-pane active" id="student">


                      <div class="contact-area-right" style="margin-top: 0px">
                        <!-- left sidebar -->
                        <?php $this->load->view('common/student_sidebar');?>

                        <div class=" col-md-9 col-sm-6 col-xs-12">
                         <div class="content-wrapper">
                          <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Notifications</h4>
                          <div style="margin-top: 40px">
                           <div class="" style="padding-top: 00px; background: #fff">
                            <table id="bookings-1" class="display bookings" style="width:100%">
                              <thead>
                                <tr>
                                  <th>Sl. No.</th>
                                  <th>Notifications</th>
                                  <th>Date</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php for ($i=0; $i < sizeof($notifications); $i++) { ?>
                                <tr>
                                  <td><?php echo $i+1; ?></td>
                                  <td><?php echo $notifications[$i]['message'] . ' <a target="_blank" href=' . base_url() . 'interviewer/profile/' . $notifications[$i]['interviewer_id'] .'>' . $notifications[$i]['first_name'] . '</a>' ?></td>
                                  <td><?php echo $notifications[$i]['created_date'] ?></td>
                                </tr>
                              <?php } ?>
                              </tbody>
                            </table>

                          </div>
                        </div> <!--student form ends-->

                      </div>



                    </div>









                  </div>
                </form>
              </div>
            </div>
          </div> <!--student form ends-->
        </div>
      </div>
    </div>

  </div>
</div>
</div>

</div>
</div>
</section>
<!-- End Service -->
<!-- Start footer -->
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

     <script>

        $(document).ready(function() {
            $('#bookings-1').DataTable({
                searching:false,
                sorting:false
            });
            $('#bookings-2').DataTable({
                searching:false,
                sorting:false
            });
        } );
    </script>

</body>
</html>
