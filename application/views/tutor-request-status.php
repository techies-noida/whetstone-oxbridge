<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Interviewer Request Status | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
    <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Request Status</h2>
              <p>Check your request status here</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Request Status</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
 
  <!-- end about -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Requests</h2>
            <span class="line"></span>
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-5 col-sm-6 col-xs-12" style="float:none; margin:auto">
                <div class="single-team-member">
                 <div class="team-member-img">
                       
                    <!--  <img src="<?php //echo base_url().$status[0]->profile_image;?>" alt="Tutor profile-image"> -->
                     <?php  $image=$status[0]->profile_image;
                                  if($image=='')
                             echo '<img id="croppedImg" src="'.base_url().'assets/images/default.png" alt="team member img">';
                  
                              else
                          echo '<img id="croppedImg" src="'.base_url().$image.'" alt="team member img">';
                              ?> 
                 </div>
                 <div class="team-member-name">
                  <?php $email=$status[0]->email_address;?>
                   <p><?php echo $status[0]->name;?></p>
                   <span><?php echo $email;?></span>
                 </div>
                    <table class="table table-striped">
                        <tr>
                            <td>Request Id</td>
                            <th><?php echo $a= $status[0]->request_id;?></th>
                        </tr>
                        <tr>
                            <td>Request Status</td>
                           <!--  <td><span class="label label-warning"><?php echo $status[0]->request_status;?></span></td> -->
                           <?php $status=$status[0]->request_status;
                                 //$request_id=$status[0]->request_id;
                             if($status=='waiting')
                             {
                              echo '<td><span class="label label-warning">Waiting</span></td>';
                             }else{
                              /*echo '<td><span class="label label-warning">'.$a.'</span></td>'.$a;*/
                             // $email_address=$status[0]->email_address;
                             $result=$this->Interviewer_model->check_setup_account($email);
                              if($result)
                                echo '<td><span class="label label-success">Account Created</span></td>';
                              else
                              echo '<td><span class="label label-success">Verified</span> <br>
                                       <a href="'.base_url().'interviewer/setup_account?request_id='.$a.'"> <b>Click here setup your account </b></a>
                              </td>';

                             }?>
                            
                        </tr>
                    </table>
                
                </div>
              </div>
              <!-- Start single team member -->
         
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Start Service -->
  <!-- End Service -->
  <!-- Start subscribe us -->
  <!-- <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2 class="wow fadeInUp">Subscribe Newsletter</h2>
              <form action="" class="subscrib-form wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
  </body>
</html>