<style>
.box-topsubmenu{position: relative}
.submenu .secondheader-menu{float:left}
.submenu-toggle{display: none}
@media screen and (max-width:767px) {
  .submenu-toggle{background: transparent; border:1px solid #2aaebf; color:#2aaebf; border-radius: 0; margin: 10px 10px 10px 15px}
  .submenu-toggle:hover{background: #2aaebf; border:1px solid #2aaebf; color:#fff; border-radius: 0}
  .submenu-toggle:active{background: #2aaebf; border:1px solid #2aaebf; color:#fff; border-radius: 0}
  .submenu-toggle:focus{background: #2aaebf; border:1px solid #2aaebf; color:#fff; border-radius: 0}
  .submenu-toggle.hiddenn{display: none !important}
  .submenu-toggle.shownn{display: block !important}
  .submenu .secondheader-menu{float:none}
  .submenu .secondheader-menu.shownn{display: block}
  .submenu .secondheader-menu.hiddenn{display: none}
  .submenu .secondheader-menu li{display: block !important}
}
</style>
<div class="clearfix"></div>
<section class="submenu">
  <div class="container">
    <div class="row">
     <div class="box-topsubmenu clearfix">
      <button class="btn btn-sm submenu-toggle shownn">My Pages</button>
      <?php $user_id=$this->session->userdata['logged_in']['user_id'];?>
      <ul class="secondheader-menu hiddenn">
       <?php  if($title=='student-dashboard'){$status='active';}else{$status='';}?>
       <li><a href="<?= base_url().'profile-details';?>" class="<?= $status;?>">Dashboard</a></li>
       <?php  if($title=='edit-student-profile'){$status='active';}else{$status='';}?>
       <li><a href="<?= base_url().'student/edit_profile/'.$user_id;?>" class="<?= $status;?>" >Information</a></li>

       <?php  if($title=='student-booking'){$status='active';}else{$status='';}?> 
       <li><a href="<?= base_url().'student/bookings';?>" class="<?= $status;?>">Interviews</a></li>

       <?php  if($title=='student-rating'){$status='active';}else{$status='';}?>
       <li><a href="<?= base_url().'student/ratings';?>" class="<?= $status;?>">Feedback</a></li>


       <?php  if($title=='student-change-password'){$status='active';}else{$status='';}?>
       <li><a href="<?= base_url().'change_password';?>" class="<?= $status;?>" >Change Password</a></li>
       <li><a href="<?= base_url().'uploads/interview_advice.pdf';?>"  targer="" download > Download Interview Advice</a></li>

     </ul>
     <div class="pull-right" style="padding: 0">
      <ul class="notif-icons">
        <li class="dropdown">
          <?php
          $array = array('notify_applicant' =>'Yes','applicant_view_status' =>0, 'applicant_id' => $user_id);
                                        // $query= $this->db->select('message')->from('notification')->where('notify_applicant','Yes','applicant_view_status',0,'applicant_id',$user_id)->get();
          $query= $this->db->select('message')->from('notification')->where($array)->get();

          $notifi=$query->result();
                                          //print_r($notifi); //exit();
          $notification=count($notifi);
          if($notification>0){
            echo ' <a  href="" value="applicant" class="dropdown-toggle update_notification" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="items" id="unread">'.$notification.'</span></a>';
          }else{
            echo ' <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="" id="read"></span></a>';

          }
          ?>


          <?php 
          $query= $this->db->select('notification_id,interviewer_id,applicant_id,message,n.created_date,first_name')->from('notification as n')->join('users as u','n.interviewer_id=u.user_id')->where('notify_applicant','Yes')->where('applicant_id',$user_id)->order_by('notification_id','DESC')->limit(5)->get();
          $messages=$query->result();
                                                              //echo '<pre>';print_r($messages); echo '</pre>';exit();
          $total=count($messages);
                                                              // $messages[0]->message;
          if ($total > 0) {
           ?>
           <ul class="dropdown-menu dropdown-menu-right" role="menu" >
             <?php

             for($i=0;$i<$total;$i++){
                                                                //$time = date('Y-m-d H:i:s', time()); 
              $dates=$messages[$i]->created_date;
              $real_time = date('Y-m-d h:i A', strtotime($dates));
              ?>
              <li>
                <a href="#" class="notif new">
                  <table>
                    <tbody><tr>
                     <!--  <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td> -->
                     <td>
                      <h4><?= $messages[$i]->message;?><b style="margin-left: -10px;"><a target="_blank" href="<?php echo base_url() . 'interviewer/profile/' . $messages[$i]->interviewer_id; ?>"><?= $messages[$i]->first_name;?></a></b></h4>
                      <p>&nbsp; </p>
                    </td>
                  </tr>
                </tbody>
              </table>
              <span class="notif-time"><i class="fa fa-clock-o"></i>  <?= $real_time;?></span>
            </a>
          </li>  
        <?php } ?>  
        <li>
          <a href="<?php echo base_url(); ?>student-notifications" class="notif new">
            <table>
              <tbody><tr>
               <!--  <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td> -->
               <td>
                <h4 style="text-align: center; margin-left: 100px;">Get all notifications...</h4>
              </td>
            </tr>
          </tbody>
        </table>
      </a>
    </li>              
  </ul>
<?php } ?> 
</li>                                             
</ul>
</div>    
</div>
</div>
</div>
</section>  