 <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">

              <h2 class="wow fadeInUp">Whetstone Newsletter</h2>

              <p>Subscribe and keep up to date with the latest Oxbridge news and application advice.</p>
              <form action="" id="newsletter" class="subscrib-form wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <input type="email" name="newsletter_email" placeholder="Enter Your E-mail.." id="newsletter_email">
                <button class="subscribe-btn" class="ladda-button" type="submit">Submit</button>
              </form>
                <div id="news_message"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  