<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <div class="footer-left">
          <p>&copy; Whetstone Education Ltd 2018 | All Rights Reserved </p>
        </div>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="footer-right">
          <a href="privacy-policy" style="font-size: 14px">Privacy</a>
          <a href="terms-conditions" style="font-size: 14px">Terms & conditions</a>
          <a href="https://www.facebook.com/whetstone.oxbridge/"><i class="fa fa-facebook"></i></a>
          <a href="https://www.twitter.com/@WhetstoneOxbri1"><i class="fa fa-twitter"></i></a>
          <!--<a href="#"><i class="fa fa-google-plus"></i></a>-->
          <!--<a href="#"><i class="fa fa-linkedin"></i></a>-->
          <!--<a href="#"><i class="fa fa-pinterest"></i></a>-->
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- End footer -->

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  
<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-jvectormap-2.0.3/jquery-jvectormap.js"></script>-->  
<!--<script src="http://code.highcharts.com/mapdata/custom/world.js"></script>-->
<!-- Custom js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
<script>  
</script>

<!--  script for user sign up login  -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script> 

<script>
  $('.close').click(function(){
    $('#signup').trigger("reset");
    $("#message1").html("");
     $("#alreday_mail").html("");
     $('#login').trigger("reset");
     $("#msg").html("");
     $('#forgot_password').trigger("reset");
     $("#message2").html("");
     $("#email_not_exist").html("");
     $(".error").html("");
  });

  $(function myFunction(){
   //alert('hiiii');
   $('#login').validate({
    rules:{
      email:{
        required:true,
        email:true
      },
      password:{
        required:true
      }
    },
    messages:{
      email:{
        required:"  Please enter email id ",
        email:"Please enter valid email id "
      },
      password:{
        required: "Please enter password"
      }
    },
    unhighlight: function (element) {

      $(element).parent().removeClass('has_error')
    },
    submitHandler: function(form) {
            //form.submit();

            $.post('<?=base_url()?>loginSignup/userlogin', 
              $('#login').serialize(), 
              function(data){
           //alert (data); 
           if(data == 1)
           {
                    //alert('login sucessfull');
                    //window.location='<?=base_url()?>profile-details'; 
                    window.location.reload();
                  }
                  if(data == 2)
                  {


                   $('#msg').text(' Please verify your email address'); 
                 }
                 if(data==0)
                 {
                  //alert('login again ');
                  $('#msg').text('Invalid email address or password '); 
                }
                

              });
          }
        });
 });


  $(function myFunction(){

    $.validator.addMethod(
      "regex",
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
      "Please enter valid name"
      );

    $.validator.addMethod(
      "regexpass",
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
      "Password should contain at least one uppercase letter, one lowercase letter, one number and one special character"
      );

    $('#signup').validate({
      rules:{
        user_name:{
          required:true,
          minlength:3,
          maxlength:25,
          regex: "^[A-Za-z ]+$",

        },
        email_address:{
          required:true,
          email:true,
        },
        password:{
          required:true,
          minlength: 6,
          maxlength: 15,
          regexpass: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$",
        },
        confirm_password:{
          required:true,
          equalTo: "#password",
        }
      },
      messages:{
        user_name:{
          required:"  Please enter user name ",
          minlength:"Please enter name minimum 3 character",
          maxlength:"User name should not more than 25 character",

        },

        email_address:{
          required:"  Please enter email_address ",
          email:"Please enter valid email id ",
        },
        password:{
          required: "Please enter password",
          minlength: "Please eneter minimum 6 digits",
          maxlength:"Password should not more than 15 digits",


        },
        confirm_password:{
          required: "Please enter confirm password",
          equalTo:"Confirm password not match ",
        }
      },
      unhighlight: function (element) {

        $(element).parent().removeClass('has_error')
      },
      submitHandler: function(form) {
            //form.submit();
            $("#alreday_mail").html('');
            $(".loading").show();
            $.post('<?=base_url()?>loginSignup/student_signup', 
              $('#signup').serialize(), 
              function(data){
           //alert(data);
           if(data == 1)
           { 
             responseText = '<span style="color:green;font-size: 16px;font-weight: normal;">Thank you for signing up, please check your email to verify your account</span>';
             $("#message1").html(responseText);
             $(".loading").hide();
             $('#signup').trigger("reset");
           }
           if(data == 0)
           {
            responseText = '<span style="color:red;font-size: 16px;font-weight: normal;">An account with this email address is already registered.</span>';
                  //$('#message1').text('Your email Id already exist');
                  $("#alreday_mail").html(responseText);
                  $(".loading").hide();
                }
               /*if(data==0)
                {
                  //alert('login again ');
                   $('#msg').text('Invalid email id password '); 
                 }*/


               });
          }
        });
  });

  $(function myFunction(){

    $('#forgot_password').validate({
      rules:{
        email_address:{
          required:true,
          email:true,
        },
      },
      messages:{
        email_address:{
          required:"  Please enter email_address ",
          email:"Please enter valid email id ",
        },
      },
      unhighlight: function (element) {

        $(element).parent().removeClass('has_error')
      },
      submitHandler: function(form) {
            //form.submit();
            $(".loading").show();
            $.post('<?=base_url()?>loginSignup/forgot_password', 
              $('#forgot_password').serialize(), 
              function(data){
          // alert(data);
          if(data == 1)
          { 
           responseText = '<span style="color:green;font-size: 16px;font-weight: normal;">Your Password has been send on your email Id please check email </span>';
           $("#email_not_exist").html("");
           $("#message2").html(responseText);
           $(".loading").hide();
         }
         if(data == 0)
         {
          responseText = '<span style="color:red;font-size: 16px;font-weight: normal;">Your email id is not exist </span>';
                  //$('#message1').text('Your email Id already exist');
                  $("#message2").html("");
                  $("#email_not_exist").html(responseText);
                  $(".loading").hide();
                }
               /*if(data==0)
                {
                  //alert('login again ');
                   $('#msg').text('Invalid email id password '); 
                 }*/


               });
          }
        });
  });
</script>
<script type="text/javascript">
  $(function myFunction(){

    $('#newsletter').validate({
      rules:{
        newsletter_email:{
          required:true,
          email:true,
        },    
      },
      messages:{

        newsletter_email:{
          required:"  Please enter email address ",
          email:"Please enter valid email id ",
        },    
      },
      unhighlight: function (element) {

        $(element).parent().removeClass('has_error')
      },
      submitHandler: function(form) {
            //form.submit();
            $.post('<?=base_url()?>oxbridge/newsletter', 
              $('#newsletter').serialize(), 
              function(data){
           //alert(data);
           if(data == 1)
           { 
             responseText = '<span style="color:#2aaebf;font-size: 16px;font-weight: normal;margin-left: 40px;">Thank you for subscribtion. please verify your email and keep up to date with the <br> latest Oxbridge news and application advice.</span>';
             $("#news_message").html(responseText);
             $("#newsletter_email").val('');


           }
           if(data == 0)
           {
            responseText = '<span style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;">Your have already subscribed user</span>';
            $("#news_message").html(responseText);
                 /* setInterval(function (){
                      $("#news_message").hide();
                      $("#newsletter")[0].reset();
                      
                    }, 5000);*/
                  }

                  setInterval(function (){
                    $("#news_message").html('');
                      //$("#newsletter")[0].reset();
                      
                    }, 3000);
                });
          }
        });
  });
</script>

<script>
  $(document).ready(function(){

    $('.submenu-toggle').on('click',function(){
      console.log('submenu clicked');
      if($('.secondheader-menu').hasClass('hiddenn')){
       $('.secondheader-menu').removeClass('hiddenn');
       $('.secondheader-menu').addClass('shownn');
       $(this).html("<i class='fa fa-times'></i>");
     } 
     else if($('.secondheader-menu').hasClass('shownn')){
       $('.secondheader-menu').removeClass('shownn');
       $('.secondheader-menu').addClass('hiddenn');
       $(this).html("My Pages");
     } 
   });
    
  });
</script>
<!--  script for update notofication  -->
<script>
 $('.update_notification').on('click',function(){
  var msg=$(this).attr("value");
    //alert(msg);
    // var msg='update';
    $.getJSON('<?php echo base_url(); ?>Appointment/update_notification?message=' + msg ,
      function (data){
      });

    setTimeout(function(){
     $('#unread').hide();
     $('#read').show();
   }, 100);
  });

</script> 