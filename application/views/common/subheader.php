<style>
.box-topsubmenu{position: relative}
.submenu .secondheader-menu{float:left}
.submenu-toggle{display: none}
@media screen and (max-width:767px) {
  .submenu-toggle{background: transparent; border:1px solid #2aaebf; color:#2aaebf; border-radius: 0; margin: 10px 10px 10px 15px}
  .submenu-toggle:hover{background: #2aaebf; border:1px solid #2aaebf; color:#fff; border-radius: 0}
  .submenu-toggle:active{background: #2aaebf; border:1px solid #2aaebf; color:#fff; border-radius: 0}
  .submenu-toggle:focus{background: #2aaebf; border:1px solid #2aaebf; color:#fff; border-radius: 0}
  .submenu-toggle.hiddenn{display: none !important}
  .submenu-toggle.shownn{display: block !important}
  .submenu .secondheader-menu{float:none}
  .submenu .secondheader-menu.shownn{display: block}
  .submenu .secondheader-menu.hiddenn{display: none}
  .submenu .secondheader-menu li{display: block !important}
}
</style>


<div class="clearfix"></div>
<section class="submenu">
  <div class="container">
    <div class="row">
     <div class="box-topsubmenu clearfix">
      <button class="btn btn-sm submenu-toggle shownn">My Pages</button>
      <?php $user_id=$this->session->userdata['logged_in']['user_id'];?>
      <ul class="secondheader-menu hiddenn">
        <?php  if($title=='dashboard'){$status='active';}else{$status='';}?>
        <li><a href="<?= base_url();?>profile-details" class="<?= $status;?>">Dashboard</a></li>
        <?php  if($title=='interviewer-edit-profile'){$status='active';}else{$status='';}?>
        <li><a href="<?= base_url().'interviewer/edit_profile/'.$user_id;?>" class="<?= $status;?>" >Profile</a></li>
        <?php  if($title=='calendar'){$status='active';}else{$status='';}?>
        <li><a href="<?= base_url();?>interviewer_schedule" class="<?= $status;?>" >Calendar</a></li>

        <?php  if($title=='interviewer-booking'){$status='active';}else{$status='';}?>
        <li><a href="<?= base_url();?>interviewer/bookings" class="<?= $status;?>">Bookings</a></li>

        <?php  if($title=='interviewer-rating'){$status='active';}else{$status='';}?>
        <li><a href="<?= base_url();?>interviewer/ratings" class="<?php echo $status;?>">Reviews</a></li>
        <?php  if($title=='interviewer-change-password'){$status='active';}else{$status='';}?>
        <li><a href="<?= base_url();?>change_password" class="<?php echo $status;?>" >Change Password</a></li>
        <!-- <li><a href="#">Settings</a></li> -->
      </ul>
      <div class="pull-right" style="padding: 0">
        <ul class="notif-icons">
          <li class="dropdown">
            <?php
            $query= $this->db->select('message')->from('notification')->where('interviewer_view_status',0,'interviewer_id',$user_id)->get();

            $notifi=$query->result();
                                         // print_r($notifi); exit();
            $notification=count($notifi);
            if($notification>0){
              echo ' <a href="#" value="interviewer" class="dropdown-toggle update_notification" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="items" id="unread">'.$notification.'</span></a>';
            }else{
              echo ' <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="" id="read"></span></a>';

            }
            ?>
            
            
            <?php 
            $query= $this->db->select('notification_id,interviewer_id,applicant_id,message,n.created_date,first_name')->from('notification as n')->join('users as u','n.applicant_id=u.user_id')->where('interviewer_id',$user_id)->order_by('notification_id','DESC')->limit(5)->get();
            $messages=$query->result();
                //echo '<pre>';print_r($messages); echo '</pre>';exit();
            $total=count($messages);
                // $messages[0]->message;
            if ($total > 0) {
             ?>
             <ul class="dropdown-menu dropdown-menu-right" role="menu" >
              <?php 

              for($i=0;$i<$total;$i++){
                $dates=$messages[$i]->created_date;
                $real_time = date('Y-m-d h:i A', strtotime($dates));
                ?>
                <li>
                  <a href="#" class="notif new">
                    <table>
                      <tbody><tr>
                        <!-- <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td> -->
                        <td>
                          <h4><?= $messages[$i]->message;?> <b><?= $messages[$i]->first_name;?></b></h4>
                          <p>&nbsp;</p> 
                        </td>
                      </tr>
                    </tbody></table>
                    <!--  <span class="notif-time"><i class="fa fa-clock-o"></i> 2 Mins ago</span> -->
                    <span class="notif-time"><i class="fa fa-clock-o"></i>  <?= $real_time;?></span>
                    
                  </a>
                </li>  
              <?php } ?> 
              <li>
                  <a href="<?php echo base_url();?>interviewer-notifications" class="notif new">
                    <table>
                      <tbody><tr>
                        <!-- <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td> -->
                        <td>
                          <h4 style="text-align: center; margin-left: 100px;">Get all notifications...</h4>
                        </td>
                      </tr>
                    </tbody></table>                    
                  </a>
                </li>              
                <!--  <li><a href="#" class="notif">
                    <table>
                        <tbody><tr>
                            <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td>
                            <td>
                                <h4><b>John Smith</b>expressed interest in you</h4>
                                <p>respond back</p>
                            </td>
                        </tr>
                    </tbody></table>
                    <span class="notif-time"><i class="fa fa-clock-o"></i> 2 Mins ago</span>
                </a></li>                
                 <li><a href="#" class="notif">
                  <table>
                      <tbody><tr>
                          <td><img src="<?php echo base_url();?>assets/images/default.png" alt="."></td>
                          <td>
                              <h4><b>John Smith</b>expressed interest in you</h4>
                              <p>respond back</p>
                          </td>
                      </tr>
                  </tbody></table>
                  <span class="notif-time"><i class="fa fa-clock-o"></i> 2 Mins ago</span>
                </a></li>      -->           
                
              </ul>
            <?php } ?>  
          </li> 
                                          <!--  <li>
                                            <a role="button"><i class="fa fa-sign-out"></i></a>
                                           </li>--> 
                                         </ul>
                                       </div>    
                                     </div>
                                   </div>
                                 </div>
                               </section>  


