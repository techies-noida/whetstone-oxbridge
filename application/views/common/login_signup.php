<!-- Start login modal window -->
  <div aria-hidden="false" role="dialog" tabindex="-1" id="login-form" class="modal leread-modal fade in">
    <div class="modal-dialog">
      <!-- Start login section -->
      <div id="login-content" class="modal-content">
        <div class="modal-header">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">User Login</h4>
        </div>
        <div class="modal-body">
          <!-- <form id="login" method="post" action="<?php echo base_url();?>loginSignup/userlogin"> -->
             <form id="login">
            <div class="form-group">
                <label>Email Address*</label>
              <input type="text" name="email" placeholder="" class="form-control">
            </div>
            <div class="form-group">
                <label>Password*</label>
              <input type="password" name="password" placeholder="" class="form-control">
            </div>
             <div class="loginbox">
                 <label><a href="#" id="forgot-btn" style="float: right">Forgot password ?</a></label><br>
                 <div><span id="msg" style="color:red;font-size: 16px;font-weight: normal;"></span></div>
              <button class="btn signin-btn" type="submit">Login</button><br>
              <span>Not a member yet? <a id="signup-btn" href="#">Register Now</a></span>
            </div>                    
          </form>
        </div>
<!--        <div class="modal-footer footer-box">
          Recommended: Connect with Social Networks!
          <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
                      
        </div>-->
      </div>
      <!-- Start login section -->
      <div id="forgot-content" class="modal-content">
        <div class="modal-header">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Forgot Password</h4>
        </div>
        
        <div class="modal-body">
           <div id="message2">
           <!--  <span id="message1" style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;"></span> -->
          </div>
          <form id="forgot_password">
            <div class="form-group">
                <label>Email Address*</label>
              <input type="text"  name="email_address" placeholder="" class="form-control">
            </div>
            <div style="display:none" class="loading">
              <img src="<?php echo base_url();?>assets/images/loader.gif"  style="margin-left: 100px;" />
            </div>
           
             <div class="loginbox">
                  <span>At Wrong Place? <a id="login-btn" href="#">Sign In.</a></span><br>
                  <p id="email_not_exist"></p>
              <button class="btn signin-btn" type="submit">Reset Password</button><br>
              
            </div>                    
          </form>
        </div>
        
      </div>
      <!-- Start signup section -->
      <div id="signup-content" class="modal-content">
        <div class="modal-header">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Sign Up</h4>
        </div>

        <div class="modal-body">
           <div id="message1">
           <!--  <span id="message1" style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;"></span> -->
          </div> 
         <!-- <div >
          <span id="message1" style="color:green;font-size: 16px;font-weight: normal;margin-left: 40px;"></span></div>  -->
           
          <form id="signup">
            
            <div class="form-group">
              <input placeholder="Full Name" name='user_name' class="form-control">
            </div>
            <div class="form-group">
              <input placeholder="Email" name='email_address'class="form-control">
            </div>
            <div class="form-group">
              <input type="password" name='password' id="password" placeholder="Password" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" name='confirm_password'id="confirm_password" placeholder="Confirm Password" class="form-control">
            </div>
            <div class="loginbox">
                <label><input type="checkbox"><span>I agree with <a href="<?php echo base_url();?>privacy-policy" target="_blank" >GDPR Policy</a></span></label><br>
                <p id="alreday_mail"></p>
              <button class="btn signin-btn" type="submit">SIGN UP</button>
            </div>
            <div style="display:none" class="loading">
              <img src="<?php echo base_url();?>assets/images/loader.gif"  style="margin-left: 100px;" />
            </div>
            <br>
            <div class="signupbox">
              <span>Already got account? <a id="login-btn2" href="#">Sign In.</a></span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End login modal window -->
 
 