<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interviewer Signup | Whetstone Oxbridge</title>
   <?php $this->load->view('common/header_assets');?>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
 <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <!-- End single page header -->
  
  <!-- Start subscribe us -->
  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
                <h2 class="wow fadeInUp"><i class="fa fa-thumbs-up"></i></h2>
              <p>Your request has been accepted, Now create your account</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End subscribe us -->
  <!-- end about -->
<section id="testimonial" style='background: #fff'>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" >Create Account</h2>
                <span class="line"></span>           
              </div>
            </div>
              <div class="clearfix"></div>
                <div class="col-md-10" style="float: none; margin: auto">
                  <div style="margin-top: 40px">
                  <!-- Tab panes -->
                 
                  <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="student">
                           <div class="contact-area-right" style="margin-top: 20px">
                                   <form  method="post" action="<?php echo base_url();?>interviewer/create_account" class="comments-form contact-form" id="create_tutor_account">
                                       <div class="col-md-6" style="float: none; margin: auto">
                                           
                                    <div class="form-group">                        
                                      <label style="color:#444; font-weight: normal">First Name</label>
                                      <input type="text" name="first_name" class="form-control" placeholder="" value="<?php echo $status[0]->name;?>">
                                       <input type="hidden" name="profile_image" class="form-control" placeholder="" value="<?php echo $status[0]->profile_image;?>">
                                    </div>
                                     <div class="form-group">                        
                                      <label style="color:#444; font-weight: normal">last Name</label>
                                      <input type="text" name="last_name" class="form-control" placeholder="" value="">
                                    </div>
                                    <div class="form-group">  
                                         <label style="color:#444; font-weight: normal">Email</label>
                                      <input type="email" name="email_address" class="form-control" placeholder="" value="<?php echo $status[0]->email_address;?>" readonly>
                                    </div>
                                    
                                    <div class="form-group">  
                                         <label style="color:#444; font-weight: normal">Password</label>
                                      <input type="password" name="passwords" id="passwords" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">  
                                         <label style="color:#444; font-weight: normal">Confirm Password</label>
                                      <input type="password" name="confirm_passwords" id="confirm_passwords" class="form-control" placeholder="">
                                    </div>
                                   
                                 
                                       
                                     <div class="form-group" style="color:#888">                        
                                      <input type="checkbox" name="terms" style="height: auto;"> I agree with GDPR Policy
                                    </div>
                                        <button class="comment-btn" type="submit">Create account</button>
                                         <!-- <button class="btn signin-btn" type="submit">Login</button><br> -->
                                   
                                 
                                       </div>
                                      
                                   
                                  </form>
                                 </div>
                      </div> <!--student form ends-->
                </div>
                 </div>
         
              </div>
          </div>
        </div>
             
      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
  <!-- start script for validation  -->
   <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
   <!-- End  script for validation  -->
    
     <script>
        $("#upphoto").finecrop({
            viewHeight: 500,
            cropWidth: 200,
            cropHeight: 200,
            cropInput: 'inputImage',
            cropOutput: 'croppedImg',
            zoomValue: 50
        });
    </script>
    
  </body>
</html>