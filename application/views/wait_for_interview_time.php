<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
     <?php $this->load->view('common/header_assets');?>
    <!-- <link rel="shortcut icon" type="image/icon" href="<?= base_url();?>assets/images/favicon.ico"/>
    <link href="<?= base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">    
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/slick.css"/> 
    <link rel="stylesheet" href="<?= base_url();?>assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/animate.css"/> 
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/bootstrap-progressbar-3.3.4.css"/> 
    <link id="switcher" href="<?= base_url();?>assets/css/theme-color/default-theme.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>  -->   
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
 <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  
  <!-- END MENU --> 
  
 
  <!-- Start error section  -->
  <section id="error" style="background: #f6fef7; padding-top: 40px">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="errror-page-area">
              <h1 class="error-title" style="background: transparent; border:0"><img src="<?= base_url();?>assets/images/10mins.png"/></h1>
            <div class="error-content">
              <span>Please wait!</span>
              <p>‘You will only be able to access this page 10 minutes before your scheduled interview.'</p>
              <a class="error-home" href="<?php echo base_url();?>profile-details">Go to Dashboard</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End error section  -->

  <!-- Start subscribe us -->
 <!--  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2>Subscribe Newsletter</h2>
              <form action="" class="subscrib-form">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
   <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
 
  <!-- End footer -->

  <!-- jQuery library -->
  <!--  -->
    
  </body>
</html>