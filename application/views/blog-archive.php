<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="shortcut icon" type="image/icon" href="<?php echo base_url();?>assets/images/favicon.ico"/>
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slick.css"/> 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-progressbar-3.3.4.css"/> 
    <link id="switcher" href="<?php echo base_url();?>assets/css/theme-color/default-theme.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <header id="header">
    <!-- header top search -->
    <div class="header-top">
      <div class="container">
        <form action="">
          <div id="search">
          <input type="text" placeholder="Type your search keyword here and hit Enter..." name="s" id="m_search" style="display: inline-block;">
          <button type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
        </form>
      </div>
    </div>
    <!-- header bottom -->
    <div class="header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="header-contact">
              <ul>
                <li>
                  <a class="mail">
                    <i class="fa fa-twitter"></i>
              
                  </a>
                </li>
                <li>
                   <a class="mail" href='index.html#contact'>
                    Contact
              
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="header-login">
              <a class="login modal-form" data-target="#login-form" data-toggle="modal" href="#">Login / Sign Up</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- End header -->
  
  <!-- Start login modal window -->
  <div aria-hidden="false" role="dialog" tabindex="-1" id="login-form" class="modal leread-modal fade in">
    <div class="modal-dialog">
      <!-- Start login section -->
      <div id="login-content" class="modal-content">
        <div class="modal-header">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">User Login</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
                <label>Username*</label>
              <input type="text" placeholder="" class="form-control">
            </div>
            <div class="form-group">
                <label>Password*</label>
              <input type="password" placeholder="" class="form-control">
            </div>
             <div class="loginbox">
                 <label><input type="checkbox"><span>Remember me</span><a href="#" style="float: right">Forgot password ?</a></label><br>
              <button class="btn signin-btn" type="button">Login</button><br>
              <span>Not a member yet? <a id="signup-btn" href="#">Register Now</a></span>
            </div>                    
          </form>
        </div>
        <div class="modal-footer footer-box">
          Recommended: Connect with Social Networks!
          <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
                      
        </div>
      </div>
      <!-- Start signup section -->
      <div id="signup-content" class="modal-content">
        <div class="modal-header">
          <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Sign Up</h4>
        </div>
        <div class="modal-body">
          <form>
            
            <div class="form-group">
              <input placeholder="Username" class="form-control">
            </div>
            <div class="form-group">
              <input placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Confirm Password" class="form-control">
            </div>
            <div class="loginbox">
                <label><input type="checkbox"><span>Remember me</span><i class="fa"></i></label><br>
              <button class="btn signin-btn" type="button">SIGN UP</button>
            </div><br>
            <div class="signupbox">
              <span>Already got account? <a id="login-btn" href="#">Sign In.</a></span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End login modal window -->

  <!-- BEGIN MENU -->
  <section id="menu-area">      
    <nav class="navbar navbar-default" role="navigation">  
      <div class="container">
        <div class="navbar-header">
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- LOGO -->              
          <!-- TEXT BASED LOGO -->
          <a class="navbar-brand" href="index.html">Whetstone <small>Oxbridge</small> </a>              
          <!-- IMG BASED LOGO  -->
           <!-- <a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
            <li><a href="about.html">About Us</a></li>
            <li><a href="interviewers.html">Interviewers</a></li>
            <li><a href="workforus.html">Work for Us</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Resources <span class="fa fa-angle-down"></span></a>
              <ul class="dropdown-menu" role="menu">
                   <li><a href="resources.html#persta">Personal Statements</a></li>                
                <li><a href="resources.html#enttes">Entrance Tests</a></li>
                <li><a href="resources.html#colsel">College Selections</a></li>
                <li><a href="blog-archive.html">Blog</a></li>       
              </ul>
            </li>
            <li><a href="schools.html">Schools</a></li>               
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="statements.html">0 Items</a></li>                
                        
              </ul>
            </li>               
            <li><a href="#" id='dosearch'><i class="fa fa-search"></i></a></li>               
            <li><a href="#"><i class="fa fa-file-text-o"></i></a></li>               
          </ul>                     
        </div><!--/.nav-collapse -->
       
      </div>     
    </nav>
  </section>
  <!-- END MENU --> 
  
  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Blog Archive</h2>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Blog Archive</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  
  <!-- Start blog archive -->
  <section id="blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="blog-archive-area">
            <div class="row">
              <div class="col-md-8">
                <div class="blog-archive-left">
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                    <div class="blog-news-img">
                      <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-1.jpg" alt="image"></a>
                    </div>
                    <div class="blog-news-title">
                      <h2><a href="blog-single-with-right-sidebar.html">All about writing story</a></h2>
                      <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                    </div>
                    <div class="blog-news-details">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                      <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                  </article>
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                    <div class="blog-news-img">
                      <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-2.jpg" alt="image"></a>
                    </div>
                    <div class="blog-news-title">
                      <h2><a href="blog-single-with-right-sidebar.html">All about writing story</a></h2>
                      <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                    </div>
                    <div class="blog-news-details">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                      <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                  </article>
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                    <div class="blog-news-img">
                      <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-3.jpg" alt="image"></a>
                    </div>
                    <div class="blog-news-title">
                      <h2><a href="blog-single-with-right-sidebar.html">All about writing story</a></h2>
                      <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|18 October 2015</span></p>
                    </div>
                    <div class="blog-news-details">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                      <a class="blog-more-btn" href="blog-single-with-right-sidebar.html">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                  </article>
                  <!-- Start blog pagination -->
                  <div class="blog-pagination">
                    <ul class="pagination-nav">
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">6</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <aside class="blog-side-bar">
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <!-- Start blog search -->
                    <form>                    
                      <div class="search-group">                        
                        <button type="button" class="blog-search-btn"><span class="fa fa-search"></span></button>
                        <input type="search" placeholder="Search">
                      </div>                    
                    </form>
                    <!-- End blog search -->                                
                  </div>
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Categories</h4>
                    <ul class="widget-catg">                      
                      <li><a href="#">Photoshop</a></li>
                      <li><a href="#">Web Design</a></li>
                      <li><a href="#">Web Development</a></li>
                      <li><a href="#">UI Design</a></li>
                      <li><a href="#">Photography</a></li>
                    </ul>
                  </div>
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Text Widget</h4>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                  </div>
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Tags</h4>
                    <div class="tag-cloud">
                      <a href="#">Responsive</a>
                      <a href="#">Design</a>
                      <a href="#">Modern</a>
                      <a href="#">Business</a>
                      <a href="#">Software</a>
                      <a href="#">Photoshop</a>
                      <a href="#">Fashion</a>                      
                      <a href="#">News</a>
                      <a href="#">Health</a>
                      <a href="#">Education</a>
                    </div>
                  </div>
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Archive</h4>
                    <ul class="widget-archive">
                      <li><a href="#">November 2015<span>(35)</span></a></li>
                      <li><a href="#">October 2015<span>(55)</span></a></li>
                      <li><a href="#">September 2015<span>(65)</span></a></li>
                      <li><a href="#">August 2015<span>(75)</span></a></li>
                      <li><a href="#">July 2015<span>(105)</span></a></li>
                    </ul>
                  </div>
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Important Links</h4>
                    <ul>
                      <li><a href="#">Link 1</a></li>
                      <li><a href="#">Link 2</a></li>
                      <li><a href="#">Link 3</a></li>
                      <li><a href="#">Link 4</a></li>
                    </ul>
                  </div>
                </aside>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  <!-- End blog archive -->

  <!-- Start subscribe us -->
  <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2>Subscribe Newsletter</h2>
              <form action="" class="subscrib-form">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="footer-left">
          <p>&copy; Oxbridge 2018 | All Rights Reserved </p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="footer-right">
            <a href="index.html"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End footer -->

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>    
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <!-- Bootstrap -->
  <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <!-- Slick Slider -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
  <!-- mixit slider -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
 <!-- counter -->
  <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
  <!-- Wow animation -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
  <!-- progress bar   -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  
    
   
  <!-- Custom js -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    
  </body>
</html>