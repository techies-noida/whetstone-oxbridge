<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Student Feedback | Whetstone Oxbridge </title>
  <?php $this->load->view('common/header_assets');?>
  <style>
    .sidebar-widget{float:none; display: block;}
    .team-member-name{float:none; display: block;}
    .single-team-member{float:none; display: block;}
    .reviews{color:#888}
  </style>
</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
  $this->load->view('common/student_header');
  ?>
  <!-- END MENU -->

  <!-- end about -->
  <section id="testimonial" style='background: #f6fef7; padding-top: 0'>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding: 0">
          <div class="container">
            <div class="row">

              <br>

              <div class="col-md-12" style="padding: 0" >
                <div style="margin-top: 0px">



                  <div class="contact-area-right" style="margin-top: 00px">

                    <!--  left sidebar  -->
                    <?php $this->load->view('common/student_sidebar');?>
                    <div class=" col-md-9 col-sm-6 col-xs-12">
                     <div class="content-wrapper">
                      <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Feedback</h4>
                      <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
                        <div class="panel panel-default">
                          <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body" style="padding: 0">
                              <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                  <li role="presentation" class="active"><a href="#pdf" aria-controls="pdf" role="tab" data-toggle="tab">PDF</a></li>
                                  <li role="presentation"><a href="#video" aria-controls="video" role="tab" data-toggle="tab">VIDEO</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                  <div role="tabpanel" class="tab-pane active" id="pdf">

                                   <div class="pre-feedback" style="max-height: 308px;">

                                    <ul>
                                      <?php foreach ($feedback as $row ) {
                                        $originalDate = $row->review_date;
                                        $newDate = date("d M, Y", strtotime($originalDate));
                                        ?>
                                        <li>
                                          <a href="#">
                                            <table>
                                              <tr>
                                                <td>
                                                  <!-- <img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/> -->
                                                  <?php $image= $row->profile_image; if($image=='')
                                                  /* echo '<img src="'.base_url().'assets/images/main-testimonials-img-2.png" alt="team member img">';*/
                                                  echo '<img  class="int-img" src="'.base_url().'assets/images/default.png" alt="team member img">';

                                                  else
                                                   echo '<img  class="int-img" src="'.base_url().$image.'" alt="team member img">';
                                                 ?>

                                               </td>
                                               <td>
                                                <h4><?php echo ucwords($row->first_name);?></h4>
                                                <small><?php echo $newDate;?> </small>
                                              </td>
                                              <td>
                                                <a href="<?php echo base_url().'Student/previous_feedback/'.$row->review_id;?>">
                                                  <img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png">
                                                </a>
                                              </td>
                                            </tr>
                                          </table>
                                        </a>
                                      </li>
                                    <?php  } ?>
                                  </ul>
                                </div>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="video">
                               <div class="pre-feedback" style="max-height: 308px;">
                                <ul>
                                  <?php 
                                  if($video) {
                                    foreach ($video as $row) {
                                     $originalDate = $row->created_date;
                                     $newDate = date("d M, Y", strtotime($originalDate));
                                     $id=$row->interviewer_id;
                                     $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);
                                     /* if($result){*/
                                      ?>

                                      <li>
                                        <a href="#">
                                          <table>
                                            <tr>
                                              <td>

                                                <?php $image= $result[0]->profile_image; if($image=='')
                                                echo '<img class="int-img" src="'.base_url().'assets/images/default.png" alt="team member img">';
                                                else
                                                  echo '<img class="int-img" src="'.base_url().$image.'" alt="team member img">';
                                                ?>
                                              </td>
                                              <td>
                                                <h4><?php echo ucwords($result[0]->first_name);?></h4>
                                                <small><?php echo $newDate;?></small>
                                              </td>
                                              <td>
                                                <a href="https://whetstone-oxbridge.com/compose_video/composed/<?php  echo $row->video_url;?>" target="_blank"><img class="pdficon" src="<?php echo base_url();?>assets/images/video-icon.png"> </a>

                                              </td> 

                                            </tr>
                                          </table>
                                        </a>
                                      </li>
                                    <?php  } }  ?>

                                  </ul>
                                </div>
                              </div>

                            </div>

                          </div>


                        </div>
                      </div>
                    </div>

                  </div>                      
                    </div>
                  </form>
                </div> <!--student form ends-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- End Service -->
<!-- Start footer -->
<?php $this->load->view('common/footer'); ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
</body>
</html>
