<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student Dashbaord | Whetstone Oxbridge</title>
    <?php  $this->load->view('common/header_assets');?>
    <link href="<?php echo base_url();?>assets/js/Super-Simple-Calendar/css/dncalendar-skin.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .pb-calendar .schedule-dot-item.blue{
			background-color: blue;
		}

		.pb-calendar .schedule-dot-item.red{
			background-color: red;
		}

		.pb-calendar .schedule-dot-item.green{
			background-color: green;
		}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
 $this->load->view('common/student_header');
  ?>
  <!-- END MENU -->

  <!-- Start Pricing table -->
  <section id="our-team" style="background: #f6fef7; padding-top: 0">
    <div class="container">
    <div class="row">
     <!--  <div class="row">
          <div class="col-md-12">

            <h3 class="title" style="text-align: left">Manage Profile</h3>

          </div>
          </div> -->
        <br>
        <div class="col-md-12" style="padding: 0">
          <div class="our-team-content" style="margin-top: 0">
            <div class="container">
            <div class="row">
              <!-- Start single team member -->
             <?php $this->load->view('common/student_sidebar');?>
              <!-- ends single team member -->

              <div class="col-md-9 col-sm-6 col-xs-12">
                  <div class="inter-more">
                   <div class="availability">
                     <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message'].
                          '</div>';
                      }?>
                          <h4 style="color:#2aaebf !important;border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; ">Upcoming Appointments
                          <div class="right" style='text-align:right; float: right'>
                           <a href="<?php echo base_url();?>interviewers-list" class="btn btn-primary" style='background:#2aaebf'><i class="fa fa-plus"></i> Book another interview</a>
                          </div>
                          </h4>

                          <br>
                       <div class="table-responsive">
                           <table id="bookings-1" class="display bookings" style="width:100%">
                            <thead>
                                <tr>
                                    <th> Interviewer Name</th>
                                    <th>Appointment Date</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                   <!--  <th>Time</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php foreach ($appointment as $row) {
                                $id=$row->interviewer_id;
                                $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);
                                ?>

                                <tr>
                                    <td>
                                         <?php $image= $result[0]->profile_image; if($image=='')
                   /* echo '<img src="'.base_url().'assets/images/main-testimonials-img-2.png" alt="team member img">';*/
                  echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="height: 28px;width: 5opx;">';

                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="height: 28px;width: 5opx;">';
                     ?>
                                        <!-- <img src="<?php echo base_url();?>assets/images/team-member-2.png" height="36" class="img img-circle"/> -->
                                        <?php echo ucwords($result[0]->first_name);?>
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i> <?php echo date('d-m-Y', strtotime($row->booking_date));?>
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $row->start_time?>
                                    </td>
                                     <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $row->end_time;?>
                                    </td>
                                    <td>
                                        
                                         <!-- <a href="<?= base_url().'session/interviewer/'.$row->room_name;?>" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a> -->
                                        <!--  <?php $time = date('Y-m-d H:i:s', time()); 
                                               $date = date('Y-m-d'); 
                                            $real_time = date('h:i A', strtotime($time));
                                            $start_time=$row->start_time;
                                            $end_time=$row->end_time;
                                            $booking_date=$row->booking_date;
                                            $schedule_time= date('h:i A',strtotime('-0 hour -15 minutes',strtotime($start_time)));
                                            $schedule_end= date('h:i A',strtotime('-0 hour -0 minutes',strtotime($end_time)));
                                          //echo '<pre>';print_r($result);echo '</pre>'; exit();
                                            if($row->call_status=='Active'){

                                              if( $date==$booking_date && $real_time <= $schedule_time && $real_time <= $schedule_end) { 
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a>';
                                              
                                              }else if($date > $booking_date ||$date==$booking_date && $real_time >= $schedule_time && $real_time >= $schedule_end )
                                              {
                                                 echo '<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-phone"></i> Expire</a>';

                                              }

                                              
                                              else /*if($date<=$booking_date || $real_time <= $schedule_time || $real_time >= $schedule_time )*/{
                                                echo '<a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i> Call</a>';

                                              }
                                            }
                                             else if($row->call_status=='Started'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Started</a>';
                                            }
                                            else if($row->call_status=='Completed'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Completed</a>';
                                            }
                                            else if($row->call_status=='Expire'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Expire</a>';
                                            }


                                         ?>  -->
                                         <!-- <a href="<?= base_url().'session/interviewer/'.$row->room_name;?>" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a> -->
                                         <?php $time = date('Y-m-d H:i:s', time()); 
                                               $date = date('Y-m-d'); 
                                            $real_time = date('h:i A', strtotime($time));
                                            $start_time=$row->start_time;
                                            $end_time=$row->end_time;
                                            $booking_date=$row->booking_date;
                                            $schedule_time= date('h:i A',strtotime('-0 hour -15 minutes',strtotime($start_time)));
                                            $schedule_end= date('h:i A',strtotime('-0 hour -0 minutes',strtotime($end_time)));
                                          //echo '<pre>';print_r($result);echo '</pre>'; exit();
                                            if($row->call_status=='Active'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Call</a>';
                                            }
                                             else if($row->call_status=='Started'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Started</a>';
                                            }
                                            else if($row->call_status=='Completed'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Completed</a>';
                                            }
                                            else if($row->call_status=='Expire'){
                                              echo '<a href="'.base_url().'session/interviewer/'.$row->room_name.'" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> Expire</a>';
                                            }


                                         ?> 


                                    </td>
                                </tr>
                            <?php }?>
                            </tbody>
                           </table>
                       </div>
                      </div>
                      </div>
              </div>

            </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- alert  Modal content-->
      <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header " style="width: 100%;
             background-color:#15585F;
             height: 50px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Session help Message</h4>
              </div>
              <div class="modal-body " >
                <p> Link will be active before 15 Minute to schedule session time to move on session screen, Once refresh page and click on link.</p>  
              </div>
              <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>

      <!-- model  -->




  <!-- Start footer -->

  <?php  $this->load->view('common/footer');?>
  <!-- End footer -->

    <!-- jQuery library -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<!--    <script type="text/javascript" src="<?php echo base_url();?>assets/js/pg-calendar-master/dist/js/pignose.calendar.full.min.js"></script>

   <!- Bootstrap -->



  <script type="text/javascript" src="<?php echo base_url();?>assets/js/Super-Simple-Calendar/js/dncalendar.min.js"></script>
    <script>
		$(document).ready(function() {
			var my_calendar = $("#dncalendar-container").dnCalendar({
				minDate: "2018-01-15",
				maxDate: "2018-12-02",
				defaultDate: "2018-05-10",
				monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
				monthNamesShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
				dayNames: [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                dayNamesShort: [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ],
                dataTitles: { defaultDate: 'Selected', today : 'hari ini' },
                notes: [
                		{ "date": "2018-05-25", "note": ["Not Available"] },
                		{ "date": "2018-05-12", "note": ["Not Available"] }
                		],
                showNotes: true,
                startWeek: 'Monday',
                dayClick: function(date, view) {
                	alert("You'r going to un-available on: "+date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear());
                        my_calendar.update({
                            defaultDate: date.getFullYear()+"-"+(date.getMonth() + 1)+"-"+date.getDate()
			});
                }
			});

			// init calendar
			my_calendar.build();

			// update calendar
			// my_calendar.update({
			// 	minDate: "2016-01-05",
			// 	defaultDate: "2016-05-04"
			// });
		});
		</script>
     <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
      <script>
        $(document).ready(function() {
            $('#bookings-1').DataTable({
                searching:false,
                 sorting:false,
                "paging": false,
                "bInfo" : false
            });
        } );
      </script>

  </body>
</html>
