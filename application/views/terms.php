<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Terms & Conditions | Whetstone Oxbridge</title>
    <?php $this->load->view('common/header_assets');?>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .tnc{background: #fff; width: 100%; padding: 40px 45px; margin-bottom: 10px; text-align:left}
    .tnc .org-logo{text-align: center}
    .tnc .org-logo > img{ margin-bottom: 20px}
    .tnc h1{text-align: center; font-size: 24px; }
    .tnc .tnc-category{font-size: 16px;  color: #333; margin-top: 60px; display: block}
    .tnc .tnc-subcategory{font-size: 14px;  color: #333; margin-top: 20px; display: block}
    .tnc p {font-size: 14px; color: #333; margin-top: 20px; line-height: 24px}
    .tnc p.uppercase{text-transform: uppercase}
    .uppercase{text-transform: uppercase}
    .tnc p.lowercase{text-transform: lowercase}
    .lowercase{text-transform: lowercase}
    .tnc capitalcase{text-transform: capitalize}
    .tnc p.bold{font-weight: bold}
    .tnc ul{margin-top: 20px; list-style: disc; list-style-position: outside; padding-left: 0 }
    .tnc ul li{margin-top: 20px; line-height: 24px; margin-left: 20px; font-size: 14px; color:#333}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
   <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  
 <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Terms & Conditions</h2>
              <p></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Terms & Conditions</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start error section  -->
  <section id="error">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="errror-page-area">
       
            <div class="error-content">
              <span>Terms & Conditions</span>
         
                               
                                               
                            <div class="tnc">
                            
                            
                            <a href="javascript:void(0)" class="tnc-category">What we expect of our Interviewers</a>
                             <ul>
                                 <li>Interviewers must be currently enrolled in a degree program at either Oxford or Cambridge University.</li>
                                 <li>Interviewers are required to respond as quickly as possible and to all applicants who have made requests for interview practice. In they fail to respond Whetstone Oxbridge reserves the right to suspend an interviewer from the site. Interviewers must not take clients outside of the platform or contact clients outside the platform. Whetstone Oxbridge will be regulating the activity on the site and if we suspect that an interviewer has been taking business outside the site or contacting a client outside the site (e.g. via email or phone) we reserve the right to suspend an interviewer from the site and fine them up to £200 for the loss of business and inconvenience caused. </li>
                                 <li>Whetstone Oxbridge also reserves the right to filter messages between interviewer and applicant and directly contact the sender if it is deemed spam or messages do not match the purpose of the site. Whetstone Oxbridge reserves the right to keep and delete the exchanged of such messages and e-mails.</li>
                                 <li>If a client tries to contact you outside of the Whetstone Oxbridge site then you should notify Whetstone Oxbridge immediately. </li>
                            </ul>
                            
                            <a href="javascript:void(0)" class="tnc-category">Payments, cancellations, and refunds</a>
                             <ul>
                                 <li>Payments by Whetstone Education Ltd. to Interviewers shall be made upon completion of satisfactory service once a review has been submitted by the Client.</li>
<li>If the Client fails to submit a review of service by Interviewer then Whetstone Education Ltd. will prompt the Client. Failing this, the Interviewer will be payed in full after a maximum of seven days.</li>
<li>All sums payable by either Party pursuant to these Terms and Conditions are inclusive of any value added or other tax (except corporation tax) or other taxes on profit, for which that Party shall be additionally liable.</li>
<li>If a Client requests to rearrange more than three full days before the scheduled time then the Interviewer should make every effort to accommodate this change.</li>
<li>If a Client cannot make the scheduled interview time and does not give three days warning, or if the Client fails to show up for the interview, then no monetary refund will be offered and the Interviewer will still be payed.</li>
<li>Client’s can requested to change the scheduled interview time up to 24 hours before the scheduled time. If the interviewer only sees the request and responds after the 24 deadline the request is still valid. If the interviewer cannot accommodate this change the Client will not be refunded, but rather will able to use the credit for another Interviewer.

                            </ul>
                            
                            <a href="javascript:void(0)" class="tnc-category">If you can’t make an interview time</a>
                             <ul>
                                 <li>If an Interviewer is unable to make a scheduled time they are under obligation to inform the Client and Whetstone Education Ltd. as soon as possible. </li>
                                 <li>It is then at the discretion of the Client as to whether they would like to reschedule, receive a refund or use another Interviewer. </li>
                                 <li>If an Interviewer fails to show up for a scheduled interview appointment then the Client will be fully refunded and Whetstone Education Ltd. shall reserve the right to fine the Interviewer for the cost of the session and suspend the Interviewer from the platform. </li>


                            </ul>
                            
                            <a href="javascript:void(0)" class="tnc-category">Client complaints </a>
                             <ul>
                                 <li>If the client feels that the service provided by the Supplier and / or Employee was not satisfactory then they have 24 hours to register a complaint. The Supplier will then review the complaint and watch the recorded video of the interview. It is then at the discretion of Whetstone Education Ltd as to whether or not the client’s quality complaint is justified and therefore whether or not they will be refunded for the service.</li>
                                 <li>	If a refund is granted to the Client then the Interviewer will not receive payment for the services. </li>
                                 <li>If the interviewer is deemed by Whetstone Education Ltd. to have acted inappropriately or unprofessionally then Whetstone Education Ltd. reserves the right to suspend Interviewers from the website and pursue legal proceedings if necessary.</li>

                            </ul>
                            
                          
                            
                            <a href="javascript:void(0)" class="tnc-category">General Conditions of Use</a>
                            <p class="lowercase"><span class="uppercase">T</span>he Whetstone Oxbridge site is owned, edited and run by Whetstone Education Ltd.  Use of the Whetstone Oxbridge site is subject to acceptance of these Terms and Conditions. By accessing Whetstone Oxbridge, you agree to regularly refer to the latest version of the General Conditions permanently available on the site. 
In case of disagreement with these conditions or unacceptable behaviour towards a fellow user, Whetstone Education Ltd. reserves the right to prohibit or restrict access to the site.
</p>
<p>
    <b>Interviewer / Employee</b> - Current Oxbridge students employed by Whetstone Oxbridge to provide interview practice services via the site.<br>

    <b>Client / Applicant</b> - an individual or corporate body which purchases services from the Supplier.<br>

    <b>Supplier</b> - Whetstone Education Ltd.<br>

    <b>Services</b> - The services supplied to the Client / applicant by the Supplier via the Interviewer / Employee.<br>

    <b>Commencement / scheduled date</b> - The commencement date for the agreement. This includes the scheduled time of interview practices.<br>

</p>
                        
                            <a href="javascript:void(0)" class="tnc-category">Liability</a>
                        
                            <ul>
                                <li>The purpose of the Whetstone Oxbridge site is to connect Oxbridge applicants with current Oxbridge students in order to practice their interview skills. The services of the Whetstone Oxbridge site are exclusively reserved for those over the age of 16.</li>
                                <li>Whetstone Education Ltd. shall not be liable for delay or failure to provide its services if the causes are beyond the reasonable control of the Supplier or Interviewer / Employee.</li>
                                <li>Whetstone Education Ltd provides interview advice in the form of practice interview related Services. Whetstone Education Ltd does not claim to guarantee success at interviews. Whetstone Education Ltd is solely an advisory service, and as such the information/preparation we offer should be taken in that context. Whetstone Education Ltd cannot guarantee applicants a place at any university. As such, Whetstone Education Ltd are not liable for any applicant’s unsuccessful application.</li>
                                <li>Whetstone Oxbridge makes every effort to control the quality, the accuracy of information contained in advertisements, the veracity of interviewer qualifications, contact information and availability, but cannot under any circumstances guarantee it.</li>
<li>Whetstone Education Ltd. does not offer refunds or free help for Clients / Applicants who have been unsuccessful at their interview.

                            </ul>
                            
                            <a href="javascript:void(0)" class="tnc-category">Payment</a>
                            <ul>
                                <li>The Client agrees to pay the Fees for the services provided by the Supplier (via the Interviewer / Employee).</li>
                                    <li>Payments by Client shall be made upon booking.  </li>
                                    <li>Payments by Supplier to Employee shall be made upon completion of satisfactory service once a review has been submitted by the Client. </li>
                                    <li>If the Client fails to submit a review of service by Employee then the Supplier will prompt the Client. Failing this, the Employee will be payed in full after a maximum of seven days.</li>
                                    <li>The Client will pay the Supplier for any additional services provided by the Supplier that are not specified in the original agreement in accordance with the Supplier’s then current, applicable daily / hourly rate in effect at the time of the performance or such other rate as may be agreed.  Any charge for additional services will be supplemental to the amounts that may be due for expenses.</li>
                                    <li>All sums payable by either Party pursuant to these Terms and Conditions are inclusive of any value added or other tax (except corporation tax) or other taxes on profit, for which that Party shall be additionally liable.</li>

                            </ul>
                        
                            <a href="javascript:void(0)" class="tnc-category">Cancellation Policy</a>
                            <ul>
                                <li>Requests from the Client to cancel a previously booked interview service must be made three days before the scheduled time of service. </li>
                                <li>If a Client cancels less than three full days before the scheduled interview service, no refund will be given. If a client cancels three or more business days before the scheduled service, Whetstone will refund the client’s booking minus a £10 administrative charge for arranging the interviews (this covers time in making the booking, as well as any credit card fees incurred taking the booking. Please note this £10 charge is per interview session arranged). </li>
	<li>If a Client cannot make the scheduled interview time and does not give three days warning, or if the Client fails to show up for the interview, then no monetary refund will be offered and the Interviewer will still be payed. </li>
	<li>If a Client cancels or wants to reschedule less than 30 full days before a scheduled Whetstone Roadshow school visit service this may incur a charge of as much as £300 (this covers the time spent organising the Roadshow and transport and accommodation already organised for our student teams). Requests from the client to cancel a previously booked service must be made by email. The day of receipt of the email (business days only) by Whetstone is the time that notice is given of your request to cancel your booking. Requests made by email after operating hours (09.00-17.00 GMT) will be dated to the next day.</li>
	<li>For all school services: where Whetstone Education Ltd has a contract with a school to provide support within the school, if a student cancels more than three days before the scheduled service date a 100% refund will be granted; cancellations made less than three days before the scheduled service: no refund of fees will be made. This cancellation policy is relevant for all students who sign up to Whetstone Education Ltd’s services via the school.</li>
	 <li>If the client fails to attend the service on the date booked without giving notice within the above time periods Whetstone Education Ltd cannot offer any monetary refund. Retrospective notice (for any reason) given after the session date will not be accepted and no refund will be considered.</li>
	<li>At the discretion of Whetstone Education Ltd, and where space is available, clients can move the date of their booking to another date free of charge once Whetstone Education Ltd is provided with more than 24 hours before the service date. </li>
	<li>If the specific Employee / Interviewer cannot accommodate this change the Client will not be refunded, but rather will able to use the credit for another Interviewer.</li>
	<li>Clients cannot reschedule more than once. If a client needs to reschedule a second time or after the 24 hour deadline then this is at the discretion of the Employee providing the interview service. If the Employee cannot make reasonable adjustment to their schedule to accommodate the needs of the Client then the Client will no monetary refund will be offered and clients will forfeit their booking and booking fee in full.</li>
	<li>If an Interviewer is unable to make a scheduled time they are under obligation to inform the Client and Whetstone Education Ltd. as soon as possible. It is then at the discretion of the Client as to whether they would like to reschedule, receive a refund or use another Interviewer. </li>
	<li>If an Interviewer fails to show up for a scheduled interview appointment then the Client will be fully refunded.

                            </ul>
                            
                            <a href="javascript:void(0)" class="tnc-category">Refund policy</a>
                            <p><span class="uppercase">I</span>f the client feels that the service provided by the Supplier and / or Employee was not satisfactory then they have 24 hours to register a complaint. The Supplier will then review the complaint and watch the recorded video of the interview. It is then at the discretion of Whetstone Education Ltd as to whether or not the client’s quality complaint is justified and therefore whether or not they will be refunded for the service. </p>
                        
                  <a href="javascript:void(0)" class="tnc-category">Cancellation by Whetstone Education Ltd</a>
                            <p><span class="uppercase">W</span>
                               Whetstone Education Ltd reserves the right to cancel any of its services. This is a last resort and all attempts to replace interviewers/trainers if sick/absent will be made before any consideration is given to cancelling a service. Full provisions to move/reschedule the service will be made. In the unlikely event that an interview or Roadshow has to be cancelled the client will be informed immediately and be refunded the charge. Whetstone Education Ltd cannot be held liable for other expenses that the client may have occurred in this event.</p>
                        
                               
                            <a href="javascript:void(0)" class="tnc-category">Force Majeure</a>
                            <p><span class="uppercase">N</span>
                               either the Client nor the Supplier shall be liable for any failure or delay in performing their obligations under these Terms and Conditions where such failure or delay results from any cause that is beyond the reasonable control of that Party.  Such causes include, but are not limited to: power failure, Internet Service Provider failure, industrial action, civil unrest, fire, flood, storms, earthquakes, acts of terrorism, acts of war, governmental action or any other event that is beyond the control of the Party in question.
                            </p>
                            
                            
                            <a href="javascript:void(0)" class="tnc-category">Severance</a>
                            <p><span class="uppercase">N</span>
                              In the event that one or more of the provisions of these Terms and Conditions are found to be unlawful, invalid or otherwise unenforceable, then these provisions shall be removed from the Terms and Conditions. However, the remaining Terms and Conditions will remain intact and enforceable. 
                            </p>
                             
                            <a href="javascript:void(0)" class="tnc-category">Copyright</a>
                            <p><span class="uppercase">T</span>
                             he textual and visual content and site design are the copyright of Whetstone Education Ltd. Whetstone Education Ltd. also reserves all copyright and other rights in connection with the provision of its services, and reserves the right to take appropriate legal action if this copyright is infringed. 
                            </p>
                            
                            <a href="javascript:void(0)" class="tnc-category">Complaints Policy</a>
                            <p><span class="uppercase">Y</span>
                                ou can make a complaint by contacting us on <a href="mailto:info@whetstone-oxbridge.com">info@whetstone-oxbridge.com</a>. We endeavour to respond to all customer complaints within five working days.
                            </p>
                             
                            <a href="javascript:void(0)" class="tnc-category">Law and Jurisdiction</a>
                            <p><span class="uppercase">Y</span>
                                These Terms and Conditions, and any dispute relating to them, shall be governed by the laws of England and Wales.
                            </p>
                             
                      
                        </div>
                    
                            
              <a class="error-home" href="<?php echo base_url();?>">Home Page</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End error section  -->

  

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
    
  </body>
</html>