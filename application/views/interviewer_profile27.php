<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interviewer-details</title>
     <?php $this->load->view('common/header_assets');?>
     <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!--<link href="<?php echo base_url();?>assets/js/Super-Simple-Calendar/css/dncalendar-skin.css" rel="stylesheet">-->
    <link href="<?php echo base_url();?>assets/js/fullcalendar-3.9.0/fullcalendar.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/js/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
           html{overflow-x:hidden}
        .pb-calendar .schedule-dot-item.blue{
			background-color: blue;
		}

		.pb-calendar .schedule-dot-item.red{
			background-color: red;
		}

		.pb-calendar .schedule-dot-item.green{
			background-color: green;
		}
                   .addperiodbutton{background: #2aaebf; color:#fff !important; display: block; text-align: center; height: 92px; }
          .addperiodbutton i{margin-top: 30px}
          .addperiodbutton:hover{background: #2aaebf; color:#fff !important; text-decoration: none; opacity: 0.7}
          #event-detail .table-striped > tbody > tr:nth-of-type(odd){border:0; background: rgba(42,174,191,0.05)}
          #event-detail .table-striped > tbody > tr th{border:0; }
          #event-detail .table-striped > tbody > tr td{border:0; }
          #event-detail .btn-primary{background: #2aaebf; border-color:#2aaebf; border-radius: 0}
          #event-detail .btn-danger{background: red; border-color:red; border-radius: 0}
                     .fc-center h2{color:#2aaebf !important}

          #appointment .form-control{border-radius: 0; box-shadow: none; margin-bottom: 5px}
          #appointment table tr td:first-of-type{padding-right: 35px; color:#333}
          #appointment .form-control:focus{border-radius: 0; box-shadow: none;}
          #appointment .btn-primary{border-radius: 0; box-shadow: none;; border:0; background: #2aaebf; color:#fff; padding: 8px 15px}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU -->
  <!-- Start Pricing table -->
  <section id="our-team">

    <div class="container">
      <div class="row">

        <div class="col-md-12">
          <div class="our-team-content" style="margin-top: 0">
            <div class="row">
              <!-- Start single team member -->
               <?php  $this->load->view('common/interviewer_sidebar');?>
              <!-- ends single team member -->
              <div class="col-md-9 col-sm-6 col-xs-12">

                 <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message'].
                          '</div>';
                      }?>
                  <div class="inter-more">
                      <div class="reviews">
                          <?php  $size=count($reviews);
                        if($size>0){

                        }?>
                          <div class="row">

                            <div class="inter-more">
                      <h4 style="color:#2aaebf !important">Academic interests...</h4>
                      <p><?php echo $details[0]->academic_interest;?></p>
                      <hr>
                      <h4 style="color:#2aaebf !important">What I wish I'd known before my Oxbridge interviews...</h4>
                      <p><?php echo $details[0]->wish_to_told_oxbridge;?></p>
                      <hr>
                      <h4 style="color:#2aaebf !important">Extra-curricular interests...</h4>
                      <p><?php echo $details[0]->extra_curricular_interest;?></p>
                      <hr>

                  </div>
                          </div>



                      </div>

                      <br>




                  </div>
              </div>

            </div>
              <div class="row">
                  <div class="col-md-3">
                  <div class="inter-more">
                       <div class="reviews">
                            <?php
                              //$size=count($reviews);
                              for($i=0;$i<$size;$i++)
                              { ?>

                                      <blockquote>
                                          <div class="">
                                               <a href="#">
                                                <?php if($reviews[$i]->rating === 0){
                                                     for($j=0;$j<5;$j++){
                                                      echo ' <i class="fa fa-star-o" style="color:#f7c80a"></i>';
                                                     }
                                                    }else {
                                                      $norating=5-$reviews[$i]->rating;
                                                      for($j=0;$j<$reviews[$i]->rating;$j++)
                                                      {
                                                        echo'<i class="fa fa-star" style="color:#f7c80a"></i>';
                                                      }
                                                      for($k=0;$k<$norating;$k++)
                                                      {
                                                        echo'<i class="fa fa-star-o" style="color:#f7c80a"></i>';
                                                      }
                                                    } ?>
                                                 <!--  <i class="fa fa-star" style="color:#f7c80a"></i>
                                                  <i class="fa fa-star" style="color:#f7c80a"></i>
                                                  <i class="fa fa-star" style="color:#f7c80a"></i>
                                                  <i class="fa fa-star-o" style="color:#f7c80a"></i>
                                                  <i class="fa fa-star-o" style="color:#f7c80a"></i> -->
                                              </a>
                                          </div>
                                          <p>"<?= $reviews[$i]->comments;?>"</p>
                                          <span><?= $reviews[$i]->first_name;?></span>
                                      </blockquote>

                            <?php }?>


                  </div>
                  </div>
                  </div>
                  <div class="col-md-9">
                  <div class="inter-more">

                           <div class="availability">
                                        <?php
                                        if (!isset($this->session->userdata['logged_in'])){ ?>
                                        
                                        
                                       <!--  <h4>Login / Signup in order to view availability is there</h4> -->
                                        <a class="login modal-form" data-target="#login-form" data-toggle="modal" href="#"><h4>Login / Signup in order to view availability is there</h4></a>
                                         <?php   }else{ ?>
                                        <div id='calendar'></div>
                                        <br>
                                      <?php } ?>

                                    </div>
                  </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>



<!-- <div class="modal fade" id="event-detail" tabindex="-1" role="dialog" aria-labelledby="event-detail">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
       <div class="modal-header" style="padding: 8px">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Appointment Detail</h4>
      </div> -->
      <!-- <div class="modal-body" style="padding: 15px; font-size: 13px">
         <form action="<?= base_url()?>appointment/paynow" method="post" class="comments-form contact-form">
          <table class="table table-striped">
              <tr>
                  <th>Appointment Id</th><td><span class="app-color"><i class="fa fa-square"></i></span>&nbsp;&nbsp;<span class="app-desc"></span></td>
                   <input type="hidden" name="schedule_id" id="schedule_id" /> <input type="hidden" name="appointment_date" id="appointment_date" />
              </tr>
              <tr>
                 <th>Interviewer</th>
                <td><div class="">

                      <input type="hidden" id="" name="interviewer_id" value="<?= $details[0]->user_id;?>"  />
                      <input type="text" class="form-control" id="" name="interviewer_name" value="<?= ucwords($user_name);?>"  readonly />

                    </div>

                </td>
              </tr>
              <tr>
                 <th>Student</th>
                <td><div class="">

                       <input type="hidden" id="" name="user_id" value="<?=$this->session->userdata['logged_in']['user_id'];?>"  />
                      <input type="text" id="" class="form-control" name="student_name" value="<?=ucwords($this->session->userdata['logged_in']['user_name']);?>"  readonly />
                    </div>
                </td>
              </tr>
               <tr>
                  <th>Appointment Time</th><td><span class="app-time"></span></td>
              </tr>
              <tr>
                <th>Package</th>
                <td>
              <select class="form-control" name="slot_time" id="slot_time" onchange="return get_slot(this.value)" required>
                        <option value="" >Select Package </option>

                        <option value="30">45 Minute</option>
                        <option value="60">60 Minute</option>
                      </select>
                    </td>
              </tr>
              <tr>
                <th> Start Time</th>
                <td>
              <select class="form-control" name="start_time" id="slot_id" onclick="return get_slot(this.value)"  onchange="return get_end_time(this.value)"  required>
                        <option value=""  >Select schedule</option>


              </select>

                    </td>
              </tr>
              <tr>
                <th> End Time</th>
                <td>
                      <input class="form-control" type="text"  name="end_time" id="end_time" readonly  required/>
                    </td>
              </tr>

              <tr>
                  <th>Appointment Date</th><td><span class="app-date"></span></td>

              </tr>

              <tr>
                 <th>Amount</th>
                <td><div class="">

                       <input type="text" id="" name="amount" value="100"  />

                    </div>
                </td>
              </tr>



          </table>
          <button type="submit" name="submit" class="btn btn-xs btn-primary">Confirm Appointment</button>

          <button class="btn btn-xs btn-danger">Cancel Appointment</button>
      </div>
    </form>
    </div>
  </div>
</div> -->

          <!-- Modal -->
<!-- <div class="modal fade" id="addtime" tabindex="-1" role="dialog" aria-labelledby="addtime">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Time Period</h4>
      </div>
      <div class="modal-body">
         <form action="" class="comments-form contact-form">
                       <div class=" col-md-8" style="float:none; margin: auto">

                        <div class="form-group">
                             <label style="color:#444; font-weight: normal">Start Time</label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>

                            <div class="form-group">
                                 <label style="color:#444; font-weight: normal">End Time</label>
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                            <div class="form-group">
                                 <label style="color:#444; font-weight: normal">Select Date</label>
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>





                        <button class="comment-btn">Confirm and Set  </button>


                       </div>


                  </form>
      </div>

    </div>
  </div>
</div> -->
<!-- <div id="fullCalModal" class="modal fade">
  <div class="modal-dialog modal-book">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> </button>
        <h4 id="modalTitle" class="modal-title">Booked</h4>
      </div>
      <div id="modalBody" class="modal-body text-center">
        <h5>This time is already booked</h5>
        <p>Please choose another Date and Time.</p>
      </div>
    </div>
  </div>
</div> -->
<div id="fullCalModal_blank" class="modal fade">
  <div class="modal-dialog modal-book modal-md">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top: -8px; right: -8px; z-index: 999; color: #ccc; opacity: 0.6; font-weight: 100"><span class="fa fa-times-circle"></span></button>
      <div class="modal-header" style="padding-bottom: 0">

        <h4 id="" class="modal-title" style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; ">Add Your Appointment</h4>
        <h5 style="font-size: 20px;font-weight: 600;color: #2aaebf;margin-bottom: 20px;">This interview session will cost £50.</h5>

      </div>

      <div id="" class="modal-body">
        <span class="error" id="appointment_error" style="text-align: center"></span>
        <div class="appointment-div">
          <form method="post" action="" name="appointment_frm" id="appointment_frm"  enctype="multipart/form-data" accept-charset="utf-8">
            <table>

              <tr>
              <tr>
                <td>Date</td>
                <td style="padding-bottom: 10px"><input type="text"  name="appointment_session_date" id="appointment_session_date1"  class="form-control" />
                    <span id="astro_date_error" class="error"></span>
                </td>
              </tr>
                <td>Package</td>
                <div style='display:none'>
                <input type="text" name="appointment_date" id="appointment_date" class="appointment_date" value="">
                 <input type="text" name="appointment_end_date" id="appointment_end_date" class="appointment_date" value="">
                <input type="text" name="avilable_start_time" id="avilable_start_time" value="">
                <input type="text" name="avilable_end_time" id="avilable_end_time" value="">
                <input type="text" name="session_time" id="session_time" value="">
                <input type="text" name="time_end" id="time_end" value="">
                <input type="text" name="from_profile_id" id="from_profile_id" value="<?=$this->session->userdata['logged_in']['user_id']?>">
                <input type="text" name="vendor_id" id="vendor_id" value="<?= $this->uri->segment(3) ?>">

                </div>
                <td style="padding-bottom: 10px"><select class="form-control" name="astro_pack" id="astro_pack">
                    <option value="" >Select</option>
                    <option value="2700">45 Min</option>
                   <!--  <option value="3600">60 Min</option> -->
                  </select>
                  <span id="astro_pack_error" class="error"></span>
                </td>
              </tr>

              <tr>
                <td>Time</td>
                <td class="time-td" style="padding-bottom: 10px">
                    <div class="input-group">
                        <div class="col-md-5" style="padding: 0">
                            <select name="time_start" id="time_start" class="form-control" >
                            <option value="">Start Time</option>
                        </select>
                        </div>
                        <div class="col-md-2">To</div>
                        <div class="col-md-5" style="padding: 0"><input type="text"  class="form-control" readonly id="end_result"></div>



                    </div>
                  <span id="time_start_error" class="error"></span>

                </td>
                <span id="time_start_error1" class="error"></span>
<!--                <td><input type="text" name="av_start_time" class="form-control timepicker" id="av_start_time" placeholder="Start Time"><input type="text" name="av_end_time" id="av_end_time" class="form-control timepicker" placeholder="End Time"> </td>-->
              </tr>
              <tr>
                <td>Message</td>
                <td style="padding-bottom: 10px"><textarea class="form-control" name="appointment_message" id="appointment_message" placeholder="Introduce yourself and outline anything in particular you are interested in"></textarea></td>
              </tr>

              <tr>
                <td>Upload your Personal Statement</td>
                <td style="padding-bottom: 10px">
                  <input type="file" name="upload_file" id="upload_file">
                   <span style="font-size: 12px; color: #2aaebf;">Mention personal statement for interview </span>
                </td>
              </tr>

            </table>
            <div class="text-right" style="margin-top:10px; padding-top: 10px; border-top:1px solid #eee"> <input type="button" name="submit_appointment" id="submit_appointment" value="Request Appointment" class="btn btn-sm btn-primary"> </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>



  <!-- Start footer -->

 <?php $this->load->view('common/footer');?>
    <!-- jQuery library -->
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>  -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>

    <!-- <script src="<?php  //echo base_url();?>assets/js/bootstrap.js"></script> -->
    <!-- Slick Slider -->
    <!-- <script type="text/javascript" src="<?php // echo base_url();?>assets/js/slick.js"></script>     -->
    <!-- mixit slider -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script> -->
    <!-- Add fancyBox -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script>
   <!-- counter -->
    <!-- <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script> -->
    <!-- Wow animation -->
   <!--  <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script>  -->
    <!-- progress bar   -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>   -->


    <script type="text/javascript" src="<?php echo base_url();?>assets/js/Super-Simple-Calendar/js/dncalendar.min.js"></script>

     <script src="<?php echo base_url();?>assets/fullcalendar/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0/js/bootstrap-datepicker.min.js"></script>
    <?php   $interviewer_id=$this->uri->segment(3); 
      if (isset($this->session->userdata['logged_in'])){
          $login_status=($this->session->userdata['logged_in']['login']);
          }else{
            $login_status='false';
          }
          ?>

    <script>

        $(document).ready(function () {
       var today = new Date();
        //alert (today);

        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        today = yyyy+'-'+mm+'-'+dd;
         var interviewer_id= '<?= $this->uri->segment(3) ?>';
var login_status = '<?php echo $login_status; ?>';

        var go_date='';


        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultView: 'agendaDDay',
            views: {
                agendaDDay: {
                    type: 'agenda',
                    duration: { days: 7 },
                    buttonText: '7 day'
                }
            },
            //defaultDate: today,
            editable: false,
            navLinks: true, // can click day/week names to navigate views
            eventLimit: true, // allow "more" link when too many events
            timeFormat: '(hh:mm A)',
            ignoreTimezone: false,
            events: {
                url: '<?php echo base_url();?>interviewer_schedule/get_available_data_ajax',
                data: {interviewer_id:interviewer_id},
                dataType: 'json',

                success: function (response) {

                },
                error: function() {
                    //$('#script-warning').show();
                }
            },
            timezone: 'local',

            eventClick:  function(event, jsEvent, view) {
                if (event.title == "Available") {
                    
                    console.log(event.appointment_date);

                    $('#fullCalModal_blank').modal();
                    $('#submit_appointment').prop('disabled', false);
                    $('#appointment_date').val(event.appointment_date);
                    $('#appointment_end_date').val(event.appointment_end);
                    
                    $('#avilable_start_time').val(event.available_start);
                    $('#avilable_end_time').val(event.available_end);
                    $('#session_time').val(event.session_time);
                    var end_date = new Date(event.appointment_date);
                    end_date.setDate(end_date.getDate() + 7);
                    var today=new Date();
                    today.setDate(today.getDate());


                    var last_7_day=new Date(event.appointment_date);
                    last_7_day.setDate(last_7_day.getDate() - 7);
                    //alert(today);alert(last_7_day);
                    if (today > last_7_day )
                    {
                        var start_date=today;
                    }
                    else
                    {
                        var start_date=last_7_day;
                    }

                    $('#appointment_session_date1').datepicker('destroy');
                    $('#appointment_session_date1').datepicker({
                        format: "dd-mm-yyyy",
                        startDate: new Date(start_date),
                        endDate:end_date,
                        autoclose: true,
                    });
                    
                    $("#appointment_session_date1").val(event.appointment_date);
                    //alert();
                    var package=$('#astro_pack').val();
        var start=$('#avilable_start_time').val();
        var end=$('#avilable_end_time').val();
        var appointment_date=$('#appointment_session_date').val();
        var session_time=$('#session_time').val();
        var vander_id='<?= $this->uri->segment(3) ?>';
                    $.ajax({
    type: "POST",
    url: "<?php echo base_url();?>interviewer_schedule/get_time_input",
    data: {package: package,start:start,end:end,appointment_date:appointment_date,vander_id:vander_id,session_time:session_time},
    dataType: "json",
    success: function (result) {
        if (result['status'] == true) {
            //$('.astro_loader').hide();
            $('#time_start').html('');
            $('#time_start').html(result['response']);
            //window.location.replace(url);
        }
        else
        {
            $('#time_start').html('');
            $('#astro_date_error').html(result['response']);
        }
    }

});
                }
//                    
            },

            eventRender: function(event, element) {
                if (event.title == "booked") {
                    var date = $.fullCalendar.formatDate(event.start, 'dd-MM-yyyy');//event.start;

                    $('.fc-day[data-date="' + date + '"]').addClass('booked');
                }
          },

        });
        });

        $(document).on("click", ".popover .close" , function(){
            $(this).parents(".popover").popover('hide');
        });

        $('#appointment_session_date1').change(function(){
           
        $('#astro_pack').val('');
        $('#time_start').empty();
        $('#time_end').val('');
        $('#end_result').val('');
        $('#appointment_date').val($(this).val());
        $('#appointment_end_date').val($(this).val());
        });

        // $('#end_time').timepicker({interval: 60});
        $('.modal').on('hidden.bs.modal', function(){
            $(this).find('form')[0].reset();
             $('#time_start').empty();
        });

        $(document).on('change','#astro_pack',function(e){
          
        var package=$('#astro_pack').val();
        var start=$('#avilable_start_time').val();
        var end=$('#avilable_end_time').val();
        var appointment_date=$('#appointment_session_date').val();
        var session_time=$('#session_time').val();
        var vander_id='<?= $this->uri->segment(3) ?>';
        //var end_start=parseInt(start)+parseInt(add);
        $('#time_end').val('');
        $('#end_result').val('');
        $('#end_result').css('border-color', '');
        $('#time_start').html('<option value="" selected disabled>loading</option>');
if(package!='' )
{$.ajax({
    type: "POST",
    url: "<?php echo base_url();?>interviewer_schedule/get_time_input",
    data: {package: package,start:start,end:end,appointment_date:appointment_date,vander_id:vander_id,session_time:session_time},
    dataType: "json",
    success: function (result) {
        if (result['status'] == true) {
            //$('.astro_loader').hide();
            $('#time_start').html('');
            $('#time_start').html(result['response']);
            //window.location.replace(url);
        }
        else
        {
            $('#time_start').html('');
            $('#astro_date_error').html(result['response']);
        }
    }

});}
  else
{
    $('#time_end').val('');
    $('#end_result').val('');
    $('#end_result').css('border-color', '');
    $('#time_start').html('');
    var desing='';
    desing+='<option value="">Loading</option>';
    $('#time_start').html(desing);
}
        
    });
    $(document).on('change','#time_start',function(e){
       var error=0;
        var start=$('#time_start').val();

        //var date=$('#appointment_date').val();
        var package=$('#astro_pack').val();
        var end=$('#time_end').val();
        var appointment_date=$('#appointment_date').val();
        var vander_id='<?= $this->uri->segment(3) ?>';
        $('#end_result').css('border-color', '');
        $('#appointment_error').html('');

        var next=$('#time_start option:selected').next().val();

        if(!next)
        {
            error=1;
            $('#time_end').val('');
            $('#end_result').val('');
            $('#appointment_error').html('Session not available for this time');
            $('#end_result').css('border-color', 'red');
            $('#time_start_error').html('Session not available for this time');

        }
        else
        {
            $('#time_end').val(next);
            $('#end_result').val('');
            $('#end_result').val(next);

        }


        //alert(next);

        if(package!='' && start!='' && error!=1 )
        {$.ajax({
        type: "POST",
            url: "<?php echo base_url();?>interviewer_schedule/check_time_already_booked",
            data: {package: package,start:start,end:next,appointment_date:appointment_date,vander_id:vander_id},
            dataType: "json",
            success: function (result) {
                if (result['status'] == true) {
                    //$('.astro_loader').hide();
                    $('#time_start_error').html('');
                    $('#time_start_error').html('Session is Already Booked');
                    //window.location.replace(url);
                }
                else
                {
                    $('#time_start_error').html('');

                }
            }

        });
    }

         });

 $('#appointment_frm').submit(function(e){
     //alert();
     e.preventDefault(); 
 
        var error=0;
        
        var plan=$('#astro_pack').val();
        var session_start_time=$('#time_start').val();

        var session_end_time=$('#time_end').val();

        var appointment_date=$('#appointment_session_date1').val();
        var message=$('#appointment_message').val();
        if(!appointment_date)
        {
            error=1;
            $('#astro_date_error_error').html('').html('Please Select appointment date');
        }
        
        if(!plan)         {
            error=1;
            $('#astro_pack_error').html('').html('Please Select Package');
        }
        
        if(!session_start_time)
        {
            error=1;
            $('#time_start_error').html('').html('Please Select Start & End Time');
        }
       
       /* if(!session_end_time)
        {
            error=1;
            $('#time_start_error').html('').html('Please Select Start & End Time');
        }*/
       
        if(error==0)
        {   
            $('#submit_appointment').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>interviewer_schedule/add_appointment",
                //data: {from_profile_id: from_profile_id,vendor_id:vendor_id,plan:plan,appointment_date:appointment_date,session_start_time:session_start_time,session_end_time:session_end_time,message:message},
                data:new FormData(this),
             processData:false,
             contentType:false,
             cache:false,
             async:false,
             //dataType: "json",
             success: function (result) {
                  // console.log(result.status,result['status']);
                    if (result.status == true) {
                       // alert();
                        $('#submit_appointment').show();
                        var url=result['redirect_url'];
                        window.location.replace(url);
                    }
                    else
                    {
                        
                        $('#appointment_error').html(result['response']);
                        $('#submit_appointment').prop('disabled', false);
                    }
                }

            });
        }

    });

    </script>
  
      
        <script>
            $(function() {
              var interviewer_id = '<?php echo $interviewer_id; ?>';
              var login_status = '<?php echo $login_status; ?>';
              //alert('hiii');
              //alert(login_status);
                $('#calendar1').fullCalendar({
                    selectable: true,
                    header: {
                      left: 'prev,next today',
                      center: 'title',
                      right: 'month,agendaWeek,agendaDay'
                    },
                    dayClick: function(date) {
                      alert('clicked ' + date.format());

                    },
                    select: function(startDate, endDate) {
                      alert('selected ' + startDate.format() + ' to ' + endDate.format());
                    },

                       eventSources: [
                    {
                             events: function(start, end, timezone, callback) {
                                 $.ajax({
                                 url: '<?php echo base_url() ?>interviewer_schedule/slot_listById/' + interviewer_id ,
                                 dataType: 'json',
                                 data: {
                                 // our hypothetical feed requires UNIX timestamps
                                 start: start.unix(),
                                 end: end.unix()
                                 },
                                 success: function(msg) {
                                  //alert(msg);
                                     var events = msg.events;
                                     callback(events);
                                 }
                                 });
                             }
                         },
                     ],
                       eventClick: function(calEvent, jsEvent, view) {

//                            alert('Event: ' + calEvent.title);
//                            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                            alert('View: ' + view.name);

//                            $(this).css('border-color', 'red');

                            //$('#schedule_id').val(calEvent.start);
                             $('#schedule_id').val(calEvent.schedule_id);
                             $('#appointment_date').val(calEvent.start);

                            $('#txt_name').val(calEvent.title);

                            $('.app-schedule').text(calEvent.schedule_id);
                            $('.app-time').text(calEvent.title);
                            $('.app-date').text(calEvent.start);
                            $('.app-desc').text(calEvent.description);
                            $('.app-color').find('i').css('color',calEvent.backgroundColor);
//                            $('#addtime').modal();
                            if(login_status==1)
                            $('#event-detail').modal();
                            else
                            $('#login-form').modal();

                            var schedule_id=calEvent.schedule_id;

                          }

                });

            });

            /*$('#event-detail').on('hidden.bs.modal', function () {
     location.reload();*/
        </script>



         <!--  Get slot time on select time   -->




    <script type="text/javascript">
      function get_slot(slot_time) {
       /* //$(document).ready(function(){

          $("#event-detail").click(function () {*/

          var schedule_id = $('#schedule_id').val();
          //var slot_time=2700;
          //alert(schedule_id);
          //alert(slot_time);
            $.ajax({
               type: 'post',
               url: '<?php echo base_url();?>interviewer_schedule/schedule_time/',

               data: {
                schedule_id: schedule_id,
                slot_time: slot_time

                 },
                success: function(response)
                {
                  alert(response);
                   jQuery('#slot_id').html(response);
                }
            });
        }

        function get_end_time(slot_id) {
          var package = $('#slot_time').val();
          //alert(schedule_id);
            $.ajax({
               type: 'post',
               url: '<?php echo base_url();?>interviewer_schedule/end_schedule_time/',

               data: {
                package: package,
                slot_id: slot_id

                 },
                success: function(response)
                {
                  $('#end_time').val(response);
                  //alert(response);

                }
            });
        }
    </script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
  </body>
</html>
