<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interviewer Bookings | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <style>
        .sidebar-widget{float:none; display: block;}
        .team-member-name{float:none; display: block;}
        .single-team-member{float:none; display: block;}
        .bookings tr td{color:#666}
          .bookings tr th{color:#666}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
   $this->load->view('common/subheader');
  ?>
  <!-- END MENU --> 

  <!-- end about -->
 
  
<section id="testimonial" style='background: #f6fef7; padding-top: 0'>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding: 0">
          <div class="row">
              <br>

                  <div class="col-md-12" style="padding: 0" >
                      <div style="margin-top: 0px">

     

    <div class="contact-area-right" style="margin-top: 0px">
 
      <?php $this->load->view('common/interviewer_sidebar');?>

         <div class=" col-md-9 col-sm-6 col-xs-12"> 
             <div class="content-wrapper">
                <h4 style="color:#2aaebf; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Bookings</h4>
           <div class="" style="padding-top: 00px; background: #fff">
                   <form action="" class="comments-form contact-form">
                       <div class=" col-md-12 mobilenopadding mobileNoPadding" style="float:none; margin: auto">
                       <div class="table-responsive">
                           
                           <table id="bookings-1" class="display bookings" style="width:100%">
                            <thead>
                                <tr>
                                    <th> Applicant</th>
                                    <th>Appointment Date</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                     <th>Personal Statement</th> 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($appointment as $row) {
                                $id=$row->student_id;
                                $result=$this->Common_model-> getDataByID($table='users',$fname='user_id',$id);

                                ?>
                                   
                                <tr>
                                    <td>
                                         <?php $image= $result[0]->profile_image; if($image=='')
                   /* echo '<img src="'.base_url().'assets/images/main-testimonials-img-2.png" alt="team member img">';*/
                  echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="height: 28px;width: 5opx;">';
                  
                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="height: 28px;width: 5opx;">';
                     ?>
                                        <!-- <img src="<?php echo base_url();?>assets/images/team-member-2.png" height="36" class="img img-circle"/> -->
                                        <?php echo $result[0]->first_name;
                                        // echo ucwords($row->first_name);?>
                                    </td>
                                    <td>
                                        <i class="fa fa-calendar"></i>  <?php echo date('d-m-Y', strtotime($row->booking_date));?>
                                    </td>
                                    <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $row->start_time?>
                                    </td>
                                     <td>
                                        <i class="fa fa-clock-o"></i> <?php echo $row->end_time;?>
                                    </td>
                                     <td>
                                      <?php if($row->attachment) { 
                                        /*echo 'yes'; } else { echo 'no';}*/ ?>
                                         <a href="<?php  echo base_url().'uploads/attachment/'.$row->attachment;?>" target="_blank" download><i class="fa fa-file-text-o" style="font-size:24px;margin-left: 50px;"></i></a> 
                                        <?php  } ?>
                                    </td>
                                    <td>
                                        <!-- <button class="btn btn-sm btn-info"><i class="fa fa-phone"></i> Call</button> -->
                                        <a href="<?= base_url().'session/student/'.$row->room_name;?>" class="btn btn-sm btn-success"><i class="fa fa-phone"></i> </a><?=$row->call_status?></a>
                                    </td>
                                </tr>
                            <?php }?>
                           
                             
                              
                              
                           
                             
                            </tbody>
                           </table>
                       </div>
                       </div>
                      
                   
                  </form>
                 </div>
      
          
             
     
     
         
         </div>
    </form>
  </div>
  </div>
 
                 </div>
         
              </div>
          </div>
        </div>
             
      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
     <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
          <script>
      
        $(document).ready(function() {
            $('#bookings-1').DataTable({
                searching:false,
                sorting:false
            });
           
        } );
    </script>
  </body>
</html>