<!doctype html>
<html lang="en">

    <head>
        <title>Home | Gulf Tour</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link rel="stylesheet" href="<?= base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/vendor/linearicons/style.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/vendor/chartist/css/chartist-custom.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/scripts/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?= base_url();?>assets/css/demo.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
        <!-- ICONS -->
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url();?>assets/img/apple-icon.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url();?>assets/img/favicon.png">

        <style>

            .mid-title{background:#f4f8f9; font-size: 13px; padding: 3px}
            .well{background:#f4f8f9; box-shadow: none; border:0}
            .modal-body{padding: 20px;}
            table tr th{padding: 2px 5px !important }
            table tr td{padding: 2px 5px !important }
            .table tr td .btn{padding: 0px 2px; }
            .report-wrapper{border:1px solid #d4d4d4; background: #fff;  padding: 20px; font-size: 13px;}
            .report-subhead{font-weight: bold; background: #2aaebf; padding: 5px; text-align: center; color:#fff}
            .report-row{background:rgba(9,120,120,0.1); color:#097878;}
            .report-maintitle{color:#2aaebf ; margin-top: 0 ; font-weight: 800 ; margin-bottom: 0 }
            .width-100{width: 100%}
            .width-50{width: 50%}
            .border-eee{border:1px solid #eee }
            .float-left{float: left }
            .border-1 tr td{border:1px solid #eee }
            .margin-0{margin: 0 }
            .table-head tr td p{color:#444 !important}
            .table-head tr td:first-of-type {width: 180px;}
        </style>

        <style media="print">
            @media print {

                .mid-title{background:#f4f8f9; font-size: 13px; padding: 3px}
                .well{background:#f4f8f9; box-shadow: none; border:0}
                 table tr th{padding: 2px 5px !important; font-size: 12px !important }
                 table tr td{padding: 2px 5px !important; font-size: 12px !important }
                .table tr td .btn{padding: 0px 2px; }
                .report-wrapper{border:1px solid #d4d4d4 !important; color:#333 !important; background: #fff !important; padding: 20px !important; font-size: 12px !important; font-family:  'Arial', 'Calibri', 'Time New Roman' !important}
                .report-subhead{font-weight: bold !important; background: #2aaebf !important; padding: 5px !important; text-align: center !important; color:#fff !important}
                .report-maintitle{color:#2aaebf !important; margin-top: 0 !important;  font-size: 18px !important; font-weight: 800 !important; margin-bottom: 0 !important}
                .width-100{width: 100% !important}
                .width-50{width: 50% !important}
                .border-eee{border:1px solid #eee !important}
                .float-left{float: left !important}
                .text-center{text-align: center !important}
                .text-left{text-align: left !important}
                .text-right{text-align: right !important}
                .border-1 tr td{border:1px solid #eee !important}
                .margin-0{margin: 0 !important}
                .table-head tr td p{color:#444 !important; font-size: 11px !important}
                .table-head tr td:first-of-type {width: 180px !important;}
                .report-row{background:rgba(9,120,120,0.1) !important; color:#097878 !important}
            }
        </style>
    </head>

    <body>


        <div class="report-wrapper" >
            <table class="width-100 table-head">
                <tr>
                    <!-- <td>
                         <img src="<?= base_url();?>assets/images/main-testimonials-img-2.png" height="60"/> 
                        <?php  echo $image= $feedback[0]->profile_image; if($image=='')
                            echo '<img  class="int-img" src="'.base_url().'assets/images/default.png" alt="team member img" height="60">';
                    else
                       echo '<img  class="int-img" src="'.base_url().$image.'" alt="team member img" height="60"> '; 
                     ?>
                        
                    </td> -->
                    <td><h3 class="report-maintitle">Feedback By</h3>
                        <p class="margin-0"><b><?php echo ucwords($feedback[0]->first_name);?></b></p>
                        <p class="margin-0"><?php echo ucwords($feedback[0]->college_name);?></p>
                        <p class="margin-0"><?php echo ucwords($feedback[0]->university_name);?> </p>
                    </td>
                    
                </tr>
            </table>
            <br>
            <table class="width-100"><tr><td class="report-subhead">Feedback Points</td></tr></table>
            <br>
            <br>

          

          
           
          
            <table  class="width-100">
                <tr>
                    <td>
                        <p><b>1. Questions and content covered in the interview.</b></p>
                        <div><p><?php echo $feedback[0]->comments;?></p></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><b>2. Area in which the applicant performed well (this can be both knowledge and interview techniques).</b></p>
                        <div><p><?php echo $feedback[0]->comments2;?></p></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><b>3. Areas to improve upon: (Knowledge, including any recommended readings here)</b></p>
                        <div><p><?php echo $feedback[0]->comments3;?></p></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><b>4. Areas to improve upon: (Interview techniques)</b></p>
                        <div><p><?php echo $feedback[0]->comments4;?></p></div>
                    </td>
                </tr>
                <tr>
                    <td ><br></td>
                </tr>



            </table>
        </div>



        <!-- END WRAPPER -->


        <!-- Javascript -->
        <script src="<?= base_url();?>assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    </body>

</html>
