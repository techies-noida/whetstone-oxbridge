<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interviewer Reviews | Whetstone Oxbridge</title>
     <?php $this->load->view('common/header_assets');?>
    <style>
        .sidebar-widget{float:none; display: block;}
        .team-member-name{float:none; display: block;}
        .single-team-member{float:none; display: block;}
          .reviews{color:#888}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');
   $this->load->view('common/subheader');
  ?>
  <!-- END MENU -->

  <!-- end about -->


<section id="testimonial" style='background: #f6fef7; padding-top: 0'>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="padding: 0">
          <div class="container">
          <div class="row">

              <br>

                  <div class="col-md-12" style="padding: 0" >
                      <div style="margin-top: 0px">



    <div class="contact-area-right" style="margin-top: 0px">

         <?php  $this->load->view('common/interviewer_sidebar');?>
         <div class=" col-md-9 col-sm-6 col-xs-12">
             <div class="content-wrapper">
                <h4 style="color:#2aaebf !important; border-bottom: 1px solid #eee; margin-bottom: 20px; padding-bottom: 20px; font-weight: bold;">Reviews</h4>
                   <div class="reviews">
                    <div class="row">
                      <?php
                        $size=count($reviews);
                        for($i=0;$i<$size;$i++)
                        { ?>
                          <div class="col-md-4">
                                <blockquote>
                                    <div class="">
                                          <?php if($reviews[$i]->rating === 0){
                                               for($j=0;$j<5;$j++){
                                                echo ' <i class="fa fa-star-o" style="color:#f7c80a"></i>';
                                               }
                                              }else {
                                                $norating=5-$reviews[$i]->rating;
                                                for($j=0;$j<$reviews[$i]->rating;$j++)
                                                {
                                                  echo'<i class="fa fa-star" style="color:#f7c80a"></i>';
                                                }
                                                for($k=0;$k<$norating;$k++)
                                                {
                                                  echo'<i class="fa fa-star-o" style="color:#f7c80a"></i>';
                                                }
                                              } ?>
                                    </div> 
                                    <p>"<?= $reviews[$i]->comments;?>"</p>
                                    <span><?= ucwords($reviews[$i]->first_name);?></span>
                                </blockquote>
                          </div>
                      <?php }?>

                    </div>


                   </div>



         </div>
    </form>

  </div>
  </div>
  </div> <!--student form ends-->
</div>
                 </div>
               </div>

              </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- End Service -->
  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
  </body>
</html>
