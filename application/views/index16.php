<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <?php $this->load->view('common/header_assets');?>
    <style>
        .single-team-member > p{min-height: 160px; max-height: 160px; overflow: hidden}
    </style>
  </head>
  <body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  
  <!-- Start slider -->
  <section id="slider">
    <div class="main-slider">
      <div class="single-slide">
        <img src="<?php echo base_url();?>assets/images/slider-1.jpg" alt="img">
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center">
                <div class="slide-article text-center">
                  <h1 class="wow fadeInUp text-center" style="color:#2aaebf" data-wow-duration="0.5s" data-wow-delay="0.5s">Oxbridge Interview Practice</h1>
                  <p class="wow fadeInUp text-center" style="font-size:19px; margin-bottom: 90px" data-wow-duration="0.5s" data-wow-delay="0.75s">The confidence to know that you will perform to the best of your ability.</p>
                  <a class="read-more-btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" href="#about">Read More</a>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="single-slide">
        <img src="<?php echo base_url();?>assets/images/slider-3.jpg" alt="img" style>
        <div class="slide-content">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center">
                <div class="slide-article text-center">
                  <h1 class="wow fadeInUp text-center" style="color:#2aaebf" data-wow-duration="0.5s" data-wow-delay="0.5s">Unparalleled Student Network</h1>
                  <p class="wow fadeInUp text-center" style="font-size:19px; margin-bottom: 90px" data-wow-duration="0.5s" data-wow-delay="0.75s">From Oxbridge students to school students…</p>
                  <a class="read-more-btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s" href="#about">Read More</a>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="slider-img wow fadeInUp">
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>          
    </div>
  </section>
  <!-- End slider -->
  
  <!-- Start about  -->
  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">What we do</h2>
            <span class="line"></span>
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="about-content">
            <div class="row">
              <div class="col-md-6">
                <div class="our-skill">
                               
                  <div class="our-skill-content">
                  <p>Oxbridge interviews are constantly changing as competition for these two leading universities ever increases.
Whetstone is an Oxbridge applications advisory guaranteeing the highest quality interview practice with successful interviewees currently studying at Oxford and Cambridge.<br> </p><p>We have found that the advice given to applicants all too often comes from graduates and reflects the out-of-date interview experience that they had. Therefore, Whetstone has formed a network of current students covering Oxford and Cambridge who are dedicated to properly prepareapplicants for what is going on behind the interview doors right now. 
</p>
                  
                    <a class="read-more-btn wow fadeInUp" style="border:1px solid #2aae8f; color:#2aae8f" data-wow-duration="1s" data-wow-delay="1s" href="<?php echo base_url();?>about-us">More Info</a>
                  </div>                  
                </div>
              </div>
              <div class="col-md-6">
             <!--   <div class="our-skill">
                  <p>% of students placed</p>
                  <div class="our-skill-content">
                    <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="70">
                        <span class="progress-title">Cambridge</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="85">
                        <span class="progress-title">Oxford</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="70">
                        <span class="progress-title">Velley</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="60">
                        <span class="progress-title">MacLarn</span>
                      </div>
                  </div>
                  <div class="progress">
                      <div class="progress-bar six-sec-ease-in-out" role="progressbar" data-transitiongoal="40">
                        <span class="progress-title">The Khan's</span>
                      </div>
                  </div>
                 
                  </div>       -->
                 
               
            
                  <img src="<?php echo base_url();?>assets/images/map.png" class="img img-responsive">
                      <div class="single-feature wow zoomIn">
                  <i class="fa fa-thumbs-o-up feature-icon"></i>
                  <h4 class="feat-title">Internationally trusted </h4>
                  <p>Our network of current Oxbridge students have successfully helped applicants from these countries. </p>
                </div>         
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end about -->
<section id="testimonial">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
                <h2 class="title" style='color:#fff'>How it works</h2>
                <span class="line"></span>           
              </div>
            </div>
            <div class="col-md-12">
              <!-- Start testimonial slider -->
              <div class="testimonial-slider">
                <!-- Start single slider -->
                <div class="single-slider">
                  <div class="testimonial-img">
                    <img src="<?php echo base_url();?>assets/images/testi1.jpg" alt="testimonial image">
                  </div>
                  <div class="testimonial-content">
                    <p>Register with Oxbridge, Choose an interviewer and Schedule an interview with.</p>
                    <h6><span>1</span></h6>
                  </div>
                </div>
                <!-- Start single slider -->
                <div class="single-slider">
                  <div class="testimonial-img">
                    <img src="<?php echo base_url();?>assets/images/testi3.jpg" alt="testimonial image">
                  </div>
                  <div class="testimonial-content">
                    <p>Pay securely online and get ready to a session.</p>
                    <h6><span>2</span></h6>
                  </div>
                </div>
                <!-- Start single slider -->
                <div class="single-slider">
                  <div class="testimonial-img">
                    <img src="<?php echo base_url();?>assets/images/testi2.jpg" alt="testimonial image">
                  </div>
                  <div class="testimonial-content">
                    <p>Join the session, interact and receive valuable feedback.</p>
                    <h6><span>3</span></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6"></div>        
      </div>
    </div>
  </section>
  <!-- Start Feature -->
  <section id="feature">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Why Choose Us?</h2>
            <span class="line"></span>
            <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="feature-content">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-desktop feature-icon"></i>
                  <h4 class="feat-title">Accessible </h4>
                  <p>Our affordable online tool enables applicants to hone and sharpen their interview skills anytime and anywhere. There is no need to come to London and pay large sums of money to attend a single information crammed interview prep day. Instead applicants can choose from hundreds of current Oxbridge students available through  Whetstone and practice at their own rate.</p>
                </div>
              </div>
               <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-star feature-icon"></i>
                  <h4 class="feat-title">High Quality Feedback </h4>
                  <p>At the end of your practice interview you will receive the video of yourself during the interview with real time feedback comments. You will also have a 15 minute de-brief in which your interviewer will outline where you need to improve. You can then take their notes and the video and go over it in your own time before coming back for another practice.</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-support feature-icon"></i>
                  <h4 class="feat-title">Additional Support </h4>
                  <p>We work with schools and teachers to provide them with additional support in their students' applications. Schools can only provide a limited number of practice interviews. We encourage teachers to help their students use Whetstone Oxbridge as another preparation tool. If requested, we send interview feedback to both teacher and student so that they can work on the necessary improvements together.</p>
                </div>
              </div>
             
                <div class="clearfix"></div>
                
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-gears feature-icon"></i>
                  <h4 class="feat-title">Largest student network </h4>
                 <p> You can select interviewers based on university, course and even college. Spanning all courses and colleges, at both Oxford and Cambridge, we will help connect you with students who successfully went through the exact same interviews that you will be taking.</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-bank feature-icon"></i>
                  <h4 class="feat-title">Current students </h4>
                  <p>We only employ current Oxbridge students. Results have shown that this leads to greater chances of success because no one can shed better light on the nature of the Oxbridge interview at the moment than those who successfully completed the process only a year or two before you.</p>
                </div>
              </div>
              <div class="col-md-4 col-sm-6">
                <div class="single-feature wow zoomIn">
                  <i class="fa fa-smile-o feature-icon"></i>
                  <h4 class="feat-title">Practice makes Perfect </h4>
                  <p>We offer practice interview packages which enable you to mix-and-match and practice with as many different Oxbridge students as you like to get experience with different interview styles and questions.</p>
                </div>
              </div>
                
             
                
            </div>
          </div>
        </div>
      </div>
        
        <div class="row">
            
<!--      <div class="map-container">
        <div id="worldmap" style="width: 600px; height: 400px"></div>
    </div>-->
        </div>
        
    </div>
  </section>
  <!-- End Feature -->


  <!-- Start counter -->
  <section id="counter">
    <div class="counter-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="counter-area">
              <div class="row">
                <!-- Start single counter -->
                <div class="col-md-4 col-sm-6">
                  <div class="single-counter">
                    <div class="counter-icon">
                      <i class="fa fa-suitcase"></i>
                    </div>
                    <div class="counter-no counter">
                     <?php echo $this->db->where('user_role_id',2)->count_all_results('users');?>
                    </div>
                    <div class="counter-label">
                      Interviewers
                    </div>
                  </div>
                </div>
                <!-- End single counter -->
                <!-- Start single counter -->
                <div class="col-md-4 col-sm-6">
                  <div class="single-counter">
                    <div class="counter-icon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="counter-no counter">
                      <?php echo $this->db->count_all_results('appointment');?>
                    </div>
                    <div class="counter-label">
                      Interviews
                    </div>
                  </div>
                </div>
                <!-- End single counter -->
                <!-- Start single counter -->
                <div class="col-md-4 col-sm-6">
                 <div class="single-counter">
                    <div class="counter-icon">
                      <i class="fa fa-users"></i>
                    </div>
                    <div class="counter-no counter">
                    <?php echo $this->db->where('user_role_id',3)->count_all_results('users');?>
                    </div>
                    <div class="counter-label">
                      Student
                    </div>
                  </div>
                </div>
                <!-- End single counter -->
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End counter -->
  
  <!-- 
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Top Ranking Interviewers</h2>
            <span class="line"></span>
          
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
            
               <?php foreach ($interviewer as $row) {?>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   
                    <?php $image=$row->profile_image;
                     if($image=='')
                    echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="filter:grayscale(100%)"  style="filter:grayscale(100%)">';
                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="filter:grayscale(100%)">';
                     ?>
                 </div>
                 <div class="team-member-name">
                 
                   <p><?php echo $row->first_name.''.$row->last_name;?></p>
                   <span><?php echo $row->university_name;?></span>
                 </div>
                 <?php echo '<p>'.$row->about_us.'</p>';?>
                 <div class="team-member-link">
                   <a href="<?php echo base_url().'interviewer/profile/'.$row->user_id;?>">View</a>
                   
                 </div>
                </div>
              </div>
            <?php }?>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
   -->


  <!-- Start Service -->
  <section id="service">
    <div class="container-fluid">
      
        <div class="row">
            <div class="side">
                <div class="side-content">
                    <div>
                        <h4 style="color:#fff; opacity: 0.5">Whetstone for schools</h4>
                        <h2><span style="color: #ffffff;">We are proud to be trusted by top performing schools not only in the UK, but across the world.</span></h2>
                    <a itemprop="url" href="<?php echo base_url();?>schools" target="_self" style="color: #ffffff" class="eltdf-btn eltdf-btn-medium eltdf-btn-simple eltdf-btn-arrow">
                        <span class="eltdf-btn-text">Read More</span>
                    </a>		
                    </div>
                </div>
                <div class="side-img" style="background-image: url(<?php echo base_url();?>assets/images/become-a-teacher-slider.jpg)">
                </div>
            </div>
      </div>
    </div>
  </section>
  <!-- End Service -->



<section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title">Contact Us</h2>
              <span class="line"></span>
            </div>
         </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               <div class="col-md-5">
                 <div class="contact-area-left">
                   <h4>Contact Info</h4>
                   <p>If you would like to learn more about the support we offer, or if you have any questions or queries please contact us:</p>
                   <br>
                   <table style="width: 100%; margin-top: 10px">
                       <tr>
                           <td style="vertical-align:top"> <div style="max-height: 140px;overflow: hidden;"> 
                                   <img src='<?php echo base_url();?>assets/images/blog-img-1.jpg' height="200"/> </div></td>
                           <td style="vertical-align:top; padding-left: 20px">
                                <address class="single-address">
                    
                                <p style="color:#333; font-weight: bold; font-size:21px;">Fenella Chesterfield</p>
                                <p>info@whetstone-oxbridge.com</p>
                                <p></p>
                              </address> 
                           </td>
                       </tr>
                   </table>
                  
                              
                 </div>
               </div>
               <div class="col-md-7">
                 <!-- <div class="contact-area-right">
                   <form action="" class="comments-form contact-form">
                    <div class="form-group">                        
                      <input type="text" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">                        
                      <input type="email" class="form-control" placeholder="Email">
                    </div>
                   
                    <div class="form-group">                        
                      <textarea placeholder="Comment" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">                        
                   <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a style="color:#888" target="_blank" href="<?php echo base_url();?>privacy-policy">GDPR Policy</a>
                    </div>
                        <button class="comment-btn">Submit</button>
                  </form>
                 </div> -->

                 <div class="contact-area-right">
                  <div id="contact_message"></div>
                   <form action="" id="contact_us" class="comments-form contact-form">
                    <div class="form-group">                        
                      <input type="text" name="contact_name" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">                        
                      <input type="email" name="contact_email" class="form-control" placeholder="Email">
                    </div>
                   
                    <div class="form-group">                        
                      <textarea placeholder="Comment"  name="contact_comment"rows="3" class="form-control"></textarea>
                    </div>
                   <!--  <div class="form-group">                        
                      <input type="checkbox" name="agree_terms" style='height: auto'> I agree with GDPR Policy
                    </div> -->
                     <div class="form-group">                        
                   <input name="agree_terms" style="height: auto;" type="checkbox"> I agree with <a style="color:#888" target="_blank" href="<?php echo base_url();?>privacy-policy">GDPR Policy</a>
                    </div>
                        <button class="comment-btn" type="submit">Submit</button>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  
  <!-- Start subscribe us -->
 <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer'); ?>
  <script type="text/javascript">
    $(function myFunction(){
  
    $('#contact_us').validate({
        rules:{
            contact_name:{
                required:true,
                minlength:4,
                maxlength:60,
               
            },
            contact_email:{
                required:true,
                email:true,
            },
            contact_comment:{
                required:true,
                 minlength: 6,
                maxlength: 650,
            },
            agree_terms:{
                required:true,
                
            },
            
        },
        messages:{
            contact_name:{
                required:"  Please enter  name ",
                minlength:"Please enter name minmum 4 character",
                maxlength:"Name should not more than 60 character",
                
            },

            contact_email:{
                required:"  Please enter email address ",
                email:"Please enter valid email id ",
            },
            contact_comment:{
                required: "Please enter comment",
                minlength: "Please write comment minmum 15 characters",
                maxlength:"Comment should not more than 650 characters",


            },
            agree_terms:{
                required: "Please check I agree with GDPR Policy",
               


            },
            
        },
        unhighlight: function (element) {

            $(element).parent().removeClass('has_error')
        },
        submitHandler: function(form) {
            //form.submit();

           $.post('<?=base_url()?>oxbridge/contact_us', 
            $('#contact_us').serialize(), 
            function(data){
          
                if(data == 1)
                { 
                     responseText = '<span style="color:green;font-size: 16px;font-weight: normal;margin-left: 40px;">Thank you for contact us, Whetstone Oxbridge Team will reply soon</span>';
                    $("#contact_message").html(responseText);
                }
                 if(data == 0)
                {
                  responseText = '<span style="color:red;font-size: 16px;font-weight: normal;margin-left: 40px;">Your mail id already exist</span>';
                  //$('#message1').text('Your email Id already exist');
                  $("#contact_message").html(responseText);
                }
               
                

            });
       }
   });
});
  </script>
  </body>
</html>