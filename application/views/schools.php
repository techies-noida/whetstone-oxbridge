<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>School | Whetstone Oxbridge</title>
  <?php $this->load->view('common/header_assets'); ?>
</head>
<body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU -->

  <!-- Start single page header -->
  <section id="single-page-header">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Schools</h2>
              <p>We are proud to be trusted by top performing schools not only in the UK, but across the world.</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li class="active">Schools</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->

  <!-- Start Feature -->
  <section id="service" style="background: #fff; padding: 60px 0">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Interview practice</h2>
            <span class="line"></span>
          </div>
        </div>
        <div class="col-md-12">
          <div class="service-content">
            <div class="row">
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6">
                <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; margin-top: 20px;">
                  <i class="fa fa-check service-icon"></i>
                  <?php  echo $school[0]->body_1;?>
                </div>
                <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
                  <i class="fa fa-check service-icon"></i>
                  <?php  echo $school[0]->body_2;?>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
              	<img src="<?php echo base_url() . $school[0]->image_1;?>" class="img img-responsive">
              </div>
              <!-- End single service -->
              <div class="clearfix"></div>
              <!-- Start single service -->
              <div class="col-md-6 col-sm-6 col-sm-push-6">
               <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; margin-top: 40px;">
                <i class="fa fa-check service-icon"></i>
                <?php  echo $school[0]->body_3;?>
              </div>
              <div class="single-service wow zoomIn" style="visibility: visible; animation-name: zoomIn; ">
                <i class="fa fa-check service-icon"></i>
                <?php  echo $school[0]->body_4;?>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-sm-pull-6">
             <img src="<?php echo base_url() . $school[0]->image_2;?>" class="img img-responsive">
           </div>
           <!-- End single service -->
         </div>
         <div class="row">
          <!-- Start single service -->
          <div class="col-md-6 col-sm-6">

          </div>
          <!-- End single service -->
          <!-- Start single service -->
          <div class="col-md-6 col-sm-6">

          </div>
          <!-- End single service -->
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="title-area">
        <?php  echo $school[0]->footer_text;?>
      </div>
    </div>
  </div>
</div>
</section>
<!-- End Feature -->
<section id="latest-news" style="background-image: url(<?php echo base_url();?>assets/images/Bridgeofsighs.jpg); background-size: cover;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title-area">
          <h2 class="title" style="color:#fff"><?php  echo $school[0]->center_body;?></h2>
          <span class="line"></span>
          <p style="color:#fff; font-size: 16px; font-weight: bold"><?php  echo $school[0]->main_title;?></p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="latest-news-content">
          <div class="row">
            <!-- start single latest news -->
           <!--   <div class="col-md-4" style='border-right: 1px solid #ccc'>
                <article class="blog-news-single">
                  <div class="blog-news-img">
                    <a href="blog-single-with-right-sidebar.html"><img src="<?php echo base_url();?>assets/images/blog-img-1.jpg" alt="image"></a>
                  </div>
                  <div class="blog-news-title">
                    <h2><a href="blog-single-with-right-sidebar.html">Whetstone Roadshow 2019</a></h2>
                    <p>By <a class="blog-author" href="#">John Powell</a> <span class="blog-date">|10 Jan 2019</span></p>
                  </div>
                  <div class="blog-news-details">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                    <a class="blog-more-btn" href="">Read More <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </article>
              </div> -->
              <!-- start single latest news -->
              <!-- start single latest news -->
              <div class="col-md-12">
               <div class="contact-area-left text-center">
                 <ul style="list-style-image: url(<?php echo base_url();?>assets/images/check.png)">
                   <?php  echo $school[0]->body_5;?>
                 </ul>
                   <!--<h4>Upcoming</h4>-->



                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </section>

   <section id="contact" style='padding-top: 40px'>
     <div class="container">
       <div class="row">
        <div class="title-area">
          <h2 class="title">Register Interest</h2>
          <span class="line"></span>

        </div>
        <div class="col-md-12">
         <div class="cotact-area">
           <div class="row">

             <div class="col-md-6" style='float: none; margin: auto'>
               <div class="contact-area-right">
                 
                 <form action="" id="intrested_user"class="comments-form contact-form">
                  <div class="form-group">
                    <input type="text"  name="interested_user_name"class="form-control" placeholder="Your Name">
                  </div>
                  <div class="form-group">
                    <input type="text"  name="school_name"class="form-control" placeholder="School Name">
                  </div>
                  <div class="form-group">
                    <input type="email" name="interested_user_email" class="form-control" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" name="interested_user_phone" class="form-control" placeholder="Phone Number">
                  </div>

                  <div class="form-group">
                    <textarea placeholder="Message" name="interested_user_message" rows="2" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    Interested in
                    <input type="checkbox" name="interview"   value ="interviewer" style="height: auto"> Interview Practice
                    <input type="checkbox" name="roadshow"  value ="roadshow" style="height: auto"> Roadshow
                  </div>

                  <div class="col-lg-12" style=" padding: 0px">
                   <div class="form-group" style="color:#; margin-top: 10px">
                    <input name="terms_condition" style="height: auto;" type="checkbox"> I agree with <a href="/privacy-policy" target="_blank" style="color:#;">GDPR Policy</a>.
                  </div>
                </div>
                <div style="display:none" class="loading">
                  <img src="<?php echo base_url();?>assets/images/loader.gif"  style="margin-left: 100px;" />
                </div>
                <div id="intrested_user_message"></div>
                <br>
                <button class="comment-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<!-- Start subscribe us -->
<?php  $this->load->view('common/newsletter');?>
<!-- End subscribe us -->

<?php $this->load->view('common/footer');?>

<script type="text/javascript">
  $(function myFunction(){

    $.validator.addMethod(
      "regexinname",
      function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
      },
      "Please enter valid name"
      );

    $('#intrested_user').validate({
      rules:{
        interested_user_name:{
          required:true,
          minlength:3,
          maxlength:25,
          regexinname: "^[A-Za-z ]+$",

        },
        school_name:{
          minlength:5,
          maxlength:35,
          regexschool: "^[A-Za-z ]+$",
        },
        interested_user_email:{
          required:true,
          email:true,
        },
        interested_user_message:{
          required:true,
          minlength: 15,
          maxlength: 600,
        },
        interested_user_phone:{
          required:true,
          digits: true,
          minlength: 11,
          maxlength:11,
        },
        // interview: {
        //   required: true,
        // },
        // roadshow: {
        //   required: true,
        // },
        terms_condition: {
          required: true,
        },

      },
      messages:{
        interested_user_name:{
          required:"  Please enter  name ",
          minlength:"Please enter name minimum 3 character",
          maxlength:"Name should not more than 25 character",

        },

        school_name:{
          minlength:"School name should be minimum 4 character",
          maxlength:"School name should not more than 35 character",
        },

        interested_user_email:{
          required:"  Please enter email address ",
          email:"Please enter valid email address ",
        },
        interested_user_message:{
          required: "Please enter your messages",
          minlength: "Please write messages minimum 10 characters",
          maxlength:"Messages should not more than 600 characters",


        },
        interested_user_phone:{
          required: "Please enter phone number",
          digits: "Please enter valid phone number",
          minlength: "Please enter 11 digit phone number",
          maxlength:"Please enter only 11 digit phone number",


        },

       //  interview: {
       //   required: "Please select",
       // },
       // roadshow: {
       //   required: "Please select",
       // },
       terms_condition: {
         required: "Please accept GDPR Policy",
       },

     },
     unhighlight: function (element) {

      $(element).parent().removeClass('has_error')
    },
    submitHandler: function(form) {
            //form.submit();
            $(".loading").show();
            $.post('<?=base_url()?>oxbridge/interested_user',
              $('#intrested_user').serialize(),

              function(data){
               //alert(data);
               if(data == 1)
               {
                $(".loading").hide();
                responseText = '<span style="color:green;font-size: 14px;font-weight: normal;">Thank you very much for submitting your interest in, a member of our team will be in touch with you very soon to discuss your requirements and options.</span>';
                $("#intrested_user_message").html(responseText);
                $("#intrested_user")[0].reset();
                     //$("#intrested_user").resetForm();
                   }
                   if(data == 0)
                   {
                     $(".loading").hide();
                     responseText = '<span style="color:red;font-size: 16px;font-weight: normal;">Your have already  requested </span>';
                  //$('#message1').text('Your email Id already exist');
                  $("#intrested_user_message").html(responseText);
                   //$("#intrested_user").resetForm();
                 }



               });

            setTimeout(function(){
              $("#intrested_user_message").html('');
            }, 30000);
          }
        });
  });

</script>

</body>
</html>
