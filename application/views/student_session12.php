<!DOCTYPE html>
<?php $room= $this->uri->segment('3'); ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student session </title>
     <?php $this->load->view('common/header_assets');?>
    
    <link href="<?php echo base_url();?>assets/js/Monthly-Event-Calendar-pbcalendar/pb.calendar.css" rel="stylesheet">
    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>    
   <link rel="stylesheet" href="<?=base_url()?>assets/css/index.css">
    <style>
        .quicktext a{padding: 5px; font-size:14px; margin-top: 5px; text-transform:none;}
        .pb-calendar .schedule-dot-item.blue{
			background-color: blue;
		}

		.pb-calendar .schedule-dot-item.red{
			background-color: red;
		}

		.pb-calendar .schedule-dot-item.green{
			background-color: green;
		}
              .rating {
    overflow: hidden;
    display: inline-block;
}

.rating-input {
    float: right;
    width: 16px;
    height: 16px;
    padding: 0;
    margin: 0 0 0 -16px;
    opacity: 0;
}


.rating-star:hover {
    background-position: 0 0;
}

.rating-star {
    position: relative;
    float: right;
    display: block;
    width: 16px;
    height: 16px;
    background: url('<?php echo base_url();?>assets/images/star.png') 0 -16px;
}

.rating:hover .rating-star:hover,
.rating:hover .rating-star:hover ~ .rating-star,
.rating-input:checked ~ .rating-star {
    background-position: 0 0;
}

.rating-star,
.rating:hover .rating-star {
    position: relative;
    float: right;
    display: block;
    width: 16px;
    height: 16px;
    background: url('<?php echo base_url();?>assets/images/star.png') 0 -16px;
}

.quicktext a{padding: 5px; font-size:14px; margin-bottom: 5px; text-transform:none;}
#feedback-form{visibility: hidden;}



/* rating css*/
.text-center {text-align:center;}

a {
  color: tomato;
  text-decoration: none;
}

a:hover {
  color: #2196f3;
}

pre {
display: block;
padding: 9.5px;
margin: 0 0 10px;
font-size: 13px;
line-height: 1.42857143;
color: #333;
word-break: break-all;
word-wrap: break-word;
background-color: #F5F5F5;
border: 1px solid #CCC;
border-radius: 4px;
}

.header {
  padding:20px 0;
  position:relative;
  margin-bottom:10px;
  
}

.header:after {
  content:"";
  display:block;
  height:1px;
  background:#eee;
  position:absolute; 
  left:30%; right:30%;
}

.header h2 {
  font-size:3em;
  font-weight:300;
  margin-bottom:0.2em;
}

.header p {
  font-size:14px;
}



#a-footer {
  margin: 20px 0;
}

.new-react-version {
  padding: 20px 20px;
  border: 1px solid #eee;
  border-radius: 20px;
  box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);
  
  text-align: center;
  font-size: 14px;
  line-height: 1.7;
}

.new-react-version .react-svg-logo {
  text-align: center;
  max-width: 60px;
  margin: 20px auto;
  margin-top: 0;
}





.success-box {
  margin:50px 0;
  padding:10px 10px;
  border:1px solid #eee;
  background:#f9f9f9;
}

.success-box img {
  margin-right:10px;
  display:inline-block;
  vertical-align:top;
}

.success-box > div {
  vertical-align:top;
  display:inline-block;
  color:#888;
}



/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}
    </style>
    <style>
        /* chat panel ui */
.inbox_people {
  float: left;
  overflow: hidden;
  width: 24%; border-right:1px solid #E0E0E0;
}
.inbox_msg {
background: #fff;
position: relative;
    clear: both;
  overflow: hidden;
 box-shadow: 0 2px 6px rgba(0, 0, 0, 0.08);
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%; 
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #E0E0E0;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:14px; color:#3B3B3B; margin:0 0 8px 0; font-weight: 700}
.chat_ib h5 span{ font-size:11px; float:right; color:#727272; font-weight: normal}
.chat_ib p{ font-size:12px; color:#989898; margin:auto; white-space: nowrap; overflow: hidden; text-overflow: ellipsis}
.chat_img {
  float: left;
  width: 16%;
  border-radius: 50%;
}
.chat_img img{
  border-radius: 50%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 82%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #E0E0E0;
  margin: 0;
  padding: 18px 16px 10px;
  cursor: pointer;
}
.chat_list:hover {cursor: pointer; background: rgba(0,0,0,0.04)}
.chat_list.active_chat {background: rgba(73,161,239,0.08)}
.inbox_chat { height: 550px; overflow-y: auto;}

.active_chat{ }
.incoming_msg{margin-bottom: 8px}
.incoming_msg_img {
  display: inline-block;
  width: 13%; 
}
.outcoming_msg_img {
  display: inline-block;
  width: 6%; float: right;
  margin: 15px 0 0 10px;
}
.outcoming_msg_img img {
border-radius: 50%
}
.incoming_msg_img img{
  border-radius: 50%
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 80%;
 }
 .received_withd_msg p {
  background: #f1f5f1 none repeat scroll 0 0;
  border-radius: 3px;
  color: #fff;
  font-size: 12px !important;
  margin: 0;
  padding: 8px 10px 8px 12px;
  width: 100%;
  font-weight: normal;
}


.time_date {
  color: #ACACAC;
  font-size: 10px;
  margin: 8px 0 0;
      right: 0;
   
}
.received_withd_msg { width: 100%; position: relative}
.mesgs {
  float: left;
  padding: 0px 0px 0px 0px;
  width: 100%;
}

 .sent_msg p {
  background: #09BC8A none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 13px;
  margin: 0; color:#fff;
  padding: 8px 10px 8px 12px;
  width:100%; position: relative;
}
 .sent_msg p::before{
    content:"\f0d7";
    font-family: 'fontawesome';
    color:#09BC8A;
        font-size: 28px;
    transform: rotate(45deg);
    position: absolute;
    bottom: -16px;
    right: -8px;
}
.outgoing_msg{ overflow:hidden; margin:20px 0 20px;}
.sent_msg {
  float: right;
  width: 42%; position: relative;
  margin: 15px 0;
}
.sent_msg .time_date {
  color: #747474;
  font-size: 10px;
  margin: 8px 0 0;
      left: 0;
  
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #A7A7A7;
  font-size: 13px;
  min-height:36px;
      padding: 10px 65px 10px 15px;
  width: 100%;
}
.input_msg_write i{position: absolute; color: #09BC8A; left:0;    left: 28px;    top: 28px;}
.type_msg {border-top: 1px solid #e0e0e0; position: relative; padding: 15px}
.msg_send_btn {
  background: #fff none repeat scroll 0 0;
  border: medium none;
  color: #058ED9;
  cursor: pointer;
  font-size: 15px;
  height: 33px;
  position: absolute;
  right: 35px;
  top:20px;
  width:44px;
}
.input_msg_write{border:1px solid #ededed}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 404px;
  padding: 30px 15px 0 25px;
  overflow-y: auto;
}
.msg_history img{height: 36px; width: 36px}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
  <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  <!-- END MENU --> 
  <!-- Start Pricing table -->
  <section id="our-team" style="background: #f6fef7;">
    <div class="container">
      <div class="row">
          <div class="col-md-12">
          
            <h3 class="title" style="text-align: left">Student</h3>
           
          </div>
          </div>
          <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
        <br>
        <div class="row">
        <div class="col-md-12">
          <div class="our-team-content" style="margin-top: 0">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-8 col-sm-6 col-xs-12">
                  <div class="interview">
                      <div class="interview-screen">
                          <!-- <img src="<?php echo base_url();?>assets/images/Panasonic-screen-ready2.png"/> -->
                          <div id="controls" style="padding:0; margin: 0">
                          <div id="preview" style="padding:0; width:100%; margin: 0">
                          <!--<p class="instructions">Hello Beautiful</p> -->
                          <div id="local-media" style="padding:0; width:100%"></div>
                          <!--<button id="button-preview">Preview My Camera</button>-->
                          </div>
                          <div id="room-controls" style="width: auto; margin:0">
                          <!--<p class="instructions">Room Name:</p> -->
                            <input id="room-name" type="hidden" placeholder="Enter a room name" value="<?= $room;?>"  /> 
                        <!-- <input id="room-name" type="hidden" placeholder="Enter a room name"/>  -->

                          <button id="button-join"  class="btn btn-success" style="position: absolute;

right: 0px;

top: -70px;">Join Interview</button>
                          <button id="button-leave" class="btn btn-danger" style="position: absolute;

right: 0px;

top: -70px;">Leave Interview</button>
                          </div>
                          <div id="log"style="display:none"></div>
                      </div>
                      <a href="#" class="interviewee">
                          <!-- <img src="<?php echo base_url();?>assets/images/Screen-Shot-2018-03-29-at-12.10.48-PM.png"> -->
                          <div id="remote-media"></div>
                      </a>
                      <span id="console" >
                        <ul id="interview_timer">
                          <li data-time="0" data-field="Top">Time: <span>0</span>s</li>
                        </ul>
                      </span>
                  </div>
                 
                 
              </div>
              <!-- ends single team member -->
               <div class="quicktext">
                   <a href="#" class="btn comment-btn">Rushed response</a>
                  <a href="#" class="btn comment-btn">Vague response</a>
                  <a href="#" class="btn comment-btn">Answer the question</a>
                  <a href="#" class="btn comment-btn">Explain your reasoning</a>
                  <a href="#" class="btn comment-btn">Can you give examples?</a>
                  <a href="#" class="btn comment-btn">Looking nervous</a>
                  <a href="#" class="btn comment-btn">Eye contact</a>
                  <a href="#" class="btn comment-btn">Avoid 'ummm'</a>
              </div> 
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                  
                       <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                              Having trouble connecting? <small>Send a message here</small> 
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                          <div class="panel-body" style="padding: 0">
                              <div class="rushed-box">
                                      <div class="mesgs">
         
          <div class="msg_history">
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Are you available? Should we start?</p>
                  <span class="time_date"> 11:01 AM</span></div>
              </div>
            </div>
           
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Are you available? Should we start?</p>
                  <span class="time_date"> 11:01 AM</span></div>
              </div>
            </div>
           
            <div class="incoming_msg">
              <div class="incoming_msg_img"> <img src="<?php echo base_url();?>assets/images/default.png" alt="."> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
                  <p>Are you available? Should we start? There ?</p>
                  <span class="time_date"> 11:01 AM <i class="fa fa-check-double color-primary"></i></span></div>
              </div>
            </div>
          </div>
          <div class="type_msg">
            <div class="input_msg_write">
             
              <input type="text" class="write_msg" placeholder="Type a message" />
              <button class="msg_send_btn" type="button">Send</button>
            </div>
          </div>
        </div>
                              </div>
                          </div>
                      </div>
                    </div>
                    
                  </div>
                  
              </div>
            
            </div>
              <div class="clearfix"></div>
              <br>
              <div class="row">
                  <div class="col-md-8">
                      <div class="about-content"  style='background: #fff; border:1px solid #eee; padding: 15px 25px; margin-top: 0px'>
           
                <div class="our-skill">
                  <h3>Shared Whiteboard </h3>                  
                  <div class="our-skill-content">
                      <div class="contact-area-right" style="min-height: 240px">
                  
                 </div>
                  </div>                  
                </div>
          </div>
                  </div>
                  <div class="col-md-4">
                      
                       <div class="panel-group why-choose-group" id="accordion" style='margin-top: 0px'>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Previous Feedback <span class="fa fa-minus-square"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body" style="padding: 0">
                            
                            <div class="pre-feedback" style="max-height: 308px;">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <table>
                                                <tr>
                                                    <td><img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/></td>
                                                    <td>
                                                        <h4>Shelly Johnson</h4>
                                                        <small>Oct 16, 2018</small>
                                                    </td>
                                                    <td><img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png"></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <table>
                                                <tr>
                                                    <td><img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/></td>
                                                    <td>
                                                        <h4>Shelly Johnson</h4>
                                                        <small>Oct 16, 2018</small>
                                                    </td>
                                                    <td><img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png"></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <table>
                                                <tr>
                                                    <td><img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/></td>
                                                    <td>
                                                        <h4>Shelly Johnson</h4>
                                                        <small>Oct 16, 2018</small>
                                                    </td>
                                                    <td><img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png"></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <table>
                                                <tr>
                                                    <td><img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/></td>
                                                    <td>
                                                        <h4>Shelly Johnson</h4>
                                                        <small>Oct 16, 2018</small>
                                                    </td>
                                                    <td><img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png"></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <table>
                                                <tr>
                                                    <td><img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/></td>
                                                    <td>
                                                        <h4>Shelly Johnson</h4>
                                                        <small>Oct 16, 2018</small>
                                                    </td>
                                                    <td><img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png"></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <table>
                                                <tr>
                                                    <td><img class="int-img" src="<?php echo base_url();?>assets/images/main-testimonials-img-2.png"/></td>
                                                    <td>
                                                        <h4>Shelly Johnson</h4>
                                                        <small>Oct 16, 2018</small>
                                                    </td>
                                                    <td><img class="pdficon" src="<?php echo base_url();?>assets/images/pdf-icon.png"></td>
                                                </tr>
                                            </table>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  
                  </div>
              </div>  
              
          </div>
        </div>
      </div>
      </div>
  
  </section>


  
 <div aria-hidden="false" role="dialog" tabindex="-1" id="please-feedback" class="modal leread-modal fade in">
    <div class="modal-dialog">
      <!-- Start login section -->
      <div id="login-content" class="modal-content">
        <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
          <h4 class="modal-title">Feedback</h4>
        </div>
          <div class="modal-body clearfix">
              <p style='color:red'>Please provide Feedback .</p>
                      <div class="about-content"  id="feedback-form" style='visibility:visible !important; background: #fff; border:1px solid #eee; padding: 15px 25px; margin-top: 20px'>
           
                <div class="our-skill">
                  <h3>Feedback Form</h3>                  
                  <div class="our-skill-content">
                      <div class="contact-area-right">
                   <div><span id="review_msg" style="color:green;font-size: 18px;font-weight: normal;margin-left: 40px;"></span></div>
                      <form method="post" action="<?php echo site_url('oxbridge/ratings'); ?>"  id="session_review"  class="comments-form contact-form">
                      <!--  <form   id="session_review"  class="comments-form contact-form"> -->
                    <div class='rating-stars '>
                      <ul id='stars'>
                        <li class='star' title='Poor' data-value='1'>

                          <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Fair' data-value='2'>
                          <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Good' data-value='3'>
                          <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Excellent' data-value='4'>
                          <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='WOW!!!' data-value='5'>
                          <i class='fa fa-star fa-fw'></i>
                        </li>
                      </ul>
                    </div>
                     <div class='text-message'></div>
                     <div class="form-group">                        
                      <input type="hidden"  id="ratings" name="rating" required readonly>
                      <input type="hidden"  name="user_type" value="interviewer">
                      <input type="hidden"  name="interviewer_id" value="<?= $appointment[0]->interviewer_id;?>">
                      <input type="hidden"  name="student_id" value="<?= $appointment[0]->student_id;?>">
                       <input type="hidden"  name="room_name" value="<?= $appointment[0]->room_name;?>">

                    </div>
                    <div class="form-group">                        
                      <textarea placeholder="Comment" name="comment" rows="2" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">                        
                    
                    </div>
                         <button  type="submit" class="comment-btn">Submit</button> 
                        
                  </form>  
                 </div>
                  </div>                  
                </div>
          </div>
                  
          </div>
       
      </div>
      </div>
      </div>
  

  <!-- Start footer -->
  
  <?php $this->load->view('common/footer');?>

  <!-- End footer -->

    <!-- jQuery library -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>     -->
  
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/moment.min.js"></script>  

    <!-- Bootstrap -->
    <!-- <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/slick.js"></script>    
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>
         
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.pack.js"></script> -->

   <!-- counter -->
    <!-- <script src="<?php echo base_url();?>assets/js/waypoints.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js"></script> 
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-progressbar.js"></script>  --> 

    
    <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>--> 
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>-->
<!--    <script src="<?php echo base_url();?>assets/js/Mini-Event-Calendar-Plugin-jQuery/src/mini-event-calendar.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".calendar").MEC();
        });
    </script>-->
    <!-- Custom js -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/Monthly-Event-Calendar-pbcalendar/pb.calendar.js"></script>
    <script src="<?php echo base_url();?>assets/js/screentime.js"></script>
    
    <script type="text/javascript">
$(document).ready(function(){
   
      //setTimeout(function(){ alert('hiii'); }, 3000);
    //$("#button-join").click();
});
</script>


    
<script>
    $('#button-leave').on('click',function(){
        $('#feedback-form').css('visibility','visible');
        $('#please-feedback').modal();
    });
    $('#button-join').on('click',function(){
        
    
$.screentime({
  fields: [
    { selector: '#interview_timer',
      name: 'Top'
    },
    { selector: '#middle',
      name: 'Middle'
    },
    { selector: '#bottom',
      name: 'Bottom'
    }
  ],
  reportInterval: 1,
  callback: function(data) {
    $.each(data, function(key, val) {
      var $elem = $('#console li[data-field="' + key + '"]');
      var current = parseInt($elem.data('time'), 10);

      $elem.data('time', current + val);
      $elem.find('span').html(current += val);
    });
  }
});

});
</script>
    <script>
        $(document).ready(function(){
            var current_yyyymm_ = moment().format("YYYYMM");
           $("#pb-calendar").pb_calendar({

  // is date selectable?
  'day_selectable' : false,

  // callbacks
  'callback_selected_day' : $.noop,
  'callback_changed_month' : $.noop,

  // min/max dates
  'min_yyyymm' : null,
  'max_yyyymm' : null,

  // navigation arrows
  'next_month_button' : '<img src="<?php echo base_url();?>assets/images/arrow-right.png" class="icon">',
  'prev_month_button' : '<img src="<?php echo base_url();?>assets/images/arrow-left.png" class="icon">',

  // custom label format
  'month_label_format' : "MMM",
  'year_label_format' : "YYYY",
  
   schedule_list : function(callback_, yyyymm_){
    var temp_schedule_list_ = {};

    temp_schedule_list_[current_yyyymm_+"03"] = [
      {'ID' : 1, style : "red"}
    ];

    temp_schedule_list_[current_yyyymm_+"10"] = [
      {'ID' : 2, style : "red"},
      {'ID' : 3, style : "blue"},
    ];

    temp_schedule_list_[current_yyyymm_+"20"] = [
      {'ID' : 4, style : "red"},
      {'ID' : 5, style : "blue"},
      {'ID' : 6, style : "green"},
    ];
    callback_(temp_schedule_list_);
  },
  schedule_dot_item_render : function(dot_item_el_, schedule_data_){
    dot_item_el_.addClass(schedule_data_['style'], true);
    return dot_item_el_;
  }
  
});
        });
    </script>

<!--  script for star rating  -->
<script type="text/javascript">

      $(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg,ratingValue);
    
  });
  
  
});


function responseMessage(msg,ratingValue) {
 // $('.success-box').fadeIn(200);  
 $('#ratings').val(ratingValue);
  $('div.text-message').html("<span>" + msg + "</span>");
}
      
</script>


    
    
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<!-- <script src="//media.twiliocdn.com/sdk/js/video/releases/2.0.0-beta1/twilio-video.min.js"></script> -->
<script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
<script src="<?=base_url()?>assets/js/index.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
  </body>
</html>