<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Request Status | Whetstone Oxbridge</title>
    <?php $this->load->view('common/header_assets');?>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header -->
   <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
  
   <!-- Start about  -->
  <section id="about" style='padding-top: 0; padding-bottom: 0'>
       <div class="container-fluid" style='background-image: url(<?php echo base_url();?>assets/images/pattern.jpg); padding-top:150px; padding-bottom:150px'>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4">
                 <?php
                      if($this->session->flashdata('success')) {
                         $message = $this->session->flashdata('success');
                         echo'
                          <div class=" alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
                      <?php
                      if($this->session->flashdata('error')) {
                         $message = $this->session->flashdata('error');
                         echo'
                          <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              <i class="fa fa-check-circle"></i>'.$message['message']. 
                          '</div>';
                      }?> 
              </div>
          <div class="col-md-12">

            <div class="subscribe-area">
              <h2 class="wow fadeInUp" style='color:#2aaebf'>Check your interviewer request status</h2>
              <div class="col-md-6">
                
              </div>
              <form id="check_request" method="get" action="<?php echo base_url();?>interviewer/check_request_status" class="subscrib-form wow fadeInUp">
                <input type="text" name="request_id" placeholder="Enter your request ID" required>
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>

          </div>

        </div>
      </div>
   
  </section>
  <!-- end about -->
  <!-- Start Pricing table -->
 
  <!-- Start subscribe us -->
  <!-- <section id="subscribe">
    <div class="subscribe-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2 class="wow fadeInUp">Subscribe Newsletter</h2>
              <form action="" class="subscrib-form wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <input type="text" placeholder="Enter Your E-mail..">
                <button class="subscribe-btn" type="submit">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
   <!-- start script for validation  -->
   <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/js/validation.js"></script>
   <!-- End  script for validation  -->
   <script type="text/javascript">
  $( document ).ready(function() {
    setTimeout(function() {
      $('.alert').fadeOut('slow');}, 5000
      );
  });
</script>
    
  </body>
</html>