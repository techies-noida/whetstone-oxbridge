<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interviewers</title>
    <?php $this->load->view('common/header_assets');?>
    <style>
        .single-team-member > p{min-height: 160px; max-height: 160px; overflow: hidden}
    </style>
  </head>
  <body>
<!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Start header -->
   <?php $this->load->view('common/header'); $this->load->view('common/login_signup');?>
   <!-- Start about  -->
  <section id="about" style='padding-top: 0; padding-bottom: 0'>
       <div class="container-fluid" style='background:#1d1d1d; padding-top: 60px; padding-bottom: 60px'>
        <div class="row">
          <div class="col-md-12">
            <div class="subscribe-area">
              <h2 class="wow fadeInUp" style='color:#2aaebf'>Start preparing for you interviews today</h2>
            
             
            </div>
          </div>
          <div class="col-md-6" style="float:none; margin:auto; display: none">
          <div class="row">
              	<div class="col-md-4" style="padding-right:0">
              	<input type="text" placeholder="University.." style="height:42px; width:100%; border-radius:4px 0 0 4px; border:0; border-right:1px solid #eee; padding-left:15px">
              	</div>
              	<div class="col-md-4" style="padding-right:0; padding-left:0">
              		<input type="text" placeholder="College.. " style="height:42px; width:100%; border-radius:0px 0 0 0px; border:0; border-right:1px solid #eee; padding-left:15px">
              	</div>
              	<div class="col-md-4" style="padding-left:0">
 
                <input type="text" placeholder="Course.. " style="height:42px; width:100%; border-radius:0px 4px 4px 0px; border:0; padding-left:15px">
               
           
              	</div>
              </div>
              <div class="row text-center">
               <button class="btn btn-info" type="submit" style="margin-top:20px; padding:10px 25px"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
              </div>
          </div>
        </div>
      </div>
      
  </section>
  <!-- end about -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Looking for a practice interview?</h2>
            <span class="line"></span>
           <p>Book a slot with one of our Top Listed Interviewers below.</p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
             <!-- Start single team member -->
             <?php foreach ($interviewer as $row) {?>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="<?php echo base_url().'interviewer/profile/'.$row->user_id;?>" class="single-team-member">
                 <div class="team-member-img" style="border-radius: 4px; background: #f2f2f2; margin-top: 20px">
                     <?php $image=$row->profile_image;
                     if($image=='')
                    echo '<img src="'.base_url().'assets/images/default.png" alt="team member img" style="filter:grayscale(100%); height:100%; width:100%">';
                    else
                       echo '<img src="'.base_url().$image.'" alt="team member img" style="filter:grayscale(100%); height:100%; width:100%">';
                     ?>
                    
                 </div>
                 <div class="team-member-name">
                   <p><?php echo $row->first_name.''.$row->last_name;?></p>
                   <span><?php echo $row->university_name;?></span>
                 </div>
                 <?php echo '<p>'.$row->about_us.'</p>';?>
                 <!-- <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p> -->
                 <div class="team-member-link">
                      Start
                 </div>
                </a>
              </div> <?php }?>
              <!-- Start single team member -->
             
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<section id="testimonial" style="background: #fff">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <div class="title-area">
             
                <span class="line"></span>    
                <p>Alternatively, fill in this form with the University, Course and College you are applying to and we'll find a perfect match for you."</p>
              </div>
            </div>
              <div class="clearfix"></div>
                  <div class="col-md-12" style="float: none; margin: auto">
                      
                      
                      <div style="margin-top: 40px">

 


  <div class="tab-content">
      
      
      <div role="tabpanel" class="tab-pane active" id="student">
           <div class="contact-area-right" style="margin-top: 20px">
                   <form action="" class="comments-form contact-form">
                                       
                       <div class="col-md-3">
                           
                           <div class="form-group">                        
                      <label style="color:#444; font-weight: normal">University</label>
                      <input type="text" class="form-control" placeholder="" value="">
                    </div>
                    </div>
                            <div class="col-md-3">
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">College</label>
                      <input type="email" class="form-control" placeholder="" value="">
                    </div>
                    </div>
                     <div class="col-md-3">
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Course</label>
                      <input type="text" class="form-control" placeholder="">
                    </div>
                    </div>
                          <div class="col-md-3">
                    <div class="form-group">  
                         <label style="color:#444; font-weight: normal">Your Email Address</label>
                      <input type="text" class="form-control" placeholder="">
                    </div>
                    </div>
                   
                 
                       <div class="clearfix"></div><br>
                              <div class="col-md-12 text-center">
                        <button class="comment-btn" onclick="alert('Thank you for your request. Hold tight and your Interviewer will contact you in the next 24 hours to arrange a session.')">Submit</button>
                              </div>
                 
                       
                      
                   
                  </form>
                 </div>
      </div> <!--student form ends-->
    

</div>
                      
                      
                
                 </div>
         
              </div>
          </div>
        </div>
             
      </div>
    </div>
  </section>

  <!-- End Service -->
  <!-- Start subscribe us -->
  <?php  $this->load->view('common/newsletter');?>
  <!-- End subscribe us -->

  <!-- Start footer -->
  <?php $this->load->view('common/footer');?>
    
  </body>
</html>